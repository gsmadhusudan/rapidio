-------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-------------------------------------------------------------------------------


The repository contains the Bluespec and Verilog RTL of Serial RapidIO Interconnect version 3.0 developed by
Computer Architecture and Systems Lab, Department of Computer Science and Engineering, 
Indian Institute of Techonology Madras.

The code was compiled using Bluespec-2013.05.beta2 version of the compiler. 

The documents will be checked in over the next few months. Wrt PHY layer, we will most likely
just provide the digital portions. 

This IP has not been fully tested or integrated in an SoC but unit tests have been done
to test the function interfaces and behaviour. We will shortly check in testbenches and
describe a minimal testing process using tools like Modelsim.

The code is licensed under the 3 part BSD license.

RapidIO open source IP is preliminary version of the Serial RapidIO Open IP Core Project. 

The following functions are implemented and working,

1. NREAD (Ftype 2)
2. NWRITE, NWRITE_R (Ftype 5)
3. SWRITE (Ftype 6)
4. Maintenance Request and Response (Ftype 8)
5. DoorBell (Ftype 10)
6. Message Passing Implementation (Ftype 11)
6. Response (Ftype 13)
7. Atomic Read and Write operations (Ftype 2 and Ftype 5)
8. Management Registers (Partial Implementation)
9. CRC 16 Generation (Verification Pending)
10. Physical Layer Lane Stripping (Basic Model that supports 64/67b encoding, will be enhanced in future)
11. Physical Layer Control Symbol Packet Generation (Basic module supports only stype1, under development)
12. CRC 24 Generation for Physical Layer Control Symbol (Verification Pending)

The following functions are in the development phase 

1. Data Streaming (Ftype 9)
2. Flow Control (Ftype 7)
3. Globally Shared Memory (Ftype 2 and Ftype 5)
4. Management Registers
5. CRC 16 Generation (Verification), CRC 16 Checker
6. Physical Control Symbol Generation
7. CRC 24 Generation for Physical Layer Control Symbol (Verification Pending), CRC 24 Checker
8. 64/67b PCS Encoding



The Documents will be available in the due course of time. 


