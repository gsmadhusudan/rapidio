/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Top Wrapper Testbench Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO Ftype functions and Packet Generation/Decode. 
-- 1. Generation and Decode of Packet for Ftype 2 format.
-- 2. Generation and Decode of Packet for Ftype 5 format.
-- 3. Generation and Decode of Packet for Ftype 6 format. 
-- 4. Generation and Decode of Packet for Ftype 8 format. 
-- 5. Ftype 2 and 5 Atomic Read/Write packet generation and Decode. 
-- 6. Generation and Decode of Ftype 13 Response packet. 
--
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_Tb_Top_Wrapper;

import RapidIO_DTypes ::* ;
import DefaultValue ::* ;
import RapidIO_Top_Wrapper ::* ; 

(* synthesize *)
module mkRapidIO_Tb_Top_Wrapper (Empty);

// Top Module Instantiation 
Ifc_RapidIO_Top_Wrapper top_RapidIO_Top_Wrap <- mkRapidIO_Top_Wrapper();

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
//	if (reg_ref_clk == 64'hffffffffffffffff)
	if (reg_ref_clk == 469)
		$finish (0);
endrule

/*
-- Following function action is used to issue input signals to the Top module. 
-- This function is called inside the rules and input signal is sent through this function. 
-- Arguments to the functions are:
-- 1. Initiator Request signals (Control, Data, Message)
-- 2. Incoming Receiver Packets
-- 3. Initiator Response Ready signal 
-- 4. Outgoing Transmit Packet Ready signal 
*/
function Action fn_InputToTopModule (InitReqIfcCntrl tb_ireqcntrl, InitReqIfcData tb_ireqdata, InitReqIfcMsg tb_ireqmsg, RxIncomingPacket tb_rxpkt, Bool tb_iresprdyin, Bool tb_txrdyin);
   action 
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_sof_n (tb_ireqcntrl.ireq_sof);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_eof_n (tb_ireqcntrl.ireq_eof);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_vld_n (tb_ireqcntrl.ireq_vld);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_dsc_n (tb_ireqcntrl.ireq_dsc);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_tt (tb_ireqdata.ireq_tt);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_data (tb_ireqdata.ireq_data);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_crf (tb_ireqdata.ireq_crf);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_prio (tb_ireqdata.ireq_prio);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_ftype (tb_ireqdata.ireq_ftype);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_dest_id (tb_ireqdata.ireq_destid);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_addr (tb_ireqdata.ireq_addr);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_hopcount (tb_ireqdata.ireq_hopcount);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_tid (tb_ireqdata.ireq_tid);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_ttype (tb_ireqdata.ireq_ttype);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_byte_count (tb_ireqdata.ireq_byte_count);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_byte_en_n (tb_ireqdata.ireq_byte_en);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_local (tb_ireqdata.ireq_local);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_db_info (tb_ireqmsg.ireq_db_info);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_msg_len (tb_ireqmsg.ireq_msg_len);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_msg_seg (tb_ireqmsg.ireq_msg_seg);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_mbox (tb_ireqmsg.ireq_mbox);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_letter (tb_ireqmsg.ireq_letter);

 	top_RapidIO_Top_Wrap._TWIRespInterface._iresp_rdy_n (tb_iresprdyin);

	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_sof_n (tb_rxpkt.rx_sof);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_eof_n (tb_rxpkt.rx_eof);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_vld_n (tb_rxpkt.rx_vld);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_data (tb_rxpkt.rx_data);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_rem (tb_rxpkt.rx_rem);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_crf (tb_rxpkt.rx_crf);

	top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rdy_n (tb_txrdyin);

   endaction 
endfunction 

/*
-- Following rule is used to issue the initial condition for the top module input signals.
-- 1. This rule fires when the refernce clock is between 0 and 5. 
-- 2. Initial condition is given to Initiator Request signals, Incoming Receiver packet and Ready signals
-- 3. Using the function, these signals are sent to the Top module. 
*/
rule rl_Initial_Condition (reg_ref_clk >= 0 && reg_ref_clk <= 5); 
    InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    Bool lv_TxRdy = True; 
    Bool lv_iresp_rdy = True; 
    RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
endrule 

/*
-- Following rule is used to verify the Ftype 2 Packet generation Supports Dev8 and Dev16  
-- 1. The input signals are given through Initiator Request signals.
-- 2. The input Receiver packet signals carries default value. 
-- 3. The input values assigned are:
	Data : None (Read request doesn't carry Data).
	Prio : 'b01 
	Ftype : 'b0010 (Request Class)
	Ttype : 'b0100 (NREAD)
	Destination ID : 'hce (Change any value)
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'hfe (can have any value)
	Byte Count : 'd4 (4 Bytes)
	Byte Enable : 'b00001111 (Validates the Response Data)
-- The packet is generated during the 9th cycle which mean that RapidIO takes 3 clock cycle to generate the packet.
-- Once packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
*/

rule rl_Ftype2_Verification_Dev8 (reg_ref_clk >= 6 && reg_ref_clk <= 19); // Dev8 Support 
    if (reg_ref_clk == 6) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype2 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0010, ireq_destid:32'hce000000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'hfe, ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue;

    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 7 || reg_ref_clk == 8 || reg_ref_clk == 9) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 10 || reg_ref_clk == 11) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 12 && reg_ref_clk <= 19) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule

/*
-- Following rule is used to verify the logic functions of Ftype 2 Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/   
rule rl_Ftype2_Verification_Dev16 (reg_ref_clk >= 130 && reg_ref_clk <= 150); // Dev16 Support 
    if (reg_ref_clk == 130) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype2 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:0, ireq_crf:False, ireq_prio:2'b10, ireq_ftype:4'b0010, ireq_destid:32'habff0000, ireq_addr:'h000000008,     
                                                ireq_hopcount:0, ireq_tid:8'hf0, ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 131 || reg_ref_clk == 132 || reg_ref_clk == 133) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 134 || reg_ref_clk == 135) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 136 && reg_ref_clk <= 150) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the Ftype 5 (Write Class) Packet generation supports both Dev8 and Dev16 
-- 1. The input signals are given through Initiator Request signals.
-- 2. The input Receiver packet signals carries default values.  
-- 3. The input values assigned are:
	Data : 'h9999999999999999 (Data to be updated in the memory)
	Prio : 'b01 
	Ftype : 'b0101 (Write Class)
	Ttype : 'b0100 (NWRITE, No Response)
	Destination ID : 'hda (Change any value)
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'hbf (can have any value)
	Byte Count : 'd4 (4 Bytes)
	Byte Enable : 'b00001111 (Validates the Response Data)
-- The packet is generated during the 23th cycle which mean that RapidIO takes 3 clock cycle to generate the packet.
-- Once packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
*/
rule rl_Ftype5_Verification_Word_Dev8 (reg_ref_clk >= 20 && reg_ref_clk <= 30); // Dev8 
    if (reg_ref_clk == 20) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype5 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, ireq_destid:32'hda000000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'hbf, ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end 
    if (reg_ref_clk == 21 || reg_ref_clk == 22 || reg_ref_clk == 23) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 24 || reg_ref_clk == 25 || reg_ref_clk == 26) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end 
    if (reg_ref_clk == 27 || reg_ref_clk == 28 || reg_ref_clk == 29 || reg_ref_clk == 30) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 5 Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Ftype5_Verification_Word_Dev16 (reg_ref_clk >= 151 && reg_ref_clk <= 161); // Dev16 
    if (reg_ref_clk == 151) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype5 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, ireq_destid:32'hee030000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'hbf, ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end 
    if (reg_ref_clk == 152 || reg_ref_clk == 153 || reg_ref_clk == 154) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 155 || reg_ref_clk == 156 || reg_ref_clk == 157) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end 
    if (reg_ref_clk == 158 || reg_ref_clk == 159 || reg_ref_clk == 160 || reg_ref_clk == 161) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the Ftype 5 (Write Class) Packet generation supports both Dev8 and Dev16 
-- 1. The input signals are given through Initiator Request signals.
-- 2. The input Receiver packet signals carries default values.  
-- 3. The input values assigned are:
	Data : 'h1234567890123456 (Data to be updated in the memory)
	Prio : 'b01 
	Ftype : 'b0101 (Write Class)
	Ttype : 'b0101 (NWRITE_R, with Done Response)
	Destination ID : 'hab (Change any value)
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'h98 (can have any value)
	Byte Count : 'd8 (4 Bytes)
	Byte Enable : 'b11111111 (Validates the Response Data)
-- The packet is generated during the 34th cycle which mean that RapidIO takes 3 clock cycle to generate the packet.
-- Once packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
*/
rule rl_Ftype5_Verification_DoubleWord_Dev8 (reg_ref_clk >= 31 && reg_ref_clk <= 41); // Dev8 
    if (reg_ref_clk == 31) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype5 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:64'h1234567890123456, ireq_crf:False, ireq_prio:2'b10, ireq_ftype:4'b0101, ireq_destid:32'hab000000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'h98, ireq_ttype:4'b0101, ireq_byte_count:'d8, ireq_byte_en:8'hff, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end 
    if (reg_ref_clk == 32 || reg_ref_clk == 33 || reg_ref_clk == 34) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 35 || reg_ref_clk == 36 || reg_ref_clk == 37) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end 
    if (reg_ref_clk == 38 || reg_ref_clk == 39 || reg_ref_clk == 40 || reg_ref_clk == 41) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 5 Packet (with Response packet) with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Ftype5_Verification_DoubleWord_Dev16 (reg_ref_clk >= 162 && reg_ref_clk <= 171); // Dev16 
    if (reg_ref_clk == 162) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype5 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:64'h1234567890123456, ireq_crf:False, ireq_prio:2'b10, ireq_ftype:4'b0101, ireq_destid:32'h54970000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'h98, ireq_ttype:4'b0101, ireq_byte_count:'d8, ireq_byte_en:8'hff, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end 
    if (reg_ref_clk == 163 || reg_ref_clk == 164 || reg_ref_clk == 165) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 166 || reg_ref_clk == 167 || reg_ref_clk == 168) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end 
    if (reg_ref_clk == 169 || reg_ref_clk == 170 || reg_ref_clk == 171) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 



/*
-- Following rule is used to verify the Ftype 6 (Stream Write) Packet generation supports both Dev8 and Dev16 
-- 1. The input signals are given through Initiator Request signals. 
-- 2. The input Receiver packet signals carries default values. 
-- 3. The input values assigned are:
	Data : 'h1111111111111111,'h2222222222222222, 'h3333333333333333, 'h4444444444444444,'h5555555555555555 
	Prio : 'b01 
	Ftype : 'b0110 (Stream Write)
	Ttype : Not Required for Ftype6 
	Destination ID : 'hcd (Change any value)
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'h21 (can have any value)
	Byte Count : Not Required for Ftype6 
	Byte Enable : Not Required for Ftype6
-- The packet is generated during the 45th cycle which mean that RapidIO takes 3 clock cycle to generate the packet.
-- Once packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
*/
rule rl_Ftype6_Verification_Dev8 (reg_ref_clk >= 42 && reg_ref_clk <= 59); // Dev8 
    if (reg_ref_clk == 42) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0110, ireq_destid:32'hcd000000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'h21, ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end 
    if (reg_ref_clk == 43) begin  // Stream Data is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h2222222222222222, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk == 44) begin // Stream Data is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h3333333333333333, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = False; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk == 45) begin // Stream Data is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h4444444444444444, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = False; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk == 46) begin // Stream Data and end of frame is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h5555555555555555, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = False; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk >= 47 && reg_ref_clk <= 52) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end
    if (reg_ref_clk >= 53 && reg_ref_clk <= 59) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 6 Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Ftype6_Verification_Dev16 (reg_ref_clk >= 172 && reg_ref_clk <= 189); // Dev16 
    if (reg_ref_clk == 172) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b10, ireq_ftype:4'b0110, ireq_destid:32'hcdab0000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'he0, ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end 
    if (reg_ref_clk == 173) begin  // Stream Data is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h2222222222222222, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = False; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk == 174) begin // Stream Data is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h3333333333333333, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = False; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk == 175) begin // Stream Data is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h4444444444444444, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = False; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk == 176) begin // Stream Data and end of frame is issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype6 =  InitReqIfcData {ireq_tt: 0, ireq_data:64'h5555555555555555, ireq_crf:False, ireq_prio:2'b0, ireq_ftype:4'h0, ireq_destid:32'h0, ireq_addr:'h0, ireq_hopcount:0, 
						ireq_tid:8'h0,	ireq_ttype:4'h0, ireq_byte_count:'d0, ireq_byte_en:8'h0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk >= 177 && reg_ref_clk <= 182) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end
    if (reg_ref_clk >= 183 && reg_ref_clk <= 189) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule

/*
-- Following rule is used to verify the Incoming Received Ftype 2 (Request Class) Packet Decoding and Generation 
-- 1. The input packet is given through Receiver Packet signals and the format is maintained. 
-- 2. The Initiator Request signals carries defaultValue 
-- 3. The input values assigned are:
	Receiver Packet : 64'h52efab4bff000000. 
-- Implicitly, Packet contains :
	Data : Not Required for Ftype 2
	Prio : 'b01 
	Ftype : 'b0010 (Read Request)
	Ttype : 0100 (NREAD) 
	Destination ID : 'hef (Change any value)
	Source ID : 'hab (Change any value) 
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'hff (can have any value)
	Read Size : 'hb (8 Bytes)
	Word Pointer : 1'b0 (8 bytes)
-- The packet is decoded and sent to TgtOperations module through target request signals. 
-- The TgtOperations module performs the Read request operation and generates target response signals. 
-- The Target Response signals are sent to RapidIO and generate response packet and sent to destination. 
-- This rule also verifies the packet generation for Target Response of RapidIO. 
-- Once the packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_IncomingRxPkt_Ftype2_Dev8 (reg_ref_clk >= 60 && reg_ref_clk <= 80); // Dev8 
    if (reg_ref_clk == 60) begin // Intiator signals (Control and Data) are issued during this clock period 
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h42efab4bff000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 61) begin
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h1000000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 62) begin
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if(reg_ref_clk >= 63 && reg_ref_clk <= 69) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if(reg_ref_clk >= 70 && reg_ref_clk <= 74) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if(reg_ref_clk >= 75 && reg_ref_clk <= 80) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end

endrule

/*
-- Following rule is used to verify the decoding logic functions of Ftype 2 Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_IncomingRxPkt_Ftype2_Dev16 (reg_ref_clk >= 190 && reg_ref_clk <= 210); // Dev16 
    if (reg_ref_clk == 190) begin // Intiator signals (Control and Data) are issued during this clock period 
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h52efab9bff4bff00, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 191) begin
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h0000100000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 192) begin
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if(reg_ref_clk >= 193 && reg_ref_clk <= 202) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if(reg_ref_clk == 203 || reg_ref_clk == 204) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if(reg_ref_clk >= 205 && reg_ref_clk <= 210) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end

endrule 

/*
-- Following rule is used to verify the Incoming Received Ftype 5 with No Response (Write Class) Packet Decoding and Generation 
-- 1. The input packet is given through Receiver Packet signals and the format is maintained. 
-- 2. The Initiator Request signals carries defaultValue 
-- 3. The input values assigned are:
	Receiver Packet : 64'h55fbbe4bfa000000
-- Implicitly, Packet contains :
	Data : 1234567890123456 (Data to be updated in the memory)
	Prio : 'b01 
	Ftype : 'b0101 (Write Request)
	Ttype : 0100 (NWRITE) 
	Destination ID : 'hfb (Change any value)
	Source ID : 'hbe (Change any value) 
	Address : 'h000000018 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'hfa (can have any value)
	Write Size : 'hb (8 Bytes) (Change Write size to change the format of the data)
	Word Pointer : 1'b0 (8 bytes)
-- The packet is decoded and sent to TgtOperations module through target request signals. 
-- The TgtOperations module performs the Write request operation and generates target response signals. 
-- The Target Response signals are sent to RapidIO and generate response packet and sent to destination. 
-- This rule also verifies the packet generation for Target Response of RapidIO. 
-- Once the packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
-- It does not generate any responses. The data is updated in the memory and verify it by monitoring the memory location. 
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_IncomingRxPkt_Ftype5_NoDataResp_Dev8 (reg_ref_clk >= 81 && reg_ref_clk <= 92); // Dev8 
    if(reg_ref_clk == 81) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h45fbbe4bfa000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 82) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h1812345678901234, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 83) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h5600000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 84) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >= 85 && reg_ref_clk <= 92) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 5 (No Response) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_IncomingRxPkt_Ftype5_NoDataResp_Dev16 (reg_ref_clk >= 211 && reg_ref_clk <= 222); // Dev16 
    if(reg_ref_clk == 211) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h55fbbe70fa4b9900, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 212) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h0000181234567890, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 213) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h1234560000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 214) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >= 215 && reg_ref_clk <= 222) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the Incoming Received Ftype 5 with Done Response (Write Class) Packet Decoding and Generation 
-- 1. The input packet is given through Receiver Packet signals and the format is maintained. 
-- 2. The Initiator Request signals carries defaultValue 
-- 3. The input values assigned are:
	Receiver Packet : 64'h55fbbe5bfc000000
-- Implicitly, Packet contains :
	Data : 1234567890123456 (Data to be updated in the memory)
	Prio : 'b01 
	Ftype : 'b0101 (Write Request)
	Ttype : 0101 (NWRITE_R) 
	Destination ID : 'hfb (Change any value)
	Source ID : 'hbe (Change any value) 
	Address : 'h000000018 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'hfc (can have any value)
	Write Size : 'hb (8 Bytes) (Change Write size to change the format of the data)
	Word Pointer : 1'b0 (8 bytes)
-- The packet is decoded and sent to TgtOperations module through target request signals. 
-- The TgtOperations module performs the Write request operation and generates target response signals. 
-- The Target Response signals are sent to RapidIO and generate response packet and sent to destination. 
-- This rule also verifies the packet generation for Target Response of RapidIO. 
-- Once the packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_IncomingRxPkt_Ftype5_DataResp_Dev8 (reg_ref_clk >= 93 && reg_ref_clk <= 110); // Dev8 
    if(reg_ref_clk == 93) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h45fbbe5bfa000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end  
    if(reg_ref_clk == 94) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h1812345678901234, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end  
    if(reg_ref_clk == 95) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h5600000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 96) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >= 97 && reg_ref_clk <= 103) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
     if(reg_ref_clk == 104) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = False; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >= 105 && reg_ref_clk <= 110) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
endrule

/*
-- Following rule is used to verify the logic functions of Ftype 5 (Response Packet) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_IncomingRxPkt_Ftype5_DataResp_Dev16 (reg_ref_clk >= 223 && reg_ref_clk <= 240); // Dev16 
    if(reg_ref_clk == 223) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h55fb109efa5bfb00, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end  
    if(reg_ref_clk == 224) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h0000181234567890, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end  
    if(reg_ref_clk == 225) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h1234560000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 226) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >=227 && reg_ref_clk <= 233) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
     if(reg_ref_clk == 234) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = False; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >= 235 && reg_ref_clk <= 240) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
endrule

/*
-- Following rule is used to verify the Incoming Received Ftype 6 (Stream Class) Packet Decoding and Generation 
-- 1. The input packet is given through Receiver Packet signals and the format is maintained. 
-- 2. The Initiator Request signals carries defaultValue 
-- 3. The input values assigned are:
	Receiver Packet (Header) : 64'h56eadb0000001811
	Receiver Packet (Data) : 64'h1111111111111122, 64'h2222222222222233, 64'h3333333333333344, 64'h4444444444444455, 64'h5555555555555566, 64'h6666666666666600
-- Implicitly, Packet contains :
	Data : 64'h1111111111111111, 64'h2222222222222222, 64'h3333333333333333, 64'h4444444444444444, 64'h5555555555555555, 64'h6666666666666666
	Prio : 'b01 
	Ftype : 'b0110 (Write Request)
	Ttype : Not Required for Ftype6 
	Destination ID : 'hea (Change any value)
	Source ID : 'hdb (Change any value) 
	Address : 'h000000018 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : Not Required for Ftype6
	Write Size : Not Required for Ftype6 
	Word Pointer : Not Required for Ftype6
-- The packet is decoded and target request signals are generated. 
-- Monitor the Target Request signals to check the decoded packets. 
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_IncomingRxPkt_Ftype6_Dev8 (reg_ref_clk >= 111 && reg_ref_clk <= 129); // Dev8 
    if(reg_ref_clk == 111) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h46eadb0000001811, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 112) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h1111111111111122, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 113) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h2222222222222233, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 114) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h3333333333333344, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 115) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h4444444444444455, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 116) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h5555555555555566, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 117) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h6666666666666600, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 118) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk >= 119 && reg_ref_clk <= 129) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 6 Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_IncomingRxPkt_Ftype6_Dev16 (reg_ref_clk >= 241 && reg_ref_clk <= 259); // Dev16 
    if(reg_ref_clk == 241) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h56eafb12fe000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 242) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h1811111111111111, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 243) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h1122222222222222, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 244) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h2233333333333333, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 245) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h3344444444444444, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 246) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h4455555555555555, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 247) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h5566666666666666, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if(reg_ref_clk == 248) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype6 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype6 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype6 = defaultValue; 
    	Bool lv_TxRdy_Ftype6 = True; 
    	Bool lv_iresp_rdy_Ftype6 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype6 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h6600000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype6, lv_IReq_Data_Ftype6, lv_IReq_Msg_Ftype6, lv_RxPkt_Ftype6, lv_iresp_rdy_Ftype6, lv_TxRdy_Ftype6);
    end
    if (reg_ref_clk >= 249 && reg_ref_clk <= 259) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
endrule 

/*
-- Following rule is used to verify the Packet Generation of Ftype 8 Maintenance Request and Response Packets 
-- 1. For Maintenance Request packet, input is given through Initiator Request signals.  
-- 2. DefaultValue is sent to other signals which is not used. 
-- 3. Concatenation module generates logical layer packets for Maintenance request. And it sent through the IOGeneration module which adds Transport layer fields in to the packets. 
-- 4. Implicitly, Packet contains :
	Prio : 'b01 
        tt : 'b00 
	Ftype : 'b1000 (Maintenance Read Request)
	Ttype : 'b0000 
	Destination ID : 'hce (Change any value)
	Source ID : 'hdb (Change any value) 
	Configuration Offset : 'h000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 8'hff
	Write Size : 'h4
	Word Pointer : 'bffff0000
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_Ftype8_Read_Request_Verification_Dev8 (reg_ref_clk >= 260 && reg_ref_clk <= 273); // Dev8 Support 
    if (reg_ref_clk == 260) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype8 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, ireq_destid:32'hce000000, 
                                                ireq_addr:'h000000010, ireq_hopcount:0, ireq_tid:8'hff, ireq_ttype:4'b0000, ireq_byte_count:'d4, ireq_byte_en:8'hf0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue;

    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 261 || reg_ref_clk == 262 || reg_ref_clk == 263) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 264 || reg_ref_clk == 265) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 266 && reg_ref_clk <= 273) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule

/*
-- Following rule is used to verify the logic functions of Ftype 8 (Read Request) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Ftype8_Read_Request_Verification_Dev16 (reg_ref_clk >= 285 && reg_ref_clk <= 298); // Dev8 Support 
    if (reg_ref_clk == 285) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype8 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, ireq_destid:32'habcd0000, 
                                                ireq_addr:'h000000010, ireq_hopcount:0, ireq_tid:8'hfe, ireq_ttype:4'b0000, ireq_byte_count:'d4, ireq_byte_en:8'hf0, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue;

    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 286 || reg_ref_clk == 287 || reg_ref_clk == 288) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 289 || reg_ref_clk == 290) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 291 && reg_ref_clk <= 298) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule

/*
-- Following rule is used to verify the Ftype 8 (Maintenance Write Request) Packet generation supports both Dev8 and Dev16 
-- 1. The input signals are given through Initiator Request signals.
-- 2. The input Receiver packet signals carries default values.  
-- 3. The input values assigned are:
	Data : 'h9999999999999999 (Data to be updated in the memory)
	Prio : 'b01 
	Ftype : 'b1000 (Maintenance Class)
	Ttype : 'b0001 (Write Request)
	Destination ID : 'hbb (Change any value)
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'h11 (can have any value)
	Byte Count : 'd4 (4 Bytes)
	Byte Enable : 'b00001111 (Validates the Response Data)
-- Once packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
*/
rule rl_Ftype8_Write_Request_Verification_Dev8 (reg_ref_clk >= 274 && reg_ref_clk <= 284); // Dev8 
    if (reg_ref_clk == 274) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype8 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, ireq_destid:32'hbb000000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'h11, ireq_ttype:4'b0001, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 275 || reg_ref_clk == 276 || reg_ref_clk == 277) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 278 || reg_ref_clk == 279 || reg_ref_clk == 280 || reg_ref_clk == 281) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end 
    if (reg_ref_clk == 282 || reg_ref_clk == 283 || reg_ref_clk == 284) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 8 (Write Request) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Ftype8_Write_Request_Verification_Dev16 (reg_ref_clk >= 299 && reg_ref_clk <= 309); // Dev8 
    if (reg_ref_clk == 299) begin // Intiator signals (Control and Data) are issued during this clock period 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype8 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, ireq_destid:32'hbbcc0000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'h33, ireq_ttype:4'b0001, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 300 || reg_ref_clk == 301 || reg_ref_clk == 302) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 303 || reg_ref_clk == 304 || reg_ref_clk == 305 || reg_ref_clk == 306) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);	
    end 
    if (reg_ref_clk == 307 || reg_ref_clk == 308 || reg_ref_clk == 309) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

/*
-- Following rule is used to verify the Ftype 10 (DoorBell) Packet generation supports both Dev8 and Dev16 
-- 1. The input signals are given through Initiator Request control and db signals.
-- 2. The input Receiver packet signals carries default values.  
-- 3. The input values assigned are:
	Data : Not Used 
	Prio : 'b01 
	Ftype : 'b1010 (Doorbell Message Class)
	Ttype : Not Used
	Destination ID : 'h78 (Change any value)
	Address : Not Used
	Transaction ID (TID) : 'hf1 (can have any value)
	Byte Count : Not Used
	Byte Enable : Not Used
        DoorBell Info : 16'h9944
-- Once packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
*/
rule rl_Ftype10_DoorBell_Verification_Dev8 (reg_ref_clk >= 310 && reg_ref_clk <= 319);
    if (reg_ref_clk == 310) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype10 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1010, ireq_destid:32'h78000000, 
                                                ireq_addr:'h000000000, ireq_hopcount:0, ireq_tid:8'hf1, ireq_ttype:4'b0000, ireq_byte_count:'d0, ireq_byte_en:8'h00, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = InitReqIfcMsg {ireq_db_info:16'h9944, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};

    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk == 311 || reg_ref_clk == 312 || reg_ref_clk == 313) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 314 || reg_ref_clk == 315) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 316 && reg_ref_clk <= 319) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    
endrule 

/*
-- Following rule is used to verify the logic functions of Ftype 10 (Doorbell) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Ftype10_DoorBell_Verification_Dev16 (reg_ref_clk >= 320 && reg_ref_clk <= 329);
    if (reg_ref_clk == 320) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype10 =  InitReqIfcData {ireq_tt: 'b01, ireq_data:0, ireq_crf:False, ireq_prio:2'b10, ireq_ftype:4'b1010, ireq_destid:32'h78000000, 
                                                ireq_addr:'h000000000, ireq_hopcount:0, ireq_tid:8'hf1, ireq_ttype:4'b0000, ireq_byte_count:'d0, ireq_byte_en:8'h00, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = InitReqIfcMsg {ireq_db_info:16'h9944, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};

    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk == 321 || reg_ref_clk == 322) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 323 && reg_ref_clk <= 325) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 326 && reg_ref_clk <= 329) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    
endrule

/*
-- Following rule is used to verify the Incoming Received Ftype 10 (Doorbell Class) Packet Decoding  
-- 1. The input packet is given through Receiver Packet signals and the format is maintained. 
-- 2. The Initiator Request signals carries defaultValue 
-- 3. The input values assigned are:
	Receiver Packet : 64'h4a679000f1883300
-- Implicitly, Packet contains :
	Data : No Data
	Prio : 'b01 
	Ftype : 'b1010 (Doorbell)
	Ttype : Not Used 
	Destination ID : 'h90 (Change any value)
	Source ID : 'h67 (Change any value) 
	Address : Not Used
	Transaction ID (TID) : 'hf1 (can have any value)
-- The packet is decoded and sent to TgtOperations module through target request signals. 
-- The TgtOperations module performs the Write request operation and generates target response signals. 
-- The Target Response signals are sent to RapidIO and generate response packet and sent to destination. 
-- This rule also verifies the packet generation for Target Response of RapidIO. 
-- Once the packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/

rule rl_Incoming_DoorBell_Packet_Dev8 (reg_ref_clk >= 330 && reg_ref_clk <= 339); 
    if (reg_ref_clk == 330) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h4a679000f1883300, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk == 331) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk >= 332 && reg_ref_clk <= 336) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = False; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk >= 337 && reg_ref_clk <= 339) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
endrule 

/*
-- Following rule is used to verify the decoding logic functions of Ftype 10 (Doorbell) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Incoming_DoorBell_Packet_Dev16 (reg_ref_clk >= 340 && reg_ref_clk <= 349); 
    if (reg_ref_clk == 340) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h5a671070f1003355, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk == 341) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h1100000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk >= 342 && reg_ref_clk <= 346) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = False; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
    if (reg_ref_clk >= 347 && reg_ref_clk <= 349) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype10 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype10 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype10 = defaultValue; 
    	Bool lv_TxRdy_Ftype10 = True; 
    	Bool lv_iresp_rdy_Ftype10 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype10 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype10, lv_IReq_Data_Ftype10, lv_IReq_Msg_Ftype10, lv_RxPkt_Ftype10, lv_iresp_rdy_Ftype10, lv_TxRdy_Ftype10);
    end 
endrule 

/*
-- Following rule is used to verify the Packet Isolation for Ftype 8 Maintenance Read Request and generate Ftype 8 Read Response Packet 
-- 1. For Maintenance Read Request packet, input is given through Receive Packets Input.  
-- 2. DefaultValue is sent to other signals which is not used. 
-- 3. Parsing module decodes the Transport layer fields and it sent through the Packet Analyse module which decodes the received packet and 
--    generate the Logical layer fields from the packets. 
-- 4. The input given to the Receive packet is, 
        Incoming Packet : 64'h4899110bf9000000
-- 5. Implicitly, Packet contains :
        Ftype : 1000 (Maintenance Class)
        Ttype : 0000 (Read Request)
        Dest ID : 'h99
        Source ID : 'h11
        Read Size : 'hb
        Config Space Offset : 'h000004
        Transaction ID : 'h90        
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_Incoming_Maintenance_Read_Request_Read_Response_Dev8 (reg_ref_clk >= 350 && reg_ref_clk <= 369); 
    if (reg_ref_clk == 350) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h4899110bf9000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 351) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h2000000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 352 && reg_ref_clk <= 356) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 357 && reg_ref_clk <= 359) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 360 && reg_ref_clk <= 369) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
endrule 

/*
-- Following rule is used to verify the decoding logic functions of Ftype 8 (Maintenance Read Request) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/ 
rule rl_Incoming_Maintenance_Read_Request_Read_Response_Dev16 (reg_ref_clk >= 370 && reg_ref_clk <= 389); 
    if (reg_ref_clk == 370) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h58998844330b8800, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 371) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h0000240000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 372 && reg_ref_clk <= 376) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 377 && reg_ref_clk <= 379) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 380 && reg_ref_clk <= 389) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
endrule 

/*
-- Following rule is used to verify the Packet Isolation for Ftype 8 Maintenance Write Request and generate Ftype 8 Write Response Packet 
-- 1. For Maintenance Write Request packet, input is given through Receive Packets Input.  
-- 2. DefaultValue is sent to other signals which is not used. 
-- 3. Parsing module decodes the Transport layer fields and it sent through the Packet Analyse module which decodes the received packet and 
--    generate the Logical layer fields from the packets. 
-- 4. The input given to the Receive packet is, 
        Incoming Packet : 64'h4899110bf9000000
-- 5. Implicitly, Packet contains :
        Ftype : 1000 (Maintenance Class)
        Ttype : 0001 (Write Request)
        Dest ID : 'hcc
        Source ID : 'hff
        Read Size : 'hb
        Config Space Offset : 'h000004
        Transaction ID : 'h89        
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_Incoming_Maintenance_Write_Request_Write_Response_Dev8 (reg_ref_clk >= 390 && reg_ref_clk <= 409); 
    if (reg_ref_clk == 390) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h48ccff1be8900000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 391) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h2012345678901234, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 392) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h5612345678901234, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 393 && reg_ref_clk <= 396) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 397 && reg_ref_clk <= 399) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 400 && reg_ref_clk <= 409) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
endrule 

/*
-- Following rule is used to verify the decoding logic functions of Ftype 8 (Maintenance Write Request) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/
rule rl_Incoming_Maintenance_Write_Request_Write_Response_Dev16 (reg_ref_clk >= 410 && reg_ref_clk <= 429); 
    if (reg_ref_clk == 410) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h58cc8823981b5600, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 411) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h0000246543210987, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk == 412) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h6543210678901234, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 413 && reg_ref_clk <= 416) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 417 && reg_ref_clk <= 419) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = False; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
    if (reg_ref_clk >= 420 && reg_ref_clk <= 429) begin
        InitReqIfcCntrl lv_IReq_Cntrl_Ftype8 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype8 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype8 = defaultValue; 
    	Bool lv_TxRdy_Ftype8 = True; 
    	Bool lv_iresp_rdy_Ftype8 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype8 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype8, lv_IReq_Data_Ftype8, lv_IReq_Msg_Ftype8, lv_RxPkt_Ftype8, lv_iresp_rdy_Ftype8, lv_TxRdy_Ftype8);
    end 
endrule 

/*
-- Following rule is used to verify the Incoming Received Ftype 2 Atomic operations (Request Class) Packet Decoding and Generation 
-- 1. The input packet is given through Receiver Packet signals and the format is maintained. 
-- 2. The Initiator Request signals carries defaultValue 
-- 3. The input values assigned are:
	Receiver Packet :  64'h42efabcb7f000000
-- Implicitly, Packet contains :
	Data : Not Required for Ftype 2
	Prio : 'b01 
	Ftype : 'b0010 (Read Request)
	Ttype : 1100 (Post Incr) 
	Destination ID : 'hef (Change any value)
	Source ID : 'hab (Change any value) 
	Address : 'h000000008 (Make sure the Address should present in the other end point Memory)
	Transaction ID (TID) : 'h7f (can have any value)
	Read Size : 'hb (8 Bytes)
	Word Pointer : 1'b0 (8 bytes)
-- The packet is decoded and sent to TgtOperations module through target request signals. 
-- The TgtOperations module performs the request operation and generates target response signals. 
-- The Target Response signals are sent to RapidIO and generate response packet with data and sent to destination. 
-- This rule also verifies the packet generation for Target Response of RapidIO. 
-- Once the packet is available at the output, it is received by the Destination by asserting the Ready signal. 
-- The size of FIFO (which holds the packet) is 8. So make sure that FIFO is not overflowing. 
-- Support both Dev8 and Dev16. (Separate rules used in sequential order)
*/
rule rl_IncomingRxPkt_Ftype2_Atomic_Read_Dev8 (reg_ref_clk >= 430 && reg_ref_clk <= 449); // Dev8 
    if (reg_ref_clk == 430) begin // Intiator signals (Control and Data) are issued during this clock period 
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h42efabcb7f000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 431) begin
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h1000000000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 432) begin
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype2 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue; 
    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if(reg_ref_clk >= 433 && reg_ref_clk <= 439) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if(reg_ref_clk >= 440 && reg_ref_clk <= 444) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if(reg_ref_clk >= 445 && reg_ref_clk <= 449) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end

endrule

/*
-- Following rule is used to verify the decoding logic functions of Ftype 5 (Write Atomic Operations) Packet with Dev 16 support.
-- The packet information is similar to Dev 8 support.
-- Except, Destination and Source ID are 16 bit long. 
*/
rule rl_IncomingRxPkt_Ftype5_Write_Atomic_Dev16 (reg_ref_clk >= 450 && reg_ref_clk <= 469); // Dev16 
    if(reg_ref_clk == 450) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: False, rx_eof: True, rx_vld: False, rx_data: 64'h55fbbe70facb9900, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 451) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: False, rx_data: 64'h0000181234567890, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk == 452) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: False, rx_vld: False, rx_data: 64'h1234560000000000, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end   
    if(reg_ref_clk >= 453 && reg_ref_clk <= 458) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Ftype5 =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Ftype5 = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Ftype5 = defaultValue; 
    	Bool lv_TxRdy_Ftype5 = True; 
    	Bool lv_iresp_rdy_Ftype5 = True; 
    	RxIncomingPacket lv_RxPkt_Ftype5 = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 64'h0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Ftype5, lv_IReq_Data_Ftype5, lv_IReq_Msg_Ftype5, lv_RxPkt_Ftype5, lv_iresp_rdy_Ftype5, lv_TxRdy_Ftype5);
    end
    if (reg_ref_clk >= 459 && reg_ref_clk <= 465) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 466 && reg_ref_clk <= 469) begin
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
endrule 

endmodule : mkRapidIO_Tb_Top_Wrapper 

endpackage : RapidIO_Tb_Top_Wrapper
