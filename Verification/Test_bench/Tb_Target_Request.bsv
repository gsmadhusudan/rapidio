/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bench For RapidIO_TargetReqIFC

-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of both the RapidIO_IOPkt_concatenation module, RapidIO_IOPkt_Generation module, RapidIO_PktSeparation Module, Transport Parse Module, RapidIO_RxPktFTypeAnalyse and RapidIO_TargetReqIFC
-- 1. Initiator Request packet is provided as input to concatenation module.
-- 2. Output SOF, EOF, Vld, Data (Header or Data), TxRem and Crf are obtained fron generation module.
-- 3. The output from generation module is given to Transport Parse module.
-- 4. The output from Transport Parse module is given to RapidIO_RxPktFTypeAnalyse.
-- 4. The output from RapidIO_RxPktFTypeAnalyse module is given to RapidIO_TargetReqIFC Module.
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_Target_Request;

import DefaultValue ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse::*;
import RapidIO_RxPktFTypeAnalyse::*;
import RapidIO_TargetReqIFC::*;

module mkTb_Target_Request(Empty);
// Interface

Ifc_RapidIO_IOPktConcatenation ifc_con <- mkRapidIO_IOPktConcatenation;
Ifc_RapidIO_IOPkt_Generation ifc_gen <- mkRapidIO_IOPkt_Generation;
Ifc_RapidIO_InComingPkt_Separation ifc_sep <- mkRapidIO_InComingPkt_Separation;
Ifc_RapidIO_PktTransportParse ifc_parse <- mkRapidIO_PktTransportParse;
Ifc_RapidIO_RxPktFTypeAnalyse ifc_analyse <- mkRapidIO_RxPktFTypeAnalyse;
Ifc_TgtReq ifc_tar_req <- mkRapidIO_TargetReqIFC;

	
// For Concatenation Module
Wire#(InitReqIfcCntrl) wr_ireq_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_ireq_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_ireq_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet_con <- mkDWire (defaultValue);

Wire#(FType2_RequestClass) wr_ireq_FType2_RequestClass <- mkDWire (defaultValue);
Wire#(FType5_WriteClass) wr_ireq_FType5_WriteClass <- mkDWire (defaultValue);
Wire#(FType6_StreamWrClass) wr_ireq_FType6_StreamWrClass <- mkDWire (defaultValue);
Wire#(FType10_DOORBELLClass) wr_ireq_FType10_DOORBELLClass <- mkDWire (defaultValue);
Wire#(FType11_MESSAGEClass) wr_ireq_FType11_MESSAGEClass <- mkDWire (defaultValue);

Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 

// For Generation Module
Wire#(Bool) wr_gen_SOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_EOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_DSC_n <- mkDWire (False);
Wire#(Bool) wr_gen_VLD_n <- mkDWire (False);
Wire#(DataPkt) wr_gen_Data <- mkDWire (defaultValue); 
Wire#(Bit#(4)) wr_gen_TxRem <- mkDWire (0);
Wire#(Bool) wr_gen_Crf <- mkDWire (False); 
Wire#(Bool) wr_ready_from_dest <- mkDWire (False); 

// For Parse Module
Wire#(Maybe#(FType2_RequestClass)) wr_parse_FType2_RequestClass <- mkDWire (tagged Invalid);
Wire#(Maybe#(FType5_WriteClass)) wr_parse_FType5_WriteClass <- mkDWire (tagged Invalid);
Wire#(Maybe#(FType6_StreamWrClass)) wr_parse_Ftype6_StreamWrClass <- mkDWire (tagged Invalid);
Wire#(Maybe#(Ftype6StreamData)) wr_parse_Ftype6_StreamData <- mkDWire (tagged Invalid);
Wire#(Maybe#(FType10_DOORBELLClass)) wr_parse_Ftype10_DoorBellClass <- mkDWire (tagged Invalid);
Wire#(Maybe#(FType11_MESSAGEClass )) wr_parse_Ftype11_MessageHeader <- mkDWire(tagged Invalid);
Wire#(Maybe#(Ftype11MessageData)) wr_parse_Ftype11_MessageData <- mkDWire(tagged Invalid);

Wire#(ReceivedPktsInfo) wr_ReceivedPktsInfo <- mkDWire (defaultValue);
Wire#(TT) wr_tt <- mkDWire (0); 
Wire#(DestId) wr_Destid <- mkDWire (0); 
Wire#(SourceId) wr_Sourceid <- mkDWire (0);
Wire#(Prio) wr_Prio <- mkDWire (0);
Wire#(Bit#(4)) wr_max_Pkt_Count <- mkDWire (0);

// For Analyse Module
Wire#(Maybe#(TargetReqIfcPkt)) wr_tar_req_packet <- mkDWire (tagged Invalid);
Wire#(TargetReqIfcPkt) wr_treq <- mkDWire (defaultValue);
Wire#(Bool) wr_ready_from_analyse <- mkDWire (False); 

// For Target request Block
Wire#(Bool) wr_ready_from_Treq <- mkDWire (False); 

Wire#(Bool) wr_treq_sof <- mkDWire(False);
Wire#(Bool) wr_treq_eof <- mkDWire (False);
Wire#(Bool) wr_treq_vld <- mkDWire (False);
Wire#(TT) wr_treq_tt <- mkDWire (0); 
Wire#(Data) wr_treq_data <- mkDWire (0);
Wire#(Bool) wr_treq_crf <- mkDWire (False);
Wire#(Prio) wr_treq_prio <- mkDWire (0);
Wire#(Type) wr_treq_ftype <- mkDWire (0);
Wire#(DestId) wr_treq_dest_id <- mkDWire (0);
Wire#(SourceId) wr_treq_source_id <- mkDWire (0);
Wire#(TranId) wr_treq_tid <- mkDWire (0);
Wire#(Type) wr_treq_ttype <- mkDWire (0);
Wire#(Addr) wr_treq_addr <- mkDWire (0);
Wire#(ByteCount) wr_treq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_treq_byte_en <- mkDWire (0);
Wire#(DoorBell) wr_treq_db_info <- mkDWire (0);
Wire#(MsgLen) wr_treq_msg_len <- mkDWire (0);
Wire#(MsgSeg) wr_treq_msg_seg <- mkDWire (0);
Wire#(Bit#(6)) wr_treq_mbox <- mkDWire (0);
Wire#(Mletter) wr_treq_letter <- mkDWire (0);

// Clock Declaration 
Reg#(Bit#(5)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n [  ------------------------------- CLOCK == %d -----------------------------------------  ]", reg_ref_clk);
	if (reg_ref_clk == 27)
	$finish (0);
endrule

// ----------------------------------Initiator request ----------------- Ftype2 ---------- Read Request Class --------------------------------- //

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F2(reg_ref_clk == 0);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0010, ireq_destid:32'hda000000, 			   			ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, 					ireq_byte_en:8'b11110000,ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F2(reg_ref_clk == 0 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F2(reg_ref_clk == 0);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F2(reg_ref_clk == 0); 
	wr_ireq_FType2_RequestClass <=ifc_con.outputs_Ftype2_IOReqClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

// To obtain data count from Concatenation Module(will be obtained in next cycle)
rule rl_init_outputs_con_datacount_F2(reg_ref_clk == 1); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F2(reg_ref_clk == 0); 
     $display(" \n # -----Initiator request ----------------- Ftype2 ---------- Read Request Class ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n\n Reference input packet          =  %b \n\n packet provided to generation   = %b \n\n Ftype2                         = %b 			   			\n Ready from concatenation to initiator request = %b", wr_ireq_packet,wr_ireq_packet_con, wr_ireq_FType2_RequestClass,wr_ready_concatenation);     
endrule

// To display data count From Concatenation
rule rl_disp_con_datacount_F2(reg_ref_clk == 1); 
     $display("\n Data Count = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F2(reg_ref_clk == 0);   
	ifc_gen._inputs_Ftype2IOReqClass (wr_ireq_FType2_RequestClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

// Data count has to provided to generation module in the next cycle
rule rl_init_inputs_gen_datacount_F2(reg_ref_clk ==1);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule

// output from Generation Module 
rule rl_init_outputs_gen_F2(reg_ref_clk == 1); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F2(reg_ref_clk == 1); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to transport parse Module
rule rl_input_parse_F2(reg_ref_clk == 1);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Parse Module
rule rl_output_parse_F2(reg_ref_clk == 3 );
	wr_parse_FType2_RequestClass <=	ifc_parse.outputs_RxFtype2ReqClass_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
endrule

// To display output from Transport module
rule rl_disp_parse_F2(reg_ref_clk == 3 ); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b \n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_FType2_RequestClass, wr_ReceivedPktsInfo, wr_tt, wr_Destid, wr_Sourceid, wr_Prio, 			wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F2(reg_ref_clk == 3);
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype2ReqClass (wr_parse_FType2_RequestClass);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_TReq (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F2(reg_ref_clk == 3 );
	wr_tar_req_packet <=ifc_analyse.outputs_TargetReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module
rule rl_disp_analyse_F2(reg_ref_clk == 3 ); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_tar_req_packet); 
     if (wr_tar_req_packet matches tagged Valid .data_treq) begin
     wr_treq <= data_treq;  
     end  
endrule

//Input to Target Request Module
rule rl_input_Treq_F2(reg_ref_clk == 3);
	ifc_tar_req._inputs_TgtReqIfcPkt (wr_treq);
	ifc_tar_req._TgtReqIfc._treq_rdy_n(False);
endrule

// Output from Target Request Module
rule rl_output_Treq_F2(reg_ref_clk == 4 );
	wr_ready_from_Treq <=ifc_tar_req.outputs_TxReady_From_TReq_ ();
	wr_treq_sof <=ifc_tar_req._TgtReqIfc.treq_sof_n_ ();
	wr_treq_eof <=ifc_tar_req._TgtReqIfc.treq_eof_n_ ();
	wr_treq_vld <=ifc_tar_req._TgtReqIfc.treq_vld_n_ ();
	wr_treq_tt <=ifc_tar_req._TgtReqIfc.treq_tt_ ();
	wr_treq_data <=ifc_tar_req._TgtReqIfc.treq_data_ ();
	wr_treq_crf <=ifc_tar_req._TgtReqIfc.treq_crf_ ();
	wr_treq_prio <=ifc_tar_req._TgtReqIfc.treq_prio_ ();
	wr_treq_ftype <=ifc_tar_req._TgtReqIfc.treq_ftype_ ();
	wr_treq_dest_id <=ifc_tar_req._TgtReqIfc.treq_dest_id_ ();
	wr_treq_source_id <=ifc_tar_req._TgtReqIfc.treq_source_id_ ();
	wr_treq_tid <=ifc_tar_req._TgtReqIfc.treq_tid_ ();
	wr_treq_ttype <=ifc_tar_req._TgtReqIfc.treq_ttype_ ();	
	wr_treq_addr <=ifc_tar_req._TgtReqIfc.treq_addr_ ();
	wr_treq_byte_count <=ifc_tar_req._TgtReqIfc.treq_byte_count_ ();	
	wr_treq_byte_en <=ifc_tar_req._TgtReqIfc.treq_byte_en_n_ ();
	wr_treq_db_info <=ifc_tar_req._TgtReqIfc.treq_db_info_ ();
	wr_treq_msg_len <=ifc_tar_req._TgtReqIfc.treq_msg_len_ ();
	wr_treq_msg_seg <=ifc_tar_req._TgtReqIfc.treq_msg_seg_ ();
	wr_treq_mbox <=ifc_tar_req._TgtReqIfc.treq_mbox_ ();
	wr_treq_letter <=ifc_tar_req._TgtReqIfc.treq_letter_ ();
endrule

// To display output from Target Request module
rule rl_disp_treq_F2(reg_ref_clk == 4 ); 
     $display(" \n # -------Target Request------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n Address      = %b \n ByteCount		= %b \n ByteEn		= %b \n Db Info		= %b 		   			\n Msg Len		= %b  \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b \n RdyTreqmodule	= %b",wr_treq_sof, 			wr_treq_eof,wr_treq_vld,wr_treq_tt,wr_treq_data, wr_treq_crf, wr_treq_prio, wr_treq_ftype , wr_treq_dest_id,wr_treq_source_id, wr_treq_tid, 			wr_treq_ttype, wr_treq_addr,wr_treq_byte_count,wr_treq_byte_en, wr_treq_db_info, wr_treq_msg_len, wr_treq_msg_seg, wr_treq_mbox, wr_treq_letter, 			wr_ready_from_Treq);
endrule

// --------------------------------Initiator request ----------------- Ftype5 ---------- WriteClass ------------------------------- //

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F5(reg_ref_clk == 4);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, 						ireq_destid:32'hdabc0000,ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d8, 						ireq_byte_en:8'b11111111, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F5(reg_ref_clk == 4 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F5(reg_ref_clk == 4);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F5(reg_ref_clk == 4); 
	wr_ireq_FType5_WriteClass <=ifc_con.outputs_Ftype5_IOWrClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

// To obtain data count from Concatenation Module(will be obtained in next cycle)
rule rl_init_outputs_con_datacount_F5(reg_ref_clk == 5); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F5(reg_ref_clk == 4); 
     $display(" \n # -----Initiator request ----------------- Ftype5 ---------- WriteClass  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n\n Reference input packet          =  %b \n\n packet provided to generation   = %b \n\n Ftype2                          = %b 			   			\n Ready from concatenation to initiator request = %b", wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType5_WriteClass, wr_ready_concatenation);     
endrule

// To display data count From Concatenation
rule rl_disp_con_datacount_F5(reg_ref_clk == 5); 
     $display("\n Data Count = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F5(reg_ref_clk == 4);   
	ifc_gen._inputs_Ftype5IOWrClass (wr_ireq_FType5_WriteClass );
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

// Data count has to provided to generation module in the next cycle
rule rl_init_inputs_gen_datacount_F5(reg_ref_clk ==5);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F5(reg_ref_clk == 5 || reg_ref_clk == 6); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F5(reg_ref_clk == 5 || reg_ref_clk == 6 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to transport parse Module
rule rl_input_parse_F5(reg_ref_clk == 5 || reg_ref_clk == 6);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Parse Module
rule rl_output_parse_F5(reg_ref_clk == 7 || reg_ref_clk == 8 );
	wr_parse_FType5_WriteClass <=	ifc_parse.outputs_RxFtype5WriteClass_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
endrule

// To display output from Transport module
rule rl_disp_parse_F5(reg_ref_clk == 7 || reg_ref_clk == 8); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b \n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_FType5_WriteClass, wr_ReceivedPktsInfo, wr_tt, wr_Destid, wr_Sourceid, wr_Prio, 			wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F5(reg_ref_clk == 7 || reg_ref_clk == 8);
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype5WriteClass (wr_parse_FType5_WriteClass);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_TReq (False);
endrule

// For clock seven the output will be tagged Invalid
// Output from Analyse Module
rule rl_output_analyse_F5(reg_ref_clk == 8 );
	wr_tar_req_packet <=ifc_analyse.outputs_TargetReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module
rule rl_disp_analyse_F5(reg_ref_clk == 8 ); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_tar_req_packet); 
     if (wr_tar_req_packet matches tagged Valid .data_treq) begin
     wr_treq <= data_treq;  
     end  
endrule

//Input to Target Request Module
rule rl_input_Treq_F5(reg_ref_clk == 8);
	ifc_tar_req._inputs_TgtReqIfcPkt (wr_treq);
	ifc_tar_req._TgtReqIfc._treq_rdy_n(False);
endrule

// Output from Target Request Module
rule rl_output_Treq_F5(reg_ref_clk == 9 );
	wr_ready_from_Treq <=ifc_tar_req.outputs_TxReady_From_TReq_ ();
	wr_treq_sof <=ifc_tar_req._TgtReqIfc.treq_sof_n_ ();
	wr_treq_eof <=ifc_tar_req._TgtReqIfc.treq_eof_n_ ();
	wr_treq_vld <=ifc_tar_req._TgtReqIfc.treq_vld_n_ ();
	wr_treq_tt <=ifc_tar_req._TgtReqIfc.treq_tt_ ();
	wr_treq_data <=ifc_tar_req._TgtReqIfc.treq_data_ ();
	wr_treq_crf <=ifc_tar_req._TgtReqIfc.treq_crf_ ();
	wr_treq_prio <=ifc_tar_req._TgtReqIfc.treq_prio_ ();
	wr_treq_ftype <=ifc_tar_req._TgtReqIfc.treq_ftype_ ();
	wr_treq_dest_id <=ifc_tar_req._TgtReqIfc.treq_dest_id_ ();
	wr_treq_source_id <=ifc_tar_req._TgtReqIfc.treq_source_id_ ();
	wr_treq_tid <=ifc_tar_req._TgtReqIfc.treq_tid_ ();
	wr_treq_ttype <=ifc_tar_req._TgtReqIfc.treq_ttype_ ();	
	wr_treq_addr <=ifc_tar_req._TgtReqIfc.treq_addr_ ();
	wr_treq_byte_count <=ifc_tar_req._TgtReqIfc.treq_byte_count_ ();	
	wr_treq_byte_en <=ifc_tar_req._TgtReqIfc.treq_byte_en_n_ ();
	wr_treq_db_info <=ifc_tar_req._TgtReqIfc.treq_db_info_ ();
	wr_treq_msg_len <=ifc_tar_req._TgtReqIfc.treq_msg_len_ ();
	wr_treq_msg_seg <=ifc_tar_req._TgtReqIfc.treq_msg_seg_ ();
	wr_treq_mbox <=ifc_tar_req._TgtReqIfc.treq_mbox_ ();
	wr_treq_letter <=ifc_tar_req._TgtReqIfc.treq_letter_ ();
endrule

// To display output from Target Request module
rule rl_disp_treq_F5(reg_ref_clk == 9 ); 
     $display(" \n # -------Target Request------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n Address      = %b \n ByteCount		= %b \n ByteEn		= %b \n Db Info		= %b 		   			\n Msg Len		= %b  \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b \n RdyTreqmodule	= %b",wr_treq_sof, 			wr_treq_eof,wr_treq_vld,wr_treq_tt,wr_treq_data, wr_treq_crf, wr_treq_prio, wr_treq_ftype , wr_treq_dest_id,wr_treq_source_id, wr_treq_tid, 			wr_treq_ttype, wr_treq_addr,wr_treq_byte_count,wr_treq_byte_en, wr_treq_db_info, wr_treq_msg_len, wr_treq_msg_seg, wr_treq_mbox, wr_treq_letter, 			wr_ready_from_Treq);
endrule

// -------------------Initiator request ----------------- Ftype6 ---------- Streaming Write Class ------------ //

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F6(reg_ref_clk == 9);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0110, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:0, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// The following rule provides the second 64 bit data
rule rl_init_F6_in2(reg_ref_clk ==10 );  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h7777777777777777, ireq_crf:False, ireq_prio:0, ireq_ftype:0,ireq_destid:0,ireq_addr:0, 			ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0,ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// The following rule provides the third 64 bit data
rule rl_init_F6_in3(reg_ref_clk ==11 );  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h5555555555555555, ireq_crf:False, ireq_prio:0, ireq_ftype:0,ireq_destid:0,ireq_addr:0, 			ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0,ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// The following rule provides the last 64 bit data
rule rl_init_F6_in257(reg_ref_clk == 12);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h6666666666666666, ireq_crf:False, ireq_prio:0, ireq_ftype:0, 						ireq_destid:0,ireq_addr:0, ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F6(reg_ref_clk == 9 || reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F6(reg_ref_clk == 9 || reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F6(reg_ref_clk == 9 || reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12); 
	wr_ireq_FType6_StreamWrClass <=ifc_con.outputs_Ftype6_IOStreamWrClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

// To obtain data count from Concatenation Module(will be obtained in next cycle)
rule rl_init_outputs_con_datacount_F6(reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12 || reg_ref_clk == 13); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

/*
// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F6(reg_ref_clk == 9 || reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12); 
     $display(" \n # -----Initiator request -------------- Ftype6 ---------- Streaming Write Class ------- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype6      = %b                	                      			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType6_StreamWrClass, 			  			wr_ready_concatenation);     
endrule

// To display data count From Concatenation
rule rl_disp_con_datacount_F6(reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12 || reg_ref_clk == 13); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule
*/

// Input to generation module
rule rl_init_inputs_gen_F6(reg_ref_clk == 9 || reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12);   
	ifc_gen._inputs_Ftype6IOStreamClass (wr_ireq_FType6_StreamWrClass );
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

// Data count has to provided to generation module in the next cycle
rule rl_init_inputs_gen_datacount_F6(reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12 || reg_ref_clk == 13);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F6(reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12 || reg_ref_clk == 13 ); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

/*
// To display output  comming from Generation 
rule rl_disp_gen_F6(reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12 || reg_ref_clk == 13 || reg_ref_clk == 14 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 	                                    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem, wr_gen_Crf, 			wr_ready_from_dest);     
endrule
*/

// Input to transport parse Module
rule rl_input_parse_F6(reg_ref_clk == 10 || reg_ref_clk == 11 || reg_ref_clk == 12 || reg_ref_clk == 13 );
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Parse Module
rule rl_output_parse_F6(reg_ref_clk == 12 || reg_ref_clk == 13 || reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16);
	wr_parse_Ftype6_StreamWrClass <= ifc_parse.outputs_RxFtype6StreamClass_ ();
	wr_parse_Ftype6_StreamData <= ifc_parse.outputs_RxFtype6StreamData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport module
rule rl_disp_parse_F6(reg_ref_clk == 12 || reg_ref_clk == 13 || reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16 ); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n Stream data         = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	    			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b", wr_parse_Ftype6_StreamWrClass, 			wr_parse_Ftype6_StreamData, wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F6(reg_ref_clk == 12 || reg_ref_clk == 13 || reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16 );
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype6StreamClass (wr_parse_Ftype6_StreamWrClass);
	ifc_analyse._inputs_RxFtype6StreamData (wr_parse_Ftype6_StreamData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_TReq (False);
endrule

// For 12th clock the output will be Tagged Invalid
// Output from Analyse Module
rule rl_output_analyse_F6( reg_ref_clk == 13 || reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16  );
	wr_tar_req_packet <=ifc_analyse.outputs_TargetReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module
rule rl_disp_analyse_F6(reg_ref_clk == 13 || reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16 ); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_tar_req_packet); 
     if (wr_tar_req_packet matches tagged Valid .data_treq) begin
     wr_treq <= data_treq;  
     end  
endrule

//Input to Target Request Module
rule rl_input_Treq_F6(reg_ref_clk == 13 || reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16 );
	ifc_tar_req._inputs_TgtReqIfcPkt (wr_treq);
	ifc_tar_req._TgtReqIfc._treq_rdy_n(False);
endrule

// Output from Target Request Module
rule rl_output_Treq_F6(reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16 || reg_ref_clk == 17);
	wr_ready_from_Treq <=ifc_tar_req.outputs_TxReady_From_TReq_ ();
	wr_treq_sof <=ifc_tar_req._TgtReqIfc.treq_sof_n_ ();
	wr_treq_eof <=ifc_tar_req._TgtReqIfc.treq_eof_n_ ();
	wr_treq_vld <=ifc_tar_req._TgtReqIfc.treq_vld_n_ ();
	wr_treq_tt <=ifc_tar_req._TgtReqIfc.treq_tt_ ();
	wr_treq_data <=ifc_tar_req._TgtReqIfc.treq_data_ ();
	wr_treq_crf <=ifc_tar_req._TgtReqIfc.treq_crf_ ();
	wr_treq_prio <=ifc_tar_req._TgtReqIfc.treq_prio_ ();
	wr_treq_ftype <=ifc_tar_req._TgtReqIfc.treq_ftype_ ();
	wr_treq_dest_id <=ifc_tar_req._TgtReqIfc.treq_dest_id_ ();
	wr_treq_source_id <=ifc_tar_req._TgtReqIfc.treq_source_id_ ();
	wr_treq_tid <=ifc_tar_req._TgtReqIfc.treq_tid_ ();
	wr_treq_ttype <=ifc_tar_req._TgtReqIfc.treq_ttype_ ();	
	wr_treq_addr <=ifc_tar_req._TgtReqIfc.treq_addr_ ();
	wr_treq_byte_count <=ifc_tar_req._TgtReqIfc.treq_byte_count_ ();	
	wr_treq_byte_en <=ifc_tar_req._TgtReqIfc.treq_byte_en_n_ ();
	wr_treq_db_info <=ifc_tar_req._TgtReqIfc.treq_db_info_ ();
	wr_treq_msg_len <=ifc_tar_req._TgtReqIfc.treq_msg_len_ ();
	wr_treq_msg_seg <=ifc_tar_req._TgtReqIfc.treq_msg_seg_ ();
	wr_treq_mbox <=ifc_tar_req._TgtReqIfc.treq_mbox_ ();
	wr_treq_letter <=ifc_tar_req._TgtReqIfc.treq_letter_ ();
endrule

// To display output from Target Request module
rule rl_disp_treq_F6(reg_ref_clk == 14 || reg_ref_clk == 15 || reg_ref_clk == 16 || reg_ref_clk == 17);
     $display(" \n # -------Target Request------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n Address      = %b \n ByteCount		= %b \n ByteEn		= %b \n Db Info		= %b 		   			\n Msg Len		= %b  \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b \n RdyTreqmodule	= %b",wr_treq_sof, 			wr_treq_eof,wr_treq_vld,wr_treq_tt,wr_treq_data, wr_treq_crf, wr_treq_prio, wr_treq_ftype , wr_treq_dest_id,wr_treq_source_id, wr_treq_tid, 			wr_treq_ttype, wr_treq_addr,wr_treq_byte_count,wr_treq_byte_en, wr_treq_db_info, wr_treq_msg_len, wr_treq_msg_seg, wr_treq_mbox, wr_treq_letter, 			wr_ready_from_Treq); 
endrule

// -----------------------------Initiator request ----------------- Ftype 10 ---------- DOORBELLClass --------------------------- //

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F10(reg_ref_clk == 17);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1010, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0,ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F10(reg_ref_clk == 17 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F10(reg_ref_clk == 17);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F10(reg_ref_clk == 17); 
	wr_ireq_FType10_DOORBELLClass <= ifc_con.outputs_Ftype10_MgDOORBELLClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

// To obtain data count from Concatenation Module(will be obtained in next cycle)
rule rl_init_outputs_con_datacount_F10(reg_ref_clk == 18); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F10(reg_ref_clk == 17); 
     $display(" \n # -----Initiator request ----------------- Ftype 10 ---------- DOORBELLClass ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype10     = %b  		 				    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType10_DOORBELLClass,	 			wr_ready_concatenation);     
endrule

// To display data count From Concatenation
rule rl_disp_con_datacount_F10(reg_ref_clk == 18); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule


// Input to generation module
rule rl_init_inputs_gen_F10(reg_ref_clk == 17);   
	ifc_gen._inputs_Ftype10MgDOORBELLClass (wr_ireq_FType10_DOORBELLClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

// Data count has to provided to generation module in the next cycle
rule rl_init_inputs_gen_datacount_F10(reg_ref_clk ==18);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F10(reg_ref_clk == 18 ); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F10(reg_ref_clk == 18 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b                                         			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 			 wr_ready_from_dest);     
endrule

// Input to transport parse Module
rule rl_input_parse_F10(reg_ref_clk == 18 );
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Parse Module
rule rl_output_parse_F10(reg_ref_clk == 20);
	wr_parse_Ftype10_DoorBellClass <= ifc_parse.outputs_RxFtype10DoorBellClass_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport module
rule rl_disp_parse_F10(reg_ref_clk == 20); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b \n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b", wr_parse_Ftype10_DoorBellClass, wr_ReceivedPktsInfo,wr_tt,wr_Destid, wr_Sourceid,wr_Prio, 			wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F10(reg_ref_clk == 20 );
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype10DoorBellClass (wr_parse_Ftype10_DoorBellClass);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_TReq (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F10( reg_ref_clk == 20  );
	wr_tar_req_packet <=ifc_analyse.outputs_TargetReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module
rule rl_disp_analyse_F10(reg_ref_clk == 20  ); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_tar_req_packet); 
     if (wr_tar_req_packet matches tagged Valid .data_treq) begin
     wr_treq <= data_treq;  
     end  
endrule

//Input to Target Request Module
rule rl_input_Treq_F10(reg_ref_clk == 20  ); 
	ifc_tar_req._inputs_TgtReqIfcPkt (wr_treq);
	ifc_tar_req._TgtReqIfc._treq_rdy_n(False);
endrule

// Output from Target Request Module
rule rl_output_Treq_F10(reg_ref_clk == 21  ); 
	wr_ready_from_Treq <=ifc_tar_req.outputs_TxReady_From_TReq_ ();
	wr_treq_sof <=ifc_tar_req._TgtReqIfc.treq_sof_n_ ();
	wr_treq_eof <=ifc_tar_req._TgtReqIfc.treq_eof_n_ ();
	wr_treq_vld <=ifc_tar_req._TgtReqIfc.treq_vld_n_ ();
	wr_treq_tt <=ifc_tar_req._TgtReqIfc.treq_tt_ ();
	wr_treq_data <=ifc_tar_req._TgtReqIfc.treq_data_ ();
	wr_treq_crf <=ifc_tar_req._TgtReqIfc.treq_crf_ ();
	wr_treq_prio <=ifc_tar_req._TgtReqIfc.treq_prio_ ();
	wr_treq_ftype <=ifc_tar_req._TgtReqIfc.treq_ftype_ ();
	wr_treq_dest_id <=ifc_tar_req._TgtReqIfc.treq_dest_id_ ();
	wr_treq_source_id <=ifc_tar_req._TgtReqIfc.treq_source_id_ ();
	wr_treq_tid <=ifc_tar_req._TgtReqIfc.treq_tid_ ();
	wr_treq_ttype <=ifc_tar_req._TgtReqIfc.treq_ttype_ ();	
	wr_treq_addr <=ifc_tar_req._TgtReqIfc.treq_addr_ ();
	wr_treq_byte_count <=ifc_tar_req._TgtReqIfc.treq_byte_count_ ();	
	wr_treq_byte_en <=ifc_tar_req._TgtReqIfc.treq_byte_en_n_ ();
	wr_treq_db_info <=ifc_tar_req._TgtReqIfc.treq_db_info_ ();
	wr_treq_msg_len <=ifc_tar_req._TgtReqIfc.treq_msg_len_ ();
	wr_treq_msg_seg <=ifc_tar_req._TgtReqIfc.treq_msg_seg_ ();
	wr_treq_mbox <=ifc_tar_req._TgtReqIfc.treq_mbox_ ();
	wr_treq_letter <=ifc_tar_req._TgtReqIfc.treq_letter_ ();
endrule

// To display output from Target Request module
rule rl_disp_treq_F10(reg_ref_clk == 21  ); 
     $display(" \n # -------Target Request------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n Address      = %b \n ByteCount		= %b \n ByteEn		= %b \n Db Info		= %b 		   			\n Msg Len		= %b  \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b \n RdyTreqmodule	= %b",wr_treq_sof, 			wr_treq_eof,wr_treq_vld,wr_treq_tt,wr_treq_data, wr_treq_crf, wr_treq_prio, wr_treq_ftype , wr_treq_dest_id,wr_treq_source_id, wr_treq_tid, 			wr_treq_ttype, wr_treq_addr,wr_treq_byte_count,wr_treq_byte_en, wr_treq_db_info, wr_treq_msg_len, wr_treq_msg_seg, wr_treq_mbox, wr_treq_letter, 			wr_ready_from_Treq); 
endrule

// -------------------Initiator request ----------------- Ftype 11 ---------- MESSAGEClass ------------ //

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F11_in1(reg_ref_clk == 21);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h3333333333333333, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1011, 						ireq_destid:32'hda340000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:0, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:'d2, ireq_msg_seg:4'b0010, ireq_mbox:6'b000001, ireq_letter:2'b10};
endrule

// The following rule provides the second 64 bit data
rule rl_init_F11_in2(reg_ref_clk == 22);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h2222222222222222, ireq_crf:False, ireq_prio:0, ireq_ftype:0,ireq_destid:0,ireq_addr:0, 			ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0,ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// The following rule provides the third 64 bit data
rule rl_init_F11_in3(reg_ref_clk == 23);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:True, ireq_dsc:True};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h7777777777777777, ireq_crf:False, ireq_prio:0, ireq_ftype:0, 						ireq_destid:0,ireq_addr:0, ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, 					ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F11(reg_ref_clk == 21 || reg_ref_clk == 22 || reg_ref_clk == 23);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F11(reg_ref_clk == 21 || reg_ref_clk == 22 || reg_ref_clk == 23);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F11(reg_ref_clk == 21 || reg_ref_clk == 22 || reg_ref_clk == 23); 
	wr_ireq_FType11_MESSAGEClass <= ifc_con.outputs_Ftype11_MESSAGEClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F11(reg_ref_clk == 22 || reg_ref_clk == 23 || reg_ref_clk == 24); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

/*
// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F11(reg_ref_clk == 21 || reg_ref_clk == 22 || reg_ref_clk == 23); 
     $display(" \n # -----Initiator request ----------------- Ftype11 ---------- MESSAGEClass ----- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype11     = %b                                                 			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType11_MESSAGEClass,                            			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F11(reg_ref_clk == 22 || reg_ref_clk == 23 || reg_ref_clk == 24); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule
*/

// Input to generation module
rule rl_init_inputs_gen_F11(reg_ref_clk == 21 || reg_ref_clk == 22 || reg_ref_clk == 23);   
	ifc_gen._inputs_Ftype11MESSAGEClass (wr_ireq_FType11_MESSAGEClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F11(reg_ref_clk == 22 || reg_ref_clk == 23 || reg_ref_clk == 24);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule

// output from Generation Module 
rule rl_init_outputs_gen_F11(reg_ref_clk == 22 || reg_ref_clk == 23 || reg_ref_clk == 24);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

/*
// To display output  comming from Generation 
rule rl_disp_gen_F11(reg_ref_clk == 22 || reg_ref_clk == 23 || reg_ref_clk == 24);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n, wr_gen_EOF_n, wr_gen_VLD_n, wr_gen_DSC_n, wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 		wr_ready_from_dest);     
endrule
*/

// Input to transport parse Module
rule rl_input_parse_F11(reg_ref_clk == 22 || reg_ref_clk == 23 || reg_ref_clk == 24);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Parse Module
rule rl_output_parse_F11(reg_ref_clk == 24 || reg_ref_clk == 25 || reg_ref_clk == 26); 
	wr_parse_Ftype11_MessageHeader <= ifc_parse. outputs_RxFtype11MsgHeader_ ();
	wr_parse_Ftype11_MessageData <= ifc_parse.outputs_RxFtype11Data_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport module
rule rl_disp_parse_F11(reg_ref_clk == 24 || reg_ref_clk == 25 || reg_ref_clk == 26); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n Stream data         = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype11_MessageHeader, 			wr_parse_Ftype11_MessageData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F11(reg_ref_clk == 24 || reg_ref_clk == 25 || reg_ref_clk == 26);
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype11MsgHeader (wr_parse_Ftype11_MessageHeader);
	ifc_analyse._inputs_RxFtype11Data (wr_parse_Ftype11_MessageData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_TReq (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F11(reg_ref_clk == 24 || reg_ref_clk == 25 || reg_ref_clk == 26);
	wr_tar_req_packet <=ifc_analyse.outputs_TargetReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module
rule rl_disp_analyse_F11(reg_ref_clk == 24 ||reg_ref_clk == 25 || reg_ref_clk == 26); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_tar_req_packet); 
     if (wr_tar_req_packet matches tagged Valid .data_treq) begin
     wr_treq <= data_treq;  
     end  
endrule

//Input to Target Request Module
rule rl_input_Treq_F11(reg_ref_clk == 24 ||reg_ref_clk == 25 || reg_ref_clk == 26); 
	ifc_tar_req._inputs_TgtReqIfcPkt (wr_treq);
	ifc_tar_req._TgtReqIfc._treq_rdy_n(False);
endrule

// Output from Target Request Module
rule rl_output_Treq_F11(reg_ref_clk == 25 ||reg_ref_clk == 26 || reg_ref_clk == 27);  
	wr_ready_from_Treq <=ifc_tar_req.outputs_TxReady_From_TReq_ ();
	wr_treq_sof <=ifc_tar_req._TgtReqIfc.treq_sof_n_ ();
	wr_treq_eof <=ifc_tar_req._TgtReqIfc.treq_eof_n_ ();
	wr_treq_vld <=ifc_tar_req._TgtReqIfc.treq_vld_n_ ();
	wr_treq_tt <=ifc_tar_req._TgtReqIfc.treq_tt_ ();
	wr_treq_data <=ifc_tar_req._TgtReqIfc.treq_data_ ();
	wr_treq_crf <=ifc_tar_req._TgtReqIfc.treq_crf_ ();
	wr_treq_prio <=ifc_tar_req._TgtReqIfc.treq_prio_ ();
	wr_treq_ftype <=ifc_tar_req._TgtReqIfc.treq_ftype_ ();
	wr_treq_dest_id <=ifc_tar_req._TgtReqIfc.treq_dest_id_ ();
	wr_treq_source_id <=ifc_tar_req._TgtReqIfc.treq_source_id_ ();
	wr_treq_tid <=ifc_tar_req._TgtReqIfc.treq_tid_ ();
	wr_treq_ttype <=ifc_tar_req._TgtReqIfc.treq_ttype_ ();	
	wr_treq_addr <=ifc_tar_req._TgtReqIfc.treq_addr_ ();
	wr_treq_byte_count <=ifc_tar_req._TgtReqIfc.treq_byte_count_ ();	
	wr_treq_byte_en <=ifc_tar_req._TgtReqIfc.treq_byte_en_n_ ();
	wr_treq_db_info <=ifc_tar_req._TgtReqIfc.treq_db_info_ ();
	wr_treq_msg_len <=ifc_tar_req._TgtReqIfc.treq_msg_len_ ();
	wr_treq_msg_seg <=ifc_tar_req._TgtReqIfc.treq_msg_seg_ ();
	wr_treq_mbox <=ifc_tar_req._TgtReqIfc.treq_mbox_ ();
	wr_treq_letter <=ifc_tar_req._TgtReqIfc.treq_letter_ ();
endrule

// To display output from Target Request module
rule rl_disp_treq_F11(reg_ref_clk == 25 ||reg_ref_clk == 26 || reg_ref_clk == 27);  
     $display(" \n # -------Target Request------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n Address      = %b \n ByteCount		= %b \n ByteEn		= %b \n Db Info		= %b 		   			\n Msg Len		= %b  \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b \n RdyTreqmodule	= %b",wr_treq_sof, 			wr_treq_eof,wr_treq_vld,wr_treq_tt,wr_treq_data, wr_treq_crf, wr_treq_prio, wr_treq_ftype , wr_treq_dest_id,wr_treq_source_id, wr_treq_tid, 			wr_treq_ttype, wr_treq_addr,wr_treq_byte_count,wr_treq_byte_en, wr_treq_db_info, wr_treq_msg_len, wr_treq_msg_seg, wr_treq_mbox, wr_treq_letter, 			wr_ready_from_Treq); 
endrule

endmodule
endpackage
