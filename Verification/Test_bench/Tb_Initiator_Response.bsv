/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bench For RapidIO_InitiatorRespIFC

-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of both the RapidIO_IOPkt_concatenation module, RapidIO_IOPkt_Generation module, RapidIO_PktSeparation Module, Transport Parse Module, RapidIO_RxPktFTypeAnalyse and RapidIO_InitiatorRespIFC
-- 1. Initiator Request packet is provided as input to concatenation module.
-- 2. Output SOF, EOF, Vld, Data (Header or Data), TxRem and Crf are obtained fron generation module.
-- 3. The output from generation module is given to Transport Parse module.
-- 4. The output from Transport Parse module is given to RapidIO_RxPktFTypeAnalyse.
-- 4. The output from RapidIO_RxPktFTypeAnalyse module is given to RapidIO_InitiatorRespIFC Module.
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package Tb_Initiator_Response;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse::*;
import RapidIO_RxPktFTypeAnalyse::*;
import RapidIO_InitiatorRespIFC::*;

module mkTb_Initiator_Response(Empty);

// Interface
Ifc_RapidIO_IOPktConcatenation ifc_con <- mkRapidIO_IOPktConcatenation;
Ifc_RapidIO_IOPkt_Generation ifc_gen <- mkRapidIO_IOPkt_Generation;
Ifc_RapidIO_InComingPkt_Separation ifc_sep <- mkRapidIO_InComingPkt_Separation;
Ifc_RapidIO_PktTransportParse ifc_parse <- mkRapidIO_PktTransportParse;
Ifc_RapidIO_RxPktFTypeAnalyse ifc_analyse <- mkRapidIO_RxPktFTypeAnalyse;
Ifc_InitResp ifc_init_resp <- mkRapidIO_InitiatorRespIFC;

	
// For Concatenation Module
Wire#(TargetRespIfcCntrl) wr_tar_control <- mkDWire (defaultValue);
Wire#(TargetRespIfcData) wr_tar_data <- mkDWire (defaultValue);
Wire#(TargetRespIfcMsg) wr_tar_msg <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet_con <- mkDWire (defaultValue);

Wire#(MaintenanceRespIfcCntrl) wr_main_control <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcData) wr_main_data <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet_con <- mkDWire (defaultValue);


Wire#(FType13_ResponseClass) wr_tar_FType13_ResponseClass <- mkDWire (defaultValue);
Wire#(FType8_MaintenanceClass) wr_main_FType8_MaintenanceClass <- mkDWire (defaultValue);


Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 

// For Generation Module
Wire#(Bool) wr_gen_SOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_EOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_DSC_n <- mkDWire (False);
Wire#(Bool) wr_gen_VLD_n <- mkDWire (False);
Wire#(DataPkt) wr_gen_Data <- mkDWire (defaultValue); 
Wire#(Bit#(4)) wr_gen_TxRem <- mkDWire (0);
Wire#(Bool) wr_gen_Crf <- mkDWire (False); 
Wire#(Bool) wr_ready_from_dest <- mkDWire (False); 

// For Parse Module
Reg#(Maybe#(FType13_ResponseClass)) wr_parse_Ftype13_ResponseClass <- mkDWire (tagged Invalid);
Wire#(Maybe#(Data)) wr_parse_Ftype13_RespClassData <- mkDWire (tagged Invalid);
Wire#(Maybe#(FType8_MaintenanceClass)) wr_parse_Ftype8_MaintenanceClass <- mkDWire(tagged Invalid);
Wire#(Maybe#(Data)) wr_parse_MaintenanceWrData <- mkDWire(tagged Invalid);

Wire#(ReceivedPktsInfo) wr_ReceivedPktsInfo <- mkDWire (defaultValue);
Wire#(TT) wr_tt <- mkDWire (0); 
Wire#(DestId) wr_Destid <- mkDWire (0); 
Wire#(SourceId) wr_Sourceid <- mkDWire (0);
Wire#(Prio) wr_Prio <- mkDWire (0);
Wire#(Bit#(4)) wr_max_Pkt_Count <- mkDWire (0);

// For Analyse Module
Wire#(Maybe#(InitiatorRespIfcPkt)) wr_InitRespIfcPkt <- mkDWire (tagged Invalid);
Wire#(InitiatorRespIfcPkt) wr_iresp <- mkDWire (defaultValue);
Wire#(Bool) wr_ready_from_analyse <- mkDWire (False); 

// For Initiator response Module
Wire#(Bool) wr_ready_from_iresp <- mkDWire (False); 

Wire#(Bool) wr_iresp_sof <- mkDWire(False);
Wire#(Bool) wr_iresp_eof <- mkDWire (False);
Wire#(Bool) wr_iresp_vld <- mkDWire (False);

Wire#(TT) wr_iresp_tt <- mkDWire (0); 
Wire#(Data) wr_iresp_data <- mkDWire (0);
Wire#(Bool) wr_iresp_crf <- mkDWire (False);
Wire#(Prio) wr_iresp_prio <- mkDWire (0);

Wire#(Type) wr_iresp_ftype <- mkDWire (0);
Wire#(DestId) wr_iresp_dest_id <- mkDWire (0);
Wire#(SourceId) wr_iresp_source_id <- mkDWire (0);
Wire#(TranId) wr_iresp_tid <- mkDWire (0);
Wire#(Type) wr_iresp_ttype <- mkDWire (0);
Wire#(Status) wr_iresp_status <- mkDWire (0);

Wire#(Bool) wr_iresp_local <- mkDWire (False);
Wire#(MsgSeg) wr_iresp_msg_seg <- mkDWire (0);
Wire#(Bit#(2)) wr_iresp_mbox <- mkDWire (0);
Wire#(Mletter) wr_iresp_letter <- mkDWire (0);

// Clock Declaration 
Reg#(Bit#(5)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n [  ------------------------------- CLOCK == %d -----------------------------------------  ]", reg_ref_clk);
	if (reg_ref_clk == 20)
	$finish (0);
endrule


// -------------------- Target response ---------------- ResponseClass ---------------------------------//

// Response class (ttype = 1) with Payload

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F13_p_in1(reg_ref_clk == 0);  
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b00, tresp_data:64'h3333333333333333, tresp_crf:False, tresp_prio:2'b00, tresp_ftype:4'b1101, 			   	                  tresp_dest_id:32'hda000000,tresp_status:4'b0000,tresp_tid:8'hff,tresp_ttype:4'b1000, tresp_no_data:False};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:0, tresp_mbox:0, tresp_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F13_p(reg_ref_clk == 0);  
	wr_tar_packet<= TargetRespIfcPkt {trespcntrl:wr_tar_control, trespdata:wr_tar_data, trespmsg:wr_tar_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F13_p(reg_ref_clk == 0);   
	ifc_con._inputs_TargetRespIfcPkt (wr_tar_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F13_p(reg_ref_clk == 0);
	wr_tar_FType13_ResponseClass <= ifc_con.outputs_Ftype13_IORespPacket_ ();
	wr_tar_packet_con<= ifc_con.outputs_TgtRespIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F13_p(reg_ref_clk == 1);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F13_p(reg_ref_clk == 0);
     $display(" \n # ----- Target response ----------------- Ftype13 ---------- Response class with Payload  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype13     = %b              					    			\n Ready from concatenation to initiator request = %b", wr_tar_packet, wr_tar_packet_con, wr_tar_FType13_ResponseClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F13_p(reg_ref_clk == 1); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule


// Input to generation module
rule rl_init_inputs_gen_F13_p(reg_ref_clk == 0);  
	ifc_gen._inputs_Ftype13IORespClass (wr_tar_FType13_ResponseClass);
	ifc_gen._inputs_TgtRespIfcPkt (wr_tar_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F13_p(reg_ref_clk == 1);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F13_p(reg_ref_clk == 1);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F13_p(reg_ref_clk == 1);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Transport Parse Module
rule rl_input_parse_F13_p(reg_ref_clk == 1);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Transport Parse Module
rule rl_output_parse_F13_p(reg_ref_clk == 3); 
	wr_parse_Ftype13_ResponseClass <= ifc_parse.outputs_RxFtype13ResponseClass_ ();
	wr_parse_Ftype13_RespClassData <= ifc_parse.outputs_RxFtype13ResponseData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport Parse module
rule rl_disp_parse_F13_p(reg_ref_clk == 3); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n data                = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype13_ResponseClass, 			wr_parse_Ftype13_RespClassData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F13_p(reg_ref_clk == 3);
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype13ResponseClass (wr_parse_Ftype13_ResponseClass);
	ifc_analyse._inputs_RxFtype13ResponseData (wr_parse_Ftype13_RespClassData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_IResp (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F13_p(reg_ref_clk == 3);
	wr_InitRespIfcPkt <=ifc_analyse.outputs_InitRespIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module 
rule rl_disp_analyse_F13_p(reg_ref_clk == 3); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_InitRespIfcPkt);
     if (wr_InitRespIfcPkt matches tagged Valid .data_iresp) begin
     wr_iresp <= data_iresp;  
     end  
endrule

//Input to Initiator response Module
rule rl_input_Iresp_F13_p(reg_ref_clk == 3);
	ifc_init_resp._inputs_InitRespIfcPkt (wr_iresp);
	ifc_init_resp._InitiatorRespIFC._iresp_rdy_n(False);
endrule

// Output from Initiator response Module
rule rl_output_Iresp_F13_p(reg_ref_clk == 4 );
	wr_ready_from_iresp <=ifc_init_resp.outputs_TxReady_From_IResp_ ();
	wr_iresp_sof <=ifc_init_resp._InitiatorRespIFC.iresp_sof_n_ ();
	wr_iresp_eof <=ifc_init_resp._InitiatorRespIFC.iresp_eof_n_ ();
	wr_iresp_vld <=ifc_init_resp._InitiatorRespIFC.iresp_vld_n_ ();
	wr_iresp_tt <=ifc_init_resp._InitiatorRespIFC.iresp_tt_ ();
	wr_iresp_data <=ifc_init_resp._InitiatorRespIFC.iresp_data_ ();
	wr_iresp_crf <=ifc_init_resp._InitiatorRespIFC.iresp_crf_ ();
	wr_iresp_prio <=ifc_init_resp._InitiatorRespIFC.iresp_prio_ ();
	wr_iresp_ftype <=ifc_init_resp._InitiatorRespIFC.iresp_ftype_ ();
	wr_iresp_dest_id <=ifc_init_resp._InitiatorRespIFC.iresp_dest_id_ ();
	wr_iresp_source_id <=ifc_init_resp._InitiatorRespIFC.iresp_source_id_ ();
	wr_iresp_tid <=ifc_init_resp._InitiatorRespIFC.iresp_tid_ ();
	wr_iresp_ttype <=ifc_init_resp._InitiatorRespIFC.iresp_ttype_ ();	
	wr_iresp_msg_seg <=ifc_init_resp._InitiatorRespIFC.iresp_msg_seg_ ();
	wr_iresp_mbox <=ifc_init_resp._InitiatorRespIFC.iresp_mbox_ ();
	wr_iresp_letter <=ifc_init_resp._InitiatorRespIFC.iresp_letter_ ();
	wr_iresp_local <=ifc_init_resp._InitiatorRespIFC.iresp_local_ ();
	wr_iresp_status <=ifc_init_resp._InitiatorRespIFC.iresp_status_ ();
endrule

// To display output from Target Request module
rule rl_disp_Iresp_F13_p(reg_ref_clk == 4 );
     $display(" \n # -------Initiator Response------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b \n Status		= %b \n Local		= %b \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b 			\n Rdyirespmodule	= %b",wr_iresp_sof,wr_iresp_eof,wr_iresp_vld,wr_iresp_tt,wr_iresp_data, wr_iresp_crf, wr_iresp_prio, wr_iresp_ftype , 			wr_iresp_dest_id,wr_iresp_source_id, wr_iresp_tid, wr_iresp_ttype, wr_iresp_status, wr_iresp_local, wr_iresp_msg_seg, wr_iresp_mbox, 			wr_iresp_letter,wr_ready_from_iresp);
endrule

// Response class (ttype = 0) without Payload

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F13(reg_ref_clk == 5);  
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:True, tresp_vld:True, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b00, tresp_data:0, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b1101, 			   	                  tresp_dest_id:32'hda000000,tresp_status:4'b0000,tresp_tid:8'hbf,tresp_ttype:4'b0000, tresp_no_data:True};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F13(reg_ref_clk == 5);  
	wr_tar_packet<= TargetRespIfcPkt {trespcntrl:wr_tar_control, trespdata:wr_tar_data, trespmsg:wr_tar_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F13(reg_ref_clk == 5);   
	ifc_con._inputs_TargetRespIfcPkt (wr_tar_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F13(reg_ref_clk == 5);
	wr_tar_FType13_ResponseClass <= ifc_con.outputs_Ftype13_IORespPacket_ ();
	wr_tar_packet_con<= ifc_con.outputs_TgtRespIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F13(reg_ref_clk == 6);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F13(reg_ref_clk == 5);
     $display(" \n # ----- Target response ---------------- Ftype13 ----------Response class without Payload  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype13     = %b						    			\n Ready from concatenation to initiator request = %b", wr_tar_packet, wr_tar_packet_con, wr_tar_FType13_ResponseClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F13(reg_ref_clk == 6); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F13(reg_ref_clk == 5);  
	ifc_gen._inputs_Ftype13IORespClass (wr_tar_FType13_ResponseClass);
	ifc_gen._inputs_TgtRespIfcPkt (wr_tar_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F13(reg_ref_clk == 6);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F13(reg_ref_clk == 6);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F13(reg_ref_clk == 6);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    		 	\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Transport Parse Module
rule rl_input_parse_F13(reg_ref_clk == 6);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Transport Parse Module
rule rl_output_parse_F13(reg_ref_clk == 8); 
	wr_parse_Ftype13_ResponseClass <= ifc_parse.outputs_RxFtype13ResponseClass_ ();
	wr_parse_Ftype13_RespClassData <= ifc_parse.outputs_RxFtype13ResponseData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport Parse module
rule rl_disp_parse_F13(reg_ref_clk == 8); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n data                = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype13_ResponseClass, 			wr_parse_Ftype13_RespClassData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F13(reg_ref_clk == 8);
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse. _inputs_RxFtype13ResponseClass (wr_parse_Ftype13_ResponseClass);
	ifc_analyse._inputs_RxFtype13ResponseData (wr_parse_Ftype13_RespClassData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_IResp (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F13(reg_ref_clk == 8);
	wr_InitRespIfcPkt <=ifc_analyse.outputs_InitRespIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module 
rule rl_disp_analyse_F13(reg_ref_clk == 8); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_InitRespIfcPkt);
     if (wr_InitRespIfcPkt matches tagged Valid .data_iresp) begin
     wr_iresp <= data_iresp;  
     end  
endrule

//Input to Initiator response Module
rule rl_input_Iresp_F13(reg_ref_clk == 8);
	ifc_init_resp._inputs_InitRespIfcPkt (wr_iresp);
	ifc_init_resp._InitiatorRespIFC._iresp_rdy_n(False);
endrule

// Output from Initiator response Module
rule rl_output_Iresp_F13(reg_ref_clk == 9);
	wr_ready_from_iresp <=ifc_init_resp.outputs_TxReady_From_IResp_ ();
	wr_iresp_sof <=ifc_init_resp._InitiatorRespIFC.iresp_sof_n_ ();
	wr_iresp_eof <=ifc_init_resp._InitiatorRespIFC.iresp_eof_n_ ();
	wr_iresp_vld <=ifc_init_resp._InitiatorRespIFC.iresp_vld_n_ ();
	wr_iresp_tt <=ifc_init_resp._InitiatorRespIFC.iresp_tt_ ();
	wr_iresp_data <=ifc_init_resp._InitiatorRespIFC.iresp_data_ ();
	wr_iresp_crf <=ifc_init_resp._InitiatorRespIFC.iresp_crf_ ();
	wr_iresp_prio <=ifc_init_resp._InitiatorRespIFC.iresp_prio_ ();
	wr_iresp_ftype <=ifc_init_resp._InitiatorRespIFC.iresp_ftype_ ();
	wr_iresp_dest_id <=ifc_init_resp._InitiatorRespIFC.iresp_dest_id_ ();
	wr_iresp_source_id <=ifc_init_resp._InitiatorRespIFC.iresp_source_id_ ();
	wr_iresp_tid <=ifc_init_resp._InitiatorRespIFC.iresp_tid_ ();
	wr_iresp_ttype <=ifc_init_resp._InitiatorRespIFC.iresp_ttype_ ();	
	wr_iresp_msg_seg <=ifc_init_resp._InitiatorRespIFC.iresp_msg_seg_ ();
	wr_iresp_mbox <=ifc_init_resp._InitiatorRespIFC.iresp_mbox_ ();
	wr_iresp_letter <=ifc_init_resp._InitiatorRespIFC.iresp_letter_ ();
	wr_iresp_local <=ifc_init_resp._InitiatorRespIFC.iresp_local_ ();
	wr_iresp_status <=ifc_init_resp._InitiatorRespIFC.iresp_status_ ();
endrule

// To display output from Target Request module
rule rl_disp_Iresp_F13(reg_ref_clk == 9);
     $display(" \n # -------Initiator Response------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b \n Status		= %b \n Local		= %b \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b 			\n Rdyirespmodule	= %b",wr_iresp_sof,wr_iresp_eof,wr_iresp_vld,wr_iresp_tt,wr_iresp_data, wr_iresp_crf, wr_iresp_prio, wr_iresp_ftype , 			wr_iresp_dest_id,wr_iresp_source_id, wr_iresp_tid, wr_iresp_ttype, wr_iresp_status, wr_iresp_local, wr_iresp_msg_seg, wr_iresp_mbox, 			wr_iresp_letter,wr_ready_from_iresp);
endrule

// ------------------ Maintenance Response ---------------- Ftype 8 --------------------- //

// Response Class - Read Response

//  Control and Data Signals are provied to create reference packet 
rule rl_init_F8_Rd_resp(reg_ref_clk == 10);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:True, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b01, mresp_data:64'hffffffffffffffff, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda340000,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0010, 							mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Rd_resp(reg_ref_clk == 10);  
	wr_main_packet<= MaintenanceRespIfcPkt {mrespcntrl:wr_main_control, mrespdata:wr_main_data};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Rd_resp(reg_ref_clk == 10);   
	ifc_con._inputs_MaintenanceIfcPkt (wr_main_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Rd_resp(reg_ref_clk == 10);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_main_packet_con<= ifc_con.outputs_MaintainRespIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Rd_resp(reg_ref_clk == 11);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Rd_resp(reg_ref_clk == 10);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ---------- Read Response ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b 						    			\n Ready from concatenation to initiator request = %b", wr_main_packet, wr_main_packet_con, wr_main_FType8_MaintenanceClass ,     			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Rd_resp(reg_ref_clk == 11); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Rd_resp(reg_ref_clk == 10);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_MaintenanceRespIfcPkt (wr_main_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Rd_resp(reg_ref_clk == 11);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Rd_resp(reg_ref_clk == 11 || reg_ref_clk == 12);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Rd_resp(reg_ref_clk == 11 || reg_ref_clk == 12);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					   			\n\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Transport Parse Module
rule rl_input_parse_F8_Rd_resp(reg_ref_clk == 11 || reg_ref_clk == 12);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Transport Parse Module
rule rl_output_parse_F8_Rd_resp(reg_ref_clk == 13 || reg_ref_clk == 14); 
	wr_parse_Ftype8_MaintenanceClass <= ifc_parse.outputs_RxFtype8MainReqClass_ ();
	wr_parse_MaintenanceWrData <= ifc_parse.outputs_RxFtype8MaintainData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport Parse module
rule rl_disp_parse_F8_Rd_resp(reg_ref_clk == 13 || reg_ref_clk == 14); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n data                = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype8_MaintenanceClass, 			wr_parse_MaintenanceWrData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F8_Rd_resp(reg_ref_clk == 13 || reg_ref_clk == 14); 
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse._inputs_RxFtype8MainReqClass (wr_parse_Ftype8_MaintenanceClass);
	ifc_analyse._inputs_RxFtype8MaintainData (wr_parse_MaintenanceWrData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_IResp (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F8_Rd_resp(reg_ref_clk == 14); 
	wr_InitRespIfcPkt <=ifc_analyse.outputs_InitRespIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module 
rule rl_disp_analyse_F8_Rd_resp(reg_ref_clk == 14); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_InitRespIfcPkt);
     if (wr_InitRespIfcPkt matches tagged Valid .data_iresp) begin
     wr_iresp <= data_iresp;  
     end  
endrule

//Input to Initiator response Module
rule rl_input_Iresp_F8_Rd_resp(reg_ref_clk == 14); 
	ifc_init_resp._inputs_InitRespIfcPkt (wr_iresp);
	ifc_init_resp._InitiatorRespIFC._iresp_rdy_n(False);
endrule

// Output from Initiator response Module
rule rl_output_Iresp_F8_Rd_resp(reg_ref_clk == 15); 
	wr_ready_from_iresp <=ifc_init_resp.outputs_TxReady_From_IResp_ ();
	wr_iresp_sof <=ifc_init_resp._InitiatorRespIFC.iresp_sof_n_ ();
	wr_iresp_eof <=ifc_init_resp._InitiatorRespIFC.iresp_eof_n_ ();
	wr_iresp_vld <=ifc_init_resp._InitiatorRespIFC.iresp_vld_n_ ();
	wr_iresp_tt <=ifc_init_resp._InitiatorRespIFC.iresp_tt_ ();
	wr_iresp_data <=ifc_init_resp._InitiatorRespIFC.iresp_data_ ();
	wr_iresp_crf <=ifc_init_resp._InitiatorRespIFC.iresp_crf_ ();
	wr_iresp_prio <=ifc_init_resp._InitiatorRespIFC.iresp_prio_ ();
	wr_iresp_ftype <=ifc_init_resp._InitiatorRespIFC.iresp_ftype_ ();
	wr_iresp_dest_id <=ifc_init_resp._InitiatorRespIFC.iresp_dest_id_ ();
	wr_iresp_source_id <=ifc_init_resp._InitiatorRespIFC.iresp_source_id_ ();
	wr_iresp_tid <=ifc_init_resp._InitiatorRespIFC.iresp_tid_ ();
	wr_iresp_ttype <=ifc_init_resp._InitiatorRespIFC.iresp_ttype_ ();	
	wr_iresp_msg_seg <=ifc_init_resp._InitiatorRespIFC.iresp_msg_seg_ ();
	wr_iresp_mbox <=ifc_init_resp._InitiatorRespIFC.iresp_mbox_ ();
	wr_iresp_letter <=ifc_init_resp._InitiatorRespIFC.iresp_letter_ ();
	wr_iresp_local <=ifc_init_resp._InitiatorRespIFC.iresp_local_ ();
	wr_iresp_status <=ifc_init_resp._InitiatorRespIFC.iresp_status_ ();
endrule

// To display output from Target Request module
rule rl_disp_Iresp_F8_Rd_resp(reg_ref_clk == 15);
     $display(" \n # -------Initiator Response------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b \n Status		= %b \n Local		= %b \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b 			\n Rdyirespmodule	= %b",wr_iresp_sof,wr_iresp_eof,wr_iresp_vld,wr_iresp_tt,wr_iresp_data, wr_iresp_crf, wr_iresp_prio, wr_iresp_ftype , 			wr_iresp_dest_id,wr_iresp_source_id, wr_iresp_tid, wr_iresp_ttype, wr_iresp_status, wr_iresp_local, wr_iresp_msg_seg, wr_iresp_mbox, 			wr_iresp_letter,wr_ready_from_iresp);
endrule

// Response Class - Write Response

//  Control and Data Signals are provied to create reference packet 
rule rl_init_F8_Wr_resp(reg_ref_clk == 16);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:True, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b01, mresp_data:0, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda340000,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0011, 							mresp_hop_count:8'h2f,mresp_local:True};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Wr_resp(reg_ref_clk == 16);  
	wr_main_packet<= MaintenanceRespIfcPkt {mrespcntrl:wr_main_control, mrespdata:wr_main_data};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Wr_resp(reg_ref_clk == 16);   
	ifc_con._inputs_MaintenanceIfcPkt (wr_main_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Wr_resp(reg_ref_clk == 16);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_main_packet_con<= ifc_con.outputs_MaintainRespIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Wr_resp(reg_ref_clk == 17);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Wr_resp(reg_ref_clk == 16);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ----------Write Response --- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b 	       					    			\n Ready from concatenation to initiator request = %b", wr_main_packet, wr_main_packet_con, wr_main_FType8_MaintenanceClass ,    			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Wr_resp(reg_ref_clk == 16); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Wr_resp(reg_ref_clk == 16);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_MaintenanceRespIfcPkt (wr_main_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Wr_resp(reg_ref_clk == 14);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Wr_resp(reg_ref_clk == 17);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Wr_resp(reg_ref_clk == 17);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					   			\n\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem, wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Transport Parse Module
rule rl_input_parse_F8_Wr_resp(reg_ref_clk == 17);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Transport Parse Module
rule rl_output_parse_F8_Wr_resp(reg_ref_clk == 19); 
	wr_parse_Ftype8_MaintenanceClass <= ifc_parse.outputs_RxFtype8MainReqClass_ ();
	wr_parse_MaintenanceWrData <= ifc_parse.outputs_RxFtype8MaintainData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport Parse module
rule rl_disp_parse_F8_Wr_resp(reg_ref_clk == 19); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n data                = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype8_MaintenanceClass, 			wr_parse_MaintenanceWrData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F8_Wr_resp(reg_ref_clk == 19);
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse._inputs_RxFtype8MainReqClass (wr_parse_Ftype8_MaintenanceClass);
	ifc_analyse._inputs_RxFtype8MaintainData (wr_parse_MaintenanceWrData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_IResp (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F8_Wr_resp(reg_ref_clk == 19); 
	wr_InitRespIfcPkt <=ifc_analyse.outputs_InitRespIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module 
rule rl_disp_analyse_F8_Wr_resp(reg_ref_clk == 19);
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_InitRespIfcPkt);
     if (wr_InitRespIfcPkt matches tagged Valid .data_iresp) begin
     wr_iresp <= data_iresp;  
     end  
endrule

//Input to Initiator response Module
rule rl_input_Iresp_F8_Wr_resp(reg_ref_clk == 19);
	ifc_init_resp._inputs_InitRespIfcPkt (wr_iresp);
	ifc_init_resp._InitiatorRespIFC._iresp_rdy_n(False);
endrule

// Output from Initiator response Module
rule rl_output_Iresp_F8_Wr_resp(reg_ref_clk == 20); 
	wr_ready_from_iresp <=ifc_init_resp.outputs_TxReady_From_IResp_ ();
	wr_iresp_sof <=ifc_init_resp._InitiatorRespIFC.iresp_sof_n_ ();
	wr_iresp_eof <=ifc_init_resp._InitiatorRespIFC.iresp_eof_n_ ();
	wr_iresp_vld <=ifc_init_resp._InitiatorRespIFC.iresp_vld_n_ ();
	wr_iresp_tt <=ifc_init_resp._InitiatorRespIFC.iresp_tt_ ();
	wr_iresp_data <=ifc_init_resp._InitiatorRespIFC.iresp_data_ ();
	wr_iresp_crf <=ifc_init_resp._InitiatorRespIFC.iresp_crf_ ();
	wr_iresp_prio <=ifc_init_resp._InitiatorRespIFC.iresp_prio_ ();
	wr_iresp_ftype <=ifc_init_resp._InitiatorRespIFC.iresp_ftype_ ();
	wr_iresp_dest_id <=ifc_init_resp._InitiatorRespIFC.iresp_dest_id_ ();
	wr_iresp_source_id <=ifc_init_resp._InitiatorRespIFC.iresp_source_id_ ();
	wr_iresp_tid <=ifc_init_resp._InitiatorRespIFC.iresp_tid_ ();
	wr_iresp_ttype <=ifc_init_resp._InitiatorRespIFC.iresp_ttype_ ();	
	wr_iresp_msg_seg <=ifc_init_resp._InitiatorRespIFC.iresp_msg_seg_ ();
	wr_iresp_mbox <=ifc_init_resp._InitiatorRespIFC.iresp_mbox_ ();
	wr_iresp_letter <=ifc_init_resp._InitiatorRespIFC.iresp_letter_ ();
	wr_iresp_local <=ifc_init_resp._InitiatorRespIFC.iresp_local_ ();
	wr_iresp_status <=ifc_init_resp._InitiatorRespIFC.iresp_status_ ();
endrule

// To display output from Target Request module
rule rl_disp_Iresp_F8_Wr_resp(reg_ref_clk == 20);
     $display(" \n # -------Initiator Response------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b \n Status		= %b \n Local		= %b \n Meg Seg		= %b \n Mbox		= %b \n Letter		= %b 			\n Rdyirespmodule	= %b",wr_iresp_sof,wr_iresp_eof,wr_iresp_vld,wr_iresp_tt,wr_iresp_data, wr_iresp_crf, wr_iresp_prio, wr_iresp_ftype , 			wr_iresp_dest_id,wr_iresp_source_id, wr_iresp_tid, wr_iresp_ttype, wr_iresp_status, wr_iresp_local, wr_iresp_msg_seg, wr_iresp_mbox, 			wr_iresp_letter,wr_ready_from_iresp);
endrule

endmodule
endpackage
