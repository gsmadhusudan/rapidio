/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

class rapidio_adapter extends uvm_reg_adapter;

 // Register the object with factory for future overrides //
  `uvm_object_utils(rapidio_adapter)

    int support_byte_enable,provides_responses;
    bit [31:0] ral_data;
    
  // Construct an object for the base class //
  function new(string name = "rapidio_adapter");
   super.new(name);
   support_byte_enable = 0;
   provides_responses = 1;
  endfunction

  // Instantiate the logical layer and transport layer sequence items //
   rapidio_ll_sequence_item ll;
   rapidio_tl_sequence_item tl; 

  // Declare a function ( REGISTER TO BUS ) //
  virtual function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw );
    // create objects for the logical transport layer sequence items //
    ll = rapidio_ll_sequence_item::type_id::create("ll");
    tl = rapidio_tl_sequence_item::type_id::create("tl");
    if(!$cast(ll.ral_data,rw.data)) 
    $display(" This is from SEQUENCE FROM DUT REGISTERS \n");
    //rw = uvm_reg_bus_op::type_id::create("rw");
    ll.select = (rw.kind == UVM_READ) ? 0 : 1;
    ll.addr = rw.addr;
    ll.ral_data = rw.data; 
    return ll;
 endfunction 

  // Declaration of a function ( BUS TO REGISTER ) //
  virtual function void bus2reg(uvm_sequence_item bus_item,ref uvm_reg_bus_op rw);
  
   // Instantiate the sequene item //
    rapidio_ll_sequence_item ll;
    rapidio_tl_sequence_item tl;
  
   // With the help of dynamic casting operator assign the type to the bus_item //
   if(!$cast(ll,bus_item))
    begin
      `uvm_fatal("RAPIDIO_RAL_ADAPTER",$sformatf("TYPE CONVERSIONS MISMATHCED "))
       return;
    end
   else
     begin
      `uvm_info("RAPIDIO_RAL_ADAPTER",$sformatf("TYPE CONVERSIONS MATHCHED "),UVM_HIGH)
        rw.kind = ll.select ? UVM_WRITE : UVM_READ; 
        rw.addr = ll.addr;
        rw.status = UVM_IS_OK;
        if(!$cast(rw.data,ll.data[0]))
          $display(" YES THE TYPE COMPARISIONS HAS NOT MATCHED \n");
        else
          begin
            $display(" YES THE TYPE COMPARISIONS HAS MATCHED \n");
    	    rw.data   = ll.data[0]; 
          end
       end 
  endfunction

endclass


 
