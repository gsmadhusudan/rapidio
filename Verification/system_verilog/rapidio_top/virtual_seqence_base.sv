/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -------------------------------------------------------------------------------------------------------------------------------------------*/
class virtual_sequence_base extends uvm_sequence #(uvm_sequence_item);

  // Regisrting the object in factory //

  `uvm_object_utils(virtual_sequence_base)

  // This is needed to get to the sub-sequencers in the m_sequencer //
 
  rapidio_virtual_sequencer v_sqr;

  // Local sub-sequencer handles

  rapidio_ll_sequencer rio_ll_seqcr;
  rapidio_tl_sequencer    rio_tl_seqcr;

  // Object constructor //

  function new(string name = "virtual_sequence_base");
  super.new(name);
  endfunction

  // Assign pointers to the sub-sequences in the base body method //

  task body();
    if(!$cast(v_sqr, m_sequencer)) begin
     `uvm_error(get_full_name(), "Virtual sequencer pointer cast failed");
    end
    rio_ll_seqcr = v_sqr.rio_ll_seqcr;
    rio_tl_seqcr = v_sqr.rio_tl_seqcr;
  endtask: body

endclass : virtual_sequence_base

