/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_PKG_SVH 
`define RAPIDIO_PKG_SVH 

package RAPIDIO_PKG;

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "rapidio_ll_define.sv"
`include "rapidio_ll_sequence_item.sv"
`include "rapidio_tl_sequence_item.sv"

`include "rapidio_ll_agent_config.sv"
`include "rapidio_adapter.sv"

`include "rapidio_ll_monitor.sv"
`include "rapidio_ll_bfm.sv" 
`include "rapidio_ll_sequencer.sv"
`include "rapidio_ll_agent.sv"
`include "rapidio_ll_scoreboard.sv"

`include "register_reg.sv"
`include "rapidio_register_reg_block.sv"
`include "rapidio_ll_seq_lib.sv"

`include "rapidio_ll_env_config.sv"

`include "rapidio_tl_agent_config.sv"  
`include "rapidio_tl_driver.sv"  
`include "rapidio_tl_monitor.sv"  
`include "rapidio_tl_sequencer.sv"  
`include "rapidio_tl_sequence.sv"  
`include "rapidio_tl_agent.sv" 

`include "rapidio_sb.sv"
`include "rapidio_virtual_sequencer.sv"
`include "virtual_seqence_base.sv"
`include "rapidio_virtual_sequence_lib.sv"
`include "rapidio_env_config.sv"
`include "rapidio_ll_coverage.sv"
`include "rapidio_tl_coverage.sv"
`include "rapidio_env.sv"
`include "rapidio_base_test.sv"




endpackage : RAPIDIO_PKG

`endif
