/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_AGENT_CONFIG_SVH
`define RAPIDIO_AGENT_CONFIG_SVH
parameter RAPIDIO_AWIDTH=32;

// CLASS : rapidio_agent_config
class rapidio_ll_agent_config extends uvm_object;

  // Variable declarations
  string                             agent_name       = "DEFAULT";
  bit                                has_checks       = 1'b1;
  bit                                has_coverage     = 1'b1;
  bit                                is_initiator        = 1'b1;
  rand bit unsigned[RAPIDIO_AWIDTH-1:0] start_address    = 32'h0000_0000;
  rand bit unsigned[RAPIDIO_AWIDTH-1:0] end_address      = 32'h0000_0000;
  uvm_active_passive_enum            is_active        = UVM_ACTIVE;
  bit                                car_bridge;    
  bit                                car_memory;    
  bit                                car_processor;    
  bit                                car_switch;    
  bit                                car_extended_features;    
  bit    [2:0]                       car_extended_addr_mode;    
  bit    [7:0]                       car_port_count;    
  bit    [7:0]                       car_port_number;
  
  //Source CAR operations support  
  bit                                source_car_read_support;    
  bit                                source_car_write_support;    
  bit                                source_car_steaming_write_support;    
  bit                                source_car_write_with_response_support;    
  bit                                source_car_compare_n_swap_support;    
  bit                                source_car_test_n_swap_support;    
  bit                                source_car_increment_support;    
  bit                                source_car_decrement_support;    
  bit                                source_car_set_support;    
  bit                                source_car_clear_support;    
  bit                                source_car_swap_support;    
  bit                                source_car_port_write_support;    

  //desitination CAR operations support  
  bit                                destination_car_read_support;    
  bit                                destination_car_write_support;    
  bit                                destination_car_steaming_write_support;    
  bit                                destination_car_write_with_response_support;    
  bit                                destination_car_compare_n_swap_support;    
  bit                                destination_car_test_n_swap_support;    
  bit                                destination_car_increment_support;    
  bit                                destination_car_decrement_support;    
  bit                                destination_car_set_support;    
  bit                                destination_car_clear_support;    
  bit                                destination_car_swap_support;    
  bit                                destination_car_port_write_support;    

 //Logical layer Control 
 bit    [2:0]                       csr_extended_addr_control;    
 
 bit    [62:0]                       base_addr_for_66bit_addressing;    
 bit    [46:0]                       base_addr_for_50bit_addressing;    
 bit    [30:0]                       base_addr_for_34bit_addressing;    

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_ll_agent_config)
    `uvm_field_string(agent_name, UVM_ALL_ON)
    `uvm_field_int(has_checks, UVM_ALL_ON)
    `uvm_field_int(has_coverage, UVM_ALL_ON)
    `uvm_field_int(is_initiator, UVM_ALL_ON)
    `uvm_field_int(start_address, UVM_ALL_ON)
    `uvm_field_int(end_address, UVM_ALL_ON)
    `uvm_field_enum(uvm_active_passive_enum, is_active, UVM_ALL_ON)
  `uvm_object_utils_end

  // FUNC : Constructor
  function new(string name = "rapidio_ll_agent_config");
    super.new(name);
  endfunction:new

  // FUNC : Check_target_config - more of sanity check
  virtual function automatic void check_target_config();
    // Check that if target has assigned an address space.
    if(start_address == 32'h0000_0000 && end_address == 32'h0000_0000) begin
      `uvm_fatal("RAPIDIO_AGNT_CFG_FATAL", $psprintf("NO ADDR SPACE CONFIGURATION FOUND"));
    end

    // Check that 'start_address' of all targets is a multiple of 1K.
    if(start_address % 1024 != 0) begin
      `uvm_fatal("RAPIDIO_AGNT_CFG_FATAL", $psprintf("START ADDR ='h%h NOT MULTIPLE OF 1K.",start_address));
    end

    // Check that the size of all targets is a multiple of 1K.
    if((end_address - start_address + 1) % 1024 != 0)begin
      `uvm_fatal("RAPIDIO_AGNT_CFG_FATAL", $psprintf("MEM SEGMENT ='h%h NOT MULTIPLE OF 1K.", (end_address - start_address + 1)));
    end

    print_segments();
  endfunction: check_target_config


  // FUNC : Print memory segments.
  virtual function void print_segments();
    `uvm_info("RAPIDIO_AGNT_CFG_INFO", $sformatf("START ADDR = 'h%h, END ADDR = 'h%h", start_address, end_address), UVM_LOW)
  endfunction: print_segments

endclass : rapidio_ll_agent_config


// CLASS : rapidio_assertion_enables
class rapidio_assertion_enables extends uvm_object;

  // FUNC : Constructor
  function new(string name = "rapidio_assertion_enables");
    super.new(name);
  endfunction:new

endclass : rapidio_assertion_enables

`endif
