/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_MASTER_DRIVER_SVH
`define RAPIDIO_MASTER_DRIVER_SVH

//typedef class rapidio_ll_bfm;

// Callbacks defined for setup & access phase
class rapidio_ll_bfm_cbs extends uvm_callback; 
//  virtual task initiator_drvr  (rapidio_ll_bfm xactor , rapidio_ll_sequence_item cycle);endtask
endclass : rapidio_ll_bfm_cbs


// CLASS : rapidio_ll_driver
class rapidio_ll_driver extends uvm_driver #(rapidio_ll_sequence_item);


  // Virtual interface declaration
  virtual interface         rapidio_interface  vif;


//SB ANALYSIS PORT ADDED HERE 
  uvm_analysis_port#(rapidio_ll_sequence_item)dr2sb_ll_port;
  uvm_analysis_port#(rapidio_ll_sequence_item)dr2sb_ll_dut_port;
 uvm_analysis_port#(rapidio_ll_sequence_item)dr2cov_ll_port;
//EVENT
event packet2send_ll_bfm;
 


// Variable declarations
  rapidio_ll_sequence_item         req;
  rapidio_ll_sequence_item         ll_pkt;
  rapidio_ll_agent_config          rapidio_ll_cfg;
  uvm_blocking_put_port #(byte unsigned ) put_pkt_port; 
  uvm_blocking_put_port #(int ) put_cntrl_port; 
  bit                          item_available = 0;     //Used to check if item is active in driver or not

  // UVM automation macros
  `uvm_component_utils_begin (rapidio_ll_driver)
    `uvm_field_object(req, UVM_ALL_ON)
    `uvm_field_object(rapidio_ll_cfg, UVM_ALL_ON)
    `uvm_field_int(item_available,UVM_ALL_ON)
  `uvm_component_utils_end

  `uvm_register_cb(rapidio_ll_driver, rapidio_ll_bfm_cbs)

  extern function new (string name, uvm_component parent);
  extern function void build_phase(uvm_phase phase);
  extern virtual task run_phase(uvm_phase phase);   
  extern virtual task get_and_drive(rapidio_ll_sequence_item req);
  extern virtual task drive_reset_values();
  extern virtual task check_resetn_assertion();

endclass : rapidio_ll_driver


// FUNC : Constructor
function rapidio_ll_driver::new (string name, uvm_component parent);
  super.new(name, parent);
  req = new();
  ll_pkt = new();
  put_pkt_port = new("put_pkt_port",this);
  put_cntrl_port = new("put_cntrl_port",this);
  dr2sb_ll_port  =  new("dr2sb_ll_port",this);
  dr2sb_ll_dut_port  =  new("dr2sb_ll_dut_port",this);
  dr2cov_ll_port  =  new("dr2cov_ll_port",this);
endfunction : new
 
// FUNC : Build Phase
function void rapidio_ll_driver::build_phase(uvm_phase phase);
  super.build_phase(phase);
  if(!uvm_config_db#(virtual rapidio_interface)::get(this, "", "vif", vif))
    `uvm_fatal("RAPIDIO_LL_DRVR_FATAL",{"VIRTUAL INTERFACE MUST BE SET FOR: ",get_full_name(),".vif"});
  void'(uvm_config_db#(rapidio_ll_agent_config)::get(this,"","rapidio_ll_agent_config",rapidio_ll_cfg));
endfunction: build_phase

// TASK : RUN PHASE
task rapidio_ll_driver::run_phase(uvm_phase phase);
  super.run_phase(phase);
  drive_reset_values();                //Driving reset values on the interface
  `uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf("RAPIDIO_LL_BFM STARTED\n"),UVM_LOW)

  forever begin
    wait(vif.rio_resetn);                 //Wait for reset to be deasserted to process transactions
   // `uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf("REEST RELEASED =%d\n",vif.rio_resetn),UVM_LOW)
    seq_item_port.get_next_item(req);  //get the item from the sequence
      item_available = 1;                //Set this flag to track the reset driving logic.
    fork
      begin
        get_and_drive(req);
      end
      check_resetn_assertion(); 
    join_any
    seq_item_port.item_done();         //once item_done is called next item will be available 
    disable fork;
  end
endtask : run_phase

// TASK : get_and_drive - To get the item & drive on DUT interface
task rapidio_ll_driver::get_and_drive(rapidio_ll_sequence_item req);

    byte unsigned bytes[];
    int pkt_len;
  pkt_len = req.pack_bytes(bytes);
 //`uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf("LL PACKET LENGTH in BYTES =%d\n",pkt_len),UVM_LOW)
  pkt_len =   pkt_len/8 ;
 //`uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf("LL PACKET LENGTH in Dwords =%d\n",pkt_len),UVM_LOW)
  put_cntrl_port.put(pkt_len); 
  ll_pkt.dword_length=pkt_len; 
  foreach (bytes[i])   begin 
     put_pkt_port.put(bytes[i]); 
     //`uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf(" LLBYTES =%h,i=%d\n",bytes[i],i),UVM_LOW)
      dr2cov_ll_port.write(req);
   //`uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf("\nSENDING DATA TO THE DUT BACK TO BACK\n"),UVM_LOW)
  -> packet2send_ll_bfm;
  end
  void' (ll_pkt.unpack_bytes(bytes));
 dr2sb_ll_port.write(ll_pkt);
  dr2sb_ll_dut_port.write(ll_pkt);
  //`uvm_info("RAPIDIO_LL_BFM_INFO",$sformatf("\nSENDING DATA TO THE DUT BACK TO BACK\n"),UVM_LOW)
 
  item_available    = '0;            //Reset the variable to indicate the transaction is completed.  
endtask : get_and_drive

// TASK : drive_reset_values - Drive reset values in the RAPIDIO interface
task rapidio_ll_driver::drive_reset_values();
  //`uvm_info("RAPIDIO_MST_DRV_INFO",$sformatf("DRIVING RESET VALUES ON MASTER I/F \n"), UVM_LOW);
endtask : drive_reset_values
   
// TASK : check_presetn_assertion - Check if presetn is asserted in middle of transaction
task rapidio_ll_driver::check_resetn_assertion();
  if(item_available) begin        // TRUE indicates transaction in progress
    item_available = 0;
  end
    drive_reset_values();
endtask:check_resetn_assertion
    
`endif
