/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_SMSS_BASE_TEST_SVH
`define RAPIDIO_SMSS_BASE_TEST_SVH

// TEST: rapidio_base_test
//`include "rapidio_env_config.sv"
class rapidio_b2b_base_test extends rapidio_base_test;

  // Class handle & Variable declarations
  rapidio_env    rapidio_env0;
  rapidio_env_config  env_cfg0;

  rapidio_env    rapidio_env1;
  rapidio_env_config  env_cfg1;

   // Instantiate the reg block //
  rapidio_reg_block rap_reg_ll_blk;
  rapidio_reg_block rap_reg_tl_blk;


  // UVM automation macros 
  `uvm_component_utils_begin(rapidio_b2b_base_test)
    `uvm_field_object(rapidio_env0, UVM_ALL_ON)
    `uvm_field_object(env_cfg0, UVM_ALL_ON)
    `uvm_field_object(env_cfg0.ll_cfg, UVM_ALL_ON)
    `uvm_field_object(rapidio_env1, UVM_ALL_ON)
    `uvm_field_object(env_cfg1, UVM_ALL_ON)
     `uvm_field_object(rap_reg_ll_blk, UVM_ALL_ON)
    `uvm_field_object(rap_reg_tl_blk, UVM_ALL_ON)
  `uvm_component_utils_end

  // FUNC : Constructor
  function new(string name = "rapidio_b2b_base_test", uvm_component parent = null);
    super.new(name,parent);
  endfunction : new

  // FUNC : Build Phase
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    rap_reg_ll_blk = rapidio_reg_block::type_id::create("rap_reg_ll_blk");
    rap_reg_tl_blk = rapidio_reg_block::type_id::create("rap_reg_tl_blk");

    uvm_config_db#(rapidio_reg_block)::set(null,"*","FROM_BASETEST2SEQ",rap_reg_ll_blk);
    uvm_config_db#(rapidio_reg_block)::set(null,"*","FROM_BASETEST2SEQ",rap_reg_tl_blk);

    rapidio_env0 = rapidio_env::type_id::create("rapidio_env0", this);
    env_cfg0  = rapidio_env_config::type_id::create("env_cfg0",this);

    rapidio_env1 = rapidio_env::type_id::create("rapidio_env1", this);
    env_cfg1  = rapidio_env_config::type_id::create("env_cfg1",this);

     
    // Enable all types of coverage in the register model //
    uvm_reg::include_coverage("*",UVM_CVR_ALL);
    // Create the register model //
    rap_reg_ll_blk.build(); 
    rap_reg_tl_blk.build(); 
    // Assign a handle to the register model in the env config //
     env_cfg0.rap_reg_ll_blk = rap_reg_ll_blk;
     env_cfg0.rap_reg_tl_blk = rap_reg_tl_blk;
     
//Scoreboard connections
    env_cfg0.has_scoreboard = 1;
    env_cfg0.ll_cfg.agent_name = " RAPIDIO_AGENT";
    env_cfg0.ll_cfg.is_active = UVM_ACTIVE;
    env_cfg1.has_scoreboard = 0;
    env_cfg0.ll_cfg.agent_name = " RAPIDIO_AGENT";
    env_cfg1.ll_cfg.is_active = UVM_ACTIVE;
    `uvm_info("RAPIDIO_BASE_TEST",$sformatf(" SCOREBOARD VARIABLE HAS SETTED \n"),UVM_HIGH);

//Coverage configurations
env_cfg0.has_coverage = 1;
    env_cfg0.ll_cfg.agent_name = " RAPIDIO_AGENT";
    env_cfg0.ll_cfg.is_active = UVM_ACTIVE;
    env_cfg1.has_coverage = 0;
    env_cfg0.ll_cfg.agent_name = " RAPIDIO_AGENT";
    env_cfg1.ll_cfg.is_active = UVM_ACTIVE;
    `uvm_info("RAPIDIO_BASE_TEST",$sformatf(" COVERAGE VARIABLE HAS SETTED \n"),UVM_HIGH); 

//RAL  configurations
env_cfg0.has_ral = 1;
    env_cfg0.ll_cfg.agent_name = " RAPIDIO_AGENT";
    env_cfg0.ll_cfg.is_active = UVM_ACTIVE;
    env_cfg1.has_ral =1 ;
    env_cfg0.ll_cfg.agent_name = " RAPIDIO_AGENT";
    env_cfg1.ll_cfg.is_active = UVM_ACTIVE;
    `uvm_info("RAPIDIO_BASE_TEST",$sformatf(" RAL VARIABLE HAS SETTED \n"),UVM_HIGH); 

 

    // Set the ENV Configuration
    void'(uvm_config_db#(rapidio_env_config)::set(this.rapidio_env0,"","rapidio_env_config",env_cfg0));
    void'(uvm_config_db#(rapidio_env_config)::set(this.rapidio_env1,"","rapidio_env_config",env_cfg1)); 
  endfunction : build_phase


function void connect_phase(uvm_phase phase);
      rapidio_env0.rio_tl_agent.rapidio_tl_drvr.packet_to_send.connect(rapidio_env1.rio_tl_agent.ingress_pl_to_tl_pkt_fifo.put_export);
      rapidio_env0.rio_tl_agent.rapidio_tl_drvr.control_to_send.connect(rapidio_env1.rio_tl_agent.ingress_pl_to_tl_cntrl_fifo.put_export);
      rapidio_env1.rio_tl_agent.rapidio_tl_drvr.packet_to_send.connect(rapidio_env0.rio_tl_agent.ingress_pl_to_tl_pkt_fifo.put_export);
      rapidio_env1.rio_tl_agent.rapidio_tl_drvr.control_to_send.connect(rapidio_env0.rio_tl_agent.ingress_pl_to_tl_cntrl_fifo.put_export);
      rapidio_env1.rio_ll_agent.rapidio_ll_monitor.mon2sb_ll_dut_port.connect(rapidio_env0.sb.mon2sb_ll_dut_export);
      rapidio_env1.rio_tl_agent.rapidio_tl_mon.mon2sb_tl_dut_port.connect(rapidio_env0.sb.mon2sb_tl_dut_export);
      rapidio_env1.rio_ll_agent.rapidio_ll_bfm.dr2sb_ll_dut_port.connect(rapidio_env0.sb.dr2sb_ll_dut_export);
      rapidio_env1.rio_tl_agent.rapidio_tl_drvr.dr2sb_tl_dut_port.connect(rapidio_env0.sb.dr2sb_tl_dut_export);

 endfunction:connect_phase



  // FUNC : End of Elaboration Phase
  function void end_of_elaboration_phase(uvm_phase phase);
    rapidio_env0.rio_ll_agent.set_report_verbosity_level(UVM_LOW);
    rapidio_env1.rio_ll_agent.set_report_verbosity_level(UVM_LOW);
    uvm_top.set_report_id_action_hier("ILLEGALNAME", UVM_NO_ACTION);
    `uvm_info("RAPIDIO_B2B_BASE_TEST_INFO",$sformatf("Printing the test topology :\n%s", this.sprint(printer)), UVM_LOW)
  endfunction : end_of_elaboration_phase

  // FUNC : Report Phase
  function void report_phase(uvm_phase phase);
    server = get_report_server();
    if(server.get_severity_count(UVM_ERROR) == 0) begin
      `uvm_info("RAPIDIO_B2B_BASE_TEST_INFO", " ** UVM TEST PASSED ** ", UVM_NONE)
    end
    else begin
      `uvm_info("RAPIDIO_B2B_BASE_TEST_INFO", " ** UVM TEST FAIL ** ", UVM_NONE)
    end
  endfunction

endclass : rapidio_b2b_base_test

`endif
