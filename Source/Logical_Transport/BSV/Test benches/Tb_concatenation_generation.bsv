/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bench For RapidIO_IOPkt_concatenation module and the RapidIO_IOPkt_Generation module 
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of both the RapidIO_IOPkt_concatenation module and the RapidIO_IOPkt_Generation module. 
-- 1. Initiator Request, target response and maintenance packets are provided as input to concatenation module.
-- 2. Output SOF, EOF, Vld, Data (Header or Data), TxRem and Crf are obtained from generation module.
--
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_concatenation_generation;

import DefaultValue ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;

module mkTb_concatenation_generation(Empty);

Ifc_RapidIO_IOPktConcatenation ifc_con <- mkRapidIO_IOPktConcatenation;
Ifc_RapidIO_IOPkt_Generation ifc_gen <- mkRapidIO_IOPkt_Generation;
Ifc_InitiatorReqSignals ifc_init <- mkRapidIO_InitiatorReqIFC;


// For Concatenation Module

Wire#(InitReqIfcCntrl) wr_ireq_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_ireq_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_ireq_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet_con <- mkDWire (defaultValue);
Wire#(TargetRespIfcCntrl) wr_tar_control <- mkDWire (defaultValue);
Wire#(TargetRespIfcData) wr_tar_data <- mkDWire (defaultValue);
Wire#(TargetRespIfcMsg) wr_tar_msg <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet_con <- mkDWire (defaultValue);

Wire#(MaintenanceRespIfcCntrl) wr_main_control <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcData) wr_main_data <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet_con <- mkDWire (defaultValue);

Wire#(FType2_RequestClass) wr_ireq_FType2_RequestClass <- mkDWire (defaultValue);
Wire#(FType5_WriteClass) wr_ireq_FType5_WriteClass <- mkDWire (defaultValue);
Wire#(FType6_StreamWrClass) wr_ireq_FType6_StreamWrClass <- mkDWire (defaultValue);
Wire#(FType10_DOORBELLClass) wr_ireq_FType10_DOORBELLClass <- mkDWire (defaultValue);
Wire#(FType11_MESSAGEClass) wr_ireq_FType11_MESSAGEClass <- mkDWire (defaultValue);
Wire#(FType13_ResponseClass) wr_tar_FType13_ResponseClass <- mkDWire (defaultValue);
Wire#(FType8_MaintenanceClass) wr_main_FType8_MaintenanceClass <- mkDWire (defaultValue);


Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 

// For Generation Module

Wire#(Bool) wr_gen_SOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_EOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_DSC_n <- mkDWire (False);
Wire#(Bool) wr_gen_VLD_n <- mkDWire (False);
Wire#(DataPkt) wr_gen_Data <- mkDWire (defaultValue); 
Wire#(Bit#(4)) wr_gen_TxRem <- mkDWire (0);
Wire#(Bool) wr_gen_Crf <- mkDWire (False); 
Wire#(Bool) wr_ready_from_dest <- mkDWire (False);

Wire#(Bool) wr_ready <- mkDWire (False); 

// Clock Declaration 
Reg#(Bit#(10)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n [  ------------------------------- CLOCK == %d -----------------------------------------  ]", reg_ref_clk);
	if (reg_ref_clk == 20)
	$finish (0);
endrule

/*
//###############################################################################################################################################################
//###############################################################################################################################################################

// -------------------Initiator request ----------------- Ftype2 ---------- Streaming Write Class ------------ //

//###############################################################################################################################################################
//###############################################################################################################################################################


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F2(reg_ref_clk == 0);  

	ifc_init._InitReqIfc._ireq_sof_n (True);
	ifc_init._InitReqIfc._ireq_eof_n (False);
	ifc_init._InitReqIfc._ireq_vld_n (True);
	ifc_init._InitReqIfc._ireq_dsc_n (False);
	ifc_init._InitReqIfc._ireq_tt (2'b01);
	ifc_init._InitReqIfc._ireq_data (128'h99FFFF0000000000FFFFFFFFFFFFFF1);
	ifc_init._InitReqIfc._ireq_crf (False);
	ifc_init._InitReqIfc._ireq_prio (2'b01);
	ifc_init._InitReqIfc._ireq_ftype (4'b0010);
	ifc_init._InitReqIfc._ireq_dest_id (32'hda34568c);
	ifc_init._InitReqIfc._ireq_addr (50'h000000008);
	ifc_init._InitReqIfc._ireq_hopcount (0);
	ifc_init._InitReqIfc._ireq_tid (8'hbf);
	ifc_init._InitReqIfc._ireq_ttype (4'b0100);
	ifc_init._InitReqIfc._ireq_byte_count (0);
	ifc_init._InitReqIfc._ireq_byte_en_n (0);
	ifc_init._InitReqIfc._ireq_local (False);
	ifc_init._InitReqIfc._ireq_db_info (16'b0000111100001111);
	ifc_init._InitReqIfc._ireq_msg_len (4'b0000);
	ifc_init._InitReqIfc._ireq_msg_seg (4'b0010);
	ifc_init._InitReqIfc._ireq_mbox (6'b001000);
	ifc_init._InitReqIfc._ireq_letter (2'b10);

endrule

rule rl_output_ready;
  	wr_ready <= ifc_init._InitReqIfc.ireq_rdy_n_ ();		//output from initiator request
endrule

rule rl_output_init(reg_ref_clk == 0);
     	wr_ireq_packet <= ifc_init.outputs_InitReqIfcPkt_ ();	//output from initiator request
endrule


// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F2(reg_ref_clk == 0);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F2(reg_ref_clk == 0); 
	wr_ireq_FType2_RequestClass <=ifc_con.outputs_Ftype2_IOReqClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F2(reg_ref_clk == 1); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F2(reg_ref_clk == 0); 
     $display(" \n # -----Initiator request ----------------- Ftype2 ---------- Read Request Class ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n\n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype2      = %b                                  			 \n Ready from concatenation to initiator request = %b", wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType2_RequestClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F2(reg_ref_clk == 1); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F2(reg_ref_clk == 0);   
	ifc_gen._inputs_Ftype2IOReqClass (wr_ireq_FType2_RequestClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F2(reg_ref_clk ==1);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F2(reg_ref_clk == 1); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F2(reg_ref_clk == 1); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule
*/

//###############################################################################################################################################################
//###############################################################################################################################################################

// -------------------Initiator request ----------------- Ftype6 ---------- Streaming Write Class ------------ //

//###############################################################################################################################################################
//###############################################################################################################################################################

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F2(reg_ref_clk == 0);  

	ifc_init._InitReqIfc._ireq_sof_n (False);
	ifc_init._InitReqIfc._ireq_eof_n (True);
	ifc_init._InitReqIfc._ireq_vld_n (False);
	ifc_init._InitReqIfc._ireq_dsc_n (False);
	ifc_init._InitReqIfc._ireq_tt (2'b01);
	ifc_init._InitReqIfc._ireq_data (128'h99FFFF0000000000FFFFFFFFFFFFFF1);
	ifc_init._InitReqIfc._ireq_crf (False);
	ifc_init._InitReqIfc._ireq_prio (2'b01);
	ifc_init._InitReqIfc._ireq_ftype (4'b0110);
	ifc_init._InitReqIfc._ireq_dest_id (32'hda34568c);
	ifc_init._InitReqIfc._ireq_addr (50'h000000008);
	ifc_init._InitReqIfc._ireq_hopcount (0);
	ifc_init._InitReqIfc._ireq_tid (0);
	ifc_init._InitReqIfc._ireq_ttype (0);
	ifc_init._InitReqIfc._ireq_byte_count (0);
	//ifc_init._InitReqIfc._ireq_byte_en_n (0);
	//ifc_init._InitReqIfc._ireq_local (False);
	ifc_init._InitReqIfc._ireq_db_info (0);
	ifc_init._InitReqIfc._ireq_msg_len (0);
	ifc_init._InitReqIfc._ireq_msg_seg (0);
	ifc_init._InitReqIfc._ireq_mbox (0);
	ifc_init._InitReqIfc._ireq_letter (0);

endrule

rule rl_output_destination;

ifc_gen.pkgen_rdy_n (False);

endrule

rule rl_output_gene;

ifc_con._inputs_RxReady_From_IOGeneration(ifc_gen.outputs_RxRdy_From_Dest_());

endrule

rule rl_output_con;

ifc_init._inputs_IreqRDYIn_From_Concat(ifc_con.outputs_RxRdy_From_Concat_());

endrule

/*
rule rl_output_ready;
  	wr_ready <= ifc_init._InitReqIfc.ireq_rdy_n_ ();		//output from initiator request
endrule
*/

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F6(reg_ref_clk == 0);   
	ifc_con._inputs_InitReqIfcPkt (ifc_init.outputs_InitReqIfcPkt_ ()); 
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F6(reg_ref_clk == 0); 
     $display(" \n # -----Initiator request ----------------- Ftype6 ---------- SWRITE Request Class ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n\n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype6      = %b \n\n Ready from concatenation to initiator request = %b  \n\n\n Ready from initiator request to source = %b ", ifc_con.outputs_InitReqIfcPkt_ (),ifc_con.outputs_InitReqIfcPkt_ (),ifc_con.outputs_Ftype6_IOStreamWrClassPacket_ (), ifc_con.outputs_RxRdy_From_Concat_ (), ifc_init._InitReqIfc.ireq_rdy_n_ () );     
endrule

rule rl_disp_con_datacount_F6(reg_ref_clk == 1); 
     $display("\n Data Count  = %b ",ifc_con.outputs_InitReqDataCount_ ());     
endrule

// Input to generation module
rule rl_init_inputs_gen_F6(reg_ref_clk == 0);   
	ifc_gen._inputs_Ftype6IOStreamClass (ifc_con.outputs_Ftype6_IOStreamWrClassPacket_ () );
	ifc_gen._inputs_InitReqIfcPkt (ifc_con.outputs_InitReqIfcPkt_ ());
//	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F6(reg_ref_clk ==1);   
	ifc_gen._inputs_InitReqDataCount (ifc_con.outputs_InitReqDataCount_ ()); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F6(reg_ref_clk == 1 || reg_ref_clk == 2); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
//	wr	_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F6(reg_ref_clk == 1 );//|| reg_ref_clk == 2 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %h  \n TxRem  = %b \n Crf    = %b 	                                    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem, wr_gen_Crf, 			ifc_gen.outputs_RxRdy_From_Dest_());     
endrule

// The following rule provides 255(64 bit) data

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F6_in2_to_256(reg_ref_clk >= 1 && reg_ref_clk <= 5);  

	ifc_init._InitReqIfc._ireq_sof_n (True);
	ifc_init._InitReqIfc._ireq_eof_n (True);
	ifc_init._InitReqIfc._ireq_vld_n (False);
	ifc_init._InitReqIfc._ireq_dsc_n (False);
	ifc_init._InitReqIfc._ireq_tt (2'b01);
	ifc_init._InitReqIfc._ireq_data (128'h99FFFF0000000000FFFFFFFFFFFFFF1);
	ifc_init._InitReqIfc._ireq_crf (False);
	ifc_init._InitReqIfc._ireq_prio (2'b10);
	ifc_init._InitReqIfc._ireq_ftype (4'b0110);
	ifc_init._InitReqIfc._ireq_dest_id (32'hda34568c);
	ifc_init._InitReqIfc._ireq_addr (50'h000000008);
	ifc_init._InitReqIfc._ireq_hopcount (0);
	ifc_init._InitReqIfc._ireq_tid (0);
	ifc_init._InitReqIfc._ireq_ttype (0);
	ifc_init._InitReqIfc._ireq_byte_count (0);
	//ifc_init._InitReqIfc._ireq_byte_en_n (0);
	//ifc_init._InitReqIfc._ireq_local (False);
	ifc_init._InitReqIfc._ireq_db_info (0);
	ifc_init._InitReqIfc._ireq_msg_len (0);
	ifc_init._InitReqIfc._ireq_msg_seg (0);
	ifc_init._InitReqIfc._ireq_mbox (0);
	ifc_init._InitReqIfc._ireq_letter (0);

endrule


rule rl_init_pkt_F6_in2_to_256(reg_ref_clk >= 1 && reg_ref_clk <= 5);
     	wr_ireq_packet <= ifc_init.outputs_InitReqIfcPkt_ ();	//output from initiator request
endrule


// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F6_in2_to_256(reg_ref_clk >= 1 && reg_ref_clk <= 5);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	//ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F6_in2_to_256(reg_ref_clk >= 1 && reg_ref_clk <= 5); 
	wr_ireq_FType6_StreamWrClass <=ifc_con.outputs_Ftype6_IOStreamWrClassPacket_ ();
//	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
//	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F6_in2_to_256(reg_ref_clk >= 1 && reg_ref_clk <= 10); 
     $display(" \n # -----Initiator request -------------- Ftype6 ---------- Streaming Write Class ------- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype6      = %b \n\n Ready from concatenation to initiator request = %b \n\n\n Ready from initiator request to source = %b", wr_ireq_packet, ifc_con.outputs_InitReqIfcPkt_ (), wr_ireq_FType6_StreamWrClass, ifc_con.outputs_RxRdy_From_Concat_ (), ifc_init._InitReqIfc.ireq_rdy_n_ ());     
endrule

rule rl_disp_con_datacount_F6_in2_to_256(reg_ref_clk >= 2 && reg_ref_clk <= 6); 
     $display("\n Data Count  = %b ",ifc_con.outputs_InitReqDataCount_ ());     
endrule

// Input to generation module
rule rl_init_inputs_gen_F6_in2_to_256(reg_ref_clk >= 1 && reg_ref_clk <= 5);   
	ifc_gen._inputs_Ftype6IOStreamClass (wr_ireq_FType6_StreamWrClass );
	ifc_gen._inputs_InitReqIfcPkt (ifc_con.outputs_InitReqIfcPkt_ ());
//	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F6_in2_to_256(reg_ref_clk >=2 && reg_ref_clk <= 6);   
	ifc_gen._inputs_InitReqDataCount (ifc_con.outputs_InitReqDataCount_ ()); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F6_in2_to_256(reg_ref_clk >= 2 && reg_ref_clk <= 7); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	//wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F6_in2_to_256(reg_ref_clk >= 2 && reg_ref_clk <= 7); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %h  \n TxRem  = %b \n Crf    = %b 	                                    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem, wr_gen_Crf, 			ifc_gen.outputs_RxRdy_From_Dest_());     
endrule

// The following rule provides the 257th 64 bit data

rule rl_init_F6_in257(reg_ref_clk == 6);  
	ifc_init._InitReqIfc._ireq_sof_n (True);
	ifc_init._InitReqIfc._ireq_eof_n (False);
	ifc_init._InitReqIfc._ireq_vld_n (False);
	ifc_init._InitReqIfc._ireq_dsc_n (False);
	ifc_init._InitReqIfc._ireq_tt (2'b01);
	ifc_init._InitReqIfc._ireq_data (128'h99FFFF0000000000FFFFFFFFFFFFFF1);
	ifc_init._InitReqIfc._ireq_crf (False);
	ifc_init._InitReqIfc._ireq_prio (2'b11);
	ifc_init._InitReqIfc._ireq_ftype (4'b0110);
	ifc_init._InitReqIfc._ireq_dest_id (32'hda000000);
	ifc_init._InitReqIfc._ireq_addr (50'h000000008);
	ifc_init._InitReqIfc._ireq_hopcount (0);
	ifc_init._InitReqIfc._ireq_tid (8'hbf);
	ifc_init._InitReqIfc._ireq_ttype (0);
	ifc_init._InitReqIfc._ireq_byte_count (0);
	//ifc_init._InitReqIfc._ireq_byte_en_n (0);
	//ifc_init._InitReqIfc._ireq_local (False);
	ifc_init._InitReqIfc._ireq_db_info (0);
	ifc_init._InitReqIfc._ireq_msg_len (0);
	ifc_init._InitReqIfc._ireq_msg_seg (0);
	ifc_init._InitReqIfc._ireq_mbox (0);
	ifc_init._InitReqIfc._ireq_letter (0);

endrule

rule rl_init_pkt_F6_in257(reg_ref_clk == 6 );
     	wr_ireq_packet <= ifc_init.outputs_InitReqIfcPkt_ ();	//output from initiator request
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F6_in257(reg_ref_clk == 6);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
//	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F6_in257(reg_ref_clk == 6); 
	wr_ireq_FType6_StreamWrClass <=ifc_con.outputs_Ftype6_IOStreamWrClassPacket_ ();
//	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
//	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F6_in257(reg_ref_clk == 6); 
     $display(" \n # -----Initiator request -------------- Ftype6 ---------- Streaming Write Class ------- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype6      = %b \n\n Ready from concatenation to initiator request = %b  \n\n\n Ready from initiator request to source = %b", wr_ireq_packet, ifc_con.outputs_InitReqIfcPkt_ (), wr_ireq_FType6_StreamWrClass, ifc_con.outputs_RxRdy_From_Concat_ (), ifc_init._InitReqIfc.ireq_rdy_n_ ());     
endrule

rule rl_disp_con_datacount_F6_in257;
//(reg_ref_clk == 7); 
     $display("\n Data Count  = %b ",ifc_con.outputs_InitReqDataCount_ ());     
endrule

// Input to generation module
rule rl_init_inputs_gen_F6_in257(reg_ref_clk == 6);   
	ifc_gen._inputs_Ftype6IOStreamClass (wr_ireq_FType6_StreamWrClass );
	ifc_gen._inputs_InitReqIfcPkt (ifc_con.outputs_InitReqIfcPkt_ ());
//	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F6_in257(reg_ref_clk == 7);   
	ifc_gen._inputs_InitReqDataCount (ifc_con.outputs_InitReqDataCount_ ()); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F6_in257(reg_ref_clk == 7 || reg_ref_clk == 8); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
//	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F6_in257(reg_ref_clk == 8 || reg_ref_clk == 9 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %h  \n TxRem  = %b \n Crf    = %b 	                                    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem, wr_gen_Crf, 			ifc_gen.outputs_RxRdy_From_Dest_());   
	  
endrule
/*
//###############################################################################################################################################################

// -------------------Initiator request ----------------- Ftype5 ---------- WriteClass ------------ //

//###############################################################################################################################################################
//###############################################################################################################################################################

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F5(reg_ref_clk ==1);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:128'h99FFFF0000000000FFFFFFFFFFFFFF1, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, 						ireq_destid:32'hdabc0000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F5(reg_ref_clk == 1 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F5(reg_ref_clk == 1);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
//	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F5(reg_ref_clk == 1); 
	wr_ireq_FType5_WriteClass <=ifc_con.outputs_Ftype5_IOWrClassPacket_ ();
//	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
//	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule
/*
rule rl_init_outputs_con_datacount_F5(reg_ref_clk == 2); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule
*/
/*
// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F5(reg_ref_clk == 1); 
     $display(" \n # -----Initiator request ----------------- Ftype5 ---------- WriteClass  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype5      = %b 						    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet,ifc_con.outputs_InitReqIfcPkt_ (),wr_ireq_FType5_WriteClass, ifc_con.outputs_RxRdy_From_Concat_ ());     
endrule

rule rl_disp_con_datacount_F5(reg_ref_clk == 2); 
     $display("\n Data Count  = %b ",ifc_con.outputs_InitReqDataCount_ ());     
endrule

// Input to generation module
rule rl_init_inputs_gen_F5(reg_ref_clk == 1);   
	ifc_gen._inputs_Ftype5IOWrClass (wr_ireq_FType5_WriteClass );
	ifc_gen._inputs_InitReqIfcPkt (ifc_con.outputs_InitReqIfcPkt_ ());
//	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F5(reg_ref_clk == 2);   
	ifc_gen._inputs_InitReqDataCount (ifc_con.outputs_InitReqDataCount_ ()); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F5(reg_ref_clk == 2 || reg_ref_clk == 3); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
//	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F5(reg_ref_clk == 2 || reg_ref_clk == 3); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf,ifc_gen.outputs_RxRdy_From_Dest_());     
endrule

/*
//###############################################################################################################################################################
//###############################################################################################################################################################

// ------------------Doorbell----------------- Ftype10 ---------- WriteClass ------------ //

//###############################################################################################################################################################
//###############################################################################################################################################################


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F10(reg_ref_clk == 8);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:128'h11111111111111111111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1010, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0,ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F10(reg_ref_clk == 8 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F10(reg_ref_clk == 8);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F10(reg_ref_clk == 8); 
	wr_ireq_FType10_DOORBELLClass <= ifc_con.outputs_Ftype10_MgDOORBELLClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F10(reg_ref_clk == 9); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F10(reg_ref_clk == 8); 
     $display(" \n # -----Initiator request ----------------- Ftype 10 ---------- DOORBELLClass ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype10     = %b  		 				    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType10_DOORBELLClass,	 			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F10(reg_ref_clk == 9); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule


// Input to generation module
rule rl_init_inputs_gen_F10(reg_ref_clk == 8);   
	ifc_gen._inputs_Ftype10MgDOORBELLClass (wr_ireq_FType10_DOORBELLClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F10(reg_ref_clk ==9);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F10(reg_ref_clk == 9 ); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F10(reg_ref_clk == 9 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b                                         			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 			 wr_ready_from_dest);     
endrule
*/
//###############################################################################################################################################################
//###############################################################################################################################################################

// ------------------Message Passing----------------- Ftype11 ---------- WriteClass ------------ //

//###############################################################################################################################################################
//###############################################################################################################################################################

/*

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F11(reg_ref_clk == 10);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:128'h22222222222222222222222222222222, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1011, 						ireq_destid:32'hda340000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0000, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F11(reg_ref_clk == 10 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F11(reg_ref_clk == 10);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F11(reg_ref_clk == 10); 
	wr_ireq_FType11_MESSAGEClass <= ifc_con.outputs_Ftype11_MESSAGEClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F11(reg_ref_clk == 11); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F11(reg_ref_clk == 10); 
     $display(" \n # -----Initiator request ----------------- Ftype11 ---------- MESSAGEClass ----- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype11     = %b                                                 			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType11_MESSAGEClass,                            			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F11(reg_ref_clk == 11); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F11(reg_ref_clk == 10);   
	ifc_gen._inputs_Ftype11MESSAGEClass (wr_ireq_FType11_MESSAGEClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F11(reg_ref_clk ==11);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F11(reg_ref_clk == 11 || reg_ref_clk == 12 );
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F11(reg_ref_clk == 11 || reg_ref_clk == 12);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n, wr_gen_EOF_n, wr_gen_VLD_n, wr_gen_DSC_n, wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 		wr_ready_from_dest);     
endrule
*/
endmodule
endpackage
