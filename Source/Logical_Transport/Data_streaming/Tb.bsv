package Tb;												
import RapidIO_DTypes :: *;					//This module consists of all data type definitions and its default values
import RapidIO_MainCore :: *;			//This module initiates the rapidIO operation
import RapidIO_IOPkt_Generation ::*;	//This module actually generates packets to be sent in the rapidIO fabric
import RapidIO_InitiatorReqIFC :: *;	//This has all the ports that are necessary for initiator request

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkTb_for_mkRapidIO_TopModule(Empty);		//Test Bench Module with empty interface 

Ifc_RapidIO_MainCore rio_ModIns_RapidIO_topm <- mkRapidIO_MainCore();		//Top Module instantiation for invoking its methods and passing palues

	Reg#(int) rg_clock <- mkReg(0);		//This register is for clocking the inputs.
	Reg#(int) rg_finish_cond <- mkReg(0);	////////////////////
	//Reg#(Bit#(64)) rg_data <-mkReg(0);	//


/* This rule " rl_passvalues " will pass inputs to the main top module for simulating "Multiple Segment(Start segment)" with priority "1" SOF==0,EOF==1,VLD==0,DSC==1 .
This rule fires only once at rg_clock==0 and checks only if the module is ready
An object for top module is created and the necessary values are passed through methods(ports).
*/

rule rl_testdisp;
$display("This module is being initiated .............");
endrule

rule rl_pass_tb_data;
	if(rg_clock==0 && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin //fires at clock zero
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n (False);	//Asserts active low signal ( enable signal )
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n (True);	//Asserts active High signal ( disable signal )
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n (False);	//Asserts active low signal ( enable signal ) for validating the data
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);	//Asserts active high signal ( disable signal )
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);		//Asserts active high signal ( disable signal )
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h1111111111111111);  //Passes 64 Bit Data in Hexadecimal
  		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b01);	// Lowest priority as 1
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);		//This identifies as data streaming type
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'had000000);		//Used for indentifying destination or receiver
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);	//Address is not used by data streaming 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);		//Hop Count is not used by Data Streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h12);		//This is for identifying Unique Transaction
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);	//Transaction Type is not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d40);	//For determining the number of bytes of (DATA) to be sent
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);	//Byte enable is not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(True);		//Not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);  //Not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);  //Not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);	//Not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);		//Not used by data streaming
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);		//Not used by data streaming
	        $display("Zero Clock:  %d", rg_clock);
		//rg_clock <=1;				//Moves to next clock for sending next data
			
	end // End for rule rl_passvalues
	else if(rg_clock == 1  && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin //Starts executing when ready
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h2222222222222222);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b01);
		//rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h48);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h12);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
		$display("Clock one : %d", rg_clock);
		//rg_clock <= rg_clock+1;
	end

/* This rule " rl_passvalues_2 " will pass inputs to the main top module for simulating "Multiple Segment(End segment)" with priority "2"  & SOF==1,EOF==0,VLD==0,DSC==1 . Executes only one clock (8th clock)
This rule fires only once at rg_clock==8 and also only if the module is ready
*/
	else if(rg_clock == 2  && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin //Starts executing when ready
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h22222222dddddddd);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b01);
		//rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h48);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h12);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
		$display("Clock one : %d", rg_clock);
		//rg_clock <= rg_clock+1;
	end
	else if(rg_clock == 3  && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin //Starts executing when ready
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h22222222abababab);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b01);
		//rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h48);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h12);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
		$display("Clock one : %d", rg_clock);
		//rg_clock <= rg_clock+1;
	end
	else if (rg_clock==4 && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h3333333333333333);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b01);
		//rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h48);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h12);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
	        $display("Clock Two: %d", rg_clock);
		//rg_clock <= rg_clock+1;
	end

/* This rule " rl_passvalues_3 " will pass inputs to the main top module for simulating "Multiple Segment(Start segment)" with priority "2" & SOF==0,EOF==1,VLD==0,DSC==1 . Executes only one clock (9th clock)
This rule fires only once at rg_clock==9 and also only if the module is ready
*/

	else if (rg_clock==5 && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin
 		$display("Resetting rule . . ");
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count(9'd0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
                $display("Clock Three: %d", rg_clock);
		//rg_clock <= rg_clock+1;
	end
	else if (rg_clock==6 && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin
                $display("Single Segment Test bench data . . . . rule passing clk1");
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b01);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h4444444444444444);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b10);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'hadbd0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h13);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d16);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
                $display("Clock four : %d", rg_clock);
		//rg_clock <= rg_clock+1;
	end


/* This rule " rl_passvalues_4 " will pass inputs to the main top module for simulating "Multiple Segment(End segment)" with priority "2" & SOF==1,EOF==0,VLD==0,DSC==1 . Executes only one clock (10th clock)
This rule fires only once at rg_clock==10 and also only if the module is ready
*/

	else if(rg_clock==7 && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin
                $display("Single Segment Test bench data . . . . rule passing clk2");
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h5555555555555555);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b10);
		//rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h48);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h13);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
		//$display("End segment of two segments: ",rg_clock);
		//rg_clock <= rg_clock+1;

		end
/* This rule " rl_passvalues_5 " will pass inputs to the main top module for simulating "Multiple Segment(End segment)" with priority "3" & SOF==0,EOF==0,VLD==0,DSC==1 . Executes only one clock (11th clock)
This rule fires only once at rg_clock==11 and also only if the module is ready
*/

		else if (rg_clock==8 && !(rio_ModIns_RapidIO_topm._InitReqInterface.ireq_rdy_n_())) begin
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b01);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h9999999999999999);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b11);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b1001);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'had000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h13);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d8);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
		//$display("Single Segment...", rg_clock);
		//rg_clock <= rg_clock +1 ;
		end
		else if (rg_clock==9) begin
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
		//$display("Single Segment...", rg_clock);
		//rg_clock <= rg_clock +1 ;
	end
	else if (rg_clock==10) begin
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_sof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_eof_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tt ('b00);				//It determines whether it's a 8 Bit or 16 Bit dev id 
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dsc_n(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_crf(True);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_data(64'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_prio(2'b0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ftype(4'b0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_dest_id(32'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_addr(34'h000000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_hopcount(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_tid(8'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_ttype(4'b0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_count('d0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_byte_en_n(8'h00);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_local(False);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_db_info(16'h0000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_len(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_msg_seg(4'h0);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_mbox(6'b000000);
		rio_ModIns_RapidIO_topm._InitReqInterface._ireq_letter(2'b00);
	end
/*rule rl_vald (rg_clock == 6);
rio_ModIns_RapidIO_topm._InitReqInterface._ireq_vld_n(True);
rg_clock <= rg_clock + 1;
endrule: rl_vald*/
endrule: rl_pass_tb_data

/*rule rl_disp;
$display("Clock: %d",rg_clock);
endrule
*/
rule rl_fin;
rg_clock <= rg_clock +1;
//rg_finish_cond <= rg_finish_cond+1;
//$display("Clock: %d",rg_clock);
//if (rg_finish_cond==((rg_finish_cond*4)+1))
 if (rg_clock == 13) $finish(0);
endrule: rl_fin

endmodule : mkTb_for_mkRapidIO_TopModule
endpackage : Tb
