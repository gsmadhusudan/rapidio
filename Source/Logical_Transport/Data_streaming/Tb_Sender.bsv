package Tb_Sender;

import RapidIO_DTypes ::* ;
import DefaultValue ::* ;
import RapidIO_Top_Wrapper ::* ; 

(* synthesize *)
module mkRapidIO_Tb_Sender (Empty);

Ifc_RapidIO_Top_Wrapper top_RapidIO_Top_Wrap <- mkRapidIO_Top_Wrapper();
Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 309)
		$finish (0);
endrule

function Action fn_InputToTopModule (InitReqIfcCntrl tb_ireqcntrl, InitReqIfcData tb_ireqdata, InitReqIfcMsg tb_ireqmsg, RxIncomingPacket tb_rxpkt, Bool tb_iresprdyin, Bool tb_txrdyin);
   action 
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_sof_n (tb_ireqcntrl.ireq_sof);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_eof_n (tb_ireqcntrl.ireq_eof);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_vld_n (tb_ireqcntrl.ireq_vld);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_dsc_n (tb_ireqcntrl.ireq_dsc);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_tt (tb_ireqdata.ireq_tt);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_data (tb_ireqdata.ireq_data);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_crf (tb_ireqdata.ireq_crf);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_prio (tb_ireqdata.ireq_prio);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_ftype (tb_ireqdata.ireq_ftype);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_dest_id (tb_ireqdata.ireq_destid);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_addr (tb_ireqdata.ireq_addr);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_hopcount (tb_ireqdata.ireq_hopcount);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_tid (tb_ireqdata.ireq_tid);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_ttype (tb_ireqdata.ireq_ttype);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_byte_count (tb_ireqdata.ireq_byte_count);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_byte_en_n (tb_ireqdata.ireq_byte_en);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_local (tb_ireqdata.ireq_local);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_db_info (tb_ireqmsg.ireq_db_info);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_msg_len (tb_ireqmsg.ireq_msg_len);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_msg_seg (tb_ireqmsg.ireq_msg_seg);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_mbox (tb_ireqmsg.ireq_mbox);
	top_RapidIO_Top_Wrap._TWIReqInterface._ireq_letter (tb_ireqmsg.ireq_letter);

 	top_RapidIO_Top_Wrap._TWIRespInterface._iresp_rdy_n (tb_iresprdyin);

	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_sof_n (tb_rxpkt.rx_sof);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_eof_n (tb_rxpkt.rx_eof);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_vld_n (tb_rxpkt.rx_vld);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_data (tb_rxpkt.rx_data);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_rem (tb_rxpkt.rx_rem);
	top_RapidIO_Top_Wrap._TWRxPktsLinkIfc._link_rx_crf (tb_rxpkt.rx_crf);

	top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rdy_n (tb_txrdyin);

   endaction 
endfunction 

rule rl_Initial_Condition (reg_ref_clk >= 0 && reg_ref_clk <= 5); 
    InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    Bool lv_TxRdy = True; 
    Bool lv_iresp_rdy = True; 
    RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: True, rx_eof: True, rx_vld: True, rx_data: 0, rx_rem: 0, rx_crf: False};
    
    fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
endrule 

rule rl_Ftype2_Verification_Dev8 (reg_ref_clk >= 6 && reg_ref_clk <= 29); // Dev8 Support 
    if (reg_ref_clk == 6) begin // Initiator Control and Data signals are issued during this clock 
	InitReqIfcCntrl lv_IReq_Cntrl_Ftype2 =  InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:False, ireq_dsc:True};
	InitReqIfcData lv_IReq_Data_Ftype2 =  InitReqIfcData {ireq_tt: 'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0010, ireq_destid:32'hce000000, 
                                                ireq_addr:'h000000008, ireq_hopcount:0, ireq_tid:8'hfe, ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'h0f, ireq_local:False};
    	InitReqIfcMsg lv_IReq_Msg_Ftype2 = defaultValue;

    	Bool lv_TxRdy_Ftype2 = True; 
    	Bool lv_iresp_rdy_Ftype2 = True; 
        RxIncomingPacket lv_RxPkt_Ftype2 = RxIncomingPacket {rx_sof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_sof_n_ (), 
                                                      rx_eof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_eof_n_ (), 
                                                      rx_vld: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_vld_n_ (), 
                                                      rx_data: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_data_ (), 
                                                      rx_rem: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rem_ (), 
                                                      rx_crf: False};

	fn_InputToTopModule (lv_IReq_Cntrl_Ftype2, lv_IReq_Data_Ftype2, lv_IReq_Msg_Ftype2, lv_RxPkt_Ftype2, lv_iresp_rdy_Ftype2, lv_TxRdy_Ftype2);
    end 
    if (reg_ref_clk == 7 || reg_ref_clk == 8) begin // Default value is given 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = True; 
    	RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_sof_n_ (), 
                                                      rx_eof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_eof_n_ (), 
                                                      rx_vld: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_vld_n_ (), 
                                                      rx_data: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_data_ (), 
                                                      rx_rem: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rem_ (), 
                                                      rx_crf: False};
    
    	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk == 9 || reg_ref_clk == 10) begin // Enabling the Outgoing Transmit Ready signal 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
        RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_sof_n_ (), 
                                                      rx_eof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_eof_n_ (), 
                                                      rx_vld: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_vld_n_ (), 
                                                      rx_data: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_data_ (), 
                                                      rx_rem: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rem_ (), 
                                                      rx_crf: False};
    	
	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 11 && reg_ref_clk <= 19) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = False; 
    	Bool lv_iresp_rdy = False; 
        RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_sof_n_ (), 
                                                      rx_eof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_eof_n_ (), 
                                                      rx_vld: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_vld_n_ (), 
                                                      rx_data: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_data_ (), 
                                                      rx_rem: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rem_ (), 
                                                      rx_crf: False};

       	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end 
    if (reg_ref_clk >= 20 && reg_ref_clk <= 25) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = True; 
        RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_sof_n_ (), 
                                                      rx_eof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_eof_n_ (), 
                                                      rx_vld: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_vld_n_ (), 
                                                      rx_data: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_data_ (), 
                                                      rx_rem: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rem_ (), 
                                                      rx_crf: False};

       	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
    if (reg_ref_clk >= 26 && reg_ref_clk <= 29) begin // After the Packet is transmitted, default value is sent at the remaining clock 
    	InitReqIfcCntrl lv_IReq_Cntrl_Initial =  InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};
    	InitReqIfcData lv_IReq_Data_Initial = defaultValue; 
    	InitReqIfcMsg lv_IReq_Msg_Initial = defaultValue; 
    	Bool lv_TxRdy = True; 
    	Bool lv_iresp_rdy = False; 
        RxIncomingPacket lv_RxPkt = RxIncomingPacket {rx_sof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_sof_n_ (), 
                                                      rx_eof: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_eof_n_ (), 
                                                      rx_vld: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_vld_n_ (), 
                                                      rx_data: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_data_ (), 
                                                      rx_rem: top_RapidIO_Top_Wrap._TWTxPktsLinkIfc.link_tx_rem_ (), 
                                                      rx_crf: False};

       	fn_InputToTopModule (lv_IReq_Cntrl_Initial, lv_IReq_Data_Initial, lv_IReq_Msg_Initial, lv_RxPkt, lv_iresp_rdy, lv_TxRdy);
    end
       
endrule

endmodule: mkRapidIO_Tb_Sender
endpackage
