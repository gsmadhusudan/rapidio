package Tb_Receiver;
import RapidIO_MainCore ::*;
import RapidIO_PktTransportParse ::*;
import RapidIO_TopModule ::*;
import RapidIO_DTypes ::*;
`include "RapidIO.defines"

module mkTb_Receiver(Empty);
Reg#(Bit#(8)) rg_clock <- mkReg(0);
Ifc_RapidIO_TopModule rio_ModIns_RapidIO_topm <- mkRapidIO_TopModule();

rule rl_4segments_1;
if (rg_clock == 0 ) begin // Default values passed.
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h00000000000000000000000000000000);
end
if (rg_clock == 3 ) begin //Header Received - start segment with 64 bit data. Device id 8 Bits
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h0049adcd018000121111111111111111);
end

if (rg_clock == 4 ) begin //Data & CRC Received - Start segment with second clock data. Device id 8 bits 
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h2222222222222222000000000000cccc);
end
if (rg_clock == 5)begin //Header Received - Continuous segment with 64 bit data. Device id 8 Bits
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h0049adcd0100000022222222dddddddd);
end
if (rg_clock == 6 ) begin //Data & CRC Received - Continuous segment with 64 bit data. Device id 8 Bits
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h22222222abababab000000000000cccc);

end
if (rg_clock == 7 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h0049adcd014000283333333333333333);
end
if (rg_clock == 8 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h0000000000000000000000000000cccc);
end
if (rg_clock == 9 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h00000000000000000000000000000000);
end
if (rg_clock == 10 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h0099adbdcdcd02810028444444444444);
end
if (rg_clock == 11 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h4444555555555555555500000000cccc);
end
if (rg_clock == 12 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h0099adbdcdcd02410020666666666666);
end
if (rg_clock == 13 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h6666777777777777777700000000cccc);
end
if (rg_clock == 14 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h00000000000000000000000000000000);
end
if (rg_clock == 15 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h00000000000000000000000000000000);
end
if (rg_clock ==16 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h00c9adcd038000139999999999999999);
end
if (rg_clock ==17 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(False);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'hbbbbbbbbbbbbbbbb000000000000cccc);
end
if (rg_clock == 18 ) begin
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_sof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_eof_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_vld_n(True);
	rio_ModIns_RapidIO_topm._RxPktsLinkIfc._link_rx_data(128'h00000000000000000000000000000000);
end
endrule: rl_4segments_1

rule rl_clock_increment; // rg_clock incremented to pass different sets of data in each clock cycle
 rg_clock <= rg_clock + 1 ;
 $display("-----------------------");
 $display("Clock: %d", rg_clock);
 $display("-----------------------");
endrule: rl_clock_increment

rule rl_finish;// This will stop the simulation
if(rg_clock==30)begin
$finish(0);
end
endrule

endmodule: mkTb_Receiver
endpackage: Tb_Receiver
