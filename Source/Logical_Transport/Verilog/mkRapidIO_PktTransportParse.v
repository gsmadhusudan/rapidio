/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Incoming Packet Parsing Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. After the incoming packets are separated as Header and Data Packets, it is forwarded 
--    to this module for further processing.
-- 2. The Header and Data packets are analyzed and logical layer ftype packets are generated.
-- 3. This module invokes RxFtypeFunctions package to depacketize the incoming packets.
-- 4. Supports both Dev8 and Dev16 fields. 
--
-- To Do's
-- Yet to Complete 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
--	-- Packet Description (Dev8 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:0], 8'h00 }
	Data_2 -> { Data2, 64'h0 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:8] }
	Data_1 -> { Data[7:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63:0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--      -- Packet Description (Dev16 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:8] }
	Data_1 -> { Data1[7:0], Data2 } 
	Data_2 -> { 8'h00, Data3 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:24] }
	Data_1 -> { Data[23:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63::0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--
--        
*/
//
// Generated by Bluespec Compiler, version 2013.05.beta2 (build 31258, 2013-05-21)
//
// On Mon Apr 21 12:27:11 IST 2014
//
// Method conflict info:
// Method: _PktParseRx_SOF_n
// Conflict-free: _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Conflicts: _PktParseRx_SOF_n
//
// Method: _PktParseRx_EOF_n
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Conflicts: _PktParseRx_EOF_n
//
// Method: _PktParseRx_VLD_n
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Conflicts: _PktParseRx_VLD_n
//
// Method: link_rx_rdy_n_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Sequenced after (restricted): _inputs_TxReadyIn_From_Analyze
//
// Method: _PktParseRx_data
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Conflicts: _PktParseRx_data
//
// Method: _PktParseRx_rem
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Conflicts: _PktParseRx_rem
//
// Method: _PktParseRx_crf
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Conflicts: _PktParseRx_crf
//
// Method: _inputs_TxReadyIn_From_Analyze
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
// Sequenced before (restricted): link_rx_rdy_n_
// Conflicts: _inputs_TxReadyIn_From_Analyze
//
// Method: outputs_RxFtype2ReqClass_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype5WriteClass_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype6StreamClass_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype6StreamData_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype8MainReqClass_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype8MaintainData_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype10DoorBellClass_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype11MsgHeader_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype11Data_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype13ResponseClass_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxFtype13ResponseData_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_ReceivedPkts_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_TTReceived_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxDestId_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxSourceId_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_RxPrioField_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
// Method: outputs_MaxPktCount_
// Conflict-free: _PktParseRx_SOF_n,
// 	       _PktParseRx_EOF_n,
// 	       _PktParseRx_VLD_n,
// 	       link_rx_rdy_n_,
// 	       _PktParseRx_data,
// 	       _PktParseRx_rem,
// 	       _PktParseRx_crf,
// 	       _inputs_TxReadyIn_From_Analyze,
// 	       outputs_RxFtype2ReqClass_,
// 	       outputs_RxFtype5WriteClass_,
// 	       outputs_RxFtype6StreamClass_,
// 	       outputs_RxFtype6StreamData_,
// 	       outputs_RxFtype8MainReqClass_,
// 	       outputs_RxFtype8MaintainData_,
// 	       outputs_RxFtype10DoorBellClass_,
// 	       outputs_RxFtype11MsgHeader_,
// 	       outputs_RxFtype11Data_,
// 	       outputs_RxFtype13ResponseClass_,
// 	       outputs_RxFtype13ResponseData_,
// 	       outputs_ReceivedPkts_,
// 	       outputs_TTReceived_,
// 	       outputs_RxDestId_,
// 	       outputs_RxSourceId_,
// 	       outputs_RxPrioField_,
// 	       outputs_MaxPktCount_
//
//
// Ports:
// Name                         I/O  size props
// link_rx_rdy_n_                 O     1
// outputs_RxFtype2ReqClass_      O    71 reg
// outputs_RxFtype5WriteClass_    O   136 reg
// outputs_RxFtype6StreamClass_   O    54 reg
// outputs_RxFtype6StreamData_    O    66 reg
// outputs_RxFtype8MainReqClass_  O   110 reg
// outputs_RxFtype8MaintainData_  O    65 reg
// outputs_RxFtype10DoorBellClass_  O    31 reg
// outputs_RxFtype11MsgHeader_    O    31 reg
// outputs_RxFtype11Data_         O    66 reg
// outputs_RxFtype13ResponseClass_  O    88 reg
// outputs_RxFtype13ResponseData_  O    65 reg
// outputs_ReceivedPkts_          O   261 reg
// outputs_TTReceived_            O     2 reg
// outputs_RxDestId_              O    32 reg
// outputs_RxSourceId_            O    32 reg
// outputs_RxPrioField_           O     2 reg
// outputs_MaxPktCount_           O     4
// CLK                            I     1 clock
// RST_N                          I     1 reset
// _PktParseRx_SOF_n_value        I     1
// _PktParseRx_EOF_n_value        I     1
// _PktParseRx_VLD_n_value        I     1
// _PktParseRx_data_value         I   128
// _PktParseRx_rem_value          I     4
// _PktParseRx_crf_value          I     1 unused
// _inputs_TxReadyIn_From_Analyze_value  I     1
//
// Combinational paths from inputs to outputs:
//   _inputs_TxReadyIn_From_Analyze_value -> link_rx_rdy_n_
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module mkRapidIO_PktTransportParse(CLK,
				   RST_N,

				   _PktParseRx_SOF_n_value,

				   _PktParseRx_EOF_n_value,

				   _PktParseRx_VLD_n_value,

				   link_rx_rdy_n_,

				   _PktParseRx_data_value,

				   _PktParseRx_rem_value,

				   _PktParseRx_crf_value,

				   _inputs_TxReadyIn_From_Analyze_value,

				   outputs_RxFtype2ReqClass_,

				   outputs_RxFtype5WriteClass_,

				   outputs_RxFtype6StreamClass_,

				   outputs_RxFtype6StreamData_,

				   outputs_RxFtype8MainReqClass_,

				   outputs_RxFtype8MaintainData_,

				   outputs_RxFtype10DoorBellClass_,

				   outputs_RxFtype11MsgHeader_,

				   outputs_RxFtype11Data_,

				   outputs_RxFtype13ResponseClass_,

				   outputs_RxFtype13ResponseData_,

				   outputs_ReceivedPkts_,

				   outputs_TTReceived_,

				   outputs_RxDestId_,

				   outputs_RxSourceId_,

				   outputs_RxPrioField_,

				   outputs_MaxPktCount_);
  input  CLK;
  input  RST_N;

  // action method _PktParseRx_SOF_n
  input  _PktParseRx_SOF_n_value;

  // action method _PktParseRx_EOF_n
  input  _PktParseRx_EOF_n_value;

  // action method _PktParseRx_VLD_n
  input  _PktParseRx_VLD_n_value;

  // value method link_rx_rdy_n_
  output link_rx_rdy_n_;

  // action method _PktParseRx_data
  input  [127 : 0] _PktParseRx_data_value;

  // action method _PktParseRx_rem
  input  [3 : 0] _PktParseRx_rem_value;

  // action method _PktParseRx_crf
  input  _PktParseRx_crf_value;

  // action method _inputs_TxReadyIn_From_Analyze
  input  _inputs_TxReadyIn_From_Analyze_value;

  // value method outputs_RxFtype2ReqClass_
  output [70 : 0] outputs_RxFtype2ReqClass_;

  // value method outputs_RxFtype5WriteClass_
  output [135 : 0] outputs_RxFtype5WriteClass_;

  // value method outputs_RxFtype6StreamClass_
  output [53 : 0] outputs_RxFtype6StreamClass_;

  // value method outputs_RxFtype6StreamData_
  output [65 : 0] outputs_RxFtype6StreamData_;

  // value method outputs_RxFtype8MainReqClass_
  output [109 : 0] outputs_RxFtype8MainReqClass_;

  // value method outputs_RxFtype8MaintainData_
  output [64 : 0] outputs_RxFtype8MaintainData_;

  // value method outputs_RxFtype10DoorBellClass_
  output [30 : 0] outputs_RxFtype10DoorBellClass_;

  // value method outputs_RxFtype11MsgHeader_
  output [30 : 0] outputs_RxFtype11MsgHeader_;

  // value method outputs_RxFtype11Data_
  output [65 : 0] outputs_RxFtype11Data_;

  // value method outputs_RxFtype13ResponseClass_
  output [87 : 0] outputs_RxFtype13ResponseClass_;

  // value method outputs_RxFtype13ResponseData_
  output [64 : 0] outputs_RxFtype13ResponseData_;

  // value method outputs_ReceivedPkts_
  output [260 : 0] outputs_ReceivedPkts_;

  // value method outputs_TTReceived_
  output [1 : 0] outputs_TTReceived_;

  // value method outputs_RxDestId_
  output [31 : 0] outputs_RxDestId_;

  // value method outputs_RxSourceId_
  output [31 : 0] outputs_RxSourceId_;

  // value method outputs_RxPrioField_
  output [1 : 0] outputs_RxPrioField_;

  // value method outputs_MaxPktCount_
  output [3 : 0] outputs_MaxPktCount_;

  // signals for module outputs
  wire [260 : 0] outputs_ReceivedPkts_;
  wire [135 : 0] outputs_RxFtype5WriteClass_;
  wire [109 : 0] outputs_RxFtype8MainReqClass_;
  wire [87 : 0] outputs_RxFtype13ResponseClass_;
  wire [70 : 0] outputs_RxFtype2ReqClass_;
  wire [65 : 0] outputs_RxFtype11Data_, outputs_RxFtype6StreamData_;
  wire [64 : 0] outputs_RxFtype13ResponseData_, outputs_RxFtype8MaintainData_;
  wire [53 : 0] outputs_RxFtype6StreamClass_;
  wire [31 : 0] outputs_RxDestId_, outputs_RxSourceId_;
  wire [30 : 0] outputs_RxFtype10DoorBellClass_, outputs_RxFtype11MsgHeader_;
  wire [3 : 0] outputs_MaxPktCount_;
  wire [1 : 0] outputs_RxPrioField_, outputs_TTReceived_;
  wire link_rx_rdy_n_;

  // register rg_DataPkt
  reg [127 : 0] rg_DataPkt;
  wire [127 : 0] rg_DataPkt$D_IN;
  wire rg_DataPkt$EN;

  // register rg_DestIDReceived
  reg [31 : 0] rg_DestIDReceived;
  wire [31 : 0] rg_DestIDReceived$D_IN;
  wire rg_DestIDReceived$EN;

  // register rg_Dev16TempRespData
  reg [7 : 0] rg_Dev16TempRespData;
  wire [7 : 0] rg_Dev16TempRespData$D_IN;
  wire rg_Dev16TempRespData$EN;

  // register rg_Dev8TempRespData
  reg [63 : 0] rg_Dev8TempRespData;
  wire [63 : 0] rg_Dev8TempRespData$D_IN;
  wire rg_Dev8TempRespData$EN;

  // register rg_Ftype10_DoorBellClass
  reg [30 : 0] rg_Ftype10_DoorBellClass;
  wire [30 : 0] rg_Ftype10_DoorBellClass$D_IN;
  wire rg_Ftype10_DoorBellClass$EN;

  // register rg_Ftype11LastPktDev16
  reg rg_Ftype11LastPktDev16;
  wire rg_Ftype11LastPktDev16$D_IN, rg_Ftype11LastPktDev16$EN;

  // register rg_Ftype11PktCount
  reg [3 : 0] rg_Ftype11PktCount;
  wire [3 : 0] rg_Ftype11PktCount$D_IN;
  wire rg_Ftype11PktCount$EN;

  // register rg_Ftype11Tmp_MessageData
  reg [63 : 0] rg_Ftype11Tmp_MessageData;
  reg [63 : 0] rg_Ftype11Tmp_MessageData$D_IN;
  wire rg_Ftype11Tmp_MessageData$EN;

  // register rg_Ftype11_MessageData
  reg [65 : 0] rg_Ftype11_MessageData;
  wire [65 : 0] rg_Ftype11_MessageData$D_IN;
  wire rg_Ftype11_MessageData$EN;

  // register rg_Ftype11_MessageHeader
  reg [30 : 0] rg_Ftype11_MessageHeader;
  wire [30 : 0] rg_Ftype11_MessageHeader$D_IN;
  wire rg_Ftype11_MessageHeader$EN;

  // register rg_Ftype13_ResponseClass
  reg [87 : 0] rg_Ftype13_ResponseClass;
  wire [87 : 0] rg_Ftype13_ResponseClass$D_IN;
  wire rg_Ftype13_ResponseClass$EN;

  // register rg_Ftype2_ReqClass
  reg [70 : 0] rg_Ftype2_ReqClass;
  wire [70 : 0] rg_Ftype2_ReqClass$D_IN;
  wire rg_Ftype2_ReqClass$EN;

  // register rg_Ftype5Addr
  reg [28 : 0] rg_Ftype5Addr;
  wire [28 : 0] rg_Ftype5Addr$D_IN;
  wire rg_Ftype5Addr$EN;

  // register rg_Ftype5TempData
  reg [55 : 0] rg_Ftype5TempData;
  wire [55 : 0] rg_Ftype5TempData$D_IN;
  wire rg_Ftype5TempData$EN;

  // register rg_Ftype5_WrClass
  reg [135 : 0] rg_Ftype5_WrClass;
  wire [135 : 0] rg_Ftype5_WrClass$D_IN;
  wire rg_Ftype5_WrClass$EN;

  // register rg_Ftype6AddrDev16
  reg [28 : 0] rg_Ftype6AddrDev16;
  wire [28 : 0] rg_Ftype6AddrDev16$D_IN;
  wire rg_Ftype6AddrDev16$EN;

  // register rg_Ftype6LastData
  reg rg_Ftype6LastData;
  wire rg_Ftype6LastData$D_IN, rg_Ftype6LastData$EN;

  // register rg_Ftype6LastPkt
  reg rg_Ftype6LastPkt;
  wire rg_Ftype6LastPkt$D_IN, rg_Ftype6LastPkt$EN;

  // register rg_Ftype6PktCount
  reg [3 : 0] rg_Ftype6PktCount;
  wire [3 : 0] rg_Ftype6PktCount$D_IN;
  wire rg_Ftype6PktCount$EN;

  // register rg_Ftype6XAMSBSDev16
  reg [1 : 0] rg_Ftype6XAMSBSDev16;
  wire [1 : 0] rg_Ftype6XAMSBSDev16$D_IN;
  wire rg_Ftype6XAMSBSDev16$EN;

  // register rg_Ftype6_StreamWrClass
  reg [53 : 0] rg_Ftype6_StreamWrClass;
  wire [53 : 0] rg_Ftype6_StreamWrClass$D_IN;
  wire rg_Ftype6_StreamWrClass$EN;

  // register rg_Ftype8_MaintenanceClass
  reg [109 : 0] rg_Ftype8_MaintenanceClass;
  wire [109 : 0] rg_Ftype8_MaintenanceClass$D_IN;
  wire rg_Ftype8_MaintenanceClass$EN;

  // register rg_HeaderPkt
  reg [127 : 0] rg_HeaderPkt;
  wire [127 : 0] rg_HeaderPkt$D_IN;
  wire rg_HeaderPkt$EN;

  // register rg_HeaderPktFtype11
  reg [127 : 0] rg_HeaderPktFtype11;
  wire [127 : 0] rg_HeaderPktFtype11$D_IN;
  wire rg_HeaderPktFtype11$EN;

  // register rg_HeaderPktFtype6
  reg [127 : 0] rg_HeaderPktFtype6;
  wire [127 : 0] rg_HeaderPktFtype6$D_IN;
  wire rg_HeaderPktFtype6$EN;

  // register rg_HopCountReceived
  reg [7 : 0] rg_HopCountReceived;
  wire [7 : 0] rg_HopCountReceived$D_IN;
  wire rg_HopCountReceived$EN;

  // register rg_LastPkt
  reg rg_LastPkt;
  wire rg_LastPkt$D_IN, rg_LastPkt$EN;

  // register rg_MaintenanceWrData
  reg [64 : 0] rg_MaintenanceWrData;
  wire [64 : 0] rg_MaintenanceWrData$D_IN;
  wire rg_MaintenanceWrData$EN;

  // register rg_PktCount
  reg [3 : 0] rg_PktCount;
  wire [3 : 0] rg_PktCount$D_IN;
  wire rg_PktCount$EN;

  // register rg_PrioReceived
  reg [1 : 0] rg_PrioReceived;
  wire [1 : 0] rg_PrioReceived$D_IN;
  wire rg_PrioReceived$EN;

  // register rg_RespClassData
  reg [64 : 0] rg_RespClassData;
  wire [64 : 0] rg_RespClassData$D_IN;
  wire rg_RespClassData$EN;

  // register rg_RxRem
  reg [4 : 0] rg_RxRem;
  wire [4 : 0] rg_RxRem$D_IN;
  wire rg_RxRem$EN;

  // register rg_SrcIDReceived
  reg [31 : 0] rg_SrcIDReceived;
  wire [31 : 0] rg_SrcIDReceived$D_IN;
  wire rg_SrcIDReceived$EN;

  // register rg_StreamData
  reg [65 : 0] rg_StreamData;
  wire [65 : 0] rg_StreamData$D_IN;
  wire rg_StreamData$EN;

  // register rg_TTReceived
  reg [1 : 0] rg_TTReceived;
  wire [1 : 0] rg_TTReceived$D_IN;
  wire rg_TTReceived$EN;

  // register rg_TmpStreamDataDev16
  reg [63 : 0] rg_TmpStreamDataDev16;
  wire [63 : 0] rg_TmpStreamDataDev16$D_IN;
  wire rg_TmpStreamDataDev16$EN;

  // register rg_TmpStreamDataDev8
  reg [63 : 0] rg_TmpStreamDataDev8;
  reg [63 : 0] rg_TmpStreamDataDev8$D_IN;
  wire rg_TmpStreamDataDev8$EN;

  // register rg_UpperBitOffset
  reg [15 : 0] rg_UpperBitOffset;
  wire [15 : 0] rg_UpperBitOffset$D_IN;
  wire rg_UpperBitOffset$EN;

  // ports of submodule pkt_Separation
  wire [127 : 0] pkt_Separation$_inputs_DataPkt_pkt,
		 pkt_Separation$outputs_DataPkt_,
		 pkt_Separation$outputs_HeaderPkt_;
  wire [3 : 0] pkt_Separation$outputs_MaxPktCount_,
	       pkt_Separation$outputs_PktCount_;
  wire pkt_Separation$_inputs_EOF_value,
       pkt_Separation$_inputs_SOF_value,
       pkt_Separation$_inputs_VLD_value,
       pkt_Separation$outputs_LastPkt_;

  // rule scheduling signals
  wire CAN_FIRE_RL_rl_FromIncomingPktSeparation,
       CAN_FIRE_RL_rl_Ftype10DoorBellFormat,
       CAN_FIRE_RL_rl_Ftype11RequestFormat,
       CAN_FIRE_RL_rl_Ftype13ResponseFormat,
       CAN_FIRE_RL_rl_Ftype2RequestFormat,
       CAN_FIRE_RL_rl_Ftype5WriteClassFormat,
       CAN_FIRE_RL_rl_Ftype6StreamWriteFormat,
       CAN_FIRE_RL_rl_Ftype8MaintenanceRequestFormat,
       CAN_FIRE_RL_rl_HeaderDecode_IncomingPkt,
       CAN_FIRE_RL_rl_HopCountDecode,
       CAN_FIRE_RL_rl_RxRemValid,
       CAN_FIRE__PktParseRx_EOF_n,
       CAN_FIRE__PktParseRx_SOF_n,
       CAN_FIRE__PktParseRx_VLD_n,
       CAN_FIRE__PktParseRx_crf,
       CAN_FIRE__PktParseRx_data,
       CAN_FIRE__PktParseRx_rem,
       CAN_FIRE__inputs_TxReadyIn_From_Analyze,
       WILL_FIRE_RL_rl_FromIncomingPktSeparation,
       WILL_FIRE_RL_rl_Ftype10DoorBellFormat,
       WILL_FIRE_RL_rl_Ftype11RequestFormat,
       WILL_FIRE_RL_rl_Ftype13ResponseFormat,
       WILL_FIRE_RL_rl_Ftype2RequestFormat,
       WILL_FIRE_RL_rl_Ftype5WriteClassFormat,
       WILL_FIRE_RL_rl_Ftype6StreamWriteFormat,
       WILL_FIRE_RL_rl_Ftype8MaintenanceRequestFormat,
       WILL_FIRE_RL_rl_HeaderDecode_IncomingPkt,
       WILL_FIRE_RL_rl_HopCountDecode,
       WILL_FIRE_RL_rl_RxRemValid,
       WILL_FIRE__PktParseRx_EOF_n,
       WILL_FIRE__PktParseRx_SOF_n,
       WILL_FIRE__PktParseRx_VLD_n,
       WILL_FIRE__PktParseRx_crf,
       WILL_FIRE__PktParseRx_data,
       WILL_FIRE__PktParseRx_rem,
       WILL_FIRE__inputs_TxReadyIn_From_Analyze;

  // remaining internal signals
  reg [63 : 0] CASE_pkt_Separationoutputs_PktCount_0_0_1_0_2_ETC__q4,
	       v__h12211,
	       v__h5016,
	       v__h5250;
  reg [31 : 0] CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q1,
	       CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q3;
  reg [29 : 0] CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q2;
  reg [20 : 0] config_offset__h7053, config_offset__h8789;
  reg [7 : 0] tgtTID__h13492, tgtTID__h14417, tranID__h7052, tranID__h8788;
  reg [3 : 0] x__h13551,
	      x__h13607,
	      x__h13663,
	      x__h14474,
	      x__h14530,
	      x__h14586,
	      x__h7166,
	      x__h7275,
	      x__h7384,
	      x__h8899,
	      x__h9007,
	      x__h9115;
  reg [1 : 0] tt__h13488, tt__h14413, tt__h7048, tt__h8784;
  wire [128 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d110;
  wire [106 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d278;
  wire [98 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d277;
  wire [87 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d431;
  wire [86 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d428,
		IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d429;
  wire [80 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d417;
  wire [67 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d108;
  wire [64 : 0] IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d297,
		IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d438,
		IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d351,
		IF_rg_Ftype11PktCount_17_EQ_0_18_THEN_DONTCARE_ETC___d363,
		IF_rg_HeaderPktFtype6_15_BITS_115_TO_112_16_EQ_ETC___d155,
		pkt_Separation_outputs_LastPkt_CONCAT_IF_IF_wr_ETC___d350,
		rg_Ftype11LastPktDev16_53_CONCAT_IF_rg_Ftype11_ETC___d362,
		rg_Ftype6LastPkt_36_CONCAT_IF_rg_Ftype6PktCoun_ETC___d146;
  wire [63 : 0] IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d295,
		IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d420,
		dataReceived__h3656,
		dataReceived__h3666,
		v__h12271,
		v__h5076,
		v__h5310;
  wire [44 : 0] addr__h3673;
  wire [31 : 0] x__h2367, x__h2411, x__h2498, x__h2531;
  wire [7 : 0] IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d245,
	       IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d413,
	       srcTID__h3672;
  wire [3 : 0] IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221,
	       IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d229,
	       IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d237,
	       IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d409,
	       IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d411,
	       x__h3786,
	       x__h3839;
  wire [1 : 0] IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208,
	       x__h4048;
  wire wdptr__h3674, wdptr__h7054, wdptr__h8790;

  // action method _PktParseRx_SOF_n
  assign CAN_FIRE__PktParseRx_SOF_n = 1'd1 ;
  assign WILL_FIRE__PktParseRx_SOF_n = 1'd1 ;

  // action method _PktParseRx_EOF_n
  assign CAN_FIRE__PktParseRx_EOF_n = 1'd1 ;
  assign WILL_FIRE__PktParseRx_EOF_n = 1'd1 ;

  // action method _PktParseRx_VLD_n
  assign CAN_FIRE__PktParseRx_VLD_n = 1'd1 ;
  assign WILL_FIRE__PktParseRx_VLD_n = 1'd1 ;

  // value method link_rx_rdy_n_
  assign link_rx_rdy_n_ = _inputs_TxReadyIn_From_Analyze_value ;

  // action method _PktParseRx_data
  assign CAN_FIRE__PktParseRx_data = 1'd1 ;
  assign WILL_FIRE__PktParseRx_data = 1'd1 ;

  // action method _PktParseRx_rem
  assign CAN_FIRE__PktParseRx_rem = 1'd1 ;
  assign WILL_FIRE__PktParseRx_rem = 1'd1 ;

  // action method _PktParseRx_crf
  assign CAN_FIRE__PktParseRx_crf = 1'd1 ;
  assign WILL_FIRE__PktParseRx_crf = 1'd1 ;

  // action method _inputs_TxReadyIn_From_Analyze
  assign CAN_FIRE__inputs_TxReadyIn_From_Analyze = 1'd1 ;
  assign WILL_FIRE__inputs_TxReadyIn_From_Analyze = 1'd1 ;

  // value method outputs_RxFtype2ReqClass_
  assign outputs_RxFtype2ReqClass_ = rg_Ftype2_ReqClass ;

  // value method outputs_RxFtype5WriteClass_
  assign outputs_RxFtype5WriteClass_ = rg_Ftype5_WrClass ;

  // value method outputs_RxFtype6StreamClass_
  assign outputs_RxFtype6StreamClass_ = rg_Ftype6_StreamWrClass ;

  // value method outputs_RxFtype6StreamData_
  assign outputs_RxFtype6StreamData_ = rg_StreamData ;

  // value method outputs_RxFtype8MainReqClass_
  assign outputs_RxFtype8MainReqClass_ = rg_Ftype8_MaintenanceClass ;

  // value method outputs_RxFtype8MaintainData_
  assign outputs_RxFtype8MaintainData_ = rg_MaintenanceWrData ;

  // value method outputs_RxFtype10DoorBellClass_
  assign outputs_RxFtype10DoorBellClass_ = rg_Ftype10_DoorBellClass ;

  // value method outputs_RxFtype11MsgHeader_
  assign outputs_RxFtype11MsgHeader_ = rg_Ftype11_MessageHeader ;

  // value method outputs_RxFtype11Data_
  assign outputs_RxFtype11Data_ = rg_Ftype11_MessageData ;

  // value method outputs_RxFtype13ResponseClass_
  assign outputs_RxFtype13ResponseClass_ = rg_Ftype13_ResponseClass ;

  // value method outputs_RxFtype13ResponseData_
  assign outputs_RxFtype13ResponseData_ = rg_RespClassData ;

  // value method outputs_ReceivedPkts_
  assign outputs_ReceivedPkts_ =
	     { rg_HeaderPkt, rg_DataPkt, rg_PktCount, rg_LastPkt } ;

  // value method outputs_TTReceived_
  assign outputs_TTReceived_ = rg_TTReceived ;

  // value method outputs_RxDestId_
  assign outputs_RxDestId_ = rg_DestIDReceived ;

  // value method outputs_RxSourceId_
  assign outputs_RxSourceId_ = rg_SrcIDReceived ;

  // value method outputs_RxPrioField_
  assign outputs_RxPrioField_ = rg_PrioReceived ;

  // value method outputs_MaxPktCount_
  assign outputs_MaxPktCount_ = pkt_Separation$outputs_MaxPktCount_ ;

  // submodule pkt_Separation
  mkRapidIO_InComingPkt_Separation pkt_Separation(.CLK(CLK),
						  .RST_N(RST_N),
						  ._inputs_DataPkt_pkt(pkt_Separation$_inputs_DataPkt_pkt),
						  ._inputs_EOF_value(pkt_Separation$_inputs_EOF_value),
						  ._inputs_SOF_value(pkt_Separation$_inputs_SOF_value),
						  ._inputs_VLD_value(pkt_Separation$_inputs_VLD_value),
						  .outputs_HeaderPkt_(pkt_Separation$outputs_HeaderPkt_),
						  .outputs_DataPkt_(pkt_Separation$outputs_DataPkt_),
						  .outputs_PktCount_(pkt_Separation$outputs_PktCount_),
						  .outputs_LastPkt_(pkt_Separation$outputs_LastPkt_),
						  .outputs_MaxPktCount_(pkt_Separation$outputs_MaxPktCount_));

  // rule RL_rl_FromIncomingPktSeparation
  assign CAN_FIRE_RL_rl_FromIncomingPktSeparation = 1'd1 ;
  assign WILL_FIRE_RL_rl_FromIncomingPktSeparation = 1'd1 ;

  // rule RL_rl_HeaderDecode_IncomingPkt
  assign CAN_FIRE_RL_rl_HeaderDecode_IncomingPkt = 1'd1 ;
  assign WILL_FIRE_RL_rl_HeaderDecode_IncomingPkt = 1'd1 ;

  // rule RL_rl_HopCountDecode
  assign CAN_FIRE_RL_rl_HopCountDecode = 1'd1 ;
  assign WILL_FIRE_RL_rl_HopCountDecode = 1'd1 ;

  // rule RL_rl_RxRemValid
  assign CAN_FIRE_RL_rl_RxRemValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_RxRemValid = 1'd1 ;

  // rule RL_rl_Ftype2RequestFormat
  assign CAN_FIRE_RL_rl_Ftype2RequestFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype2RequestFormat = 1'd1 ;

  // rule RL_rl_Ftype5WriteClassFormat
  assign CAN_FIRE_RL_rl_Ftype5WriteClassFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype5WriteClassFormat = 1'd1 ;

  // rule RL_rl_Ftype6StreamWriteFormat
  assign CAN_FIRE_RL_rl_Ftype6StreamWriteFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype6StreamWriteFormat = 1'd1 ;

  // rule RL_rl_Ftype8MaintenanceRequestFormat
  assign CAN_FIRE_RL_rl_Ftype8MaintenanceRequestFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype8MaintenanceRequestFormat = 1'd1 ;

  // rule RL_rl_Ftype10DoorBellFormat
  assign CAN_FIRE_RL_rl_Ftype10DoorBellFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype10DoorBellFormat = 1'd1 ;

  // rule RL_rl_Ftype11RequestFormat
  assign CAN_FIRE_RL_rl_Ftype11RequestFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype11RequestFormat = 1'd1 ;

  // rule RL_rl_Ftype13ResponseFormat
  assign CAN_FIRE_RL_rl_Ftype13ResponseFormat = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype13ResponseFormat = 1'd1 ;

  // register rg_DataPkt
  assign rg_DataPkt$D_IN = pkt_Separation$outputs_DataPkt_ ;
  assign rg_DataPkt$EN = 1'd1 ;

  // register rg_DestIDReceived
  assign rg_DestIDReceived$D_IN =
	     (pkt_Separation$outputs_PktCount_ == 4'd0) ?
	       32'd0 :
	       CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q1 ;
  assign rg_DestIDReceived$EN = 1'd1 ;

  // register rg_Dev16TempRespData
  assign rg_Dev16TempRespData$D_IN = 8'h0 ;
  assign rg_Dev16TempRespData$EN = 1'b0 ;

  // register rg_Dev8TempRespData
  assign rg_Dev8TempRespData$D_IN = 64'h0 ;
  assign rg_Dev8TempRespData$EN = 1'b0 ;

  // register rg_Ftype10_DoorBellClass
  assign rg_Ftype10_DoorBellClass$D_IN =
	     { pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd10 &&
	       pkt_Separation$outputs_PktCount_ == 4'd1 &&
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0 ||
		pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01),
	       CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q2 } ;
  assign rg_Ftype10_DoorBellClass$EN = 1'd1 ;

  // register rg_Ftype11LastPktDev16
  assign rg_Ftype11LastPktDev16$D_IN =
	     rg_HeaderPktFtype11[115:112] == 4'b1011 &&
	     rg_HeaderPktFtype11[117:116] == 2'b01 &&
	     pkt_Separation$outputs_LastPkt_ ;
  assign rg_Ftype11LastPktDev16$EN =
	     pkt_Separation$outputs_HeaderPkt_[115:112] != 4'b1011 ||
	     pkt_Separation$outputs_HeaderPkt_[117:116] != 2'b0 ;

  // register rg_Ftype11PktCount
  assign rg_Ftype11PktCount$D_IN = pkt_Separation$outputs_PktCount_ ;
  assign rg_Ftype11PktCount$EN = 1'd1 ;

  // register rg_Ftype11Tmp_MessageData
  always@(pkt_Separation$outputs_PktCount_ or pkt_Separation$outputs_DataPkt_)
  begin
    case (pkt_Separation$outputs_PktCount_)
      4'd0, 4'd1: rg_Ftype11Tmp_MessageData$D_IN = 64'd0;
      default: rg_Ftype11Tmp_MessageData$D_IN =
		   pkt_Separation$outputs_DataPkt_[119:56];
    endcase
  end
  assign rg_Ftype11Tmp_MessageData$EN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] != 4'b1011 ||
	      pkt_Separation$outputs_HeaderPkt_[117:116] != 2'b0) &&
	     rg_HeaderPktFtype11[115:112] == 4'b1011 &&
	     rg_HeaderPktFtype11[117:116] == 2'b01 ;

  // register rg_Ftype11_MessageData
  assign rg_Ftype11_MessageData$D_IN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] == 4'b1011 &&
	      pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
	       { pkt_Separation$outputs_PktCount_ != 4'd0,
		 IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d351 } :
	       { rg_HeaderPktFtype11[115:112] == 4'b1011 &&
		 rg_HeaderPktFtype11[117:116] == 2'b01 &&
		 rg_Ftype11PktCount != 4'd0,
		 IF_rg_Ftype11PktCount_17_EQ_0_18_THEN_DONTCARE_ETC___d363 } ;
  assign rg_Ftype11_MessageData$EN = 1'd1 ;

  // register rg_Ftype11_MessageHeader
  assign rg_Ftype11_MessageHeader$D_IN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] == 4'b1011 &&
	      pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
	       { pkt_Separation$outputs_PktCount_ != 4'd0,
		 pkt_Separation$outputs_HeaderPkt_[117:112],
		 pkt_Separation$outputs_HeaderPkt_[95:72] } :
	       { rg_HeaderPktFtype11[115:112] == 4'b1011 &&
		 rg_HeaderPktFtype11[117:116] == 2'b01 &&
		 rg_Ftype11PktCount != 4'd0,
		 pkt_Separation$outputs_HeaderPkt_[117:112],
		 pkt_Separation$outputs_HeaderPkt_[79:56] } ;
  assign rg_Ftype11_MessageHeader$EN =
	     pkt_Separation$outputs_HeaderPkt_[115:112] == 4'b1011 &&
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0 &&
	     (pkt_Separation$outputs_PktCount_ == 4'd0 ||
	      pkt_Separation$outputs_PktCount_ == 4'd1) ||
	     (pkt_Separation$outputs_HeaderPkt_[115:112] != 4'b1011 ||
	      pkt_Separation$outputs_HeaderPkt_[117:116] != 2'b0) &&
	     (rg_Ftype11PktCount == 4'd0 || rg_Ftype11PktCount == 4'd1 ||
	      rg_HeaderPktFtype11[115:112] != 4'b1011 ||
	      rg_HeaderPktFtype11[117:116] != 2'b01) ;

  // register rg_Ftype13_ResponseClass
  assign rg_Ftype13_ResponseClass$D_IN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd13) ?
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d431 :
	       88'h2AAAAAAAAAAAAAAAAAAAAA ;
  assign rg_Ftype13_ResponseClass$EN =
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0 ||
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01 ||
	     pkt_Separation$outputs_HeaderPkt_[115:112] != 4'd13 ;

  // register rg_Ftype2_ReqClass
  assign rg_Ftype2_ReqClass$D_IN =
	     { pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd2 &&
	       pkt_Separation$outputs_PktCount_ == 4'd1,
	       pkt_Separation$outputs_HeaderPkt_[117:112],
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
		 pkt_Separation$outputs_HeaderPkt_[79:16] :
		 pkt_Separation$outputs_HeaderPkt_[95:32] } ;
  assign rg_Ftype2_ReqClass$EN = 1'd1 ;

  // register rg_Ftype5Addr
  assign rg_Ftype5Addr$D_IN = 29'd0 ;
  assign rg_Ftype5Addr$EN =
	     pkt_Separation$outputs_PktCount_ != 4'd2 ||
	     pkt_Separation$outputs_HeaderPkt_[115:112] != 4'd5 ;

  // register rg_Ftype5TempData
  assign rg_Ftype5TempData$D_IN = 56'h0 ;
  assign rg_Ftype5TempData$EN = 1'b0 ;

  // register rg_Ftype5_WrClass
  assign rg_Ftype5_WrClass$D_IN =
	     { pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd5 &&
	       pkt_Separation$outputs_PktCount_ == 4'd2,
	       pkt_Separation$outputs_HeaderPkt_[117:112],
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d110 } ;
  assign rg_Ftype5_WrClass$EN = 1'd1 ;

  // register rg_Ftype6AddrDev16
  assign rg_Ftype6AddrDev16$D_IN = 29'h0 ;
  assign rg_Ftype6AddrDev16$EN = 1'b0 ;

  // register rg_Ftype6LastData
  assign rg_Ftype6LastData$D_IN = 1'd0 ;
  assign rg_Ftype6LastData$EN =
	     (rg_HeaderPktFtype6[115:112] != 4'd6 ||
	      rg_HeaderPktFtype6[117:116] != 2'b0) &&
	     (rg_HeaderPktFtype6[115:112] != 4'd6 ||
	      rg_HeaderPktFtype6[117:116] != 2'b01) ;

  // register rg_Ftype6LastPkt
  assign rg_Ftype6LastPkt$D_IN = pkt_Separation$outputs_LastPkt_ ;
  assign rg_Ftype6LastPkt$EN = 1'd1 ;

  // register rg_Ftype6PktCount
  assign rg_Ftype6PktCount$D_IN = pkt_Separation$outputs_PktCount_ ;
  assign rg_Ftype6PktCount$EN = 1'd1 ;

  // register rg_Ftype6XAMSBSDev16
  assign rg_Ftype6XAMSBSDev16$D_IN = 2'h0 ;
  assign rg_Ftype6XAMSBSDev16$EN = 1'b0 ;

  // register rg_Ftype6_StreamWrClass
  assign rg_Ftype6_StreamWrClass$D_IN =
	     { rg_HeaderPktFtype6[115:112] == 4'd6 &&
	       (rg_HeaderPktFtype6[117:116] == 2'b0 ||
		rg_HeaderPktFtype6[117:116] == 2'b01),
	       (rg_HeaderPktFtype6[115:112] == 4'd6 &&
		rg_HeaderPktFtype6[117:116] == 2'b0) ?
		 { rg_HeaderPktFtype6[117:112],
		   rg_HeaderPktFtype6[95:51],
		   rg_HeaderPktFtype6[49:48] } :
		 { rg_HeaderPktFtype6[117:112],
		   rg_HeaderPktFtype6[79:35],
		   rg_HeaderPktFtype6[33:32] } } ;
  assign rg_Ftype6_StreamWrClass$EN = 1'd1 ;

  // register rg_Ftype8_MaintenanceClass
  assign rg_Ftype8_MaintenanceClass$D_IN =
	     { pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd8 &&
	       ((pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		  pkt_Separation$outputs_PktCount_ == 4'd1 &&
		  (pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd0 ||
		   pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd3) ||
		  pkt_Separation$outputs_PktCount_ == 4'd2 &&
		  (pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd1 ||
		   pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd2) :
		  pkt_Separation$outputs_PktCount_ == 4'd1 &&
		  (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd0 ||
		   pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd3) ||
		  pkt_Separation$outputs_PktCount_ == 4'd2 &&
		  (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd1 ||
		   pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd2)),
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 tt__h7048 :
		 tt__h8784,
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d278 } ;
  assign rg_Ftype8_MaintenanceClass$EN =
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0 ||
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01 ||
	     pkt_Separation$outputs_HeaderPkt_[115:112] != 4'd8 ;

  // register rg_HeaderPkt
  assign rg_HeaderPkt$D_IN = pkt_Separation$outputs_HeaderPkt_ ;
  assign rg_HeaderPkt$EN = 1'd1 ;

  // register rg_HeaderPktFtype11
  assign rg_HeaderPktFtype11$D_IN = pkt_Separation$outputs_HeaderPkt_ ;
  assign rg_HeaderPktFtype11$EN = 1'd1 ;

  // register rg_HeaderPktFtype6
  assign rg_HeaderPktFtype6$D_IN = pkt_Separation$outputs_HeaderPkt_ ;
  assign rg_HeaderPktFtype6$EN = 1'd1 ;

  // register rg_HopCountReceived
  assign rg_HopCountReceived$D_IN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd8 &&
	      pkt_Separation$outputs_PktCount_ != 4'd0) ?
	       pkt_Separation$outputs_HeaderPkt_[79:72] :
	       8'd0 ;
  assign rg_HopCountReceived$EN = 1'd1 ;

  // register rg_LastPkt
  assign rg_LastPkt$D_IN = pkt_Separation$outputs_LastPkt_ ;
  assign rg_LastPkt$EN = 1'd1 ;

  // register rg_MaintenanceWrData
  assign rg_MaintenanceWrData$D_IN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd8) ?
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d297 :
	       65'h0AAAAAAAAAAAAAAAA ;
  assign rg_MaintenanceWrData$EN =
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0 ||
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01 &&
	     (pkt_Separation$outputs_PktCount_ != 4'd1 ||
	      pkt_Separation$outputs_HeaderPkt_[79:76] != 4'd0 &&
	      pkt_Separation$outputs_HeaderPkt_[79:76] != 4'd3) ||
	     pkt_Separation$outputs_HeaderPkt_[115:112] != 4'd8 ;

  // register rg_PktCount
  assign rg_PktCount$D_IN = pkt_Separation$outputs_PktCount_ ;
  assign rg_PktCount$EN = 1'd1 ;

  // register rg_PrioReceived
  assign rg_PrioReceived$D_IN =
	     (pkt_Separation$outputs_PktCount_ == 4'd0) ?
	       2'd0 :
	       pkt_Separation$outputs_HeaderPkt_[119:118] ;
  assign rg_PrioReceived$EN = 1'd1 ;

  // register rg_RespClassData
  assign rg_RespClassData$D_IN =
	     (pkt_Separation$outputs_HeaderPkt_[115:112] == 4'd13) ?
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d438 :
	       65'h0AAAAAAAAAAAAAAAA ;
  assign rg_RespClassData$EN =
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0 ||
	     pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01 ||
	     pkt_Separation$outputs_HeaderPkt_[115:112] != 4'd13 ;

  // register rg_RxRem
  assign rg_RxRem$D_IN = { 1'b0, _PktParseRx_rem_value - 4'd2 } ;
  assign rg_RxRem$EN = 1'd1 ;

  // register rg_SrcIDReceived
  assign rg_SrcIDReceived$D_IN =
	     (pkt_Separation$outputs_PktCount_ == 4'd0) ?
	       32'd0 :
	       CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q3 ;
  assign rg_SrcIDReceived$EN = 1'd1 ;

  // register rg_StreamData
  assign rg_StreamData$D_IN =
	     { rg_HeaderPktFtype6[115:112] == 4'd6 &&
	       (rg_HeaderPktFtype6[117:116] == 2'b0 ||
		rg_HeaderPktFtype6[117:116] == 2'b01),
	       (rg_HeaderPktFtype6[115:112] == 4'd6 &&
		rg_HeaderPktFtype6[117:116] == 2'b0) ?
		 rg_Ftype6LastPkt_36_CONCAT_IF_rg_Ftype6PktCoun_ETC___d146 :
		 IF_rg_HeaderPktFtype6_15_BITS_115_TO_112_16_EQ_ETC___d155 } ;
  assign rg_StreamData$EN =
	     rg_HeaderPktFtype6[115:112] == 4'd6 &&
	     rg_HeaderPktFtype6[117:116] == 2'b0 &&
	     rg_Ftype6PktCount != 4'd0 ||
	     (rg_HeaderPktFtype6[115:112] != 4'd6 ||
	      rg_HeaderPktFtype6[117:116] != 2'b0) &&
	     (rg_Ftype6PktCount != 4'd0 ||
	      rg_HeaderPktFtype6[115:112] != 4'd6 ||
	      rg_HeaderPktFtype6[117:116] != 2'b01) ;

  // register rg_TTReceived
  assign rg_TTReceived$D_IN =
	     (pkt_Separation$outputs_PktCount_ == 4'd0) ?
	       2'd0 :
	       pkt_Separation$outputs_HeaderPkt_[117:116] ;
  assign rg_TTReceived$EN = 1'd1 ;

  // register rg_TmpStreamDataDev16
  assign rg_TmpStreamDataDev16$D_IN =
	     (rg_HeaderPktFtype6[115:112] == 4'd6 &&
	      rg_HeaderPktFtype6[117:116] == 2'b01) ?
	       CASE_pkt_Separationoutputs_PktCount_0_0_1_0_2_ETC__q4 :
	       64'd0 ;
  assign rg_TmpStreamDataDev16$EN =
	     rg_HeaderPktFtype6[115:112] != 4'd6 ||
	     rg_HeaderPktFtype6[117:116] != 2'b0 ;

  // register rg_TmpStreamDataDev8
  always@(pkt_Separation$outputs_PktCount_ or pkt_Separation$outputs_DataPkt_)
  begin
    case (pkt_Separation$outputs_PktCount_)
      4'd0, 4'd1: rg_TmpStreamDataDev8$D_IN = 64'd0;
      4'd2:
	  rg_TmpStreamDataDev8$D_IN = pkt_Separation$outputs_DataPkt_[111:48];
      default: rg_TmpStreamDataDev8$D_IN =
		   pkt_Separation$outputs_DataPkt_[127:64];
    endcase
  end
  assign rg_TmpStreamDataDev8$EN =
	     rg_HeaderPktFtype6[115:112] == 4'd6 &&
	     rg_HeaderPktFtype6[117:116] == 2'b0 ;

  // register rg_UpperBitOffset
  assign rg_UpperBitOffset$D_IN = 16'h0 ;
  assign rg_UpperBitOffset$EN = 1'b0 ;

  // submodule pkt_Separation
  assign pkt_Separation$_inputs_DataPkt_pkt = _PktParseRx_data_value ;
  assign pkt_Separation$_inputs_EOF_value = !_PktParseRx_EOF_n_value ;
  assign pkt_Separation$_inputs_SOF_value = !_PktParseRx_SOF_n_value ;
  assign pkt_Separation$_inputs_VLD_value = !_PktParseRx_VLD_n_value ;

  // remaining internal signals
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d108 =
	     { wdptr__h3674,
	       x__h4048,
	       1'd1,
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
		 dataReceived__h3656 :
		 dataReceived__h3666 } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d110 =
	     { x__h3786,
	       x__h3839,
	       srcTID__h3672,
	       addr__h3673,
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d108 } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d277 =
	     { (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 x__h7384 :
		 x__h9115,
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 tranID__h7052 :
		 tranID__h8788,
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 config_offset__h7053 :
		 config_offset__h8789,
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 wdptr__h7054 :
		 wdptr__h8790,
	       65'h0AAAAAAAAAAAAAAAA } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d278 =
	     { (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 x__h7166 :
		 x__h8899,
	       (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
		 x__h7275 :
		 x__h9007,
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d277 } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d297 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
	       { pkt_Separation$outputs_PktCount_ == 4'd2 &&
		 (pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd1 ||
		  pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd2),
		 pkt_Separation$outputs_HeaderPkt_[47:0],
		 pkt_Separation$outputs_DataPkt_[127:112] } :
	       { pkt_Separation$outputs_PktCount_ == 4'd2 &&
		 (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd1 ||
		  pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd2),
		 IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d295 } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d417 =
	     { x__h14530,
	       x__h14586,
	       tgtTID__h14417,
	       (pkt_Separation$outputs_HeaderPkt_[87:84] == 4'd8) ?
		 65'h10000000000000000 :
		 65'h0AAAAAAAAAAAAAAAA } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d428 =
	     { tt__h14413,
	       x__h14474,
	       x__h14530,
	       x__h14586,
	       tgtTID__h14417,
	       pkt_Separation$outputs_HeaderPkt_[87:84] == 4'd8,
	       IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d420 } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d429 =
	     (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd0 &&
	      pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       { tt__h14413,
		 x__h14474,
		 IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d417 } :
	       IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d428 ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d431 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
	       { (pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd0 ||
		  pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd8) &&
		 pkt_Separation$outputs_PktCount_ == 4'd1,
		 tt__h13488,
		 x__h13551,
		 x__h13607,
		 x__h13663,
		 tgtTID__h13492,
		 pkt_Separation$outputs_HeaderPkt_[95:92] != 4'd0 ||
		 pkt_Separation$outputs_PktCount_ != 4'd1,
		 pkt_Separation$outputs_HeaderPkt_[79:16] } :
	       { (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd0 ||
		  pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd8) &&
		 pkt_Separation$outputs_PktCount_ == 4'd1,
		 IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d429 } ;
  assign IF_IF_wr_HeaderPkt_whas_THEN_wr_HeaderPkt_wget_ETC___d438 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b0) ?
	       { pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd8 &&
		 pkt_Separation$outputs_PktCount_ == 4'd1,
		 pkt_Separation$outputs_HeaderPkt_[79:16] } :
	       { pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd8 &&
		 pkt_Separation$outputs_PktCount_ == 4'd1,
		 IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d420 } ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[117:116] :
	       2'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[115:112] :
	       4'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d229 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[95:92] :
	       4'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d237 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[91:88] :
	       4'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d245 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[87:80] :
	       8'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d295 =
	     (pkt_Separation$outputs_PktCount_ == 4'd2 &&
	      (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd1 ||
	       pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd2)) ?
	       dataReceived__h3666 :
	       { pkt_Separation$outputs_HeaderPkt_[31:0],
		 pkt_Separation$outputs_DataPkt_[127:96] } ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d351 =
	     (pkt_Separation$outputs_PktCount_ == 4'd0) ?
	       { pkt_Separation$outputs_LastPkt_,
		 pkt_Separation$outputs_DataPkt_[127:64] } :
	       pkt_Separation_outputs_LastPkt_CONCAT_IF_IF_wr_ETC___d350 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d409 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[79:76] :
	       4'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d411 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[75:72] :
	       4'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d413 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[71:64] :
	       8'd0 ;
  assign IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d420 =
	     (pkt_Separation$outputs_PktCount_ == 4'd1) ?
	       pkt_Separation$outputs_HeaderPkt_[63:0] :
	       64'd0 ;
  assign IF_rg_Ftype11PktCount_17_EQ_0_18_THEN_DONTCARE_ETC___d363 =
	     (rg_Ftype11PktCount == 4'd0) ?
	       { rg_Ftype11LastPktDev16, rg_Ftype11Tmp_MessageData } :
	       rg_Ftype11LastPktDev16_53_CONCAT_IF_rg_Ftype11_ETC___d362 ;
  assign IF_rg_HeaderPktFtype6_15_BITS_115_TO_112_16_EQ_ETC___d155 =
	     { rg_Ftype6LastPkt,
	       (rg_Ftype6PktCount == 4'd1) ?
		 v__h5250 :
		 rg_TmpStreamDataDev16 } ;
  assign addr__h3673 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
	       pkt_Separation$outputs_HeaderPkt_[63:19] :
	       pkt_Separation$outputs_HeaderPkt_[79:35] ;
  assign dataReceived__h3656 =
	     { pkt_Separation$outputs_HeaderPkt_[15:0],
	       pkt_Separation$outputs_DataPkt_[127:80] } ;
  assign dataReceived__h3666 =
	     { pkt_Separation$outputs_HeaderPkt_[31:0],
	       pkt_Separation$outputs_DataPkt_[127:96] } ;
  assign pkt_Separation_outputs_LastPkt_CONCAT_IF_IF_wr_ETC___d350 =
	     { pkt_Separation$outputs_LastPkt_,
	       (pkt_Separation$outputs_PktCount_ == 4'd1) ?
		 pkt_Separation$outputs_HeaderPkt_[71:8] :
		 pkt_Separation$outputs_DataPkt_[127:64] } ;
  assign rg_Ftype11LastPktDev16_53_CONCAT_IF_rg_Ftype11_ETC___d362 =
	     { rg_Ftype11LastPktDev16,
	       (rg_Ftype11PktCount == 4'd1) ?
		 v__h12211 :
		 rg_Ftype11Tmp_MessageData } ;
  assign rg_Ftype6LastPkt_36_CONCAT_IF_rg_Ftype6PktCoun_ETC___d146 =
	     { rg_Ftype6LastPkt,
	       (rg_Ftype6PktCount == 4'd1) ?
		 v__h5016 :
		 rg_TmpStreamDataDev8 } ;
  assign srcTID__h3672 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
	       pkt_Separation$outputs_HeaderPkt_[71:64] :
	       pkt_Separation$outputs_HeaderPkt_[87:80] ;
  assign v__h12271 =
	     { rg_HeaderPktFtype11[55:0],
	       pkt_Separation$outputs_DataPkt_[127:120] } ;
  assign v__h5076 =
	     { rg_HeaderPktFtype6[47:0],
	       pkt_Separation$outputs_DataPkt_[127:112] } ;
  assign v__h5310 =
	     { rg_HeaderPktFtype6[31:0],
	       pkt_Separation$outputs_DataPkt_[127:96] } ;
  assign wdptr__h3674 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
	       pkt_Separation$outputs_HeaderPkt_[18] :
	       pkt_Separation$outputs_HeaderPkt_[34] ;
  assign wdptr__h7054 =
	     (pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd0) ?
	       pkt_Separation$outputs_PktCount_ == 4'd1 &&
	       pkt_Separation$outputs_HeaderPkt_[50] :
	       pkt_Separation$outputs_HeaderPkt_[95:92] == 4'd1 &&
	       pkt_Separation$outputs_HeaderPkt_[50] ;
  assign wdptr__h8790 =
	     (pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd0) ?
	       pkt_Separation$outputs_DataPkt_[34] :
	       pkt_Separation$outputs_HeaderPkt_[79:76] == 4'd1 &&
	       pkt_Separation$outputs_DataPkt_[34] ;
  assign x__h2367 = { pkt_Separation$outputs_HeaderPkt_[111:104], 24'h0 } ;
  assign x__h2411 = { pkt_Separation$outputs_HeaderPkt_[111:96], 16'd0 } ;
  assign x__h2498 = { pkt_Separation$outputs_HeaderPkt_[103:96], 24'h0 } ;
  assign x__h2531 = { pkt_Separation$outputs_HeaderPkt_[95:80], 16'd0 } ;
  assign x__h3786 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
	       pkt_Separation$outputs_HeaderPkt_[79:76] :
	       pkt_Separation$outputs_HeaderPkt_[95:92] ;
  assign x__h3839 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
	       pkt_Separation$outputs_HeaderPkt_[75:72] :
	       pkt_Separation$outputs_HeaderPkt_[91:88] ;
  assign x__h4048 =
	     (pkt_Separation$outputs_HeaderPkt_[117:116] == 2'b01) ?
	       pkt_Separation$outputs_HeaderPkt_[17:16] :
	       pkt_Separation$outputs_HeaderPkt_[33:32] ;
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  pkt_Separation$outputs_PktCount_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0:
	  config_offset__h7053 =
	      (pkt_Separation$outputs_PktCount_ == 4'd1) ?
		pkt_Separation$outputs_HeaderPkt_[71:51] :
		21'd0;
      4'd1: config_offset__h7053 = pkt_Separation$outputs_HeaderPkt_[71:51];
      default: config_offset__h7053 = 21'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[79:76])
      4'd0, 4'd1, 4'd2, 4'd3:
	  tt__h8784 = pkt_Separation$outputs_HeaderPkt_[117:116];
      default: tt__h8784 = 2'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[79:76])
      4'd0, 4'd1, 4'd2, 4'd3:
	  tranID__h8788 = pkt_Separation$outputs_HeaderPkt_[71:64];
      default: tranID__h8788 = 8'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[79:76])
      4'd0, 4'd1:
	  config_offset__h8789 = pkt_Separation$outputs_HeaderPkt_[55:35];
      default: config_offset__h8789 = 21'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[79:76])
      4'd0, 4'd1, 4'd2, 4'd3:
	  x__h8899 = pkt_Separation$outputs_HeaderPkt_[115:112];
      default: x__h8899 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[79:76])
      4'd0, 4'd1, 4'd2, 4'd3:
	  x__h9007 = pkt_Separation$outputs_HeaderPkt_[79:76];
      default: x__h9007 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[79:76])
      4'd0, 4'd1, 4'd2, 4'd3:
	  x__h9115 = pkt_Separation$outputs_HeaderPkt_[75:72];
      default: x__h9115 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_PktCount_ or v__h5076)
  begin
    case (pkt_Separation$outputs_PktCount_)
      4'd0, 4'd1: v__h5016 = 64'd0;
      4'd2: v__h5016 = v__h5076;
      default: v__h5016 = 64'd0;
    endcase
  end
  always@(pkt_Separation$outputs_PktCount_ or v__h5310)
  begin
    case (pkt_Separation$outputs_PktCount_)
      4'd0, 4'd1: v__h5250 = 64'd0;
      4'd2: v__h5250 = v__h5310;
      default: v__h5250 = 64'd0;
    endcase
  end
  always@(pkt_Separation$outputs_PktCount_ or v__h12271)
  begin
    case (pkt_Separation$outputs_PktCount_)
      4'd0, 4'd1: v__h12211 = 64'd0;
      4'd2: v__h12211 = v__h12271;
      default: v__h12211 = 64'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d229)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0:
	  x__h7275 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d229;
      4'd1, 4'd2, 4'd3: x__h7275 = pkt_Separation$outputs_HeaderPkt_[95:92];
      default: x__h7275 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d229)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0, 4'd8:
	  x__h13607 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d229;
      default: x__h13607 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0:
	  x__h7166 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221;
      4'd1, 4'd2, 4'd3: x__h7166 = pkt_Separation$outputs_HeaderPkt_[115:112];
      default: x__h7166 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0, 4'd8:
	  x__h13551 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221;
      default: x__h13551 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[87:84])
      4'd0, 4'd8:
	  x__h14474 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d221;
      default: x__h14474 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d237)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0:
	  x__h7384 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d237;
      4'd1, 4'd2, 4'd3: x__h7384 = pkt_Separation$outputs_HeaderPkt_[91:88];
      default: x__h7384 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d237)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0, 4'd8:
	  x__h13663 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d237;
      default: x__h13663 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d409)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[87:84])
      4'd0, 4'd8:
	  x__h14530 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d409;
      default: x__h14530 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d411)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[87:84])
      4'd0, 4'd8:
	  x__h14586 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d411;
      default: x__h14586 = 4'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d413)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[87:84])
      4'd0, 4'd8:
	  tgtTID__h14417 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d413;
      default: tgtTID__h14417 = 8'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0:
	  tt__h7048 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208;
      4'd1, 4'd2, 4'd3:
	  tt__h7048 = pkt_Separation$outputs_HeaderPkt_[117:116];
      default: tt__h7048 = 2'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0, 4'd8:
	  tt__h13488 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208;
      default: tt__h13488 = 2'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[87:84])
      4'd0, 4'd8:
	  tt__h14413 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d208;
      default: tt__h14413 = 2'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d245)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0:
	  tranID__h7052 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d245;
      4'd1, 4'd2, 4'd3:
	  tranID__h7052 = pkt_Separation$outputs_HeaderPkt_[87:80];
      default: tranID__h7052 = 8'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or
	  IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d245)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[95:92])
      4'd0, 4'd8:
	  tgtTID__h13492 =
	      IF_IF_wr_PktCount_whas__1_THEN_wr_PktCount_wge_ETC___d245;
      default: tgtTID__h13492 = 8'd0;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or x__h2367 or x__h2411)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[117:116])
      2'b01: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q1 = x__h2411;
      2'b10: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q1 = x__h2367;
      default: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q1 =
		   x__h2367;
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[117:116])
      2'b0:
	  CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q2 =
	      { pkt_Separation$outputs_HeaderPkt_[117:112],
		pkt_Separation$outputs_HeaderPkt_[87:64] };
      2'b01:
	  CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q2 =
	      { pkt_Separation$outputs_HeaderPkt_[117:112],
		pkt_Separation$outputs_HeaderPkt_[71:48] };
      default: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q2 =
		   { pkt_Separation$outputs_HeaderPkt_[117:112],
		     pkt_Separation$outputs_HeaderPkt_[71:48] };
    endcase
  end
  always@(pkt_Separation$outputs_HeaderPkt_ or x__h2498 or x__h2531)
  begin
    case (pkt_Separation$outputs_HeaderPkt_[117:116])
      2'b01: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q3 = x__h2531;
      2'b10: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q3 = x__h2498;
      default: CASE_pkt_Separationoutputs_HeaderPkt_BITS_117_ETC__q3 =
		   x__h2498;
    endcase
  end
  always@(pkt_Separation$outputs_PktCount_ or pkt_Separation$outputs_DataPkt_)
  begin
    case (pkt_Separation$outputs_PktCount_)
      4'd0, 4'd1:
	  CASE_pkt_Separationoutputs_PktCount_0_0_1_0_2_ETC__q4 = 64'd0;
      4'd2:
	  CASE_pkt_Separationoutputs_PktCount_0_0_1_0_2_ETC__q4 =
	      pkt_Separation$outputs_DataPkt_[95:32];
      default: CASE_pkt_Separationoutputs_PktCount_0_0_1_0_2_ETC__q4 =
		   pkt_Separation$outputs_DataPkt_[127:64];
    endcase
  end

  // handling of inlined registers

  always@(posedge CLK)
  begin
    if (RST_N == `BSV_RESET_VALUE)
      begin
        rg_DataPkt <= `BSV_ASSIGNMENT_DELAY 128'd0;
	rg_DestIDReceived <= `BSV_ASSIGNMENT_DELAY 32'd0;
	rg_Dev16TempRespData <= `BSV_ASSIGNMENT_DELAY 8'd0;
	rg_Dev8TempRespData <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype10_DoorBellClass <= `BSV_ASSIGNMENT_DELAY 31'd715827882;
	rg_Ftype11LastPktDev16 <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype11PktCount <= `BSV_ASSIGNMENT_DELAY 4'd0;
	rg_Ftype11Tmp_MessageData <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype11_MessageData <= `BSV_ASSIGNMENT_DELAY 66'h0AAAAAAAAAAAAAAAA;
	rg_Ftype11_MessageHeader <= `BSV_ASSIGNMENT_DELAY 31'd715827882;
	rg_Ftype13_ResponseClass <= `BSV_ASSIGNMENT_DELAY
	    88'h2AAAAAAAAAAAAAAAAAAAAA;
	rg_Ftype2_ReqClass <= `BSV_ASSIGNMENT_DELAY 71'h2AAAAAAAAAAAAAAAAA;
	rg_Ftype5Addr <= `BSV_ASSIGNMENT_DELAY 29'd0;
	rg_Ftype5TempData <= `BSV_ASSIGNMENT_DELAY 56'd0;
	rg_Ftype5_WrClass <= `BSV_ASSIGNMENT_DELAY
	    136'h2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
	rg_Ftype6AddrDev16 <= `BSV_ASSIGNMENT_DELAY 29'd0;
	rg_Ftype6LastData <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype6LastPkt <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype6PktCount <= `BSV_ASSIGNMENT_DELAY 4'd0;
	rg_Ftype6XAMSBSDev16 <= `BSV_ASSIGNMENT_DELAY 2'd0;
	rg_Ftype6_StreamWrClass <= `BSV_ASSIGNMENT_DELAY 54'h0AAAAAAAAAAAAA;
	rg_Ftype8_MaintenanceClass <= `BSV_ASSIGNMENT_DELAY
	    110'h0AAAAAAAAAAAAAAAAAAAAAAAAAAA;
	rg_HeaderPkt <= `BSV_ASSIGNMENT_DELAY 128'd0;
	rg_HeaderPktFtype11 <= `BSV_ASSIGNMENT_DELAY 128'd0;
	rg_HeaderPktFtype6 <= `BSV_ASSIGNMENT_DELAY 128'd0;
	rg_HopCountReceived <= `BSV_ASSIGNMENT_DELAY 8'd0;
	rg_LastPkt <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_MaintenanceWrData <= `BSV_ASSIGNMENT_DELAY 65'h0AAAAAAAAAAAAAAAA;
	rg_PktCount <= `BSV_ASSIGNMENT_DELAY 4'd0;
	rg_PrioReceived <= `BSV_ASSIGNMENT_DELAY 2'd0;
	rg_RespClassData <= `BSV_ASSIGNMENT_DELAY 65'h0AAAAAAAAAAAAAAAA;
	rg_RxRem <= `BSV_ASSIGNMENT_DELAY 5'd10;
	rg_SrcIDReceived <= `BSV_ASSIGNMENT_DELAY 32'd0;
	rg_StreamData <= `BSV_ASSIGNMENT_DELAY 66'h0AAAAAAAAAAAAAAAA;
	rg_TTReceived <= `BSV_ASSIGNMENT_DELAY 2'd0;
	rg_TmpStreamDataDev16 <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_TmpStreamDataDev8 <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_UpperBitOffset <= `BSV_ASSIGNMENT_DELAY 16'd0;
      end
    else
      begin
        if (rg_DataPkt$EN)
	  rg_DataPkt <= `BSV_ASSIGNMENT_DELAY rg_DataPkt$D_IN;
	if (rg_DestIDReceived$EN)
	  rg_DestIDReceived <= `BSV_ASSIGNMENT_DELAY rg_DestIDReceived$D_IN;
	if (rg_Dev16TempRespData$EN)
	  rg_Dev16TempRespData <= `BSV_ASSIGNMENT_DELAY
	      rg_Dev16TempRespData$D_IN;
	if (rg_Dev8TempRespData$EN)
	  rg_Dev8TempRespData <= `BSV_ASSIGNMENT_DELAY
	      rg_Dev8TempRespData$D_IN;
	if (rg_Ftype10_DoorBellClass$EN)
	  rg_Ftype10_DoorBellClass <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype10_DoorBellClass$D_IN;
	if (rg_Ftype11LastPktDev16$EN)
	  rg_Ftype11LastPktDev16 <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11LastPktDev16$D_IN;
	if (rg_Ftype11PktCount$EN)
	  rg_Ftype11PktCount <= `BSV_ASSIGNMENT_DELAY rg_Ftype11PktCount$D_IN;
	if (rg_Ftype11Tmp_MessageData$EN)
	  rg_Ftype11Tmp_MessageData <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11Tmp_MessageData$D_IN;
	if (rg_Ftype11_MessageData$EN)
	  rg_Ftype11_MessageData <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11_MessageData$D_IN;
	if (rg_Ftype11_MessageHeader$EN)
	  rg_Ftype11_MessageHeader <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11_MessageHeader$D_IN;
	if (rg_Ftype13_ResponseClass$EN)
	  rg_Ftype13_ResponseClass <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype13_ResponseClass$D_IN;
	if (rg_Ftype2_ReqClass$EN)
	  rg_Ftype2_ReqClass <= `BSV_ASSIGNMENT_DELAY rg_Ftype2_ReqClass$D_IN;
	if (rg_Ftype5Addr$EN)
	  rg_Ftype5Addr <= `BSV_ASSIGNMENT_DELAY rg_Ftype5Addr$D_IN;
	if (rg_Ftype5TempData$EN)
	  rg_Ftype5TempData <= `BSV_ASSIGNMENT_DELAY rg_Ftype5TempData$D_IN;
	if (rg_Ftype5_WrClass$EN)
	  rg_Ftype5_WrClass <= `BSV_ASSIGNMENT_DELAY rg_Ftype5_WrClass$D_IN;
	if (rg_Ftype6AddrDev16$EN)
	  rg_Ftype6AddrDev16 <= `BSV_ASSIGNMENT_DELAY rg_Ftype6AddrDev16$D_IN;
	if (rg_Ftype6LastData$EN)
	  rg_Ftype6LastData <= `BSV_ASSIGNMENT_DELAY rg_Ftype6LastData$D_IN;
	if (rg_Ftype6LastPkt$EN)
	  rg_Ftype6LastPkt <= `BSV_ASSIGNMENT_DELAY rg_Ftype6LastPkt$D_IN;
	if (rg_Ftype6PktCount$EN)
	  rg_Ftype6PktCount <= `BSV_ASSIGNMENT_DELAY rg_Ftype6PktCount$D_IN;
	if (rg_Ftype6XAMSBSDev16$EN)
	  rg_Ftype6XAMSBSDev16 <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6XAMSBSDev16$D_IN;
	if (rg_Ftype6_StreamWrClass$EN)
	  rg_Ftype6_StreamWrClass <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6_StreamWrClass$D_IN;
	if (rg_Ftype8_MaintenanceClass$EN)
	  rg_Ftype8_MaintenanceClass <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype8_MaintenanceClass$D_IN;
	if (rg_HeaderPkt$EN)
	  rg_HeaderPkt <= `BSV_ASSIGNMENT_DELAY rg_HeaderPkt$D_IN;
	if (rg_HeaderPktFtype11$EN)
	  rg_HeaderPktFtype11 <= `BSV_ASSIGNMENT_DELAY
	      rg_HeaderPktFtype11$D_IN;
	if (rg_HeaderPktFtype6$EN)
	  rg_HeaderPktFtype6 <= `BSV_ASSIGNMENT_DELAY rg_HeaderPktFtype6$D_IN;
	if (rg_HopCountReceived$EN)
	  rg_HopCountReceived <= `BSV_ASSIGNMENT_DELAY
	      rg_HopCountReceived$D_IN;
	if (rg_LastPkt$EN)
	  rg_LastPkt <= `BSV_ASSIGNMENT_DELAY rg_LastPkt$D_IN;
	if (rg_MaintenanceWrData$EN)
	  rg_MaintenanceWrData <= `BSV_ASSIGNMENT_DELAY
	      rg_MaintenanceWrData$D_IN;
	if (rg_PktCount$EN)
	  rg_PktCount <= `BSV_ASSIGNMENT_DELAY rg_PktCount$D_IN;
	if (rg_PrioReceived$EN)
	  rg_PrioReceived <= `BSV_ASSIGNMENT_DELAY rg_PrioReceived$D_IN;
	if (rg_RespClassData$EN)
	  rg_RespClassData <= `BSV_ASSIGNMENT_DELAY rg_RespClassData$D_IN;
	if (rg_RxRem$EN) rg_RxRem <= `BSV_ASSIGNMENT_DELAY rg_RxRem$D_IN;
	if (rg_SrcIDReceived$EN)
	  rg_SrcIDReceived <= `BSV_ASSIGNMENT_DELAY rg_SrcIDReceived$D_IN;
	if (rg_StreamData$EN)
	  rg_StreamData <= `BSV_ASSIGNMENT_DELAY rg_StreamData$D_IN;
	if (rg_TTReceived$EN)
	  rg_TTReceived <= `BSV_ASSIGNMENT_DELAY rg_TTReceived$D_IN;
	if (rg_TmpStreamDataDev16$EN)
	  rg_TmpStreamDataDev16 <= `BSV_ASSIGNMENT_DELAY
	      rg_TmpStreamDataDev16$D_IN;
	if (rg_TmpStreamDataDev8$EN)
	  rg_TmpStreamDataDev8 <= `BSV_ASSIGNMENT_DELAY
	      rg_TmpStreamDataDev8$D_IN;
	if (rg_UpperBitOffset$EN)
	  rg_UpperBitOffset <= `BSV_ASSIGNMENT_DELAY rg_UpperBitOffset$D_IN;
      end
  end

  // synopsys translate_off
  `ifdef BSV_NO_INITIAL_BLOCKS
  `else // not BSV_NO_INITIAL_BLOCKS
  initial
  begin
    rg_DataPkt = 128'hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_DestIDReceived = 32'hAAAAAAAA;
    rg_Dev16TempRespData = 8'hAA;
    rg_Dev8TempRespData = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype10_DoorBellClass = 31'h2AAAAAAA;
    rg_Ftype11LastPktDev16 = 1'h0;
    rg_Ftype11PktCount = 4'hA;
    rg_Ftype11Tmp_MessageData = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype11_MessageData = 66'h2AAAAAAAAAAAAAAAA;
    rg_Ftype11_MessageHeader = 31'h2AAAAAAA;
    rg_Ftype13_ResponseClass = 88'hAAAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype2_ReqClass = 71'h2AAAAAAAAAAAAAAAAA;
    rg_Ftype5Addr = 29'h0AAAAAAA;
    rg_Ftype5TempData = 56'hAAAAAAAAAAAAAA;
    rg_Ftype5_WrClass = 136'hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype6AddrDev16 = 29'h0AAAAAAA;
    rg_Ftype6LastData = 1'h0;
    rg_Ftype6LastPkt = 1'h0;
    rg_Ftype6PktCount = 4'hA;
    rg_Ftype6XAMSBSDev16 = 2'h2;
    rg_Ftype6_StreamWrClass = 54'h2AAAAAAAAAAAAA;
    rg_Ftype8_MaintenanceClass = 110'h2AAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_HeaderPkt = 128'hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_HeaderPktFtype11 = 128'hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_HeaderPktFtype6 = 128'hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_HopCountReceived = 8'hAA;
    rg_LastPkt = 1'h0;
    rg_MaintenanceWrData = 65'h0AAAAAAAAAAAAAAAA;
    rg_PktCount = 4'hA;
    rg_PrioReceived = 2'h2;
    rg_RespClassData = 65'h0AAAAAAAAAAAAAAAA;
    rg_RxRem = 5'h0A;
    rg_SrcIDReceived = 32'hAAAAAAAA;
    rg_StreamData = 66'h2AAAAAAAAAAAAAAAA;
    rg_TTReceived = 2'h2;
    rg_TmpStreamDataDev16 = 64'hAAAAAAAAAAAAAAAA;
    rg_TmpStreamDataDev8 = 64'hAAAAAAAAAAAAAAAA;
    rg_UpperBitOffset = 16'hAAAA;
  end
  `endif // BSV_NO_INITIAL_BLOCKS
  // synopsys translate_on

  // handling of system tasks

  // synopsys translate_off
  always@(negedge CLK)
  begin
    #0;
    if (RST_N != `BSV_RESET_VALUE)
      if (rg_HeaderPktFtype6[115:112] == 4'd6 &&
	  rg_HeaderPktFtype6[117:116] == 2'b0 &&
	  rg_Ftype6PktCount != 4'd0 &&
	  rg_Ftype6PktCount != 4'd1)
	$display("FType 6 Data == %h", rg_TmpStreamDataDev8);
    if (RST_N != `BSV_RESET_VALUE)
      if (rg_HeaderPktFtype6[115:112] == 4'd6 &&
	  rg_HeaderPktFtype6[117:116] == 2'b01 &&
	  rg_Ftype6PktCount != 4'd0 &&
	  rg_Ftype6PktCount != 4'd1)
	$display("FType 6 Data == %h", rg_TmpStreamDataDev16);
  end
  // synopsys translate_on
endmodule  // mkRapidIO_PktTransportParse

