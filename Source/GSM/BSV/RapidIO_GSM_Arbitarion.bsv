/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO GSM CPU Arbitration Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module contains, 
-- 
-- To Do's
--
-- Author(s):
-- Chidhambaranathan, Anshu Kumar (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
 --------------------------------------------------------------------------------------------------------------------------------------------------------
*/
      
package RapidIO_GSM_Arbitarion;

import RapidIO_DTypes ::*;
import RapidIO_Top_Wrapper ::*;
import DefaultValue ::*;
//import RapidIO_Cache_Coherence_GSM ::*;

`define PROC0 16'ha0
`define PROC1 16'hb0
`define PROC2 16'hc0
`define PROC3 16'hd0

interface Ifc_RapidIO_GSM_Arbitarion;
// Transmit Interface from all 4 cores acts as Input to Arbiter
    interface Ifc_LogicalLayerTxInterfaces _Proc0TxIfc;
    interface Ifc_LogicalLayerTxInterfaces _Proc1TxIfc;
    interface Ifc_LogicalLayerTxInterfaces _Proc2TxIfc;
    interface Ifc_LogicalLayerTxInterfaces _Proc3TxIfc;

// Receive Interface Signals to all 4 cores acts as Output from Arbiter    
    interface Ifc_LogicalLayerRxInterfaces _Proc0RxIfc;
    interface Ifc_LogicalLayerRxInterfaces _Proc1RxIfc;
    interface Ifc_LogicalLayerRxInterfaces _Proc2RxIfc;
    interface Ifc_LogicalLayerRxInterfaces _Proc3RxIfc;

endinterface : Ifc_RapidIO_GSM_Arbitarion

(* synthesize *)
(* always_enabled *)
(* always_ready *)    
module mkRapidIO_GSM_Arbitarion (Ifc_RapidIO_GSM_Arbitarion);

Wire#(Bool) wr_logicalTxProc0_SOF_n <-mkDWire(True);
Wire#(Bool) wr_logicalTxProc0_EOF_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc0_VLD_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc0_DSC_n <- mkDWire(True);

Wire#(Bool) wr_logicalTxProc0_RDY_n <- mkDWire(True);
Wire#(DataPkt) wr_logicalTxProc0_DATA <- mkDWire(defaultValue); 
Wire#(Bit#(4)) wr_logicalTxProc0_REM  <- mkDWire(0);
Wire#(Bool)   wr_logicalTxProc0_CRF  <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc0_RESPONSE <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc0_Resp_Only <- mkDWire(False);

// Processor 1 Wire Declarations //
Wire#(Bool) wr_logicalTxProc1_SOF_n <-mkDWire(True);
Wire#(Bool) wr_logicalTxProc1_EOF_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc1_VLD_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc1_DSC_n <- mkDWire(True);

Wire#(Bool) wr_logicalTxProc1_RDY_n <- mkDWire(True);
Wire#(DataPkt) wr_logicalTxProc1_DATA <- mkDWire(defaultValue); 
Wire#(Bit#(4)) wr_logicalTxProc1_REM  <- mkDWire(0);
Wire#(Bool)   wr_logicalTxProc1_CRF  <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc1_RESPONSE <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc1_Resp_Only <- mkDWire(False);

//  Processor 2 Wire Declarations //

Wire#(Bool) wr_logicalTxProc2_SOF_n <-mkDWire(True);
Wire#(Bool) wr_logicalTxProc2_EOF_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc2_VLD_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc2_DSC_n <- mkDWire(True);

Wire#(Bool) wr_logicalTxProc2_RDY_n <- mkDWire(True);
Wire#(DataPkt) wr_logicalTxProc2_DATA <- mkDWire(defaultValue); 
Wire#(Bit#(4)) wr_logicalTxProc2_REM  <- mkDWire(0);
Wire#(Bool)   wr_logicalTxProc2_CRF  <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc2_RESPONSE <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc2_Resp_Only <- mkDWire(False);


// Processor 3 Wire Declarations //

Wire#(Bool) wr_logicalTxProc3_SOF_n <-mkDWire(True);
Wire#(Bool) wr_logicalTxProc3_EOF_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc3_VLD_n <- mkDWire(True);
Wire#(Bool) wr_logicalTxProc3_DSC_n <- mkDWire(True);

Wire#(Bool) wr_logicalTxProc3_RDY_n <- mkDWire(True);
Wire#(DataPkt) wr_logicalTxProc3_DATA <- mkDWire(defaultValue); 
Wire#(Bit#(4)) wr_logicalTxProc3_REM  <- mkDWire(0);
Wire#(Bool)   wr_logicalTxProc3_CRF  <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc3_RESPONSE <- mkDWire(False);
Wire#(Bool)   wr_logicalTxProc3_Resp_Only <- mkDWire(False);
  

// Interface for  1st Processor //
interface Ifc_LogicalLayerTxInterfaces _Proc0TxIfc;

 method Action _LOGLayerTx_SOF_n (Bool value);
 
 	wr_logicalTxProc0_SOF_n <= value;
 	
 endmethod
 method Action _LOGLayerTxProc_EOF_n (Bool value);
 
 	wr_logicalTxProc0_EOF_n <= value;
 endmethod
 method Action _LOGLayerTx_VLD_n (Bool value);
 
 	wr_logicalTxProc0_VLD_n <= value; 
 endmethod
 method Action _LOGLayerTx_DSC_n (Bool value);
 
 	wr_logicalTxProc0_DSC_n <= value;
 endmethod

 method Bool _LOGLayerTx_RDY_n_ ();
 
 	return wr_logicalTxProc0_RDY_n;
 endmethod

 method Action _LOGLayerTx_DATA (DataPkt value);
 
 	wr_logicalTxProc0_DATA <= value;
 endmethod
 method Action _LOGLayerTx_REM (Bit(4) value);
 
 	wr_logicalTxProc0_REM <= value;
 endmethod
 method Action _LOGLayerTx_CRF (Bool value);
 
 	wr_logicalTxProc0_CRF <= value;
 endmethod
 method Action _LOGLayerTx_RESPONSE (Bool value);
 
 	wr_logicalTxProc0_RESPONSE <= value;
 	
 endmethod
 method Bool _LOGLayerTx_Resp_Only_ (); 
 
 	return wr_logicalTxProc0_Resp_Only;
 endmethod

endinterface 

// Interface for 2nd Processor //
interface Ifc_LogicalLayerTxInterfaces _Proc1TxIfc;

 method Action _LOGLayerTx_SOF_n (Bool value);
 
 	wr_logicalTxProc1_SOF_n <= value;
 	
 endmethod
 method Action _LOGLayerTx_EOF_n (Bool value);
 
 	wr_logicalTxProc1_EOF_n <= value;
 endmethod
 method Action _LOGLayerTx_VLD_n (Bool value);
 
 	wr_logicalTxProc1_VLD_n <= value; 
 endmethod
 method Action _LOGLayerTx_DSC_n (Bool value);
 
 	wr_logicalTxProc1_DSC_n <= value;
 endmethod

 method Bool _LOGLayerTx_RDY_n_ ();
 
 	return wr_logicalTxProc1_RDY_n;
 endmethod

 method Action _LOGLayerTx_DATA (DataPkt value);
 
 	wr_logicalTxProc1_DATA <= value;
 endmethod
 method Action _LOGLayerTx_REM (Bit(4) value);
 
 	wr_logicalTxProc1_REM <= value;
 endmethod
 method Action _LOGLayerTx_CRF (Bool value);
 
 	wr_logicalTxProc1_CRF <= value;
 endmethod
 method Action _LOGLayerTx_RESPONSE (Bool value);
 
 	wr_logicalTxProc1_RESPONSE <= value;
 	
 endmethod
 method Bool _LOGLayerTx_Resp_Only_ (); 
 
 	return wr_logicalTxProc1_Resp_Only;
 endmethod

endinterface 


//  Interface for 3rd Processor // 
interface Ifc_LogicalLayerTxInterfaces _Proc2TxIfc;

 method Action _LOGLayerTx_SOF_n (Bool value);
 
 	wr_logicalTxProc2_SOF_n <= value;
 	
 endmethod
 method Action _LOGLayerTx_EOF_n (Bool value);
 
 	wr_logicalTxProc2_EOF_n <= value;
 endmethod
 method Action _LOGLayerTx_VLD_n (Bool value);
 
 	wr_logicalTxProc2_VLD_n <= value; 
 endmethod
 method Action _LOGLayerTx_DSC_n (Bool value);
 
 	wr_logicalTxProc2_DSC_n <= value;
 endmethod

 method Bool _LOGLayerTx_RDY_n_ ();
 
 	return wr_logicalTxProc2_RDY_n;
 endmethod

 method Action _LOGLayerTx_DATA (DataPkt value);
 
 	wr_logicalTxProc2_DATA <= value;
 endmethod
 method Action _LOGLayerTx_REM (Bit(4) value);
 
 	wr_logicalTxProc2_REM <= value;
 endmethod
 method Action _LOGLayerTx_CRF (Bool value);
 
 	wr_logicalTxProc2_CRF <= value;
 endmethod
 method Action _LOGLayerTx_RESPONSE (Bool value);
 
 	wr_logicalTxProc2_RESPONSE <= value;
 	
 endmethod
 method Bool _LOGLayerTx_Resp_Only_ (); 
 
 	return wr_logicalTxProc2_Resp_Only;
 endmethod

endinterface 

//  Interface For 4th Processor //
interface Ifc_LogicalLayerTxInterfaces _Proc3TxIfc;

 method Action _LOGLayerTx_SOF_n (Bool value);
 
 	wr_logicalTxProc3_SOF_n <= value;
 	
 endmethod
 method Action _LOGLayerTx_EOF_n (Bool value);
 
 	wr_logicalTxProc3_EOF_n <= value;
 endmethod
 method Action _LOGLayerTx_VLD_n (Bool value);
 
 	wr_logicalTxProc3_VLD_n <= value; 
 endmethod
 method Action _LOGLayerTx_DSC_n (Bool value);
 
 	wr_logicalTxProc3_DSC_n <= value;
 endmethod

 method Bool _LOGLayerTx_RDY_n_ ();
 
 	return wr_logicalTxProc3_RDY_n;
 endmethod

 method Action _LOGLayerTx_DATA (DataPkt value);
 
 	wr_logicalTxProc3_DATA <= value;
 endmethod
 method Action _LOGLayerTx_REM (Bit(4) value);
 
 	wr_logicalTxProc3_REM <= value;
 endmethod
 method Action _LOGLayerTx_CRF (Bool value);
 
 	wr_logicalTxProc3_CRF <= value;
 endmethod
 method Action _LOGLayerTx_RESPONSE (Bool value);
 
 	wr_logicalTxProc3_RESPONSE <= value;
 	
 endmethod
 method Bool _LOGLayerTx_Resp_Only_ (); 
 
 	return wr_logicalTxProc3_Resp_Only;
 endmethod

endinterface 







endmodule : mkRapidIO_GSM_Arbitarion

endpackage : RapidIO_GSM_Arbitarion
