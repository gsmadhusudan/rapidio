/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Receive Ftype Functions Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This mkProcessor_Agent Module defines four rules that handle, Local request, Local Response, Remote Request, Remote Response.
-- Local Request/Response means packet being received/transmitted to processor(L2ControlInterface).
-- Remote Request/Response means packet being  received/transmitted to/from RapidIO Interconnect.
-- 1. This modules implements cache coherence.
-- 2. 
-- 
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/







package RapidIO_Cache_Coherence_Home_Node;


import RapidIO_Cache_Coherence_Types::*;
import FIFO::*;
import RapidIO_Cache_Coherence_Encode_packet::*;
import DefaultValue::*;

import RapidIO_Cache_Coherence_GSM::*;
import RapidIO_DTypes::*;
import RapidIO_Cache_Coherence_L3Interface::*;
import RapidIO_Cache_Coherence_L2Contrl::*;
import RapidIO_Cache_Coherence_Memory_Controller::*;
import RapidIO_Cache_Coherence_GSM_Interface::*;
import RapidIO_Cache_Coherence_L2ControlInterface::*;




interface Ifc_ProcessorAgent;

method Action _inputs_Packet_RapidIO(Packet pkt); // RapidIO will put packet to processor agent.
method Action _inputs_Packet_L3_Cache(Packet pkt);
method Action _inputs_Packet_Local_Processor_Local(Packet pkt); // called by Top Module to put request from L2ControlInterface.
method Action _inputs_Packet_Remote_Response(Packet pkt);
method Packet _outputs_Req_PA_TO_L2_alterState();
method Packet _outputs_Resp_PA_TO_L2Contrl();
method Packet _outputs_Resp_PA_TO_L2Contrl_LoadAndChange();
method Packet _outputs_Resp_PA_TO_L2Contrl_Remote();
method Packet _outputs_Resp_PA_TO_L2Contrl_Local();
method Packet _outputs_Req_PA_TO_L2_Remote();
//method Action _inputs_putRequestFlag(Bool val);


//method Packet _outputs_Packet_RapidIO(); // 
//method Packet _outputs_Packet_L3_Cache();
method Packet _outputs_Packet_Req_Local_processor();
//method Packet _outputs_Packet_At_Remote();  // Called by GSM
//method Bool   _outputs_Ready_To_Receive();



interface Ifc_GSM _ifc_gsm_In_ProcessorAgent;
interface Ifc_L2_CacheContrl _ifc_L2Contrl_In_ProcessorAgent;



endinterface




//(* always_enabled *)
//(* always_ready  = "_outputs_Packet_RapidIO, _outputs_Packet_L3_Cache, _outputs_Packet_Local_processor" *)
//(* always_enabled  = "_outputs_Packet_RapidIO, _outputs_Packet_L3_Cache, _outputs_Packet_Local_processor" *)
//(* always_ready = "_outputs_Packet_RapidIO", "_outputs_Packet_L3_Cache", "_outputs_Packet_Local_processor")
//(* always_enabled, always_ready *)

(* synthesize *)
(* always_enabled *)
(* always_ready *)
//(* always_enabled = "_outputs_Packet_RapidIO " *)
//(* always_ready = "_outputs_Packet_RapidIO"*)

//(*conflict_free = "rl_initCoherenceTID, rl_chkRequestLocal, rl_chkResponseRapidIO "*)
//(* conflict_free = " rl_chkRequestRapidIO, rl_getResponsePacketFromProcessorAgent, rl_chkResponseL2ContrlRemote"*)
(* descending_urgency = " rl_chkRequestRapidIO, rl_getResponsePacketFromProcessorAgent, rl_chkResponseL2ContrlRemote"*)
(* descending_urgency = " rl_chkRequestLocal, rl_chkResponseRapidIO "*)
module mkProcessor_Agent#(Bit#(2) procID)(Ifc_ProcessorAgent);  




function Packet func_makePaket(Packet pkt, State s );


return Packet{
                pID:pkt.pID,
                physical_Address:pkt.physical_Address,
                isRequest: pkt.isRequest,    // Flag indicate whether it a request or response
                reqType :  pkt.reqType,
                resType :  pkt.resType,
                state   :  s, 
                validPacket: True,  // whether data is valid or not
                data: pkt.data


            };

endfunction






Reg#(int) step <- mkReg(0);

Reg#(Packet) rg_receivedPktLocal   <- mkReg(defaultValue);
Reg#(Packet) rg_receivedPktRapidIO <- mkReg(defaultValue);
Reg#(Packet) rg_receivedPktL3Cache <- mkReg(defaultValue);
Reg#(Packet) rg_packetFromRemote <- mkReg(defaultValue);

Reg#(Packet) rg_receivedRequestFromL2 <- mkReg(defaultValue);


Reg#(Packet) rg_responsePktLocal   <- mkReg(defaultValue);
Reg#(Packet) rg_responsePktRapidIO <- mkReg(defaultValue);
Reg#(Packet) rg_responsePktL3Cache <- mkReg(defaultValue);
Wire#(Packet) wr_responsePktForGSMRemote <- mkDWire(defaultValue);

Wire#(Packet) wr_Req_PA_TO_L2_alterState <- mkDWire(defaultValue);
Wire#(Packet) wr_Resp_PA_TO_L2Contrl  <- mkDWire(defaultValue);
Wire#(Packet) wr_Resp_PA_TO_L2Contrl_LoadAndChange <- mkDWire(defaultValue);
Wire#(Packet) wr_Resp_PA_TO_L2Contrl_Remote <- mkDWire(defaultValue);
Wire#(Packet) wr_Resp_PA_TO_L2Contrl_Local <- mkDWire(defaultValue);
Wire#(Packet) wr_Resp_L2Contrl_TO_PA_Remote <- mkDWire(defaultValue);
Wire#(Packet) wr_Req_PA_TO_L2_Remote  <- mkDWire(defaultValue);


Reg#(Packet) rg_receivedFromL3Cache <- mkReg(defaultValue);
Reg#(Packet) rg_receivedFromRapidIO <- mkReg(defaultValue);
Reg#(Packet) rg_pktLocalResponseRemote <- mkReg(defaultValue); // rg_pktLocalResponse -> returned by remote end L2 Controller.


/*
Wire#(Maybe#( Packet)) wr_receivedRequestLocal   <- mkDWire(defaultValue);
Wire#(Maybe#( Packet)) wr_receivedPktRapidIO <- mkDWire(defaultValue);
Wire#(Maybe#( Packet)) wr_receivedPktL3Cache <- mkDWire(defaultValue);

Wire#(Maybe#( Packet)) wr_responsePktLocal   <- mkDWire(defaultValue);
Wire#(Maybe#( Packet)) wr_responsePktRapidIO <- mkDWire(defaultValue);
Wire#(Maybe#( Packet)) wr_responsePktL3Cache <- mkDWire(defaultValue);
*/
Wire#(Packet) wr_receivedRequestLocal   <- mkDWire(defaultValue);
Wire#(Packet) wr_receivedPktRapidIO <- mkDWire(defaultValue);
Wire#(Packet) wr_receivedPktL3Cache <- mkDWire(defaultValue);

Wire#(Packet) wr_responsePktLocal   <- mkDWire(defaultValue);
Wire#(Packet) wr_responsePktRapidIO <- mkDWire(defaultValue);
Wire#(Packet) wr_responsePktL3Cache <- mkDWire(defaultValue);
Wire#(Packet) wr_requestPktL2ContrlLocal <- mkDWire(defaultValue); // it will output to top module then there this will put to L2 as action.
Wire#(Packet) wr_requestPktL2ContrlRemote <- mkDWire(defaultValue);
//FIFO#(Reg#(Packet)) paketQueueFromLocal <- mkFIFO(defaultValue);
//FIFO#(Reg#(Packet)) paketQueueFromRapidIO <- mkFIFO(defaultValue);

// Local Packet to this module

Packet pktOfL2 = defaultValue;
Packet pktOfL3 = defaultValue;
Packet pktOfRapidIO = defaultValue;


// making ids for local core

//Bit#(4) localProcId[4];
//for(int i=0; i<4; i = i+1)begin

//	localProcId[i] = fromInteger(i);
	
//end

/// RapidIO Wire Declarations  ///



///     InitReq Wires   ////////////
Wire#(Bool) wr_ireq_sof <- mkDWire (False);
Wire#(Bool) wr_ireq_eof <- mkDWire (False);
Wire#(Bool) wr_ireq_vld <- mkDWire (False);
Wire#(Bool) wr_ireq_dsc <- mkDWire (False);
Wire#(Bool) wr_ireq_rdy_n <- mkDWire(True);


// -- Data
Wire#(TT) wr_ireq_tt <- mkDWire (0);
Wire#(Data) wr_ireq_data <- mkDWire (0);
Wire#(Bool) wr_ireq_crf <- mkDWire (False);
Wire#(Prio) wr_ireq_prio <- mkDWire (0);
Wire#(Type) wr_ireq_ftype <- mkDWire (0);
Wire#(DestId) wr_ireq_destid <- mkDWire (0);
Wire#(Addr) wr_ireq_addr <- mkDWire (0);
Wire#(Bit#(8)) wr_ireq_hopcount <- mkDWire (0);
Wire#(TranId) wr_ireq_tid <- mkDWire (0);
Reg#(TranId)  rg_ireq_tid <- mkReg (0);
Wire#(Type)  wr_ireq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_ireq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_ireq_byte_en <- mkDWire (0);

// Wires for optinal Signals -- Message
Wire#(Bool) wr_ireq_local <- mkDWire (False);
Wire#(DoorBell) wr_ireq_db_info <- mkDWire (0);
Wire#(MsgLen) wr_ireq_msg_len <- mkDWire (0);
Wire#(MsgSeg) wr_ireq_msg_seg <- mkDWire (0);
Wire#(Bit#(6)) wr_ireq_mbox <- mkDWire (0);
Wire#(Mletter) wr_ireq_letter <- mkDWire(0);

///      End InitReq Wires  ///////////////


/*
Bool treq_sof_n=False;
Bool treq_eof_n= False;
Bool treq_vld_n = False;
Bool treq_rdy_n = False;


Wire#(TT) wr_ireq_tt <- mkDWire (0);
Wire#(Data) wr_ireq_data <- mkDWire (0);
Wire#(Bool) wr_ireq_crf <- mkDWire (False);
Wire#(Prio) wr_ireq_prio <- mkDWire (0);
Wire#(Type) wr_ireq_ftype <- mkDWire (0);
Wire#(DestId) wr_ireq_destid <- mkDWire (0);
Wire#(Addr) wr_ireq_addr <- mkDWire (0);
Wire#(Bit#(8)) wr_ireq_hopcount <- mkDWire (0);
Wire#(TranId) wr_ireq_tid <- mkDWire (0);
Wire#(Type) wr_ireq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_ireq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_ireq_byte_en <- mkDWire (0);


Wire#(Bit#(64)) wr_treq_data <- mkDWire(0);
Wire#(Bit#(2))  wr_treq_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_treq_ftype <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_dest_id <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_src_id <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_tid <- mkDWire(0);
Wire#(Bit#(4))  wr_treq_ttype <- mkDWire(0);
Wire#(Bit#(34))  wr_treq_addr <- mkDWire(0);
Wire#(Bit#(9))  wr_treq_byte_count <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_byte_en_n<- mkDWire(0);

*/
/*

Wire#(TT)      wr_ireq_tt <- mkDWire (0);
Wire#(Data)    wr_ireq_data <- mkDWire (0);
Wire#(Bool)    wr_ireq_crf <- mkDWire (False);
Wire#(Prio)    wr_ireq_prio <- mkDWire (0);
Wire#(Type)    wr_ireq_ftype <- mkDWire (0);
Wire#(DestId)  wr_ireq_destid <- mkDWire (0);
Wire#(Addr)    wr_ireq_addr <- mkDWire (0);
Wire#(Bit#(8)) wr_ireq_hopcount <- mkDWire (0);
Wire#(TranId)  wr_ireq_tid <- mkDWire (0);
Wire#(Type)    wr_ireq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_ireq_byte_count <- mkDWire (0);
Wire#(ByteEn)    wr_ireq_byte_en <- mkDWire (0);

*/


//  Target request Signals  //
Wire#(Bool) wr_treq_sof_n <- mkDWire(True);
Wire#(Bool) wr_treq_eof_n <- mkDWire(True);
Wire#(Bool) wr_treq_vld_n <- mkDWire(True);
Wire#(Bool) wr_treq_rdy_n <- mkDWire(True);
Wire#(Bool) wr_treq_crf <- mkDWire(True);

Wire#(Data) wr_treq_data <- mkDWire(0);
Wire#(Bit#(2))  wr_treq_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_treq_ftype <- mkDWire(0);
Wire#(DestId)  wr_treq_dest_id <- mkDWire(0);
Wire#(SourceId )  wr_treq_src_id <- mkDWire(0);
Wire#(TranId)  wr_treq_tid <- mkDWire(0);
Wire#(Type)  wr_treq_ttype <- mkDWire(0);
Wire#(TT)    wr_treq_tt <- mkDWire(0);
Wire#(Addr)  wr_treq_addr <- mkDWire(0);
Wire#(ByteCount)  wr_treq_byte_count <- mkDWire(0);
Wire#(ByteEn)  wr_treq_byte_en_n<- mkDWire(0);


// Target Response Signals // 
Wire#(Bool) wr_tresp_sof_n <- mkDWire(True);
Wire#(Bool) wr_tresp_eof_n <- mkDWire(True);
Wire#(Bool) wr_tresp_vld_n <- mkDWire(True);
Wire#(Bool) wr_tresp_rdy_n <- mkDWire(True);
Wire#(Bool) wr_tresp_crf <-   mkDWire(False);
Wire#(Bool) wr_tresp_dsc_n <- mkDWire(False);

Wire#(Bool) variablesAssigned <- mkDWire(False);


Wire#(Bit#(64)) wr_tresp_data <- mkDWire(0);
Wire#(Bool) wr_tresp_no_data <- mkDWire(False);
Wire#(Bit#(2))  wr_tresp_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_tresp_ftype <- mkDWire(0);
Wire#(Bit#(32))  wr_tresp_dest_id <- mkDWire(0);
Wire#(Bit#(32))  wr_tresp_src_id <- mkDWire(0);
Wire#(Bit#(8))  wr_tresp_tid <- mkDWire(0);
Wire#(Type)  wr_tresp_ttype <- mkDWire(0);
Wire#(TT)  wr_tresp_tt <- mkDWire(0);

Wire#(Bit#(50))  wr_tresp_addr <- mkDWire(0);
Wire#(Bit#(9))  wr_tresp_byte_count <- mkDWire(0);
Wire#(Bit#(8))  wr_tresp_byte_en_n<- mkDWire(0);
Wire#(Bool)     wr_tresp_sof  <- mkDWire(True);
Wire#(Bit#(4))  wr_tresp_status <- mkDWire(0);
//Wire#(Mbox)  wr_tresp_mbox <- mkDWire(0);
Wire#(Mletter)  wr_tresp_letter <- mkDWire(0);

Wire#(MsgSeg) wr_tresp_msg_seg  <- mkDWire(0);
//Wire#(Bit#(32))  wr_tresp_destid <- mkDWire(0);





//  Initiator Response Signals //
Wire#(Bool)	wr_iresp_sof_n <- mkDWire(False);
Wire#(Bool) 	wr_iresp_eof_n <- mkDWire(False);
Wire#(Bool) 	wr_iresp_vld_n <- mkDWire(False);
Wire#(Bool)     wr_iresp_rdy_n <- mkDWire(False);

Wire#(TT)     wr_iresp_tt <- mkDWire(0);
Wire#(Status)   wr_iresp_status <- mkDWire(0);
Wire#(Bit#(64)) wr_iresp_data <- mkDWire(0);
Wire#(Bit#(2))  wr_iresp_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_iresp_ftype <- mkDWire(0);
Wire#(Bit#(32))  wr_iresp_dest_id <- mkDWire(0);
Wire#(Bit#(32))  wr_iresp_src_id <- mkDWire(0);
Wire#(Bit#(8))  wr_iresp_tid <- mkDWire(0);
Wire#(Type)     wr_iresp_ttype <- mkDWire(0);
Wire#(Addr)  wr_iresp_addr <- mkDWire(0);
Wire#(Bit#(9))  wr_iresp_byte_count <- mkDWire(0);
Wire#(Bit#(8))  wr_iresp_byte_en_n<- mkDWire(0);
Wire#(Bool)     wr_iresp_crf     <- mkDWire(False);
Wire#(SourceId) wr_iresp_source_id <- mkDWire(0);
Wire#(Bool)     wr_iresp_local  <- mkDWire(False);





/////////////////// Transaction ID for current Request //////////////////

Reg#(CoherenceTID) currentTID <- mkReg(defaultValue);
//Reg#(CoherenceTID) receivedRapidioTID <- mkReg(defaultValue);

////////////////// local rule variables  ///////////////////////

Reg#(int) rg_chkTurn <- mkReg(0);
Reg#(int) rg_initVar <- mkReg(0);
Reg#(int) rg_chkRequestTurn <- mkReg(0); // variable to run rules exclusively.
Reg#(Bool) chkLocalRespReady <- mkReg(False);
Reg#(Bool) resReceivedL3 <-mkReg(True);
Reg#(Bool) rg_responseReceivedRIO <- mkReg(False);
Reg#(Bool) rg_responseReceivedL3 <- mkReg(False);
Reg#(Bool) readmiss <-mkReg(False);
Reg#(int) responseCount <- mkReg(0);
Reg#(Bool) chkRequestStatus <- mkReg(True);
Reg#(Bool) resReceived <- mkReg(False);
Reg#(Bool) rg_isReady_rl_getResponsePacketFromProcessorAgent <- mkReg(False);


/////////////////////////////////////////////////////////////////


////////////////// Method Condition Variables /////////////////////////////

Reg#(Bool) readyToReceiveFromRapidIO <-mkReg(False);
Reg#(Bool) readyToReceiveFromL3 <-mkReg(False);
Reg#(Bool) rg_readyToReceiveFromLocal <-mkReg(True);
Reg#(Bool) readyToResponseRapidIO <-mkReg(False);
Reg#(Bool) readyToResponseL3 <-mkReg(False);

Reg#(Bool) readyToResponseLocal <-mkReg(False);
Reg#(Bool) readyTochkRequestRapidIO <- mkReg(True);
Reg#(Bool) readyTID <- mkReg(False); 
////////////////////////////////////////////////////////////////

/////////////////////////////////////// Interface Instantiations ///////////////
Ifc_L3_Cache ifc_L3 <- mkL3Module;					//
Ifc_RapidIO  ifc_RapidIO <- mkGSM;                  // Connecting to GSM module.
//Ifc_L2_CacheContrl ifc_L2CacheController <- mkL2CacheContrl(dataFileAsCache);
Ifc_Memory_Contrl ifc_MemoryContrl <- mkMemoryContrl;



//(*mutually_exclusive = "rl_chkRequestLocal,"*)
//(*execution_order = "rl_initCoherenceTID, rl_chkRequestLocal, rl_chkResponseRapidIO " *)
 //(*mutually_exclusive = "rl_initCoherenceTID,rl_chkRequestLocal,rl_chkRequestRapidIO, getResponsePacketFromProcessorAgent, rl_chkResponseL2ContrlRemote"*)
/*
rule rl_nextReq;
wr_Req_PA_TO_L2_alterState 
 ifc_L2CacheController._inputs_next_request();

endrule
*/

rule rl_chkRequestLocal;  // check requests  TODO define one more, same type of rule for chkRequest from RapidIO. After request getting satisfied rg_receivedRequestFromL2this rule should again activated for new requests.
// chkRequestStatus will be set by rl_chkResponse when request is satisfied.

 
 //ifc_L2CacheController._inputs_Processor_Agent_Is_Ready(True); 
 //rg_receivedRequestFromL2 <-  ifc_L2CacheController._outputs_Request_To_Processor_Agent();

  let pktFromLocal  = wr_receivedRequestLocal;
 
 rg_receivedRequestFromL2 <= wr_receivedRequestLocal;
 currentTID
  <= CoherenceTID{pID:wr_receivedRequestLocal.pID,physical_Address:wr_receivedRequestLocal.physical_Address};
	//$display("Cuurent ID is %h",currentTID);
	
	
 //let pktFromLocal = rg_receivedRequestFromL2;
 pktFromLocal.validPacket = True;
    case(wr_receivedRequestLocal.reqType) matches  // Read_Miss in Local Processors

   /////////////////////////////////////////////// Read_Miss   ////////////////////////////////          
        Read_Miss: begin       
        
            
            // put request to L3 and RapidIO cache Interface.    
            
       	    
       	    
           // responseCount <= 0;   // indicating response from oher processors  
            
            $display("ProcID = %d",procID);
            $display("Read_Miss");
            $display("Form L2 -->>  Received Address %b ", wr_receivedRequestLocal.physical_Address);
            $display("Received Request From L2 is %b",wr_receivedRequestLocal);
            
            
            //ifc_L3._inputs_Packet_L3_Cache(func_makePaket(rg_receivedRequestFromL2,InValid)); // TODO check response for L3 (One rule for L3 ) needed
            ifc_L3._inputs_Packet_L3_Cache(func_makePaket(wr_receivedRequestLocal,InValid)); // TODO check response for L3 (One rule for L3 ) needed
           //` ifc_RapidIO._inputs_Packet_To_RapidIO_From_Agent_Request(rg_receivedRequestFromL2); 
            //ifc_RapidIO._inputs_Packet_To_RapidIO_From_Agent_Request(func_makePaket(wr_receivedRequestLocal,InValid));
            ifc_RapidIO._inputs_Packet_To_RapidIO_From_Agent_Request(func_makePaket(wr_receivedRequestLocal,InValid));
            //chkRequestStatus  <= False;        
            
        end 
        
        
        Read_Hit: begin
            /* Do Nothing */  
            
            //ifc_L3._inputs_Packet_L3_Cache(pktFromLocal); // TODO check response for L3 (One rule for L3 ) needed
            //ifc_RapidIO._inputs_Packet_To_RapidIO_From_Agent_Request(pktFromLocal);  
            
            //chkRequestStatus <= False;        
                 
        
        end
        
        Write_Hit: begin
        
        
        	
        	
        	$display("ProcID = %d",procID);
        	$display("Write_Hit");
            	$display("Form L2 -->>  Received Address %b", wr_receivedRequestLocal.physical_Address);        
            	$display("Received Request From L2 is %b",pktFromLocal);
               if(wr_receivedRequestLocal.state == Exclusive) begin    
           
               
                    //let pktToLocal= rg_receivedRequestFromL2;
                    pktFromLocal.state = Modified;
                    //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_alterState(rg_receivedRequestFromL2);
                    wr_Req_PA_TO_L2_alterState <= wr_receivedRequestLocal;
                    //wr_responsePktLocal <= pktToLocal;
                    $display("Received State is Exclusive");
                    $display("New State is for local block Modified");
            
            
               end // TODO the most difficult work first, perhaps breaking it up with some easier tasks.
			//	Don't wait until the last minute to complete your projects.
               
               else if(wr_receivedRequestLocal.state == Modified) begin 
                  
                  /* Do Nothing as Block is already Modified */
                  $display("Received State is Modified");
                  $display("New State at Local remains Modified");
                  
                    
               end   
               
               else if(wr_receivedRequestLocal.state == Shared) begin 
                    
                    //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_alterState(func_makePaket(wr_receivedRequestLocal,Modified));
                    wr_Req_PA_TO_L2_alterState <= wr_receivedRequestLocal;
                    //ifc_L3._inputs_Packet_L3_Cache(func_makePaket(wr_receivedRequestLocal,InValid));
                    ifc_RapidIO._inputs_Packet_To_RapidIO_From_Agent_Request(func_makePaket(wr_receivedRequestLocal,InValid));
                     //chkRequestStatus <= False;
                    $display("Received State is Shared");
                    $display("New State at local is Modified");
                    $display("New state at Remote is InValid");
               
               end
               
               else if(wr_receivedRequestLocal.state == Owned) begin 
                    
			//             let pktToLocal= rg_receivedRequestFromL2;
 	      //                pktToLocal.state = Modified;
                    
                     //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(func_makePaket(rg_receivedRequestFromL2,Modified));
                     wr_Resp_PA_TO_L2Contrl_Local <= func_makePaket(wr_receivedRequestLocal,Modified);
                    
                    $display("Received State is Owned");
                    $display("New State at local is Modified");
                    $display("New state at Remote is InValid");
                    
               
               end
               
               if(wr_receivedRequestLocal.state == InValid) begin    
           
               
                    let pktToLocal= rg_receivedRequestFromL2;
                    pktToLocal.state = Modified;
                    //ifc_L2CacheController._inputs_packet_L2Contrl(pktToLocal);
                   // ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp_Local(func_makePaket(rg_receivedRequestFromL2,Modified));
                   wr_Resp_PA_TO_L2Contrl_Local <= func_makePaket(wr_receivedRequestLocal,Modified);
                    
            

            
               end
               
               
                 
       
                
        end
        
        Write_Miss: begin
                 
                    //ifc_L3._inputs_Packet_L3_Cache(wr_receivedRequestLocal);
                    
                   /* $display("ProcID = %d",procID);
        	    $display("Write_Miss");
                    $display("Form L2 -->>  Received Address %b, Recived Request = %b  ", pktToLocal.physical_Address,pktToLocal.state);*/
                    ifc_RapidIO._inputs_Packet_To_RapidIO_From_Agent_Request(wr_receivedRequestLocal);
                    
                    chkRequestStatus <= False;
                  
            
        
        end
        
    
   
    endcase    
             

endrule : rl_chkRequestLocal  

//////////////////////////  ******************************   REMOTE END STARTS  **************************////////////////////////////////////

///////////////////////////////////// rule chkResponse Local /////////////////////////////

// Checking request  at remote end. 
//(*execution_order = "rl_chkRequestRapidIO, getResponsePacketFromProcessorAgent, rl_chkResponseL2ContrlRemote"*)

rule rl_chkRequestRapidIO;  

    

    		
		 rg_packetFromRemote <= ifc_RapidIO._outputs_Packet_RapidIO_Remote_Req(); // Request Packet from remote.
		
		
	/*	if(pktL3.validPacket == True)begin
		if(pktL3.state == Shared || pktL3.state == Modified || pktL3.state == Exclusive )begin
			rg_receivedFromL3Cache <= pktL3;
			rg_responseReceivedL3 <= True;Invalid

		end
		
		end
	
	    if(pktRapidIO.state == Shared || pktRapidIO.state == Modified || pktRapidIO.state == Exclusive)begin
		
			rg_receivedFromRapidIO <= pktRapidIO;   // Packet
			rg_responseReceivedRIO <= True;			// Boolean Variable. Indicating response from RapidIO has reeived.
		
		end
		*/
		if(rg_packetFromRemote.validPacket == True)begin
		// ifc_L2CacheController._inputs_packet_L2Contrl_From_RapidIO_Req(rg_packetFromRemote); // Putting packet to L2 control to get status of remote block.
		// chkLocalRespReady <= True; // making rl_chkResponseLocal to run
			wr_Req_PA_TO_L2_Remote <= rg_packetFromRemote;
		 	readyTochkRequestRapidIO <= False;
		 
		end
		
			
			

endrule: rl_chkRequestRapidIO

rule rl_getResponsePacketFromProcessorAgent;

rg_pktLocalResponseRemote <= wr_Resp_L2Contrl_TO_PA_Remote.validPacket?wr_Resp_L2Contrl_TO_PA_Remote:defaultValue;//ifc_L2CacheController._outputs_Packet_L2Contrl_To_Processor_Agent_Resp(rg_packetFromRemote);
//wr_Resp_L2Contrl_TO_PA_Remote <= rg_packetFromRemote;
rg_isReady_rl_getResponsePacketFromProcessorAgent <= True;
endrule 



rule rl_chkResponseL2ContrlRemote(rg_isReady_rl_getResponsePacketFromProcessorAgent);  // This rule will modify remote Cache
   // rg_pktLocalResponse is packet response received at remote end from L2.
  
  rg_isReady_rl_getResponsePacketFromProcessorAgent <= False;
  
   if(rg_pktLocalResponseRemote.validPacket == True)begin
   
   $display(" Checking %b  ",rg_pktLocalResponseRemote);
   
     ifc_RapidIO._inputs_From_Processor_Agent_To_GSM_At_Remote(rg_pktLocalResponseRemote);// Puts response packet to Other Nodes and change the state of Remote Node.
  
  // Now it will modify according to state of Remote Cache.
     if(rg_packetFromRemote.reqType == Read_Miss)begin  // rg_packetFromRemote request received from remote.
  
      
        let pktToLocal = rg_pktLocalResponseRemote;      // Remote Local Packet
        if(rg_pktLocalResponseRemote.state == InValid)begin //
                 pktToLocal.state = InValid;
                 //ifc_L2CacheController._inputs_alter_Packet_L2Contrl(pktToLocal,); // This packet is being transmitted to ifc_L2CacheController.
                 
        end
        
        else if(rg_pktLocalResponseRemote.state == Shared)begin  // No need to change the L2 Block at remote in case of Read_Miss.
            //pktToLocal.state = Shared; 
            //wr_responsePktForGSMRemote <= pktToLocal;
            //ifc_L2CacheController._inputs_
            ifc_MemoryContrl.stopRequestFor(rg_pktLocalResponseRemote.physical_Address); // Data found in other cache, Stop processing of this request.

            
        end
        
        else if(rg_pktLocalResponseRemote.state == Modified )begin
            //pktToLocal.state = Owned;
            ifc_MemoryContrl.stopRequestFor(rg_pktLocalResponseRemote.physical_Address); // Do not serve this requets being transferes by cache.
            //ifc_MemoryContrl.update_Block(rg_pktLocalResponseRemote);
            //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp(func_makePaket(pktToLocal,Owned));
            wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(pktToLocal,Owned);
            //wr_responsePktForGSMRemote <= pktToLocal; 
            
        end 
        
        else if( rg_pktLocalResponseRemote.state == Exclusive) begin
        
            //pktToLocal.state = Shared;
            ifc_MemoryContrl.stopRequestFor(rg_pktLocalResponseRemote.physical_Address);
            //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp(func_makePaket(pktToLocal,Shared));
            wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(pktToLocal,Shared);
            //wr_responsePktForGSMRemote <= pktToLocal;  
            
        end
        
        else if(rg_pktLocalResponseRemote.state == Owned)begin
        	
        	pktToLocal.state = Shared;
        	
        	//ifc_RapidIO._inputs_Packet_RapidIO_Response();
        	//ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp(func_makePaket(pktToLocal,Shared));
        	wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(pktToLocal,Shared);
        	
        	//wr_responsePktForGSMRemote <= func_makePaket(pktToLocal,Owned); // This packet is being transmitted to ifc_L2CacheController.	
        	//wr_responsePktLocal <= pktToLocal;
        	
        end
        
       // wr_responsePktRapidIO <=  rg_pktLocalResponse; // writing to wr_responsePktRapidIO, _output method will return it.
        
        //chkLocalRespReady <= False;
      end
  end
  
  
  
  else if( rg_packetFromRemote.reqType == Read_Hit)begin // Just change the state at remote.
            
            if(rg_pktLocalResponseRemote.state == Modified)begin
                wr_responsePktForGSMRemote <= func_makePaket(rg_pktLocalResponseRemote,Owned);
            end
  
       
  end  
  
  
  
  
  
  else if(rg_packetFromRemote.reqType == Write_Hit)begin // Invalidation case, If Write Hit happen, Local copy must be in M,E,S,O. Then at remote end it will in Shared or Invalid state. Take care of Shared state only.
  
       if(rg_pktLocalResponseRemote.state == Modified)begin
            
              
              
             // Do nothing.
                
        end
        
        else if(rg_pktLocalResponseRemote.state == Shared && rg_pktLocalResponseRemote.state == Owned)begin
            
           //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp(func_makePaket( rg_pktLocalResponseRemote,InValid));   
           wr_Resp_PA_TO_L2Contrl <= func_makePaket(rg_pktLocalResponseRemote,InValid);
              
             
                
        end
       
        
        
        
       
  end 
  
  
  
  
  
  else if(rg_packetFromRemote.reqType == Write_Miss)begin
        if(rg_pktLocalResponseRemote.validPacket == True  && rg_pktLocalResponseRemote.state == InValid)begin
            
              wr_Resp_PA_TO_L2Contrl_Remote <= rg_pktLocalResponseRemote;
               rg_readyToReceiveFromLocal <= True;
                
        end
        
        else if(rg_pktLocalResponseRemote.validPacket == True  && rg_pktLocalResponseRemote.state == Exclusive)begin
            
               //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(func_makePaket(rg_pktLocalResponseRemote,InValid));
               wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(rg_pktLocalResponseRemote,InValid);
               ifc_RapidIO._inputs_Packet_RapidIO_Response(rg_pktLocalResponseRemote);
              //ifc_L2CacheController._load_From_Memory_And_Change_State(rg_pktLocalResponseRemote,Modified);
                
        end
        
        else if(rg_pktLocalResponseRemote.validPacket == True && rg_pktLocalResponseRemote.state == Shared)begin
            
              //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(func_makePaket(rg_pktLocalResponseRemote,InValid));
              wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(rg_pktLocalResponseRemote,InValid);
              //ifc_L2CacheController._load_From_Memory_And_Change_State(rg_pktLocalResponseRemote,Modified);
              ifc_RapidIO._inputs_Packet_RapidIO_Response(rg_pktLocalResponseRemote);
              ifc_MemoryContrl._inputs_Packet_Invalidate(wr_receivedRequestLocal.physical_Address); // Ask memory controller to invalidate this.
                
        end
        
        else if(rg_pktLocalResponseRemote.validPacket == True &&  rg_pktLocalResponseRemote.state == Modified)begin
              ifc_MemoryContrl._inputs_Write_Back(rg_pktLocalResponseRemote);  
             // ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(func_makePaket(rg_pktLocalResponseRemote,InValid));
             wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(rg_pktLocalResponseRemote,InValid);
              ifc_RapidIO._inputs_Packet_RapidIO_Response(rg_pktLocalResponseRemote);
              //ifc_L2CacheController._load_From_Memory_And_Change_State(rg_pktLocalResponseRemote,Modified);
              //ifc_MemoryContrl.
              
        end
        else if(rg_pktLocalResponseRemote.validPacket == True &&  rg_pktLocalResponseRemote.state == Owned)begin
        	// Ownd state allows Dirty data sharing without updating the memory.
        	// Memory Controller will not intimidated for 
              //ifc_L2CacheController._inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(func_makePaket(rg_pktLocalResponseRemote,InValid));
              wr_Resp_PA_TO_L2Contrl_Remote <= func_makePaket(rg_pktLocalResponseRemote,InValid);
              ifc_RapidIO._inputs_Packet_RapidIO_Response(rg_pktLocalResponseRemote);
        	
        end
            
        
  end
  
	chkLocalRespReady <= False;
endrule: rl_chkResponseL2ContrlRemote

//////////////////////////  ******************************   REMOTE ENDS   **************************////////////////////////////////////








// It will receive response from RapidIO
// This rule will match the response against requested TID.
////////////////////////////////////***************************  Local End Response Checking *****************************////////////////////////////////


rule rl_chkResponseRapidIO;


let pktResponseRapidIO = ifc_RapidIO._outputs_Packet_RapidIO_Local_Resp();




let receivedTID = CoherenceTID{pID:pktResponseRapidIO.pID,physical_Address:pktResponseRapidIO.physical_Address};
    //if(receivedTID == currentTID)begin
        if(pktResponseRapidIO.validPacket == True && pktResponseRapidIO.reqType == Read_Miss && pktResponseRapidIO.state == InValid && pktResponseRapidIO.resType == DONE)begin
            //ifc_L2CacheController._load_From_Memory_And_Change_State(pktResponseRapidIO.physical_Address,Exclusive);
            wr_Resp_PA_TO_L2Contrl_LoadAndChange <= func_makePaket(pktResponseRapidIO,Exclusive);
            rg_readyToReceiveFromLocal <= True;
            //wr_responsePktLocal <= pktResponseRapidIO;
        end
        

        else if(pktResponseRapidIO.validPacket == True && pktResponseRapidIO.reqType == Read_Miss && pktResponseRapidIO.state == Shared && pktResponseRapidIO.resType == DONE)begin
            //ifc_L2CacheController._load_From_Memory_And_Change_State(pktResponseRapidIO,Shared);
            //ifc_L2CacheController._inputs_packet_To_L2Contrl_From_processor_Agent_Resp(pktResponseRapidIO);
                        //wr_responsePktLocal <= pktResponseRapidIO;
           wr_Resp_PA_TO_L2Contrl_Local <= pktResponseRapidIO;
            rg_readyToReceiveFromLocal <= True;
        end
        
        else if(pktResponseRapidIO.validPacket == True && pktResponseRapidIO.reqType == Read_Miss && pktResponseRapidIO.state == Modified && pktResponseRapidIO.resType == DONE)begin
            //ifc_L2CacheController._load_From_Memory_And_Change_State(pktResponseRapidIO.physical_Address,Shared);
            wr_Resp_PA_TO_L2Contrl_LoadAndChange <= func_makePaket(pktResponseRapidIO,Shared);
            //wr_responsePktLocal <= pktResponseRapidIO;
            rg_readyToReceiveFromLocal <= True;
        end
        
        else if(pktResponseRapidIO.validPacket == True && pktResponseRapidIO.reqType == Read_Miss && pktResponseRapidIO.state == Exclusive && pktResponseRapidIO.resType == DONE)begin
           // ifc_L2CacheController._load_From_Memory_And_Change_State(pktResponseRapidIO.physical_Address,Shared);
           wr_Resp_PA_TO_L2Contrl_Local <= pktResponseRapidIO;
            rg_readyToReceiveFromLocal <= True;
            //wr_responsePktLocal <= pktResponseRapidIO;
        end
        
        else if(pktResponseRapidIO.validPacket == True && pktResponseRapidIO.reqType == Read_Miss && pktResponseRapidIO.state == Owned && pktResponseRapidIO.resType == DONE)begin
           // ifc_L2CacheController._load_From_Memory_And_Change_State(pktResponseRapidIO.physical_Address,Shared);
           wr_Resp_PA_TO_L2Contrl_Local <= func_makePaket( pktResponseRapidIO,Owned);
            rg_readyToReceiveFromLocal <= True;
            //wr_responsePktLocal <= pktResponseRapidIO;
        end
        
        
        
        
        // Cache to Cache Transfer /////
        else if(pktResponseRapidIO.state == InValid && pktResponseRapidIO.reqType == Write_Miss && pktResponseRapidIO.resType == DONE && pktResponseRapidIO.state == Owned)begin
            //ifc_L2CacheController._load_From_Memory_And_Change_State(pktResponseRapidIO,Modified);
            //ifc_L2CacheController._inputs_packet_To_L2Contrl_From_processor_Agent_Resp(pktResponseRapidIO);
            wr_Resp_PA_TO_L2Contrl_Local <= pktResponseRapidIO;
            rg_readyToReceiveFromLocal <= True;
            

        end
        
        
        
        else if(pktResponseRapidIO.validPacket == True && pktResponseRapidIO.reqType == Write_Hit && pktResponseRapidIO.resType == DONE)begin
            //ifc_L2CacheController._inputs_packet_To_L2Contrl_From_processor_Agent_Resp(pktResponseRapidIO);
            wr_Resp_PA_TO_L2Contrl_Local <= pktResponseRapidIO;
            //chkLocalRespReady <= False; // Ready To receive new Request.
            rg_readyToReceiveFromLocal <= True;
            
        end
    
    //end


	chkRequestStatus <= False;
endrule: rl_chkResponseRapidIO
////////////////////////////////////***************************  Local End Response Checking *****************************////////////////////////////////




method Action _inputs_Packet_RapidIO(Packet pkt) ;
//pktOfRapidIO = pkt;
wr_receivedPktRapidIO <= pkt;
rg_receivedPktRapidIO <= pkt;
endmethod

method Action _inputs_Packet_L3_Cache(Packet pkt) ;
//pktOfL3 = pkt;
wr_receivedPktL3Cache <= pkt;
rg_receivedPktL3Cache <= pkt;
endmethod

method Action _inputs_Packet_Local_Processor_Local(Packet pkt);

//pktOfL2 = pkt;
wr_receivedRequestLocal <= pkt;
rg_receivedPktLocal <= pkt;
endmethod

method Action _inputs_Packet_Remote_Response(Packet pkt); // from l2 to Processor Agent at Remote.

	wr_Resp_L2Contrl_TO_PA_Remote <= pkt;
	
endmethod


method Packet _outputs_Req_PA_TO_L2_alterState();

	return wr_Req_PA_TO_L2_alterState;
endmethod

method Packet _outputs_Resp_PA_TO_L2Contrl();
	return wr_Resp_PA_TO_L2Contrl;
endmethod

method Packet _outputs_Resp_PA_TO_L2Contrl_LoadAndChange();
	return wr_Resp_PA_TO_L2Contrl_LoadAndChange;
endmethod


method Packet _outputs_Resp_PA_TO_L2Contrl_Remote();
	return wr_Resp_PA_TO_L2Contrl_Remote;
endmethod 

method Packet _outputs_Resp_PA_TO_L2Contrl_Local();
	return wr_Resp_PA_TO_L2Contrl_Local;
endmethod

method Packet _outputs_Req_PA_TO_L2_Remote();
	return wr_Req_PA_TO_L2_Remote;
endmethod






interface Ifc_GSM _ifc_gsm_In_ProcessorAgent;

method Bool _ireq_sof_n();
	return ifc_RapidIO._ifc_gsm._ireq_sof_n();
endmethod
 

method Bool _ireq_eof_n();
	return ifc_RapidIO._ifc_gsm._ireq_eof_n();
endmethod 

method Bool _ireq_vld_n();
	return ifc_RapidIO._ifc_gsm._ireq_vld_n();
 endmethod
method Bool _ireq_dsc_n ();
	return ifc_RapidIO._ifc_gsm._ireq_dsc_n();
endmethod 
// Ready
 method Action ireq_rdy_n_ (Bool value);
 	 wr_ireq_rdy_n <= value;
 endmethod

 method TT _ireq_tt();
	return ifc_RapidIO._ifc_gsm._ireq_tt(); 
 endmethod 
 
 method Data _ireq_data ();
	return ifc_RapidIO._ifc_gsm._ireq_data();
 endmethod
 method Bool _ireq_crf ();
	return ifc_RapidIO._ifc_gsm._ireq_crf();
 endmethod
 method Prio _ireq_prio ();
	return ifc_RapidIO._ifc_gsm._ireq_prio();
 endmethod
 method Type _ireq_ftype();
	return ifc_RapidIO._ifc_gsm._ireq_ftype();
 endmethod
 method DestId _ireq_dest_id ();
	return ifc_RapidIO._ifc_gsm._ireq_dest_id(); 
 endmethod 
 method Addr _ireq_addr();
	return ifc_RapidIO._ifc_gsm._ireq_addr();
 endmethod 
 method Bit#(8) _ireq_hopcount ( );
	return ifc_RapidIO._ifc_gsm._ireq_hopcount();
 endmethod
 method TranId _ireq_tid ();
	return ifc_RapidIO._ifc_gsm._ireq_tid();
 endmethod
 method Type _ireq_ttype ( );
	return ifc_RapidIO._ifc_gsm._ireq_ttype(); 
 endmethod
 method ByteCount _ireq_byte_count ();
	return ifc_RapidIO._ifc_gsm._ireq_byte_count();
 endmethod
 method ByteEn _ireq_byte_en_n ();
	return ifc_RapidIO._ifc_gsm._ireq_byte_en_n();
 endmethod
 method Bool _ireq_local ();
	return ifc_RapidIO._ifc_gsm._ireq_local();
 endmethod
 method DoorBell  _ireq_db_info ();
	return ifc_RapidIO._ifc_gsm._ireq_db_info(); 
 endmethod
 method MsgLen _ireq_msg_len ();
	return ifc_RapidIO._ifc_gsm._ireq_msg_len(); 
 endmethod
 method MsgSeg _ireq_msg_seg ();
	return ifc_RapidIO._ifc_gsm._ireq_msg_seg(); 
 endmethod
 method Bit#(6) _ireq_mbox ( );
	return ifc_RapidIO._ifc_gsm._ireq_mbox(); 
 endmethod
 method Bit#(2) _ireq_letter ();
	return ifc_RapidIO._ifc_gsm._ireq_letter(); 
 endmethod

 


//////////////// Target Response as Output Methods ////////////


 method Bool outputs_tresp_sof_n_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_sof_n_();
 endmethod
 method Bool outputs_tresp_eof_n_ (); 
	return ifc_RapidIO._ifc_gsm.outputs_tresp_eof_n_();
 endmethod
 method Bool outputs_tresp_vld_n_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_vld_n_();
 endmethod
 method Bool outputs_tresp_dsc_n_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_dsc_n_();
 endmethod
 method Bool _inputs_tresp_rdy_n_ ();
	 return wr_tresp_rdy_n?wr_tresp_rdy_n:True;
 endmethod

 method TT outputs_tresp_tt_ (); 
	return ifc_RapidIO._ifc_gsm.outputs_tresp_tt_(); 
 endmethod
 method Data outputs_tresp_data_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_data_();
 endmethod
 method Bool outputs_tresp_crf_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_crf_();
 endmethod
 method Prio outputs_tresp_prio_ ( );
	return ifc_RapidIO._ifc_gsm.outputs_tresp_prio_();
 endmethod
 method Type outputs_tresp_ftype_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_ftype_(); 
 endmethod
 method DestId outputs_tresp_dest_id_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_dest_id_();
 endmethod
 method Status outputs_tresp_status_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_status_();
 endmethod
 method TranId outputs_tresp_tid_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_tid_(); 
 endmethod
 method Type outputs_tresp_ttype_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_ttype_(); 
 endmethod
 method Bool outputs_tresp_no_data_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_no_data_(); 
 endmethod

 method MsgSeg outputs_tresp_msg_seg_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_msg_seg_();
 endmethod
/*method Bit#(2) outputs_tresp_mbox_ ( );
	return wr_tresp_mbox ; 
 endmethod
*/
 method Mletter  outputs_tresp_letter_ ();
	return ifc_RapidIO._ifc_gsm.outputs_tresp_letter_(); 
 endmethod
 
 
 
 
 
/////////////////    Initiator Response Signals // Called From Wrapper Module. ///

//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value);
 	wr_iresp_sof_n <= value;
 	
 	
 endmethod
 method Action iresp_eof_n_ (Bool value);
 	wr_iresp_eof_n <= value;
 endmethod
 

 
 
 method Action iresp_vld_n_ (Bool value);
	 wr_iresp_vld_n <= value;
 endmethod
 method Bool _iresp_rdy_n ();
 	return wr_iresp_rdy_n ? wr_iresp_rdy_n : True;
 endmethod

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value); 
 
 	wr_iresp_tt <= value;
 endmethod
 method Action iresp_data_ (Data value);
 
 	wr_iresp_data <=value;
 endmethod
 method Action iresp_crf_ (Bool value);
 	wr_iresp_crf <= value;
 endmethod
 
 method Action iresp_prio_ (Prio value);
 	wr_iresp_prio <= value;
 endmethod
 method Action iresp_ftype_ (Type value);
 	wr_iresp_ftype <= value;
 endmethod 
 method Action iresp_dest_id_ (DestId value);
 	wr_iresp_dest_id <= value;
 endmethod
 method Action iresp_source_id_ (SourceId value);
 	wr_iresp_source_id <= value;
 endmethod
 method Action iresp_tid_ (TranId value);
 
 	wr_iresp_tid <= value;
 endmethod
 method Action iresp_ttype_ (Type value);
 	wr_iresp_ttype <= value;
 endmethod
 method Action iresp_addr_ (Addr value);
 	wr_iresp_addr <= value;
 endmethod
 method Action iresp_byte_count_ (ByteCount value);
 	wr_iresp_byte_count <= value;
 endmethod
 method Action iresp_byte_en_n_ (ByteEn value);
 	wr_iresp_byte_en_n <= value;
 endmethod
 
 method Action iresp_status_(Status value);
 	wr_iresp_status <= value;
 endmethod
 
 method Action iresp_local_(Bool value);
 	wr_iresp_local <= value;
 endmethod




///////////////////  Target Request (Inputs Signals) //////////////////////


 
method Action treq_sof_n_ (Bool value);
	//wr_treq_sof_n <= value;
	ifc_RapidIO._ifc_gsm.treq_sof_n_(value);
 endmethod
 method Action treq_eof_n_ (Bool value);
//	wr_treq_eof_n <= value;
	ifc_RapidIO._ifc_gsm.treq_eof_n_(value);
 endmethod
 method Action treq_vld_n_ (Bool value);
//	wr_treq_vld_n <= value;
	ifc_RapidIO._ifc_gsm.treq_vld_n_(value);
 endmethod
 method Bool _treq_rdy_n ();
	return ifc_RapidIO._ifc_gsm._treq_rdy_n() ? ifc_RapidIO._ifc_gsm._treq_rdy_n() : True;
 endmethod

 method Action treq_tt_ (TT value); 
	//wr_treq_tt <= value;
	ifc_RapidIO._ifc_gsm.treq_tt_(value);
 endmethod 
 method Action treq_data_ (Data value);
//	wr_treq_data <= value;

	ifc_RapidIO._ifc_gsm.treq_data_(value);
 endmethod
 method Action treq_crf_ (Bool value);
//	wr_treq_crf <= value;
	ifc_RapidIO._ifc_gsm.treq_crf_(value);
 endmethod
 method Action treq_prio_ (Prio value);
//	wr_treq_prio <= value;

	ifc_RapidIO._ifc_gsm.treq_prio_(value);
 endmethod
 method Action treq_ftype_ (Type value);
//	wr_treq_ftype <= value;

	ifc_RapidIO._ifc_gsm.treq_ftype_(value);
 endmethod
 method Action treq_dest_id_ (DestId value);
//	wr_treq_dest_id <= value;
	ifc_RapidIO._ifc_gsm.treq_dest_id_(value);
 endmethod
 method Action treq_source_id_ (SourceId value);
//	wr_treq_src_id <= value;
	ifc_RapidIO._ifc_gsm.treq_source_id_(value);
 endmethod
 method Action treq_tid_ (TranId value);
//	wr_treq_tid <= value;
	ifc_RapidIO._ifc_gsm.treq_tid_(value);
 endmethod
 method Action treq_ttype_ (Type value);
//	wr_treq_ttype <= value;
	ifc_RapidIO._ifc_gsm.treq_ttype_(value);
 endmethod
 method Action treq_addr_ (Addr value);
//	wr_treq_addr <= value;
	ifc_RapidIO._ifc_gsm.treq_addr_(value);
 endmethod
 method Action treq_byte_count_ (ByteCount value);
//	wr_treq_byte_count <= value;
	ifc_RapidIO._ifc_gsm.treq_byte_count_(value);
 endmethod
 method Action treq_byte_en_n_ (ByteEn value);
//	wr_treq_byte_en_n <= value;
	ifc_RapidIO._ifc_gsm.treq_byte_en_n_(value);
 endmethod


endinterface:_ifc_gsm_In_ProcessorAgent



 
/*
method Packet _outputs_Packet_RapidIO() if(readyToResponseRapidIO); // output packet on some condition to synchronize // This will return packet to L2.
return wr_responsePktRapidIO;
endmethod

method Packet _outputs_Packet_L3_Cache();// if(readyToResponseL3);
return rg_responsePktL3Cache;
endmethod


method Packet _outputs_Packet_At_Remote();//if(readyToResponseLocal);   // This will be called by Gsm to get response from 
									// remote L2 controller.		
return wr_responsePktForGSMRemote;
endmethod


method Packet _outputs_Packet_Req_Local_processor(); // Called by mkTb , then mkTb will put this packet to L2 Controller.

	return rg_

endmethod



method Bool _outputs_Ready_To_Receive();

    return rg_readyToReceiveFromLocal;

endmethod

*/

endmodule


endpackage

