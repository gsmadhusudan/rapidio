/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package RapidIO_Cache_Coherence_GSM_Interface;

import RapidIO_DTypes ::*;
import RapidIO_Cache_Coherence_Types::*;

interface Ifc_GSM;

	
	///   InitReq Signals as Output. Connecting to Wrapper.  ////////////
method Bool _ireq_sof_n();
	
method Bool _ireq_eof_n();
method Bool _ireq_vld_n();
method Bool _ireq_dsc_n ();
method Action ireq_rdy_n_ (Bool value);

method TT _ireq_tt();
method Data _ireq_data ();
method Bool _ireq_crf ();
method Prio _ireq_prio ();
method Type _ireq_ftype();
method DestId _ireq_dest_id ();
method Addr _ireq_addr(); 
method Bit#(8) _ireq_hopcount ( );
method TranId _ireq_tid (); 
method Type _ireq_ttype ( );
method ByteCount _ireq_byte_count ();
method ByteEn _ireq_byte_en_n ();
method Bool _ireq_local ();

method DoorBell  _ireq_db_info ();
method MsgLen _ireq_msg_len ();
method MsgSeg _ireq_msg_seg ();
method Bit#(6) _ireq_mbox ( );

method Bit#(2) _ireq_letter ();
	


/// Target Response Signals as Output.  Connecting to Wrapper ////////////////////////////////

 method Bool outputs_tresp_sof_n_ ();
	
 method Bool outputs_tresp_eof_n_ (); 
	
 method Bool outputs_tresp_vld_n_ ();
	
 method Bool outputs_tresp_dsc_n_ ();
	
 method Bool _inputs_tresp_rdy_n_ ();

 method TT outputs_tresp_tt_ (); 
	
 method Data outputs_tresp_data_ ();
	
 method Bool outputs_tresp_crf_ ();
	
 method Prio outputs_tresp_prio_ ( );
	
 method Type outputs_tresp_ftype_ ();
	
 method DestId outputs_tresp_dest_id_ ();
	
 method Status outputs_tresp_status_ ();
	
 method TranId outputs_tresp_tid_ ();
	
 method Type outputs_tresp_ttype_ ();
	
 method Bool outputs_tresp_no_data_ ();

 method MsgSeg outputs_tresp_msg_seg_ ();
	
 method Bit#(2) outputs_tresp_mbox_ ( );
	
 method Mletter  outputs_tresp_letter_ ();
 

/////////////////    Treq Methods /////
 
 method Action treq_sof_n_ (Bool value);
	
 method Action treq_eof_n_ (Bool value);
	
 method Action treq_vld_n_ (Bool value);
	
 method Bool _treq_rdy_n ();
	
 method Action treq_tt_ (TT value); 
	 
 method Action treq_data_ (Data value);
	
 method Action treq_crf_ (Bool value);
	
 method Action treq_prio_ (Prio value);
	
 method Action treq_ftype_ (Type value);
	
 method Action treq_dest_id_ (DestId value);
	
 method Action treq_source_id_ (SourceId value);
	
 method Action treq_tid_ (TranId value);
	
 method Action treq_ttype_ (Type value);
	
 method Action treq_addr_ (Addr value);
	
 method Action treq_byte_count_ (ByteCount value);
	
 method Action treq_byte_en_n_ (ByteEn value);
	
	
	
//////////////////    Iresp Methods  ///////////////



//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value); 	
 method Action iresp_eof_n_ (Bool value); 
 method Action iresp_vld_n_ (Bool value);	
 method Bool _iresp_rdy_n (); 	

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value);  	
 method Action iresp_data_ (Data value); 	
 method Action iresp_crf_ (Bool value); 
 method Action iresp_prio_ (Prio value);	
 method Action iresp_ftype_ (Type value); 	 
 method Action iresp_dest_id_ (DestId value); 	
 method Action iresp_source_id_ (SourceId value); 	
 method Action iresp_tid_ (TranId value); 
 method Action iresp_ttype_ (Type value);
 method Action iresp_addr_ (Addr value); 	
 method Action iresp_byte_count_ (ByteCount value); 
 method Action iresp_byte_en_n_ (ByteEn value);
 method Action iresp_status_(Status value); 
 method Action iresp_local_(Bool value);
 

endinterface
 	
	
	
	


endpackage
