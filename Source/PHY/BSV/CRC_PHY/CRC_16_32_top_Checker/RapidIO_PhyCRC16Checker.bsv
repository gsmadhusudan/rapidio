/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- This Module is to check the CRC16 code developed for the input data.
-- 1.Design is implemented as mentioned in the RapidIO specification 3.1. 
-- 2.Function is developed to perform the CRC-16 checker (same function as used for generation in the specification). 
-- 3.Input to the data includes data appended with generated CRC16 and also zero padding if necessary.
--
-- To be done
-- 1.Crf yet to be implemented.
-- 2.Parameteristion of data width in each cycle and also width of rem value to be done later.
--
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_PhyCRC16Checker;


`define RIO_DATA 128
`define RIO_DATA_REM 3

// The Endian format is changed from Big Endian to little Endian
// Function is used to perform three level operation of CRC-16.
function Bit#(16) fn_CRC16Generation (Bit#(16) old_check_symbol, Bit#(16) data_in);

// 1st Level Operation (XORing Data and Old Symbol)
    Bit#(16) lv_IntermediateSymbol = data_in ^ old_check_symbol; 

// 2nd Level Operation (Equation Network)
    Bit#(1) lv_Check_Symbol_15 = lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[0];

    Bit#(1) lv_Check_Symbol_14 = lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];

    Bit#(1) lv_Check_Symbol_13 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];

    Bit#(1) lv_Check_Symbol_12 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];

    Bit#(1) lv_Check_Symbol_11 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4];
    
    Bit#(1) lv_Check_Symbol_10 = lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[1];  

    Bit#(1) lv_Check_Symbol_9 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];

    Bit#(1) lv_Check_Symbol_8 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];

    Bit#(1) lv_Check_Symbol_7 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];

    Bit#(1) lv_Check_Symbol_6 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[4];

    Bit#(1) lv_Check_Symbol_5 = lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[5];

    Bit#(1) lv_Check_Symbol_4 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[6];

    Bit#(1) lv_Check_Symbol_3 = lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[0];

    Bit#(1) lv_Check_Symbol_2 = lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[1];

    Bit#(1) lv_Check_Symbol_1 = lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[2];

    Bit#(1) lv_Check_Symbol_0 = lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[3];

// 3rd Level Operation - Generates Next Check Symbol 
    return {lv_Check_Symbol_0, lv_Check_Symbol_1, lv_Check_Symbol_2, lv_Check_Symbol_3, lv_Check_Symbol_4, lv_Check_Symbol_5, lv_Check_Symbol_6, lv_Check_Symbol_7, lv_Check_Symbol_8, lv_Check_Symbol_9, lv_Check_Symbol_10, lv_Check_Symbol_11, lv_Check_Symbol_12, lv_Check_Symbol_13, lv_Check_Symbol_14, lv_Check_Symbol_15};

endfunction 


interface Ifc_RapidIO_PhyCRC16Checker;

//input methods and output methods
 	method Action _link_rx_sof_n (Bool value);
 	method Action _link_rx_eof_n (Bool value);
 	method Action _link_rx_vld_n (Bool value);
 	method Action _link_rx_rem (Bit#(`RIO_DATA_REM) value);
	method Action _link_rx_data (Bit#(`RIO_DATA) value);
	//method Action _link_tx_crf (Bit#(2) value);    //yet to be implemented
 	
	method Bit#(16) output_CRC16_check_();//output value in register -- crc checksum -- if data is transmitted without error,then all zero,otherwise a non zero value is obtained

endinterface : Ifc_RapidIO_PhyCRC16Checker


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIO_PhyCRC16Checker(Ifc_RapidIO_PhyCRC16Checker);

//Input signals as Wires
Wire#(Bool) wr_rx_sof <- mkDWire (True);
Wire#(Bool) wr_rx_eof <- mkDWire (True);
Wire#(Bool) wr_rx_vld <- mkDWire (True);
Wire#(Bit#(`RIO_DATA_REM)) wr_rx_rem <- mkDWire (0);
Wire#(Bit#(`RIO_DATA)) wr_rx_data <- mkDWire (0);
//Wire#(Bit#(2)) wr_tx_crf <- mkDWire (0);


//Internal Wires and Register
Wire#(Bit#(16)) wr_CheckSymbolChk_0 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_1 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_2 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_3 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_4 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_5 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_6 <- mkDWire (0);
Wire#(Bit#(16)) wr_CheckSymbolChk_7 <- mkDWire (0);

Reg#(Bit#(16)) rg_OldCheckSymbol <- mkReg (0);


//Checking operation using corresponding bits for each tx_rem value
rule rl_CRC_16_Checker(wr_rx_vld == False);

	Bit#(16) lv_OldCheckSymbol = (wr_rx_sof == False) ? 16'hffff : rg_OldCheckSymbol;

	Bit#(16) lv_InitialData = (wr_rx_sof == False) ? {6'h00, wr_rx_data[(`RIO_DATA-7):(`RIO_DATA-16)]} : wr_rx_data[(`RIO_DATA-1):(`RIO_DATA-16)];

//tx_rem = 111
	Bit#(16) lv_CheckSymbolChk_7 = fn_CRC16Generation (lv_OldCheckSymbol, wr_rx_data[(`RIO_DATA-1):(`RIO_DATA-16)]);
	wr_CheckSymbolChk_7  <= fn_CRC16Generation (lv_OldCheckSymbol, wr_rx_data[(`RIO_DATA-1):(`RIO_DATA-16)]);//crc16 code appended at [127:112] in case of tx_rem = 111.


//tx_rem = 000
	Bit#(16) lv_CheckSymbolChk_0_1 = fn_CRC16Generation (lv_OldCheckSymbol, lv_InitialData);

	Bit#(16) lv_CheckSymbolChk_0 = fn_CRC16Generation (lv_CheckSymbolChk_0_1, wr_rx_data[(`RIO_DATA-17):(`RIO_DATA-32)]);
	wr_CheckSymbolChk_0  <= fn_CRC16Generation (lv_CheckSymbolChk_7, wr_rx_data[(`RIO_DATA-17):(`RIO_DATA-32)]);//crc16 code at [111:96]

//tx_rem = 001
	Bit#(16) lv_CheckSymbolChk_1 = fn_CRC16Generation (lv_CheckSymbolChk_0, wr_rx_data[(`RIO_DATA-33):(`RIO_DATA-48)]);
	wr_CheckSymbolChk_1 <= fn_CRC16Generation (lv_CheckSymbolChk_0, wr_rx_data[(`RIO_DATA-33):(`RIO_DATA-48)]);//crc16 code at [95:80]

//tx_rem = 010
	Bit#(16) lv_CheckSymbolChk_2 = fn_CRC16Generation (lv_CheckSymbolChk_1, wr_rx_data[(`RIO_DATA-49):(`RIO_DATA-64)]);
	wr_CheckSymbolChk_2 <= fn_CRC16Generation (lv_CheckSymbolChk_1, wr_rx_data[(`RIO_DATA-49):(`RIO_DATA-64)]);//crc16 code at [79:64]

//tx_rem = 011
	Bit#(16) lv_CheckSymbolChk_3 = fn_CRC16Generation (lv_CheckSymbolChk_2, wr_rx_data[(`RIO_DATA-65):(`RIO_DATA-80)]);
	wr_CheckSymbolChk_3 <= fn_CRC16Generation (lv_CheckSymbolChk_2, wr_rx_data[(`RIO_DATA-65):(`RIO_DATA-80)]);//crc16 code at [63:48]

//tx_rem = 100
	Bit#(16) lv_CheckSymbolChk_4 = fn_CRC16Generation (lv_CheckSymbolChk_3, wr_rx_data[(`RIO_DATA-81):(`RIO_DATA-96)]);
	wr_CheckSymbolChk_4 <= fn_CRC16Generation (lv_CheckSymbolChk_3, wr_rx_data[(`RIO_DATA-81):(`RIO_DATA-96)]);//crc16 code at [47:32]

//tx_rem = 101
	Bit#(16) lv_CheckSymbolChk_5 = fn_CRC16Generation (lv_CheckSymbolChk_4, wr_rx_data[(`RIO_DATA-97):(`RIO_DATA-112)]);
	wr_CheckSymbolChk_5 <= fn_CRC16Generation (lv_CheckSymbolChk_4, wr_rx_data[(`RIO_DATA-97):(`RIO_DATA-112)]);//crc16 code at [31:16]

//tx_rem = 110
	Bit#(16) lv_CheckSymbolChk_6 = fn_CRC16Generation (lv_CheckSymbolChk_5, wr_rx_data[(`RIO_DATA-113):(`RIO_DATA-`RIO_DATA)]);
	wr_CheckSymbolChk_6 <= fn_CRC16Generation (lv_CheckSymbolChk_5, wr_rx_data[(`RIO_DATA-113):(`RIO_DATA-`RIO_DATA)]);//crc16 code at [15:0]


	rg_OldCheckSymbol <= fn_CRC16Generation (lv_CheckSymbolChk_5, wr_rx_data[(`RIO_DATA-113):(`RIO_DATA-`RIO_DATA)]);//storing crc value for use in next cycle if required

endrule 


//Input and Output method definitions
method Action _link_rx_sof_n (Bool value);
	wr_rx_sof <= value;
endmethod 

method Action _link_rx_eof_n (Bool value);
	wr_rx_eof <= value;
endmethod 

method Action _link_rx_vld_n (Bool value);
	wr_rx_vld <= value; 
endmethod 

method Action _link_rx_data (Bit#(`RIO_DATA) value);
	wr_rx_data <= value;
endmethod
 
method Action _link_rx_rem (Bit#(`RIO_DATA_REM) value);
	wr_rx_rem <= value; 
endmethod 

/*method Action _link_tx_crf (Bit#(2) value);
	wr_tx_crf <= value; 
endmethod */

method Bit#(16) output_CRC16_check_();
	if (wr_rx_eof == False) 
	begin
	if (wr_rx_rem == 3'b000)
		return wr_CheckSymbolChk_0;
	else if (wr_rx_rem == 3'b001)
		return wr_CheckSymbolChk_1;
	else if (wr_rx_rem == 3'b010)
		return wr_CheckSymbolChk_2;
	else if (wr_rx_rem == 3'b011)
		return wr_CheckSymbolChk_3;
	else if (wr_rx_rem == 3'b100)
		return wr_CheckSymbolChk_4;
	else if (wr_rx_rem == 3'b101)
		return wr_CheckSymbolChk_5;
	else if (wr_rx_rem == 3'b110)
		return wr_CheckSymbolChk_6;
	else 
		return wr_CheckSymbolChk_7;
	end 
	else 
		return 0; 
endmethod 


endmodule:mkRapidIO_PhyCRC16Checker
endpackage:RapidIO_PhyCRC16Checker
