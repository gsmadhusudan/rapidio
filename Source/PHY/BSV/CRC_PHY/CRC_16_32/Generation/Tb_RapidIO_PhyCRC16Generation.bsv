/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- This Module is to test the generation of CRC16 code for an input data and it is connected to CRC16 generation module.
-- Input is given which is 128 bits wide in each cycle with appropriate start of frame(sof),end of frame(eof) etc.
-- 
-- To be done
-- 1.Crf yet to be implemented.
-- 2.Parameteristion of data width in each cycle and also width of rem value to be done later.
-- 
-- Author(s):
-- Gopinathan (gopinathan18@gmail.com)
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package Tb_RapidIO_PhyCRC16Generation;

import RapidIO_PhyCRC16Generation ::*;
import LFSR:: *;


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkTb_RapidIO_PhyCRC16Generation(Empty);

Ifc_RapidIO_PhyCRC16Generation crc_16 <- mkRapidIO_PhyCRC16Generation;
LFSR#(Bit#(128)) lfsr <- mkFeedLFSR(128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5);//polynomial given to lfsr

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 10)
		$finish (0);
endrule


rule r1_lfsr_start(reg_ref_clk == 1);
lfsr.seed(128'h66366);//initial value given to lfsr
endrule:r1_lfsr_start

rule r1(reg_ref_clk == 2); 
crc_16._link_tx_sof_n (False);
crc_16._link_tx_vld_n (False);
crc_16._link_tx_data (lfsr.value);
$display("Input 1 = %h" , lfsr.value);
crc_16._link_tx_eof_n (False);
crc_16._link_tx_rem (3'b010);
$display("REM value == 3'b010");
lfsr.next;//shifting operation for obtaining next value
endrule:r1


rule r1_1(reg_ref_clk == 3); 
crc_16._link_tx_sof_n (False);
crc_16._link_tx_vld_n (False);
crc_16._link_tx_data (lfsr.value);
$display("Input 2 = %h" , lfsr.value);
crc_16._link_tx_eof_n (False);
crc_16._link_tx_rem (3'b001);
$display("REM value == 3'b001");
lfsr.next;
endrule:r1_1

rule r1_4(reg_ref_clk == 4); 
crc_16._link_tx_sof_n (False);
crc_16._link_tx_vld_n (False);
crc_16._link_tx_data (lfsr.value);
$display("Input 3_1 = %h" , lfsr.value);
crc_16._link_tx_eof_n (True);
crc_16._link_tx_rem (3'b111);
$display("REM value == 3'b111");
lfsr.next;
endrule:r1_4

rule r1_5(reg_ref_clk == 5); 
crc_16._link_tx_sof_n (True);
crc_16._link_tx_vld_n (False);
crc_16._link_tx_data (lfsr.value);
$display("Input 3_2 = %h" , lfsr.value);
crc_16._link_tx_eof_n (True);
crc_16._link_tx_rem (3'b111);
$display("REM value == 3'b111");
lfsr.next;
endrule:r1_5

rule r1_6(reg_ref_clk == 6); 
crc_16._link_tx_sof_n (True);
crc_16._link_tx_vld_n (False);
crc_16._link_tx_data (lfsr.value);
$display("Input 3_3 = %h" , lfsr.value);
crc_16._link_tx_eof_n (False);
crc_16._link_tx_rem (3'b011);
$display("REM value == 3'b011");
lfsr.next;
endrule:r1_6

rule r2;
$display("CRC code=%b",crc_16.outputs_CRC16_ ());
endrule:r2

endmodule: mkTb_RapidIO_PhyCRC16Generation
endpackage: Tb_RapidIO_PhyCRC16Generation
