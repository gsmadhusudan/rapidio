1 - Basic register that is to be used in the buffer. Write and read mechanisms from a register.
2 - A 12-bit ackid generator that starts from 12'b0 to 12'hfff.
3 - Given a look up table for storing the ackid,corresponding prio section and the pointer value of the packet.
4 - Basic functionalities in case of a buffer with just two registers.
5 - Final versions of individual prio sections with functionalities like retransmission and dequeuing.
6 - Integrated buffer section in which writing and normal transmission according to priority is implemented.
