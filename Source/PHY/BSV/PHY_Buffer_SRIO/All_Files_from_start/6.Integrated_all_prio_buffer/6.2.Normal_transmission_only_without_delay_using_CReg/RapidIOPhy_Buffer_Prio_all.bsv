package RapidIOPhy_Buffer_Prio_all;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer00 ::*;
import RapidIOPhy_Buffer01 ::*;
import RapidIOPhy_Buffer10 ::*;
import RapidIOPhy_Buffer11 ::*;
import AckId_Lut::*;
import AckId_generator::*;


interface Ifc_RapidIOPhy_Buffer_Prio_all;
   //interface Ifc_RapidIOPhy_Buffer00 _RapidIOPhy_Buffer00;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   //method Action _tx_rdy_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf(Bool value);
   //method Action _tx_read_en(Bit#(1) value);
   //method Action _tx_rg12(Bit#(4) value);
//dequeing when packet accepted
   method Action _tx_deq(Bit#(1) value);
   //method Action _ack_id(Bit#(2) value);
   //method Action _tx_ack(Bit#(4) value);
   //method Action _tx_read(Bit#(1) value);
//enabling retxmsn at the time of restart from retry
   method Action _tx_retransmsn(Bit#(1) value);
//to stop txmtng when either PNA or PR control symbol comes        
   method Action _tx_stop_txmsn(Bit#(1) value); 
//retransmitting required ackid(PNA or PR)       
   method Action _ack_id_retrans(Bit#(6) value);  
//dequeing the acknowledged ackid(PA)             
   method Action _ack_id_deq(Bit#(6) value); 
   
   
   //method BufferData buf_out_();
   //method RegBuf buf_out_deq_();
   method Bool lnk_tvld_n_();
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bit#(4) lnk_trem_();
   method Bool lnk_tcrf_();

//to indicate to top module that txmtr has stopped txmtng so that RFR can be sent. 
   method Bit#(1) tx_stopped_txmsn_(); 
//to view written data to buffer
   method BufferData buf_out_00_();
   method BufferData buf_out_01_();
   method BufferData buf_out_10_();
   method BufferData buf_out_11_();
  

endinterface:Ifc_RapidIOPhy_Buffer_Prio_all

(* synthesize *)
(* always_enabled *)
(* always_ready *)

//(* descending_urgency = "section_en_11, section_en_10, section_en_01, section_en_00" *)
(* descending_urgency = "read_en_prio_11, read_en_prio_10, read_en_prio_01, read_en_prio_00" *)
//(* descending_urgency = "read_disabl_also, read_enbl, read_en_prio_11, read_en_prio_10, read_en_prio_01, read_en_prio_00" *)
//(* descending_urgency = "read_en_prio_01, read_en_prio_00" *)
module mkRapidIOPhy_Buffer_Prio_all(Ifc_RapidIOPhy_Buffer_Prio_all);

Ifc_RapidIOPhy_Buffer00 bufer_00 <- mkRapidIOPhy_Buffer00;
Ifc_RapidIOPhy_Buffer01 bufer_01 <- mkRapidIOPhy_Buffer01;
Ifc_RapidIOPhy_Buffer10 bufer_10 <- mkRapidIOPhy_Buffer10;
Ifc_RapidIOPhy_Buffer11 bufer_11 <- mkRapidIOPhy_Buffer11;

Ifc_AckId_generator ack1 <- mkAckId_generator;
Ifc_AckId_Lut lut <- mkAckId_Lut;

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bool) wr_tx_crf <- mkDWire(True);

Wire#(Bit#(4)) wr_read_ptr_00 <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr_01 <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr_10 <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr_11 <- mkDWire(0);

Wire#(Bit#(4)) wr_write_ptr_00 <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_01 <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_10 <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_11 <- mkDWire(0);

Wire#(Bit#(1)) wr_valid_check_out_00 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_check_out_01 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_check_out_10 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_check_out_11 <- mkDWire(0);

Wire#(Bit#(1)) wr_valid_next_check_out_00 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_next_check_out_01 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_next_check_out_10 <- mkDWire(0);
Wire#(Bit#(1)) wr_valid_next_check_out_11 <- mkDWire(0);

Wire#(Bit#(4)) wr_read_ptr_from_lut <- mkDWire(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_00 <- mkReg(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_01 <- mkReg(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_10 <- mkReg(0);
Reg#(Bit#(4)) rg_read_ptr_from_lut_11 <- mkReg(0);


Wire#(BufferData) buf_out_00 <- mkDWire(defaultValue);
Wire#(BufferData) buf_out_01 <- mkDWire(defaultValue);
Wire#(BufferData) buf_out_10 <- mkDWire(defaultValue);
Wire#(BufferData) buf_out_11 <- mkDWire(defaultValue);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4))  wr_lnk_trem <- mkDWire(0);
Wire#(Bool)  wr_lnk_tcrf <- mkDWire(True);
//output to indicate txmsn has stopped
Wire#(Bit#(1))  wr_txmsn_stoped_out <- mkDWire(0);
//signal to indicate to stop txmsn
Wire#(Bit#(1)) wr_stop_txmsn <- mkDWire(0);
//signal to indicate that pkt has been accepted
Wire#(Bit#(1)) wr_deq <- mkDWire(0);
//signal to retransmit
Wire#(Bit#(1)) wr_retrans <- mkDWire(0);
//ackid from which pakts should be retransmitted
Wire#(Bit#(6))  wr_ackid_retrans <- mkDWire(0);
//ackid till which pakts should be dequeued
Wire#(Bit#(6))  wr_ackid_deq <- mkDWire(0);
//to check whether there is valid entry in lut
Wire#(Bit#(1)) wr_lut_valid_out <- mkDWire(0);
Wire#(Bit#(2)) wr_lut_prio_out <- mkDWire(0);
Wire#(Bit#(6))  wr_tx_ack <- mkDWire(0);

Reg#(Bit#(2)) rg_prio <- mkReg(0);
//to ensure that txmsn has been stopped till wr_retrans comes
Reg#(Bit#(1)) rg_stop_txmsn <- mkReg(0);
//to identify from which buffer section reading is happening
Reg#(Bit#(1)) rg_buffer_00_valid <- mkReg(0);
Reg#(Bit#(1)) rg_buffer_01_valid <- mkReg(0);
Reg#(Bit#(1)) rg_buffer_10_valid <- mkReg(0);
Reg#(Bit#(1)) rg_buffer_11_valid <- mkReg(0);
/*
Reg#(Bit#(1)) rg_section_00_valid[2] <- mkCReg(2,0);
Reg#(Bit#(1)) rg_section_01_valid[2] <- mkCReg(2,0);
Reg#(Bit#(1)) rg_section_10_valid[2] <- mkCReg(2,0);
Reg#(Bit#(1)) rg_section_11_valid[2] <- mkCReg(2,0);
*/

Reg#(Bit#(1)) rg_deq <- mkReg(0);
Reg#(Bit#(6)) rg_ackid_deq <- mkReg(0);
Reg#(Bit#(6)) rg_ackid_valid <- mkReg(0);
//to validate the read pointers while reloading in case of retransmission
Reg#(Bit#(1)) rg_lut_00 <- mkReg(0);
Reg#(Bit#(1)) rg_lut_01 <- mkReg(0);
Reg#(Bit#(1)) rg_lut_10 <- mkReg(0);
Reg#(Bit#(1)) rg_lut_11 <- mkReg(0);
Reg#(Bit#(1)) rg_retrans <- mkReg(0);



//writing to buffer according to priority
rule prio_identification(wr_tx_vld_n == False && wr_tx_sof_n == False);

	rg_prio <= wr_tx_data[119:118];
	if(wr_tx_data[119:118] == 2'b00)
		begin
		bufer_00._tx_sof_n(wr_tx_sof_n);
		bufer_00._tx_eof_n(wr_tx_eof_n);
		bufer_00._tx_vld_n(wr_tx_vld_n);
		bufer_00._tx_data(wr_tx_data);
		bufer_00._tx_rem(wr_tx_rem);
		bufer_00._tx_crf(wr_tx_crf);
		end

	else if(wr_tx_data[119:118] == 2'b01)
		begin
		bufer_01._tx_sof_n(wr_tx_sof_n);
		bufer_01._tx_eof_n(wr_tx_eof_n);
		bufer_01._tx_vld_n(wr_tx_vld_n);
		bufer_01._tx_data(wr_tx_data);
		bufer_01._tx_rem(wr_tx_rem);
		bufer_01._tx_crf(wr_tx_crf);
		end

	else if(wr_tx_data[119:118] == 2'b10)
		begin
		bufer_10._tx_sof_n(wr_tx_sof_n);
		bufer_10._tx_eof_n(wr_tx_eof_n);
		bufer_10._tx_vld_n(wr_tx_vld_n);
		bufer_10._tx_data(wr_tx_data);
		bufer_10._tx_rem(wr_tx_rem);
		bufer_10._tx_crf(wr_tx_crf);
		end

	else if(wr_tx_data[119:118] == 2'b11)
		begin
		bufer_11._tx_sof_n(wr_tx_sof_n);
		bufer_11._tx_eof_n(wr_tx_eof_n);
		bufer_11._tx_vld_n(wr_tx_vld_n);
		bufer_11._tx_data(wr_tx_data);
		bufer_11._tx_rem(wr_tx_rem);
		bufer_11._tx_crf(wr_tx_crf);
		end

endrule


//writing continuous data to buffer according to prio
rule writing_according_to_prio(wr_tx_vld_n == False && wr_tx_sof_n == True);

	if(rg_prio == 2'b00)
		begin
		bufer_00._tx_sof_n(wr_tx_sof_n);
		bufer_00._tx_eof_n(wr_tx_eof_n);
		bufer_00._tx_vld_n(wr_tx_vld_n);
		bufer_00._tx_data(wr_tx_data);
		bufer_00._tx_rem(wr_tx_rem);
		bufer_00._tx_crf(wr_tx_crf);
		end

	else if(rg_prio == 2'b01)
		begin
		bufer_01._tx_sof_n(wr_tx_sof_n);
		bufer_01._tx_eof_n(wr_tx_eof_n);
		bufer_01._tx_vld_n(wr_tx_vld_n);
		bufer_01._tx_data(wr_tx_data);
		bufer_01._tx_rem(wr_tx_rem);
		bufer_01._tx_crf(wr_tx_crf);
		end

	else if(rg_prio == 2'b10)
		begin
		bufer_10._tx_sof_n(wr_tx_sof_n);
		bufer_10._tx_eof_n(wr_tx_eof_n);
		bufer_10._tx_vld_n(wr_tx_vld_n);
		bufer_10._tx_data(wr_tx_data);
		bufer_10._tx_rem(wr_tx_rem);
		bufer_10._tx_crf(wr_tx_crf);
		end

	else if(rg_prio == 2'b11)
		begin
		bufer_11._tx_sof_n(wr_tx_sof_n);
		bufer_11._tx_eof_n(wr_tx_eof_n);
		bufer_11._tx_vld_n(wr_tx_vld_n);
		bufer_11._tx_data(wr_tx_data);
		bufer_11._tx_rem(wr_tx_rem);
		bufer_11._tx_crf(wr_tx_crf);
		end

endrule

//ackid assignment
rule ackid_assign;
	bufer_00._tx_ack(wr_tx_ack);
	bufer_01._tx_ack(wr_tx_ack);
	bufer_10._tx_ack(wr_tx_ack);
	bufer_11._tx_ack(wr_tx_ack);
endrule



rule read_ptr_in;

//giving retried ackid to ackid generator in case of retransmission
	ack1._retrans(wr_stop_txmsn);
//$display("");
	rg_retrans <= wr_retrans;
	
//read_pointer location
	wr_read_ptr_00 <= bufer_00.out_rd_ptr_();
	wr_read_ptr_01 <= bufer_01.out_rd_ptr_();
	wr_read_ptr_10 <= bufer_10.out_rd_ptr_();
	wr_read_ptr_11 <= bufer_11.out_rd_ptr_();

//write pointer location
	wr_write_ptr_00 <= bufer_00.out_wrt_ptr_();
	wr_write_ptr_01 <= bufer_01.out_wrt_ptr_();
	wr_write_ptr_10 <= bufer_10.out_wrt_ptr_();
	wr_write_ptr_11 <= bufer_11.out_wrt_ptr_();

//to check wheteher data is present in the current location
	wr_valid_check_out_00 <= bufer_00.valid_check_out_();
	wr_valid_check_out_01 <= bufer_01.valid_check_out_();
	wr_valid_check_out_10 <= bufer_10.valid_check_out_();
	wr_valid_check_out_11 <= bufer_11.valid_check_out_();

//to check wheteher data is present in the next location
	wr_valid_next_check_out_00 <= bufer_00.valid_next_check_out_();
	wr_valid_next_check_out_01 <= bufer_01.valid_next_check_out_();
	wr_valid_next_check_out_10 <= bufer_10.valid_next_check_out_();
	wr_valid_next_check_out_11 <= bufer_11.valid_next_check_out_();

//to reload read pointers in case of retransmission
	wr_read_ptr_from_lut <= lut.read_ptr_out_();
//to check whether there is valid entry in lut
	wr_lut_valid_out <= lut.valid_out_();
//to identify prio section for dequeing 
	wr_lut_prio_out <= lut.prio_out_();
//output from ackid generator
	wr_tx_ack <= ack1.ack_id_out_();
/*
	if(wr_lnk_teof_n == False)//while reading, buffer_valid == 11 and after reading that particular data,buffer_valid == 10  
		begin
		rg_buffer_00_valid <= 2'b10;
		rg_buffer_01_valid <= 2'b10;
		rg_buffer_10_valid <= 2'b10;
		rg_buffer_11_valid <= 2'b10;
		end
*/
endrule

rule displ_pointrs;
	$display("write_ptr_00 = %b",wr_write_ptr_00);
	$display("write_ptr_01 = %b",wr_write_ptr_01);
	$display("write_ptr_10 = %b",wr_write_ptr_10);
	$display("write_ptr_11 = %b",wr_write_ptr_11);

	$display("read_ptr_00 = %b",wr_read_ptr_00);
	$display("read_ptr_01 = %b",wr_read_ptr_01);
	$display("read_ptr_10 = %b",wr_read_ptr_10);
	$display("read_ptr_11 = %b",wr_read_ptr_11);

	$display("rg_stop_txmsn = %b",rg_stop_txmsn);

	$display("wr_valid_check_out_00 = %b",wr_valid_check_out_00);
	$display("wr_valid_check_out_01 = %b",wr_valid_check_out_01);
	$display("wr_valid_check_out_10 = %b",wr_valid_check_out_10);
	$display("wr_valid_check_out_11 = %b",wr_valid_check_out_11);

	$display("Ackid - %b",wr_tx_ack);
endrule
/*
rule ending(wr_lnk_teof_n == False);
		rg_buffer_00_valid <= 2'b10;
		rg_buffer_01_valid <= 2'b10;
		rg_buffer_10_valid <= 2'b10;
		rg_buffer_11_valid <= 2'b10;
endrule


rule read_disabl_eof(rg_buffer_00_valid == 2'b10 || rg_buffer_01_valid == 2'b10 || rg_buffer_10_valid == 2'b10 || rg_buffer_11_valid == 2'b10);
	bufer_00._tx_read_en(1'b0);
	bufer_01._tx_read_en(1'b0);
	bufer_10._tx_read_en(1'b0);
	bufer_11._tx_read_en(1'b0);
endrule
*/


//initial read enabling by threshold crossing(transmission starts if any one section crosses threshold value)
rule r1_rd_en_ptr((wr_write_ptr_00 >= 5 && wr_read_ptr_00 == 0) || (wr_write_ptr_01 >= 4 && wr_read_ptr_01 == 0) || (wr_write_ptr_10 >= 3 && wr_read_ptr_10 == 0) || (wr_write_ptr_11 >= 2 && wr_read_ptr_11 == 0));

	bufer_00._read_ptr_valid(4'b0001);
	bufer_01._read_ptr_valid(4'b0001);
	bufer_10._read_ptr_valid(4'b0001);
	bufer_11._read_ptr_valid(4'b0001);

	if(wr_write_ptr_00 >= 5 && wr_read_ptr_00 == 0)
		begin	
		//bufer_00._tx_section_en(1'b1);	
		bufer_00._tx_read_en(1'b1);
		rg_buffer_00_valid <= 1'b1;
		end
	else if(wr_write_ptr_01 >= 4 && wr_read_ptr_01 == 0)
		begin
		//bufer_01._tx_section_en(1'b1);			
		bufer_01._tx_read_en(1'b1);
		rg_buffer_01_valid <= 1'b1;	
		end	
	else if(wr_write_ptr_10 >= 3 && wr_read_ptr_10 == 0)
		begin
		//bufer_10._tx_section_en(1'b1);			
		bufer_10._tx_read_en(1'b1);
		rg_buffer_10_valid <= 1'b1;
		end		
	else if(wr_write_ptr_11 >= 2 && wr_read_ptr_11 == 0)
		begin
		//bufer_11._tx_section_en(1'b1);			
		bufer_11._tx_read_en(1'b1);
		rg_buffer_11_valid <= 1'b1;
		end		
endrule






/*
rule section_en_00(wr_lnk_tsof_n == False && ((rg_buffer_00_valid == 1 && wr_valid_next_check_out_00 == 1'b1) || (rg_buffer_00_valid == 0 && wr_valid_check_out_00 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);

bufer_00._tx_section_en(1'b1);
bufer_01._tx_section_en(1'b0);
bufer_10._tx_section_en(1'b0);
bufer_11._tx_section_en(1'b0);

		rg_section_00_valid[0] <= 1'b1;
		rg_section_01_valid[0] <= 1'b0;
		rg_section_10_valid[0] <= 1'b0;
		rg_section_11_valid[0] <= 1'b0; 

		

endrule

rule section_en_01(wr_lnk_tsof_n == False && ((rg_buffer_01_valid == 1 && wr_valid_next_check_out_01 == 1'b1) || (rg_buffer_01_valid == 0 && wr_valid_check_out_01 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);

bufer_00._tx_section_en(1'b0);
bufer_01._tx_section_en(1'b1);
bufer_10._tx_section_en(1'b0);
bufer_11._tx_section_en(1'b0);

		rg_section_00_valid[0] <= 1'b0;
		rg_section_01_valid[0] <= 1'b1;
		rg_section_10_valid[0] <= 1'b0;
		rg_section_11_valid[0] <= 1'b0;

		


endrule

rule section_en_10(wr_lnk_tsof_n == False && ((rg_buffer_10_valid == 1 && wr_valid_next_check_out_10 == 1'b1) || (rg_buffer_10_valid == 0 && wr_valid_check_out_10 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);

bufer_00._tx_section_en(1'b0);
bufer_01._tx_section_en(1'b0);
bufer_10._tx_section_en(1'b1);
bufer_11._tx_section_en(1'b0);

		rg_section_00_valid[0] <= 1'b0;
		rg_section_01_valid[0] <= 1'b0;
		rg_section_10_valid[0] <= 1'b1;
		rg_section_11_valid[0] <= 1'b0;

		
endrule

rule section_en_11(wr_lnk_tsof_n == False && ((rg_buffer_11_valid == 1 && wr_valid_next_check_out_11 == 1'b1) || (rg_buffer_11_valid == 0 && wr_valid_check_out_11 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);

bufer_00._tx_section_en(1'b0);
bufer_01._tx_section_en(1'b0);
bufer_10._tx_section_en(1'b0);
bufer_11._tx_section_en(1'b1);

		rg_section_00_valid[0] <= 1'b0;
		rg_section_01_valid[0] <= 1'b0;
		rg_section_10_valid[0] <= 1'b0;
		rg_section_11_valid[0] <= 1'b1;

		

endrule
*/

//valid is checked for next pointer so when only when 1 packet is there in 11 fifo it doesnt read that
//when the last beat of packet is being transmitted,it checks for next valid packet in same buffer which is being read or current packet in other sections
//Higher prio section will get enabled-descending urgency attribute
rule read_en_prio_00(wr_lnk_teof_n == False && ((rg_buffer_00_valid == 1 && wr_valid_next_check_out_00 == 1'b1) || (rg_buffer_00_valid == 0 && wr_valid_check_out_00 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);
//&& rg_section_00_valid[1] == 1
	bufer_00._tx_read_en(1'b1);
	bufer_01._tx_read_en(1'b0);
	bufer_10._tx_read_en(1'b0);
	bufer_11._tx_read_en(1'b0);
	//ack1._ack_en(False);
	//rg_buffer_00_valid <= 2'b11;
		rg_buffer_00_valid <= 1'b1;
		rg_buffer_01_valid <= 1'b0;
		rg_buffer_10_valid <= 1'b0;
		rg_buffer_11_valid <= 1'b0; 
endrule


rule read_en_prio_01(wr_lnk_teof_n == False && ((rg_buffer_01_valid == 1 && wr_valid_next_check_out_01 == 1'b1) || (rg_buffer_01_valid == 0 && wr_valid_check_out_01 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);
	bufer_01._tx_read_en(1'b1);
	bufer_00._tx_read_en(1'b0);
	bufer_10._tx_read_en(1'b0);
	bufer_11._tx_read_en(1'b0);
	//ack1._ack_en(False);
	//rg_buffer_01_valid <= 2'b11; 
		rg_buffer_00_valid <= 1'b0;
		rg_buffer_01_valid <= 1'b1;
		rg_buffer_10_valid <= 1'b0;
		rg_buffer_11_valid <= 1'b0;
endrule

rule read_en_prio_10(wr_lnk_teof_n == False && ((rg_buffer_10_valid == 1 && wr_valid_next_check_out_10 == 1'b1) || (rg_buffer_10_valid == 0 && wr_valid_check_out_10 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);
	bufer_10._tx_read_en(1'b1);
	bufer_00._tx_read_en(1'b0);
	bufer_01._tx_read_en(1'b0);
	bufer_11._tx_read_en(1'b0);
	//ack1._ack_en(False);
	//rg_buffer_10_valid <= 2'b11;
		rg_buffer_00_valid <= 1'b0;
		rg_buffer_01_valid <= 1'b0;
		rg_buffer_10_valid <= 1'b1;
		rg_buffer_11_valid <= 1'b0; 
endrule

rule read_en_prio_11(wr_lnk_teof_n == False && ((rg_buffer_11_valid == 1 && wr_valid_next_check_out_11 == 1'b1) || (rg_buffer_11_valid == 0 && wr_valid_check_out_11 == 1'b1)) && wr_stop_txmsn != 1 && wr_retrans != 1);
	bufer_11._tx_read_en(1'b1);
	bufer_00._tx_read_en(1'b0);
	bufer_01._tx_read_en(1'b0);
	bufer_10._tx_read_en(1'b0);

	//rg_buffer_11_valid <= 2'b11; 
		rg_buffer_00_valid <= 1'b0;
		rg_buffer_01_valid <= 1'b0;
		rg_buffer_10_valid <= 1'b0;
		rg_buffer_11_valid <= 1'b1;
endrule
//whenever sof comes ackid gets assigned and then signal is given to ackid generator to increment
rule ack_incrmnt(wr_lnk_tsof_n == False);//ack incrmnt sgnl from basic module is replaced by sof
	ack1._ack_en(False);
endrule

//storing to lut while transmitting with ackid(ackid-prio section-pointer value)
rule lut_store(wr_stop_txmsn != 1  && wr_deq != 1 && wr_lnk_tsof_n == False);//only when sof is there storing to lut will happen
		lut._store(True);
		lut._ackid_store(wr_tx_ack); //== 0 ? 6'b111111 : (ack1.ack_id_out_() - 1'b1));ack1.ack_id_out_()
		if(rg_buffer_11_valid == 1)
			begin
			lut._rd_ptr_in(wr_read_ptr_11);
			lut._prio_in(2'b11);
			end
		else if(rg_buffer_10_valid == 1)
			begin
			lut._rd_ptr_in(wr_read_ptr_10);
			lut._prio_in(2'b10);
			end
		else if(rg_buffer_01_valid == 1)
			begin
			lut._rd_ptr_in(wr_read_ptr_01);
			lut._prio_in(2'b01);
			end
		else if(rg_buffer_00_valid == 1)
			begin
			lut._rd_ptr_in(wr_read_ptr_00);
			lut._prio_in(2'b00);
			end
endrule

//PNA or PR comes
rule read_disabl(wr_stop_txmsn == 1 && wr_deq == 0);
//retreiving position of given ackid from lut in case of retransmsn
	

	rg_ackid_valid <= wr_ackid_retrans;
	
//retried ackid given to ackid generator
	ack1._ack_in(wr_ackid_retrans);
//reloading read and write count values in basic register
	bufer_11._tx_stop_txmsn(wr_stop_txmsn);
	bufer_10._tx_stop_txmsn(wr_stop_txmsn);
	bufer_01._tx_stop_txmsn(wr_stop_txmsn);
	bufer_00._tx_stop_txmsn(wr_stop_txmsn);

	
	rg_buffer_00_valid <= 1'b0;
	rg_buffer_01_valid <= 1'b0;
	rg_buffer_10_valid <= 1'b0;
	rg_buffer_11_valid <= 1'b0;
//signal to top module that txmsn has stopped
	wr_txmsn_stoped_out <= 1'b1;
	rg_stop_txmsn <= 1'b1;
	//rg_lut <= 1;

//to find out valid entry from each section of buffer
	rg_lut_00 <= 1;
        rg_lut_01 <= 1;
	rg_lut_10 <= 1;
	rg_lut_11 <= 1;
$display("Error reported:Ackid to be retransmitted - %b",wr_ackid_retrans);
endrule

rule read_disabl_also(wr_stop_txmsn == 1);//wr_deq == 0
//disabling buffer sections
	bufer_11._tx_read_en(~(wr_stop_txmsn));
	bufer_10._tx_read_en(~(wr_stop_txmsn));
	bufer_01._tx_read_en(~(wr_stop_txmsn));
	bufer_00._tx_read_en(~(wr_stop_txmsn));
//identifying location from lut
	lut._identify(True);
	lut._ackid_retrans(wr_ackid_retrans);
	lut._ackid_valid(wr_ackid_retrans);
endrule

//all packets from retried ackid must be retransmitted
rule rl1(rg_stop_txmsn == 1);
	if(rg_ackid_valid == 6'b111111)
		rg_ackid_valid <= 6'b0;
	else
		rg_ackid_valid <= rg_ackid_valid + 1;
endrule

rule into_lut(wr_stop_txmsn != 1 && rg_stop_txmsn == 1);
	lut._ackid_valid(rg_ackid_valid);
	//if(rg_lut_00 == 0 && rg_lut_01 == 0 && rg_lut_10 == 0 && rg_lut_11 == 0)
		//rg_lut_check <= 0;	
endrule

//reloading of read pointer in every section
rule read_ptr_reloading(rg_stop_txmsn == 1);
	if(wr_lut_valid_out == 1'b1)
		begin		
		if(lut.prio_out_valid_() == 2'b00 && rg_lut_00 == 1)
			begin

			rg_read_ptr_from_lut_00 <= wr_read_ptr_from_lut;
			rg_lut_00 <= 0;
			end 
		else if(lut.prio_out_valid_() == 2'b01 && rg_lut_01 == 1)
			begin

			rg_read_ptr_from_lut_01 <= wr_read_ptr_from_lut;
			rg_lut_01 <= 0;
			end 
		else if(lut.prio_out_valid_() == 2'b10 && rg_lut_10 == 1)
			begin

			rg_read_ptr_from_lut_10 <= wr_read_ptr_from_lut;
			rg_lut_10 <= 0; 
			end
		else if(lut.prio_out_valid_() == 2'b11 && rg_lut_11 == 1)
			begin

			rg_read_ptr_from_lut_11 <= wr_read_ptr_from_lut;
			rg_lut_11 <= 0;
			end
		else
			rg_stop_txmsn <= 0;
		end
	else
		rg_stop_txmsn <= 0;
endrule 

//Restart from Retry
rule read_enblll(wr_retrans == 1);
	

	bufer_00._tx_retransmsn(wr_retrans);
	bufer_01._tx_retransmsn(wr_retrans);
	bufer_10._tx_retransmsn(wr_retrans);
	bufer_11._tx_retransmsn(wr_retrans);

	if(rg_lut_00 == 0)
			bufer_00._read_ptr_valid(rg_read_ptr_from_lut_00);
	else
			bufer_00._read_ptr_valid(wr_read_ptr_00);

	if(rg_lut_01 == 0)
        		bufer_01._read_ptr_valid(rg_read_ptr_from_lut_01);
	else
			bufer_01._read_ptr_valid(wr_read_ptr_01);

	if(rg_lut_10 == 0)
        		bufer_10._read_ptr_valid(rg_read_ptr_from_lut_10);
	else
			bufer_10._read_ptr_valid(wr_read_ptr_10);

	if(rg_lut_11 == 0)
        		bufer_11._read_ptr_valid(rg_read_ptr_from_lut_11);
	else
			bufer_11._read_ptr_valid(wr_read_ptr_11);
endrule

rule read_enbl(rg_retrans == 1);
	//rg_read_en <= 1'b1;
	
		
	//rg_stop_txmsn <= 1'b0;
$display("entering..");
//identifying section of buffer from which retransmission should start
	if(lut.prio_out_() == 2'b00)
		begin
		bufer_00._tx_read_en(1'b1);
		rg_buffer_00_valid <= 1'b1;
		//rg_buffer_01_valid <= 2'b00;
		//rg_buffer_10_valid <= 2'b00;
		//rg_buffer_11_valid <= 2'b00;
		end
	//bufer_00._read_ptr_valid(wr_read_ptr_from_lut);
	if(lut.prio_out_() == 2'b01)
		begin
		//rg_buffer_00_valid <= 2'b00;
		rg_buffer_01_valid <= 1'b1;
		//rg_buffer_10_valid <= 2'b00;
		//rg_buffer_11_valid <= 2'b00;
		bufer_01._tx_read_en(1'b1);
		end
	//bufer_01._read_ptr_valid(wr_read_ptr_from_lut);
	if(lut.prio_out_() == 2'b10)
		begin
		rg_buffer_10_valid <= 1'b1;
		bufer_10._tx_read_en(1'b1);
		end
	//bufer_10._read_ptr_valid(wr_read_ptr_from_lut);
	if(lut.prio_out_() == 2'b11)
		begin
		rg_buffer_11_valid <= 1'b1;
		bufer_11._tx_read_en(1'b1);
		end
	//bufer_11._read_ptr_valid(wr_read_ptr_from_lut);
endrule


//retreiving position of given ackid from lut in case of packet accepted
rule lut_out_deq(wr_deq == 1 && wr_stop_txmsn == 0);
	lut._identify(True);
	lut._ackid_retrans(wr_ackid_deq);
	lut._ackid_valid(wr_ackid_deq);
	rg_ackid_deq <= wr_ackid_deq;
	rg_deq <= 1;
	$display("Packet Accepted:Ackid to be dequeued - %b",wr_ackid_deq);
endrule


//packets till the accepted ackid must be dequeued
rule deq_read_pointers_a(rg_deq == 1);
if(rg_ackid_deq == 6'b0)
		rg_ackid_deq <= 6'b111111;
	else
		rg_ackid_deq <= rg_ackid_deq - 1;
endrule



rule deq_read_pointers_b(wr_deq !=1 && rg_deq == 1);
	lut._identify(True);
	lut._ackid_retrans(rg_ackid_deq);
	lut._ackid_valid(rg_ackid_deq);
endrule


//dequeing from buffer and clearing that entry from lut
rule deq_read_pointers(rg_deq == 1);
	if(wr_lut_valid_out == 1)
		begin
		lut._clear(True);
		lut._ackid_clear(rg_ackid_deq);
		if(wr_lut_prio_out == 2'b00)
			begin
			bufer_00._tx_deq(1'b1);
			bufer_00._pointer_deq(wr_read_ptr_from_lut);
			end
		if(wr_lut_prio_out == 2'b01)
			begin
			bufer_01._tx_deq(1'b1);
			bufer_01._pointer_deq(wr_read_ptr_from_lut);
			end
		if(wr_lut_prio_out == 2'b10)
			begin
			bufer_10._tx_deq(1'b1);
			bufer_10._pointer_deq(wr_read_ptr_from_lut);
			end
		if(wr_lut_prio_out == 2'b11)
			begin
			bufer_11._tx_deq(1'b1);
			bufer_11._pointer_deq(wr_read_ptr_from_lut);
			end
		end
	else
		rg_deq <= 0;
endrule

//outputs from buffer
rule buffer_outs;
	wr_lnk_tvld_n <= bufer_00.lnk_tvld_n_() && bufer_01.lnk_tvld_n_() && bufer_10.lnk_tvld_n_() && bufer_11.lnk_tvld_n_(); 
	wr_lnk_tsof_n <= bufer_00.lnk_tsof_n_() && bufer_01.lnk_tsof_n_() && bufer_10.lnk_tsof_n_() && bufer_11.lnk_tsof_n_();
	wr_lnk_teof_n <= bufer_00.lnk_teof_n_() && bufer_01.lnk_teof_n_() && bufer_10.lnk_teof_n_() && bufer_11.lnk_teof_n_();
	wr_lnk_td <= bufer_00.lnk_td_() | bufer_01.lnk_td_() | bufer_10.lnk_td_() | bufer_11.lnk_td_();
	wr_lnk_trem <= bufer_00.lnk_trem_() | bufer_01.lnk_trem_() | bufer_10.lnk_trem_() | bufer_11.lnk_trem_();
	wr_lnk_tcrf <= bufer_00.lnk_tcrf_() && bufer_01.lnk_tcrf_() && bufer_10.lnk_tcrf_() && bufer_11.lnk_tcrf_();
	buf_out_00 <= bufer_00.buf_out_();
	buf_out_01 <= bufer_01.buf_out_();
	buf_out_10 <= bufer_10.buf_out_();
	buf_out_11 <= bufer_11.buf_out_();
endrule

//rule buf_reg_out;



//input methods
method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _tx_crf(Bool value);
     wr_tx_crf <= value;
endmethod

method Action _tx_retransmsn(Bit#(1) value);
     wr_retrans <= value;
endmethod

method Action _tx_stop_txmsn(Bit#(1) value);
     wr_stop_txmsn <= value;
endmethod

method Action _ack_id_retrans(Bit#(6) value);
     wr_ackid_retrans <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
	wr_deq <= value;
endmethod

method Action _ack_id_deq(Bit#(6) value); 
	wr_ackid_deq <= value;
endmethod	



//output methods
method Bit#(1) tx_stopped_txmsn_();
	return wr_txmsn_stoped_out;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return  wr_lnk_trem;
endmethod

method Bool lnk_tcrf_();
      return  wr_lnk_tcrf;
endmethod

method BufferData buf_out_00_();
     return buf_out_00;
endmethod

method BufferData buf_out_01_();
     return buf_out_01;
endmethod

method BufferData buf_out_10_();
     return buf_out_10;
endmethod

method BufferData buf_out_11_();
     return buf_out_11;
endmethod


endmodule:mkRapidIOPhy_Buffer_Prio_all
endpackage:RapidIOPhy_Buffer_Prio_all
