/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Look Up Table Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2. A LUT Table is created to store the details of the Transmitted packet.
-- 3. AckID is used as the index value for the LUT vector register.
-- 
-- Table :	---------------------------------
		- Valid	- AckID	- Prio	-Pointer-			
		  1 bit	  6 bit	  2 bit	 4 bit	   	
		---------------------------------
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package AckId_Lut;

import Vector::*;


interface Ifc_AckId_Lut;
	method Action _identify(Bool value);//to indicate reading of lut is required,i.e.,in case of retransmission
	method Action _store(Bool value);//to indicate writing to lut is required
	method Action _clear(Bool value);//to indicate dequeuing
	method Action _ackid_store(Bit#(6) value);//ackid to be stored
	method Action _ackid_retrans(Bit#(6) value);//ackid to be identified from lut for retransmission
	method Action _ackid_valid(Bit#(6) value);//to reload read pointer if there is valid entry
	method Action _ackid_clear(Bit#(6) value);//ackid to be dequeued
	method Action _prio_in(Bit#(2) value);//for storing priority of buffer section in which a packet with a particular ackid is stored
	method Action _rd_ptr_in(Bit#(4) value);//for storing read pointer value in a buffer section(location of a packet)


	method Bit#(2) prio_out_();//to identify the section from which retransmission should begin
	method Bit#(2) prio_out_valid_();//for identifying the buffer section
	method Bit#(1) valid_out_();//for checking the entry status
	method Bit#(4) read_ptr_out_();//to reload the read pointer in case of retransmission

endinterface:Ifc_AckId_Lut

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)

module mkAckId_Lut(Ifc_AckId_Lut);


//[1-bit valid bit(To check the entry status),6-bit ackid,2-bit prio,4-bit pointer position]
Vector#(64,Reg#(Bit#(13)))  rg_vect_lut <- replicateM(mkReg(0));


Wire#(Bit#(6)) wr_ack_id_store <- mkDWire(0);
Wire#(Bit#(6)) wr_ack_id_retrans <- mkDWire(0);
Wire#(Bit#(6)) wr_ack_id_valid <- mkDWire(0);
Wire#(Bit#(6)) wr_ack_id_clear <- mkDWire(0);
Wire#(Bit#(4)) wr_rd_ptr_in <- mkDWire(0);
Wire#(Bit#(2)) wr_prio <- mkDWire(0);
Wire#(Bool) wr_id <- mkDWire(False);
Wire#(Bool) wr_store <- mkDWire(False);
Wire#(Bool) wr_clear <- mkDWire(False);

Reg#(Bit#(4)) rg_read_ptr_out <- mkReg(0);
Reg#(Bit#(2)) rg_prio_out <- mkReg(0);
Reg#(Bit#(2)) rg_prio_out_valid <- mkReg(0);
Reg#(Bit#(6)) rg_ack_id_retrans <- mkReg(0);
Reg#(Bit#(1)) rg_valid <- mkReg(0);




rule store(wr_store == True);
	let lv_id = wr_ack_id_store;
//vector of registers indexed by ackid
	rg_vect_lut[lv_id] <= {1'b1, (wr_ack_id_store == 6'b0 ? 6'b111111 :(wr_ack_id_store - 6'b000001)), wr_prio, wr_rd_ptr_in};//to be changed when retransmission comes into picture:wr_ack_id_store can be directly given
	$display("LUT entry:Valid bit - 1, Ackid - %b, Priority - %b, Pointer - %b",wr_ack_id_store, wr_prio, wr_rd_ptr_in);
endrule

//********************************************TO BE USED FOR DEQUEUING******************************************************
rule clear_lut(wr_clear == True);//put some other input like wr_clear for identifying its a clear operation and at the same time retreiving position is also possible  
	let lv_id = wr_ack_id_clear;
	rg_vect_lut[lv_id] <= {13'b0};
endrule
//*************************************************************************************************************************


//RULES USED DURING RETRANSMISSION
//reading from lut
rule out_retrans(wr_id == True);
	let lv_id = wr_ack_id_retrans;
	rg_prio_out <= rg_vect_lut[lv_id][5:4];
//rg_ack_id_retrans <= wr_ack_id_retrans;
endrule

rule valid_check;
	let lv_id = wr_ack_id_valid;
	rg_valid <= rg_vect_lut[lv_id][12];//checking valid entry in lut
	rg_read_ptr_out <= rg_vect_lut[lv_id][3:0];
	rg_prio_out_valid <= rg_vect_lut[lv_id][5:4];
endrule

//Input Methods
method Action _ackid_store(Bit#(6) value);
       wr_ack_id_store <= value;
endmethod

method Action _ackid_retrans(Bit#(6) value);
       wr_ack_id_retrans <= value;
endmethod

method Action _ackid_valid(Bit#(6) value);
	wr_ack_id_valid <= value;
endmethod

method Action _ackid_clear(Bit#(6) value);
	wr_ack_id_clear <= value;
endmethod

method Action _rd_ptr_in(Bit#(4) value);
       wr_rd_ptr_in <= value;
endmethod

method Action _prio_in(Bit#(2) value);
	wr_prio <= value;
endmethod
	
method Action _identify(Bool value);
       wr_id <= value;
endmethod

method Action _store(Bool value);
       wr_store <= value;
endmethod

method Action _clear(Bool value);
	wr_clear <= value;
endmethod

//Output Methods
method Bit#(4) read_ptr_out_();
       return rg_read_ptr_out;
endmethod

method Bit#(2) prio_out_();
	return rg_prio_out;
endmethod

method Bit#(2) prio_out_valid_();
	return rg_prio_out_valid;
endmethod

method Bit#(1) valid_out_();
	return rg_valid;
endmethod


endmodule:mkAckId_Lut
endpackage:AckId_Lut
