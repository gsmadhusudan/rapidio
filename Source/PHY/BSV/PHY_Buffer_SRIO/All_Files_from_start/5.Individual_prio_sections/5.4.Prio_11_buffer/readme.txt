Read is enabled after attaining the threshold value (write pointer =2). Read enabling is given from the same module.

Whenever PNA or PR comes a bit will be set  in the top module(Phy layer). Based on that transmission is stopped and then when phy layer is ready another bit will be set and retransmission is enabled from the read pointer and ackid at which error occured, which is obtained from the look up table.Also when packet accepted comes dequeing is also done at the pointer which is also obtained from lut according to the ackid which was accepted. Only that ackid is dequeued. Writing to same location must take place only after dequeuing of the packet after it has been accepted,otherwise overwriting. Works pending in this:Whenever overwriting comes(without dequeing) it should be notified to the top. Also dequeing must happen in all packets till the particular ackid accepted till then.

Final version of Individual section of prio_11 as of now.  
