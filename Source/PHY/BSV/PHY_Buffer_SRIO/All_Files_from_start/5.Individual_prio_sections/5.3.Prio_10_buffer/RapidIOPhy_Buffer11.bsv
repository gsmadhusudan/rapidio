package RapidIOPhy_Buffer11;

//import RapidIOPhy_Buffer_read_enable ::*;
import RapidIO_DTypes ::*;
import DefaultValue ::*;
import RapidIOPhy_Buffer2 ::*;
import AckId_Lut::*;
import AckId_generator::*;


interface Ifc_RapidIOPhy_Buffer11;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   //method Action _tx_rdy_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf(Bool value);
   //method Action _tx_read_en(Bit#(1) value);
   //method Action _tx_rg12(Bit#(4) value);
   method Action _tx_deq(Bit#(1) value);//packet accepted
   //method Action _ack_id(Bit#(2) value);
   //method Action _tx_ack(Bit#(4) value);
   //method Action _tx_read(Bit#(1) value);
//enabling retxmsn at the time of restart from retry
   method Action _tx_retransmsn(Bit#(1) value);
//to stop txmtng when either PNA or PR control symbol comes        
   method Action _tx_stop_txmsn(Bit#(1) value); 
//retransmitting required ackid(PNA or PR)       
   method Action _ack_id_retrans(Bit#(6) value);  
//dequeing the acknowledged ackid(PA)             
   method Action _ack_id_deq(Bit#(6) value);               
   
   method BufferData buf_out_();
   //method RegBuf buf_out_deq_();
   method Bool lnk_tvld_n_();
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bit#(4) lnk_trem_();
   method Bool lnk_tcrf_();
//to the read write pointer module to determine when to start reading 
   method Bit#(4) out_wrt_ptr_();                     
   method Bit#(4) out_rd_ptr_(); 
//to indicate to top module that txmtr has stopped txmtng so that RFR can be sent. 
   method Bit#(1) tx_stopped_txmsn_();   

endinterface:Ifc_RapidIOPhy_Buffer11



(* synthesize *)
(* always_enabled *)
(* always_ready *)



module mkRapidIOPhy_Buffer11(Ifc_RapidIOPhy_Buffer11);

//set of 7 registers that will make up the priority "10" area in the buffer.

Ifc_RapidIOPhy_Buffer2 reg1 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg2 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg3 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg4 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg5 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg6 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg7 <- mkRapidIOPhy_Buffer2;



Ifc_AckId_generator ack1 <- mkAckId_generator;
//Ifc_RapidIOPhy_Buffer_read_enable rd_enbl <- mkRapidIOPhy_Buffer_read_enable;
Ifc_AckId_Lut lut <- mkAckId_Lut;

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
//Wire#(Bool) tx_rdy_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bool) wr_tx_crf <- mkDWire(True);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4))  wr_lnk_trem <- mkDWire(0);
Wire#(Bool)  wr_lnk_tcrf <- mkDWire(True);

//pointer indicating to which register write is happening.
Reg#(Bit#(4)) rg_write_ptr <- mkReg(4'b0001);  
//pointer indicating from which register read is happening.    
Reg#(Bit#(4)) rg_read_ptr <- mkReg(4'b0000);     
//Wire#(Bit#(4)) wr_read_intr <- mkDWire(0);
Reg#(Bit#(1)) rg_read_en <- mkReg(0);
Wire#(Bit#(1)) wr_read_en <- mkDWire(0);
Wire#(BufferData) buf_out <- mkDWire(defaultValue);
//to be assigned to the packet
Wire#(Bit#(6))  wr_tx_ack <- mkDWire(0);
//6 bit ackid coming through 6 bit bus from control symbol for retransmission      
Wire#(Bit#(6)) wr_ackid_retrans <- mkDWire(0);  
//asserted whenever packet accepted comes     
Wire#(Bit#(1)) wr_deq <- mkDWire(0);          
Reg#(Bit#(1)) rg_deq <- mkReg(0); 
//6 bit ackid coming through 6 bit bus from control symbol for dequeing         
Wire#(Bit#(6)) wr_ackid_deq <- mkDWire(0);       
Wire#(Bit#(1))  wr_retrans <- mkDWire(0);
Reg#(Bit#(1))  rg_retrans <- mkReg(0);
Wire#(Bit#(1))  wr_stop_txmsn <- mkDWire(0);
//to indicate to top that txmsn has stopped
Wire#(Bit#(1))  wr_txmsn_stoped_out <- mkDWire(0);
//writing
Wire#(Bit#(4)) wr_write_ptr <- mkDWire(0);
//reading
Wire#(Bit#(4)) wr_read_ptr <- mkDWire(0);
//dequeing
Wire#(Bit#(4)) wr_write_ptr_deq <- mkDWire(0);

(* descending_urgency = "read_ptr_retrans,read_ptr_incr" *)
(* descending_urgency = "read_ptr_retrans,r1_rd_en_ptr" *)
//(* descending_urgency = "r1_rd_en_ptr,read_ptr_incr" *)


//moving to next register whenever eof comes
rule write_ptr_incr;                                         
	wr_write_ptr <= rg_write_ptr;
	$display("write_ptr =%b",rg_write_ptr);
	if(wr_tx_eof_n == False)
		begin
//set of 7 registers
		if(rg_write_ptr == 4'b0111) 
			rg_write_ptr <= 4'b0001;
		else
			rg_write_ptr <= rg_write_ptr + 1;
		end
endrule


/*rule ptrs_to_rd_en;

	//rd_enbl._in_write_ptr(rg_write_ptr);
	//rd_enbl._in_read_ptr(rg_read_ptr);
	//rd_enbl._tx_retransmsn(wr_retrans);
	//rd_enbl._tx_stop_txmsn(wr_stop_txmsn);


endrule*/

//threshold to start txmtng - 3
rule r1_rd_en_ptr((wr_write_ptr >= 2 && wr_read_ptr == 0));
//enabling read
	rg_read_en <= 1'b1;  
//initial value of read pointer                
	rg_read_ptr <= 4'b0001;                
endrule



rule read_disabl(wr_stop_txmsn == 1);
	rg_read_en <= 1'b0;
	wr_txmsn_stoped_out <= 1'b1;
endrule

rule read_enbl(wr_retrans == 1);
	rg_read_en <= 1'b1;
endrule

/*
rule read_enbl;
//read enabling
	rg_read_en <= rd_enbl.tx_read_en_();                
endrule

*/
rule read_en_deq_retrans_readptr_copy;
	//$display("read_en in top module =%b",rg_read);
//retreiving required ackid in case of retransmission
	ack1._retrans(wr_retrans);	
	wr_read_en <= rg_read_en;
	wr_read_ptr <= rg_read_ptr;
	rg_retrans <= wr_retrans;
	rg_deq <= wr_deq;
endrule
/*
//condition under which read is enabled
rule read_ptr_initial((rg_write_ptr >= 4 && rg_read_ptr == 0));  
//read pointer is given the value of first register        
	rg_read_ptr <= rd_enbl.read_ptr_init_();             
endrule
*/
//whenever retransmission comes,the position at which the packet is stored is obtained from lut .
rule read_ptr_retrans(wr_retrans == 1);
	rg_read_ptr <= lut.read_ptr_out_();
	ack1._ack_in(lut.ackid_out_());	
endrule


rule write_ptr_deq(rg_deq == 1);
	wr_write_ptr_deq <= lut.read_ptr_out_();
endrule


rule read_ptr_incr(rg_read_en == 1);
	if(reg1.read_eof_() == 1 || reg2.read_eof_() == 1 || reg3.read_eof_() == 1 || reg4.read_eof_() == 1 || reg5.read_eof_() == 1 ||      			reg6.read_eof_() == 1 || reg7.read_eof_() == 1)
			begin
			//$display("entering.................");
//ackid incrementing enabling
			ack1._ack_en(False);                     
			if(rg_read_ptr == 4'b0111) 
				begin
				rg_read_ptr <= 4'b0001;
				end
			else
				begin
				rg_read_ptr <= rg_read_ptr + 1;
				end
			end
endrule




rule ackid_return;
//generated ackid given out to be appended
	wr_tx_ack <= ack1.ack_id_out_();                        
	$display("read_ptr =%b",rg_read_ptr);
endrule


rule disp;
	$display("Ackid given to packet =%b",wr_tx_ack);
	$display("Deque ptr = %b",wr_write_ptr_deq);
	$display("read_enable = %b",rg_read_en);
	//$display("rg_read in top = %b",rg_read);
	//$display("wr_read in top = %b",wr_read);
endrule

//storing to lut while reading
rule lut_store( wr_stop_txmsn != 1 && wr_deq != 1);
		lut._identify(2'b01);
		lut._ackid_store(wr_tx_ack);
		lut._rd_ptr_in(rg_read_ptr);
		lut._prio_in(2'b10);
endrule

//retreiving position of given ackid from lut in case of retransmsn
rule lut_out_retrans(wr_stop_txmsn == 1 && wr_deq == 0);
	lut._identify({wr_stop_txmsn,1'b0});
	lut._ackid_retrans(wr_ackid_retrans);
$display("Error reported:Ackid to be retransmitted - %b",wr_ackid_retrans);
endrule

//retreiving position of given ackid from lut in case of packet accepted
rule lut_out_deq(wr_deq == 1 && wr_stop_txmsn == 0);
	lut._identify({wr_deq,1'b0});
	lut._ackid_retrans(wr_ackid_deq);
	$display("Packet Accepted:Ackid to be dequeued - %b",wr_ackid_deq);
endrule

//in case of retransmission,read and write counts reloaded
rule retrans_reload_cnt_values(wr_retrans == 1);
	//$display("retrans in top module =%b",wr_retrans);
	for(Integer j=1;j<8;j=j+1)
		begin
		case(j)
			1:reg1._tx_retransmsn(wr_retrans);
			2:reg2._tx_retransmsn(wr_retrans);
			3:reg3._tx_retransmsn(wr_retrans);
			4:reg4._tx_retransmsn(wr_retrans);
			5:reg5._tx_retransmsn(wr_retrans);
			6:reg6._tx_retransmsn(wr_retrans);
			7:reg7._tx_retransmsn(wr_retrans);
		endcase
		end
endrule


rule r1_vld;
	if(wr_write_ptr == 4'b0001)
		reg1._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0110)
		reg6._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0111)
		reg7._tx_vld_n(wr_tx_vld_n);
endrule


rule r1_sof;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0110)
		reg6._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0111)
		reg7._tx_sof_n(wr_tx_sof_n);
endrule


rule r1_eof;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0110)
		reg6._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0111)
		reg7._tx_eof_n(wr_tx_eof_n);
endrule



rule r1_data;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0110)
		reg6._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0111)
		reg7._tx_data(wr_tx_data);
endrule


rule r1_rem;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0110)
		reg6._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0111)
		reg7._tx_rem(wr_tx_rem);
endrule


rule r1_crf;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0110)
		reg6._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0111)
		reg7._tx_crf_n(wr_tx_crf);
endrule


rule r1_ack_assign(rg_read_en == 1);
	if(rg_read_ptr == 4'b0001)
		reg1._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0010)
		reg2._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0011)
		reg3._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0100)
		reg4._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0101)
		reg5._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0110)
		reg6._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0111)
		reg7._tx_ack(wr_tx_ack);
endrule


rule r1_read_en(wr_stop_txmsn != 1);
	if(wr_read_ptr == 4'b0001)
		reg1._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0010)
		reg2._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0011)
		reg3._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0100)
		reg4._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0101)
		reg5._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0110)
		reg6._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0111)
		reg7._tx_read(rg_read_en);
endrule


//reading disabled for all registers
rule r1_read_disbl(wr_stop_txmsn == 1);
	for(Integer j=1;j<8;j=j+1)
		begin
		case(j)
		1:reg1._tx_read(~(wr_stop_txmsn));
		2:reg2._tx_read(~(wr_stop_txmsn));
		3:reg3._tx_read(~(wr_stop_txmsn));
		4:reg4._tx_read(~(wr_stop_txmsn));
		5:reg5._tx_read(~(wr_stop_txmsn));
		6:reg6._tx_read(~(wr_stop_txmsn));
		7:reg7._tx_read(~(wr_stop_txmsn));
		endcase
		end

endrule

//when rg_buf[ackid]==lnk_last_ack,deq = 1;position of a particular ackid to be found out from lookup table.
rule r1_deq;
	if(wr_write_ptr_deq == 4'b0001)     
		reg1._tx_deq(rg_deq);
	else if(wr_write_ptr_deq == 4'b0010)
		reg2._tx_deq(rg_deq);
	else if(wr_write_ptr_deq == 4'b0011)
		reg3._tx_deq(rg_deq);
	else if(wr_write_ptr_deq == 4'b0100)
		reg4._tx_deq(rg_deq);
	else if(wr_write_ptr_deq == 4'b0101)
		reg5._tx_deq(rg_deq);
	else if(wr_write_ptr_deq == 4'b0110)
		reg6._tx_deq(rg_deq);
	else if(wr_write_ptr_deq == 4'b0111)
		reg7._tx_deq(rg_deq);
endrule


rule r1_lnk_vld(wr_read_en == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_tvld_n <= reg1.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_tvld_n <= reg2.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_tvld_n <= reg3.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_tvld_n <= reg4.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_tvld_n <= reg5.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_tvld_n <= reg6.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_tvld_n <= reg7.lnk_tvld_n_();
endrule


rule r1_lnk_sof(wr_read_en == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_tsof_n <= reg1.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_tsof_n <= reg2.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_tsof_n <= reg3.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_tsof_n <= reg4.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_tsof_n <= reg5.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_tsof_n <= reg6.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_tsof_n <= reg7.lnk_tsof_n_();
endrule


rule r1_lnk_eof(wr_read_en == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_teof_n <= reg1.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_teof_n <= reg2.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_teof_n <= reg3.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_teof_n <= reg4.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_teof_n <= reg5.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_teof_n <= reg6.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_teof_n <= reg7.lnk_teof_n_();
endrule


rule r1_lnk_data(wr_read_en == 1);
	if(wr_read_ptr == 4'b0001)
		wr_lnk_td <= reg1.lnk_td_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_td <= reg2.lnk_td_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_td <= reg3.lnk_td_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_td <= reg4.lnk_td_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_td <= reg5.lnk_td_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_td <= reg6.lnk_td_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_td <= reg7.lnk_td_();
	
endrule


rule r1_lnk_rem(wr_read_en == 1);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_trem <= reg1.lnk_trem_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_trem <= reg2.lnk_trem_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_trem <= reg3.lnk_trem_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_trem <= reg4.lnk_trem_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_trem <= reg5.lnk_trem_();
	else if(wr_read_ptr == 4'b0110)
		wr_lnk_trem <= reg6.lnk_trem_();
	else if(wr_read_ptr == 4'b0111)
		wr_lnk_trem <= reg7.lnk_trem_();
endrule


rule r1_lnk_crf(wr_read_en == 1);

	if(wr_read_ptr == 4'b0001)
			wr_lnk_tcrf <= reg1.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0010)
			wr_lnk_tcrf <= reg2.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0011)
			wr_lnk_tcrf <= reg3.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0100)
			wr_lnk_tcrf <= reg4.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0101)
			wr_lnk_tcrf <= reg5.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0110)
			wr_lnk_tcrf <= reg6.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0111)
			wr_lnk_tcrf <= reg7.lnk_tcrf_();

endrule


rule r1_lnk_out;
	if(wr_write_ptr == 4'b0001)
		buf_out <= reg1.buf_out_();
	else if(wr_write_ptr == 4'b0010)
		buf_out <= reg2.buf_out_();
	else if(wr_write_ptr == 4'b0011)
		buf_out <= reg3.buf_out_();
	else if(wr_write_ptr == 4'b0100)
		buf_out <= reg4.buf_out_();
	else if(wr_write_ptr == 4'b0101)
		buf_out <= reg5.buf_out_();
	else if(wr_write_ptr == 4'b0110)
		buf_out <= reg6.buf_out_();
	else if(wr_write_ptr == 4'b0111)
		buf_out <= reg7.buf_out_();
endrule

















method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _tx_crf(Bool value);
     wr_tx_crf <= value;
endmethod

method Action _tx_retransmsn(Bit#(1) value);
     wr_retrans <= value;
endmethod

method Action _tx_stop_txmsn(Bit#(1) value);
     wr_stop_txmsn <= value;
endmethod

//method Action _tx_read_en(Bit#(1) value);
	//rg_read <= value;
//endmethod
/*method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod*/

//method Action _tx_rg12(Bit#(4) value);
     //wr_wrt_ptr <= value;
//endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

/*method Action _tx_ack(Bit#(4) value);
		wr_tx_ack <= value;
endmethod*/


method Action _ack_id_retrans(Bit#(6) value);
     wr_ackid_retrans <= value;
endmethod

method Action _ack_id_deq(Bit#(6) value);
	wr_ackid_deq <= value;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method BufferData buf_out_();
     return buf_out;
endmethod



method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return  wr_lnk_trem;
endmethod

method Bool lnk_tcrf_();
      return  wr_lnk_tcrf;
endmethod

method Bit#(4) out_wrt_ptr_();
	return rg_write_ptr;
endmethod

method Bit#(4) out_rd_ptr_();
	return rg_read_ptr;
endmethod 

method Bit#(1) tx_stopped_txmsn_();
	return wr_txmsn_stoped_out;
endmethod   

endmodule:mkRapidIOPhy_Buffer11
endpackage:RapidIOPhy_Buffer11
