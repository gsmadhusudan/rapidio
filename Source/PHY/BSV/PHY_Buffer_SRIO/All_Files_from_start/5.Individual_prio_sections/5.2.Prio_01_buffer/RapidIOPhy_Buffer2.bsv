//Top module for the set of 11 registers in the buffer which holds the lowest priority packets



package RapidIOPhy_Buffer2;

import RapidIO_DTypes ::*;
import DefaultValue ::*;


interface Ifc_RapidIOPhy_Buffer2;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf_n(Bool value);
//to enable dequeing.
   method Action _tx_deq(Bit#(1) value);  
//to enable reading.           
   method Action _tx_read(Bit#(1) value);   

//enables retransmission if equal to 1.         
   method Action _tx_retransmsn(Bit#(1) value);      
   method Action _tx_ack(Bit#(6) value);
   
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bool lnk_tvld_n_();
   method Bit#(4) lnk_trem_();
   method Bool lnk_tcrf_();
//to indicate end of packet while reading.
   method Bit#(1) read_eof_();                         
   method BufferData buf_out_();
   //method RegBuf buf_out_deq_();



endinterface:Ifc_RapidIOPhy_Buffer2



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer2(Ifc_RapidIOPhy_Buffer2);

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bool) wr_tx_crf <- mkDWire(True);
//Wire#(Bit#(1)) wr_tx_crf_bit <- mkDWire(0);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4)) wr_lnk_trem <- mkDWire(0);
Wire#(Bool) wr_lnk_tcrf <- mkDWire(True);
Wire#(Bit#(1)) wr_eof <- mkDWire(0);
//6 bit ack id;actually ackid is 12 bits long but only lower 6 bits are given to packet
Wire#(Bit#(6)) wr_tx_ack <- mkDWire(0);                

//buffer register to which data is written.total register size = 2309 bits.
Reg#(BufferData) rg_buffer <- mkReg(defaultValue);  
//reg for enabling reading ie making read and write mutually exclusive.                   
Wire#(Bit#(1)) wr_read_en <- mkDWire(0);
//read pointer in a single register.       
Reg#(Bit#(5)) rg_cnt_read <- mkReg(0);
//write pointer indicating the position of each cycle of data in the register.
Reg#(Bit#(5)) rg_cnt_write <- mkReg(0);  
//to update write count register to initial value in case of retransmission.              
Reg#(Bit#(5)) rg_cnt_wr_retrnsmsn <- mkReg(0);
//for deciding about dequeue.       
Wire#(Bit#(1)) wr_deq <- mkDWire(0);                 
Wire#(Bit#(1)) wr_retrans <- mkDWire(0);             



//writing into register
rule r1_write(wr_tx_vld_n == False || wr_deq == 1);                            

  			//$display("value of write count =%b",rg_cnt_write);
 	if(wr_deq == 1)
		begin
$display("dequeingggggggggg");
		rg_cnt_write <= 0;              
		rg_cnt_wr_retrnsmsn <= 0;		
		rg_cnt_read <= 0;
		rg_buffer <= BufferData {tx_crf:True,tx_rem:0,tx_data:0};
		end
	else if(wr_tx_sof_n == False && wr_tx_eof_n == False)
		begin 
		
//only one cycle(128 bits) of data; [2308:2304] stores  crf and rem 5 bits 
		rg_buffer <= BufferData {tx_crf:wr_tx_crf,tx_rem:wr_tx_rem,tx_data:{2176'b0,wr_tx_data}};          
//write pointer incremented once if only single cycle of data
		rg_cnt_write <= rg_cnt_write +1;              
		rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 1;		
		rg_cnt_read <= 0; 
		end

	else if(wr_tx_sof_n == False)
		begin   
//first cycle always goes to position [127:0]         
		rg_buffer.tx_data[127:0] <= wr_tx_data;
//pointer incremented by four in the first cycle in case of multiple cycles of data.                                      
		rg_cnt_write <= rg_cnt_write + 4;             
		rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 4;		
		end

	else if(wr_tx_eof_n == False)
		begin
		let lv_rg_intrmediate = rg_buffer.tx_data[2175:0];
//last cycle always goes to position [2303:2176]; [2308:2304] stores crf and rem in 5 bits
		rg_buffer <= BufferData {tx_crf:wr_tx_crf,tx_rem:wr_tx_rem,tx_data:{wr_tx_data,lv_rg_intrmediate}}; 
		rg_cnt_read <= 0; 
		end 

	else 
		begin
		let lv_rg_shift_in_data = rg_buffer.tx_data[2175:128];
//data is first written to msb of local register and then shifted to right whenever next 128 bits come. 
		lv_rg_shift_in_data = lv_rg_shift_in_data >> 128;   
//data is first written to msb of local register(local register is of 2048 bits excluding initial 8 bits and storage area of first cycle and last cycle of data.)                              
		lv_rg_shift_in_data[2047:1920]= lv_rg_shift_in_data[2047:1920] | wr_tx_data;   
		rg_buffer.tx_data[2175:128]<= lv_rg_shift_in_data;
		rg_cnt_write <= rg_cnt_write +1;
		rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 1;		
		end  
endrule


rule retxmsn(wr_retrans == 1);
	rg_cnt_write <= rg_cnt_wr_retrnsmsn;
	rg_cnt_read <= 0;
endrule
		


//reading enabled by rg_read
rule r1_read(wr_read_en == 1);                 

			//$display("rd_enbl in basic =%b",rg_read);
 //pointer = 1 implies single cycle of data                                           
	if(rg_cnt_write == 1) 
		begin
		if(rg_cnt_read == 0)
			begin
//ackid assignment according to big endian-little endian conversion
  			$display("value of read count = %b",rg_cnt_read);
			rg_buffer.tx_data[127:122] <= wr_tx_ack;        
			//$display("Ackid given to packet =%b",wr_tx_ack);
			$display("Ackid assigning single data....");
			rg_cnt_read <= rg_cnt_read + 1;
			end
		else if(rg_cnt_read == 1)
			begin
  			$display("value of read count = %b",rg_cnt_read);
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= False;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[127:0];
			wr_lnk_trem <= rg_buffer.tx_rem;
			wr_lnk_tcrf <= rg_buffer.tx_crf;
//to facilitate read pointer incrementing
			wr_eof <= 1'b1;                 
			rg_cnt_read <= 0;
			rg_cnt_write <= 0;
			end
		end


	else if(rg_cnt_write != 0 && rg_cnt_write != 1 && rg_cnt_write != 2)

		begin
		if(rg_cnt_read == 0)
			begin
                   //ackid assignment
			rg_buffer.tx_data[127:122] <= wr_tx_ack;         
			//$display("Ackid given to packet =%b",wr_tx_ack);
			$display("Ackid assigning....");
			rg_cnt_write <= rg_cnt_write-1;
			rg_cnt_read <= rg_cnt_read + 1;
			end



		else if(rg_cnt_read == 1)
			begin
			//$display("data1..........................");
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[127:0];
			rg_cnt_write <= rg_cnt_write-1;
			rg_cnt_read <= rg_cnt_read + 1;
			//wr_lnk_trem <= rg_buf[2308:2305];
			//wr_lnk_tcrf <= rg_buf[2310:2309];
			//wr_eof <= 1'b1;
			end
		else if(rg_cnt_write == 18)
			begin
			//$display("data2..........................");
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[255:128];
			rg_cnt_read <= rg_cnt_read + 1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 17)
			begin
			//$display("data3..........................");
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[383:256];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 16)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[511:384];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 15)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[639:512];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 14)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[767:640];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 13)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[895:768];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 12)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1023:896];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 11)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1151:1024];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 10)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1279:1152];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 9)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1407:1280];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 8)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1535:1408] ;
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 7)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1663:1536];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 6)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1791:1664];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 5)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[1919:1792];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 4)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[2047:1920];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 3)
			begin
			//$display("datalast..........................");
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer.tx_data[2175:2048];
			rg_cnt_read <= rg_cnt_read+1;
			rg_cnt_write <= rg_cnt_write - 1;
			end



		end


	else if(rg_cnt_write == 2 && rg_cnt_read != 0)
		begin
		wr_lnk_teof_n <= False;
		wr_lnk_tsof_n <= True;
		wr_lnk_tvld_n <= False;
		wr_lnk_td <= rg_buffer.tx_data[2303:2176];
//facilitating read pointer incrementing
		wr_eof <= 1'b1;                                
		wr_lnk_trem <= rg_buffer.tx_rem;
		wr_lnk_tcrf <= rg_buffer.tx_crf;
		//wr_lnk_tcrf <= rg_buffer[2309];
		rg_cnt_read <= 0;
		rg_cnt_write <= 0;
		end
endrule



method Action _tx_ack(Bit#(6) value);
	wr_tx_ack <= value;
//$display("ackid in method = %b",value);
endmethod

method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_crf_n(Bool value);
     wr_tx_crf <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     wr_read_en <= value;
endmethod

method Action _tx_retransmsn(Bit#(1) value);
	wr_retrans <= value;
endmethod



method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return wr_lnk_trem;
endmethod

method Bool lnk_tcrf_();
      return wr_lnk_tcrf;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method Bit#(1) read_eof_();
	return wr_eof;
endmethod

method BufferData buf_out_();
      return rg_buffer;
endmethod
/*
method RegBuf buf_out_deq_();
      return rg_buf;
endmethod*/

endmodule:mkRapidIOPhy_Buffer2
endpackage:RapidIOPhy_Buffer2
