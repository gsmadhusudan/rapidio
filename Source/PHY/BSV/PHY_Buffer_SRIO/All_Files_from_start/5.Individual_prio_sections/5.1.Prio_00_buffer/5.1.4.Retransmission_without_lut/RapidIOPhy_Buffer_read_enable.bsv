package RapidIOPhy_Buffer_read_enable;


import AckId_generator::*;

interface Ifc_RapidIOPhy_Buffer_read_enable;

	method Action _in_wrt_ptr(Bit#(4) value);
	method Action _in_rd_ptr(Bit#(4) value);
	method Action _tx_retransmsn(Bit#(1) value);        //enabling retxmsn 
        method Action _tx_stop_txmsn(Bit#(1) value);        //to stop txmtng when either PNA or PR control symbol comes
	
	method Bit#(1) tx_read_en_();
	method Bit#(4) read_ptr_init_();

endinterface:Ifc_RapidIOPhy_Buffer_read_enable

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer_read_enable(Ifc_RapidIOPhy_Buffer_read_enable);


//Ifc_AckId_generator a1 <- mkAckId_generator;

Wire#(Bit#(4)) wr_wrt_ptr <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr <- mkDWire(0);
Reg#(Bit#(1)) rg_read <- mkReg(0);
Reg#(Bit#(4)) rg_ptr <- mkReg(0);
Wire#(Bit#(1))  wr_retrans <- mkDWire(0);
Wire#(Bit#(1))  wr_stop_txmsn <- mkDWire(0);

//(* descending_urgency = "read_disabl,r1_rd_en_ptr,read_enbl" *)

rule r1_rd_en_ptr(wr_wrt_ptr >= 4 && wr_read_ptr == 0);
//$display("wrt_ptr =%b",wr_wrt_ptr);
//$display("read_en =%b",rg_read);
	rg_read <= 1'b1;                  //enabling read
	rg_ptr <= 4'b0001;                //initial value of read pointer
endrule

rule read_disabl(wr_stop_txmsn == 1);
	rg_read <= 1'b0;
	rg_ptr <= wr_read_ptr;
endrule

rule read_enbl(wr_retrans == 1);
	rg_read <= 1'b1;
endrule

rule disp;
	//$display("read_ptr_in_read_enable =%b",wr_read_ptr);
	$display("read_en =%b",rg_read);
endrule

method Action _in_wrt_ptr(Bit#(4) value);
	wr_wrt_ptr <= value;
endmethod

method Action _in_rd_ptr(Bit#(4) value); 
	wr_read_ptr <= value;
endmethod

method Action _tx_retransmsn(Bit#(1) value);
     wr_retrans <= value;
endmethod

method Action _tx_stop_txmsn(Bit#(1) value);
     wr_stop_txmsn <= value;
endmethod

method Bit#(1) tx_read_en_();
	return rg_read;
endmethod

method Bit#(4) read_ptr_init_();
	return rg_ptr;
endmethod
	

endmodule:mkRapidIOPhy_Buffer_read_enable
endpackage:RapidIOPhy_Buffer_read_enable
