package Tb_RapidIOPhy_Buffer8;

import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer8 ::*;
import RapidIOPhy_Buffer5 ::*;
import AckId_generator::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)

module mkTb_RapidIOPhy_Buffer8(Empty);

Ifc_RapidIOPhy_Buffer8 bfr6 <- mkRapidIOPhy_Buffer8;
//Ifc_RapidIOPhy_Buffer5 bfr5 <- mkRapidIOPhy_Buffer5;
Ifc_AckId_generator a1 <- mkAckId_generator;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);
//Wire#(Bit#(2)) rg_x <- mkDWire (0);
//Reg#(Bit#(2)) rg_q <- mkReg (0);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 65 )
		$finish (0);
endrule

rule r10(reg_ref_clk == 0);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_1(reg_ref_clk == 1);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hF52AD3E66EFC11375A8D9C8EFC0ACEA2);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_2(reg_ref_clk <= 10);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_16(reg_ref_clk == 11);
//bfr6._tx_vld_n(False);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3(reg_ref_clk == 12);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b1111);
bfr6._tx_crf(2'b11);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule



//rule r102(reg_ref_clk == 13);
//bfr6._tx_rg12(4'b0010);
//endrule

rule r2_1(reg_ref_clk == 13);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EF0);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2(reg_ref_clk == 14);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r15(reg_ref_clk == 15);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_3(reg_ref_clk == 16);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule



//rule r101(reg_ref_clk == 18);
//bfr6._tx_rg12(4'b0011);
//endrule

/*rule r2_1q(reg_ref_clk == 19);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EF0);
bfr6._tx_rem(4'b0101);
bfr6._tx_crf(2'b11);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r15a(reg_ref_clk == 20);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r101z(reg_ref_clk == 21);
//bfr6._tx_rg12(4'b0100);
//endrule*/

rule r2_1x(reg_ref_clk == 17);
//bfr6._tx_rg12(2'b11);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h5A8D9C8EFC0ACEA0F52AD3E66EF0113F);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2s(reg_ref_clk == 18);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r2_2w(reg_ref_clk == 19);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2e(reg_ref_clk == 20);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r15c(reg_ref_clk == 21);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_3d(reg_ref_clk == 22);

bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b1011);
bfr6._tx_crf(2'b01);
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r15c(reg_ref_clk == 27);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule


//-------------------------oooooooooooooooooooooooooooooooooo---------------------------------
//ooooooooooooooooooooooooooooo--------------------
//rule r101wv(reg_ref_clk == 28);
//bfr6._tx_rg12(4'b0001);
//endrule

rule r1_3_1f1(reg_ref_clk == 29);
a1._ack_en(False);
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_25r(reg_ref_clk == 30);
a1._ack_en(True);
//bfr6._tx_ack(a1.ack_id_());
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_256t(reg_ref_clk == 31);
//a1._ack_en(False);
//bfr6._tx_ack(a1.ack_id_());
bfr6._tx_read(1'b1);
//rg_q <= a1.ack_id_();
//$display("rg_q value is %b",rg_q);
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_256rg(reg_ref_clk == 32);
//a1._ack_en(False);
//bfr6._tx_ack(a1.ack_id_());

//$display("rg_q value is %b",rg_q);
$display("ack value is %b",a1.ack_id_());
endrule

/*rule r1_3_2(reg_ref_clk == 23);

$display("ack value is %b",a1.ack_id_());

$display("sof is %b",bfr6.lnk_tsof_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule*/


rule r1_3_3v(reg_ref_clk == 33);
//bfr6._tx_read(1'b1);
bfr6._tx_ack(a1.ack_id_());
endrule

rule r1_3_3i(reg_ref_clk >= 34 && reg_ref_clk <= 37);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule
/*
rule r1_3_3u(reg_ref_clk == 23);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3y(reg_ref_clk == 24);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3t(reg_ref_clk == 25);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3r(reg_ref_clk == 26);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3e(reg_ref_clk == 27);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3w(reg_ref_clk == 28);
//bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3q(reg_ref_clk == 29);
//bfr6._tx_read(1'b0);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3f(reg_ref_clk == 30);
//bfr6._tx_read(1'b0);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3d(reg_ref_clk == 31);
//bfr6._tx_read(1'b0);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3s(reg_ref_clk == 32);
//bfr6._tx_read(1'b0);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule*/

rule r1_3_3a(reg_ref_clk == 38);
bfr6._tx_read(1'b0);
$display("ack value is %b",a1.ack_id_());
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule


rule r1_3_4(reg_ref_clk == 39);
//bfr6._tx_read(1'b0);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule
/*
rule r101w(reg_ref_clk == 46);
bfr6._tx_rg12(4'b0100);
endrule

rule r1_3_11w(reg_ref_clk == 47);
a1._ack_en(False);
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_25w(reg_ref_clk == 48);
a1._ack_en(True);
//bfr6._tx_ack(a1.ack_id_());
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_256w(reg_ref_clk == 49);
//a1._ack_en(False);
//bfr6._tx_ack(a1.ack_id_());
bfr6._tx_read(1'b1);
$display("ack value is %b",a1.ack_id_());
endrule

rule r1_3_256wr(reg_ref_clk == 50);
//a1._ack_en(False);
//bfr6._tx_ack(a1.ack_id_());
//bfr6._tx_ack(a1.ack_id_());
$display("ack value is %b",a1.ack_id_());
$display("Regstr value is %h",bfr6.buf_out_());
endrule


rule r1_3_115(reg_ref_clk == 51);
bfr6._tx_ack(a1.ack_id_());
//$display("sof is %b",bfr6.lnk_tsof_n_());
//$display("vld is %b",bfr6.lnk_tvld_n_());
//$display("eof is %b",bfr6.lnk_teof_n_());
//$display("DATA is %h",bfr6.lnk_td_());
//$display("Rem is %b",bfr6.lnk_trem_());
//$display("CRF is %b",bfr6.lnk_tcrf_());
//$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_255(reg_ref_clk == 52);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_75(reg_ref_clk == 53);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_1_215(reg_ref_clk == 54);

$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
$display("Regstr value is %h",bfr6.buf_out_());
endrule



rule r1_1_1(reg_ref_clk == 55);

$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_1_2(reg_ref_clk == 56);
bfr6._tx_read(1'b0);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
$display("Regstr value is %h",bfr6.buf_out_());
endrule


rule r1_3_41(reg_ref_clk == 57);
//bfr6._tx_read(1'b0);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_3_82(reg_ref_clk == 58);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule 
//rule r1_1_21(reg_ref_clk == 43);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule
*/
/*
rule r1_ack(reg_ref_clk == 32);
a1._ack_en(False);
$display("ack value is %b",a1.ack_id_());
$display("Regstr value is %h",bfr6.buf_out_());

endrule

//rule r1_12(reg_ref_clk == 48);
//rg_x <= a1.ack_id_();
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r2_3_81(reg_ref_clk == 33);
bfr6._tx_ack(a1.ack_id_());
$display("ack value is %b",a1.ack_id_());
endrule */
/*
rule r1_3_21(reg_ref_clk == 47);
bfr6._tx_read(1'b1);
$display("Regstr value is %h",bfr6.buf_out_());
endrule*/
/*
rule r1_3_21r(reg_ref_clk == 34);
//a1._ack_en(True);
$display("ack value is %b",a1.ack_id_());
bfr6._tx_read(1'b1);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());

endrule

rule r1_3_31(reg_ref_clk == 35);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_41(reg_ref_clk == 36);
bfr6._tx_read(1'b0);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r2_3_82(reg_ref_clk == 37);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule 

rule r2_3_83(reg_ref_clk == 38);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule */
/*
rule r2_3_84(reg_ref_clk == 55);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule 

rule r1_3_6(reg_ref_clk == 56);
bfr6._tx_deq(1'b1);
//bfr6._ack_id(00);
endrule

//rule r1_3_61(reg_ref_clk == 54);
//bfr6._tx_deq(1'b1);
//bfr6._ack_id(00);
//endrule

*/

rule r1_3_8(reg_ref_clk == 59);
bfr6._tx_deq(1'b1);
bfr6._ack_id(4'b0000);

$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_8_1(reg_ref_clk == 60);
bfr6._tx_deq(1'b0);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_4_8(reg_ref_clk == 61);
//bfr6._tx_rg12(4'b0001);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r11(reg_ref_clk == 62);
$display("Regstr value is %h",bfr6.buf_out_());
endrule



endmodule:mkTb_RapidIOPhy_Buffer8
endpackage:Tb_RapidIOPhy_Buffer8
