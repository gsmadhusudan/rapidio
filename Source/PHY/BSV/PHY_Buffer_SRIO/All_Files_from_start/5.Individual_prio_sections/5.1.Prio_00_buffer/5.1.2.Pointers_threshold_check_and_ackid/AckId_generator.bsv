package AckId_generator;

interface Ifc_AckId_generator;
	method Action _ack_en(Bool value);
method Bit#(4) ack_id_();
endinterface:Ifc_AckId_generator

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkAckId_generator(Ifc_AckId_generator);

Reg#(Bit#(4)) rg_new_ackid <- mkReg(4'b0000);
Wire#(Bool) wr_enbl <- mkDWire(True);

rule r8(wr_enbl == False);

	if(rg_new_ackid == 4'b1111)
		rg_new_ackid <= 4'b0000;
	else
		rg_new_ackid <= rg_new_ackid + 1;
endrule

method Action _ack_en(Bool value);
	wr_enbl <= value;
endmethod

method Bit#(4) ack_id_();
      

        return rg_new_ackid;
endmethod

endmodule:mkAckId_generator
endpackage:AckId_generator
