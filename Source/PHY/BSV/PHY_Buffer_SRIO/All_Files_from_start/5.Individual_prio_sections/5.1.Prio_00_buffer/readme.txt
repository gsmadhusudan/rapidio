5.1.1 - Implementation of read and write pointers and assignment of a 4-bit ackid.
5.1.2 - Automatic incrementing of both read and write pointer and enabling of reading from an external module when a particular threshold is reached.
5.1.3 - Complete functionalities of prio_00 except retransmission and dequeing.
5.1.4 - Retransmission from the point where transmission stopped, without lut.
5.1.5 - Final version of Individual section of prio_00 as of now - includes retransmission from errored ackid, lut and read enabling from same module.
