A basic register which is used in the buffer in which, for writing, data is shifted and iteration is given.Reading is done by giving each case separately by using write and read counters(position, from which data should be read, is given explicitly).

Reading can also be changed to iteration method using bit by bit updation(for loop).
