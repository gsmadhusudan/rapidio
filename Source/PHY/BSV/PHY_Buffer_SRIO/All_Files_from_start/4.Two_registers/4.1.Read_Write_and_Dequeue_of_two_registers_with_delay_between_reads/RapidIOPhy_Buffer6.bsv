package RapidIOPhy_Buffer6;

import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer5 ::*;

interface Ifc_RapidIOPhy_Buffer6;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
//method Action _tx_rdy_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf(Bit#(2) value);
   method Action _tx_read(Bit#(1) value);
method Action _tx_rg12(Bit#(2) value);
   method Action _tx_deq(Bit#(1) value);
   
   method RegBuf buf_out_();
   method Bool lnk_tvld_n_();
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bit#(4) lnk_trem_();
   method Bit#(2) lnk_tcrf_();

endinterface:Ifc_RapidIOPhy_Buffer6



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer6(Ifc_RapidIOPhy_Buffer6);


Ifc_RapidIOPhy_Buffer5 r1 <- mkRapidIOPhy_Buffer5;
Ifc_RapidIOPhy_Buffer5 r2 <- mkRapidIOPhy_Buffer5;

Wire#(Bool) tx_sof_n <- mkDWire(True);
Wire#(Bool) tx_eof_n <- mkDWire(True);
Wire#(Bool) tx_vld_n <- mkDWire(True);
//Wire#(Bool) tx_rdy_n <- mkDWire(True);
Wire#(DataPkt) tx_data <- mkDWire(0);
Wire#(Bit#(4)) tx_rem <- mkDWire(0);
Wire#(Bit#(2)) tx_crf <- mkDWire(0);

Wire#(Bool) lnk_tvld_n <- mkDWire(True);
Wire#(Bool) lnk_tsof_n <- mkDWire(True);
Wire#(Bool) lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) lnk_td <- mkDWire(0);
Wire#(Bit#(4)) lnk_trem <- mkDWire(0);
Wire#(Bit#(2)) lnk_tcrf <- mkDWire(0);

Reg#(Bit#(2)) rg_12 <- mkReg(0);
Reg#(Bit#(1)) rg_read <- mkReg(0);
Reg#(Bit#(1)) rg_deq <- mkReg(0);
Wire#(RegBuf) buf_out <- mkDWire(0);

rule r1_vld;
$display("rg_12 = %b",rg_12);
if(rg_12 == 2'b10)
r1._tx_vld_n(tx_vld_n);
else if(rg_12 == 2'b11)
r2._tx_vld_n(tx_vld_n);
endrule

rule r1_sof;
if(rg_12 == 2'b10)
r1._tx_sof_n(tx_sof_n);
else if(rg_12 == 2'b11)
r2._tx_sof_n(tx_sof_n);
endrule

rule r1_eof;
if(rg_12 == 2'b10)
r1._tx_eof_n(tx_eof_n);
else if(rg_12 == 2'b11)
r2._tx_eof_n(tx_eof_n);
endrule



rule r1_data;
if(rg_12 == 2'b10)
r1._tx_data(tx_data);
else if(rg_12 == 2'b11)
r2._tx_data(tx_data);
endrule

rule r1_rem;
if(rg_12 == 2'b10)
r1._tx_rem(tx_rem);
else if(rg_12 == 2'b11)
r2._tx_rem(tx_rem);
endrule

rule r1_crf;
if(rg_12 == 2'b10)
r1._tx_crf(tx_crf);
else if(rg_12 == 2'b11)
r2._tx_crf(tx_crf);
endrule

rule r1_read;
if(rg_12 == 2'b10)
r1._tx_read(rg_read);
else if(rg_12 == 2'b11)
r2._tx_read(rg_read);
endrule

rule r1_deq;
if(rg_12 == 2'b10)
r1._tx_deq(rg_deq);
else if(rg_12 == 2'b11)
r2._tx_deq(rg_deq);
endrule

rule r1_lnk_vld;
if(rg_12 == 2'b10)
lnk_tvld_n <= r1.lnk_tvld_n_();
else if(rg_12 == 2'b11)
lnk_tvld_n <= r2.lnk_tvld_n_();
endrule

rule r1_lnk_sof;
if(rg_12 == 2'b10)
lnk_tsof_n <= r1.lnk_tsof_n_();
else if(rg_12 == 2'b11)
lnk_tsof_n <= r2.lnk_tsof_n_();
endrule

rule r1_lnk_eof;
if(rg_12 == 2'b10)
lnk_teof_n <= r1.lnk_teof_n_();
else if(rg_12 == 2'b11)
lnk_teof_n <= r2.lnk_teof_n_();
endrule

rule r1_lnk_data;
if(rg_12 == 2'b10)
lnk_td <= r1.lnk_td_();
else if(rg_12 == 2'b11)
lnk_td <= r2.lnk_td_();
endrule

rule r1_lnk_rem;
if(rg_12 == 2'b10)
lnk_trem <= r1.lnk_trem_();
else if(rg_12 == 2'b11)
lnk_trem <= r2.lnk_trem_();
endrule

rule r1_lnk_crf;
if(rg_12 == 2'b10)
lnk_tcrf <= r1.lnk_tcrf_();
else if(rg_12 == 2'b11)
lnk_tcrf <= r2.lnk_tcrf_();
endrule

rule r1_lnk_out;
if(rg_12 == 2'b10)
buf_out <= r1.buf_out_();
else if(rg_12 == 2'b11)
buf_out <= r2.buf_out_();
endrule











//if(rg_12 == 1)
//begin
//r1._tx_sof_n(tx_sof_n);
//r1._tx_eof_n(tx_sof_n);
//r1._tx_vld_n(tx_sof_n);
//r1._tx_data(tx_data);
//r1._tx_rem(tx_rem);
//r1._tx_crf(tx_crf);
//r1._tx_read(rg_read);
//r1._tx_deq(rg_deq);

/*lnk_tvld_n <= r1.lnk_tvld_n_();
lnk_tsof_n <= r1.lnk_tsof_n_();
lnk_teof_n <= r1.lnk_teof_n_();
lnk_td <= r1.lnk_td_();
lnk_trem <= r1.lnk_trem_();
lnk_tcrf <= r1.lnk_tcrf_();
buf_out <= r1.buf_out_();*/
/*end

else

begin
r2._tx_sof_n(tx_sof_n);
r2._tx_eof_n(tx_sof_n);
r2._tx_vld_n(tx_sof_n);
r2._tx_data(tx_data);
r2._tx_rem(tx_rem);
r2._tx_crf(tx_crf);
r2._tx_read(rg_read);
r2._tx_deq(rg_deq);*/

/*lnk_tvld_n <= r1.lnk_tvld_n_();
lnk_tsof_n <= r1.lnk_tsof_n_();
lnk_teof_n <= r1.lnk_teof_n_();
lnk_td <= r1.lnk_td_();
lnk_trem <= r1.lnk_trem_();
lnk_tcrf <= r1.lnk_tcrf_();
buf_out <= r1.buf_out_();*/
//end

//endrule

//rule r1_2;
//if(rg_12 == 1)
//begin
//lnk_tvld_n <= r1.lnk_tvld_n_();
//lnk_tsof_n <= r1.lnk_tsof_n_();
//lnk_teof_n <= r1.lnk_teof_n_();
//lnk_td <= r1.lnk_td_();
//lnk_trem <= r1.lnk_trem_();
//lnk_tcrf <= r1.lnk_tcrf_();
//buf_out <= r1.buf_out_();
//nd

/*else

begin
lnk_tvld_n <= r1.lnk_tvld_n_();
lnk_tsof_n <= r1.lnk_tsof_n_();
lnk_teof_n <= r1.lnk_teof_n_();
lnk_td <= r1.lnk_td_();
lnk_trem <= r1.lnk_trem_();
lnk_tcrf <= r1.lnk_tcrf_();
buf_out <= r1.buf_out_();
end

endrule*/



method Action _tx_sof_n(Bool value);
     tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     tx_rem <= value;
endmethod

method Action _tx_crf(Bit#(2) value);
     tx_crf <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod

method Action _tx_rg12(Bit#(2) value);
     rg_12 <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
     rg_deq <= value;
endmethod

method Bool lnk_tvld_n_();
     return lnk_tvld_n;
endmethod

method RegBuf buf_out_();
     return buf_out;
endmethod

method Bool lnk_tsof_n_();
     return lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return lnk_tcrf;
endmethod

endmodule:mkRapidIOPhy_Buffer6
endpackage:RapidIOPhy_Buffer6
