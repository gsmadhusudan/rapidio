/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Look Up Table Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2. A LUT Table is created to store the details of the Transmitted packet.
-- 3. AckID is used as the index value for the LUT vector register.
-- 
-- Table :	---------------------------------
		- Valid	- AckID	- Prio	-Pointer-			
		  1 bit	  6 bit	  2 bit	 4 bit	   	
		---------------------------------
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package AckId_Lut;

import Vector::*;
import DReg :: * ;
import FIFO::*;

interface Ifc_AckId_Lut;
	method Action _retrans(Bool value);//to indicate request for retransmission
	method Action _stop_txmsn(Bool value);//to indicate stopping of transmission when error is detected
	method Action _store(Bool value);//to indicate writing to lut is required
	method Action _copy(Bool value);//to copy locations for dequeuing
	method Action _clear(Bool value);//to clear locations to be dequeued 
	method Action _ackid_store(Bit#(6) value);//ackid to be stored
	//method Action _ackid_retrans(Bit#(6) value);//ackid to be identified from lut for retransmission
	method Action _ackid_copy(Bit#(6) value);//starting ackid to be copied or dequeued
	method Action _ackid_clear(Bit#(6) value);//starting ackid to be dequeued from original
	method Action _prio_in(Bit#(2) value);//for storing priority of buffer section in which a packet with a particular ackid is stored
	method Action _rd_ptr_in(Bit#(4) value);//for storing read pointer value in a buffer section(location of a packet)
	//method Action _ackid_deq(Bit#(6) value);//for dequeing from buffer
	method Action _lut_access_out(Bool value);
	method Action _ackid_access(Bit#(6) value);

	method Bit#(4) out_00_();//to return location from which transmission should begin in prio section '00' while retransmitting
	method Bit#(4) out_01_();//to return location from which transmission should begin in prio section '01' while retransmitting
	method Bit#(4) out_10_();//to return location from which transmission should begin in prio section '10' while retransmitting
	method Bit#(4) out_11_();//to return location from which transmission should begin in prio section '11' while retransmitting
//to identify whether any packet had been transmitted after an errored packet,from any section,at the time of retransmission
	method Bool out_00_enter_();
	method Bool out_01_enter_();
	method Bool out_10_enter_();
	method Bool out_11_enter_();
	method Bit#(2) prio_out_();
	method Bit#(4) read_pointer_out_();
	method Bit#(6) last_deq_ackid_();
endinterface:Ifc_AckId_Lut


(* synthesize *)
//(* always_enabled *)
//(* always_ready *)


module mkAckId_Lut(Ifc_AckId_Lut);


//[1-bit valid bit(To check the entry status),6-bit ackid,2-bit prio,4-bit pointer position]
Vector#(64,Reg#(Bit#(13)))  rg_vect_lut <- replicateM(mkReg(0));
//Copy of the original lut that stores entries to be dequeued
Reg#(Bit#(13)) rg_vect_lut_deq[64];
for (Integer i=0; i<64; i=i+1)
	rg_vect_lut_deq[i] <-mkReg(0);

Reg#(Bit#(6)) rg_ack_id_store[2] <- mkCReg(2,0);//This is made a concurrent register so that its value can be displayed in the same cycle in which it is getting updated.

Wire#(Bit#(6)) wr_ack_id_store <- mkDWire(0);
//Wire#(Bit#(6)) wr_ack_id_retrans <- mkDWire(0);
Wire#(Bit#(6)) wr_ack_id_copy <- mkDWire(0);
Wire#(Bit#(6)) wr_ack_id_clear <- mkDWire(0);
Wire#(Bit#(6)) wr_ackid_access <- mkDWire(0);
Wire#(Bit#(4)) wr_rd_ptr_in <- mkDWire(0);
Wire#(Bit#(4)) wr_read_pointer_out <- mkDWire(0);
Wire#(Bit#(2)) wr_prio <- mkDWire(0);
Wire#(Bit#(2)) wr_prio_out <- mkDWire(0);
Wire#(Bool) wr_retrans <- mkDWire(False);
Wire#(Bool) wr_stop_txmsn <- mkDWire(False);
Wire#(Bool) wr_store <- mkDWire(False);
Wire#(Bool) wr_copy <- mkDWire(False);
Wire#(Bool) wr_clear <- mkDWire(False);
Wire#(Bool) wr_access <- mkDWire(False);

Reg#(Bit#(4)) rg_00 <- mkReg(0);
Reg#(Bit#(4)) rg_01 <- mkReg(0);
Reg#(Bit#(4)) rg_10 <- mkReg(0);
Reg#(Bit#(4)) rg_11 <- mkReg(0);
Reg#(Bool) rg_00_enter <- mkReg(False);
Reg#(Bool) rg_01_enter <- mkReg(False);
Reg#(Bool) rg_10_enter <- mkReg(False);
Reg#(Bool) rg_11_enter <- mkReg(False);
Reg#(Bit#(6)) rg_ackid_till_now <- mkReg(0);
Reg#(Bit#(6)) rg_ackid_last_deq <- mkReg(0);




rule store(wr_store == True);
	$display("wr_store to lut = %b",wr_store);
	rg_ack_id_store[0] <= wr_ack_id_store;//CReg used just for displaying the ackid which is being stored
rg_ackid_till_now <= wr_ack_id_store;//ackid till which the packet has been transmitted from buffer 
	let lv_id = wr_ack_id_store;
	rg_vect_lut[lv_id] <= {1'b1, wr_ack_id_store, wr_prio, wr_rd_ptr_in};//vector of registers indexed by ackid
endrule

/*
//For displaying in the same cycle,concurrent register is used
rule disp;
	$display("wr_store to lut_disp = %b",wr_store);
	let lv_id = rg_ack_id_store[1];
	$display("LUT entry_from_vect_reg:Valid bit - %b, Ackid - %b, Priority - %b, Pointer - %b",rg_vect_lut[lv_id][12], rg_vect_lut[lv_id][11:6], rg_vect_lut[lv_id][5:4], rg_vect_lut[lv_id][3:0]);
endrule
*/


//********************************************TO BE USED FOR DEQUEUING******************************************************
//Copying ackids to be dequeued to another similar lut.
rule copy_lut(wr_copy == True); 
	for(Integer i=0; i < 64; i = i+1)
	begin
		Bit#(6) lv_deq_ackid = wr_ack_id_copy - fromInteger(i);
		$display("deq_ack_id = %b",lv_deq_ackid);
		if(wr_ack_id_copy > rg_ackid_last_deq)//normal case
		begin
			if(lv_deq_ackid <= wr_ack_id_copy && lv_deq_ackid >= rg_ackid_last_deq)
			begin
				let lv_vect_lut = rg_vect_lut[lv_deq_ackid];
				if(lv_vect_lut[12] == 1)
				//begin
					rg_vect_lut_deq[i] <= lv_vect_lut;
				//end
			end
		end
		else if(wr_ack_id_copy < rg_ackid_last_deq)//case in which ackid overflow occurs
		begin
			if(lv_deq_ackid <= wr_ack_id_copy && lv_deq_ackid >= 0)
			begin
				let lv_vect_lut = rg_vect_lut[lv_deq_ackid];
				if(lv_vect_lut[12] == 1)
				//begin
					rg_vect_lut_deq[i] <= lv_vect_lut;
				//end
			end
			else if(lv_deq_ackid <= 6'b111111 && lv_deq_ackid > rg_ackid_last_deq)
			begin
				let lv_vect_lut = rg_vect_lut[lv_deq_ackid];
				if(lv_vect_lut[12] == 1)
				//begin
					rg_vect_lut_deq[i] <= lv_vect_lut;
				//end
			end
		end
	end
endrule


//Clearing entries to be dequeued from original lut.
rule clear_lut(wr_clear == True); 
	for(Integer i=0; i < 64; i = i+1)
	begin
		Bit#(6) lv_deq_ackid = wr_ack_id_clear - fromInteger(i);
		$display("clear_ack_id = %b",lv_deq_ackid);
		if(wr_ack_id_clear > rg_ackid_last_deq)//normal case
		begin
			if(lv_deq_ackid <= wr_ack_id_clear && lv_deq_ackid >= rg_ackid_last_deq)
			//begin
				rg_vect_lut[lv_deq_ackid] <= 13'b0;
			//end
		end
		else if(wr_ack_id_clear < rg_ackid_last_deq)//case in which ackid overflow occurs
		begin
			if(lv_deq_ackid <= wr_ack_id_clear && lv_deq_ackid >= 0)
			//begin
rg_vect_lut[lv_deq_ackid] <= 13'b0;		
			//end
			else if(lv_deq_ackid <= 6'b111111 && lv_deq_ackid > rg_ackid_last_deq)
			//begin
				rg_vect_lut[lv_deq_ackid] <= 13'b0;
			//end
		end
	end
rg_ackid_last_deq <= wr_ack_id_clear;//last ackid dequeued from buffer
endrule
//*************************************************************************************************************************


//RULES USED FOR RETRANSMISSION
//reading read pointer value in each prio section from lut
rule valid_check(wr_stop_txmsn == True);
	Bool lv_wr_00_enter = False;
	Bool lv_wr_01_enter = False;
	Bool lv_wr_10_enter = False;
	Bool lv_wr_11_enter = False;
	Bit#(4) lv_00 = 0;
	Bit#(4) lv_01 = 0;
	Bit#(4) lv_10 = 0;
	Bit#(4) lv_11 = 0;

	for(Integer i=0; i<64; i=i+1)
	begin
		let lv_ack_id_valid = rg_ackid_till_now - fromInteger(i);//starting from errored packet,all subsequent packets from all sections must be retransmitted
		let lv_buf_location= rg_vect_lut[lv_ack_id_valid];
		$display("lut_index_value for reloading - %d",lv_ack_id_valid);
		if(lv_buf_location[12] == 1'b1)//checking whether lut has a valid entry at the corresponding location
		begin
			if(lv_buf_location[5:4] == 2'b00)//once read pointer in a section is updated,then that section pointer should not be updated again
			begin
				lv_00 = lv_buf_location[3:0];
				lv_wr_00_enter = True;//For identifying section of buffer from which retransmission should start
			end

			else if(lv_buf_location[5:4] == 2'b01)//once read pointer in a section is updated,then that section pointer should not be updated again
			begin
				lv_01 = lv_buf_location[3:0];
				lv_wr_01_enter = True;//For identifying section of buffer from which retransmission should start
			end

			else if(lv_buf_location[5:4] == 2'b10)//once read pointer in a section is updated,then that section pointer should not be updated again
			begin
				lv_10 = lv_buf_location[3:0];
				lv_wr_10_enter = True;//For identifying section of buffer from which retransmission should start
			end

			else if(lv_buf_location[5:4] == 2'b11)//once read pointer in a section is updated,then that section pointer should not be updated again
			begin
				lv_11 = lv_buf_location[3:0];
				lv_wr_11_enter = True;//For identifying section of buffer from which retransmission should start
			end
		end
	end
	rg_00<= lv_00;
	rg_01<= lv_01;
	rg_10<= lv_10;
	rg_11<= lv_11;

	rg_00_enter <= lv_wr_00_enter;
	rg_01_enter <= lv_wr_01_enter;
	rg_10_enter <= lv_wr_10_enter;
	rg_11_enter <= lv_wr_11_enter;

endrule


//updating default value after reloading read pointer values required for retransmission
rule out_retrans(wr_retrans == True);
	rg_00_enter <= False;
	rg_01_enter <= False;
	rg_10_enter <= False;
	rg_11_enter <= False;
endrule

//Retreiving location of ackid to be dequeued from buffer
rule access_out(wr_access == True);
Bit#(2) lv_prio_out = 0;
Bit#(4) lv_read_pointer_out = 0;
	for(Integer i = 0;i < 64; i=i+1)
	begin
		if(rg_vect_lut_deq[i][12] == 1)
		begin
			let lv_ackid_access = wr_ackid_access;
			if(rg_vect_lut_deq[i][11:6] == lv_ackid_access)
			begin
				lv_prio_out = rg_vect_lut_deq[i][5:4];
				lv_read_pointer_out = rg_vect_lut_deq[i][3:0];
			end
		end
	end
wr_prio_out <= lv_prio_out;
wr_read_pointer_out <= lv_read_pointer_out;
endrule







//Input Methods
method Action _ackid_store(Bit#(6) value);
       wr_ack_id_store <= value;
endmethod
/*
method Action _ackid_retrans(Bit#(6) value);
       wr_ack_id_retrans <= value;
endmethod
*/
method Action _ackid_copy(Bit#(6) value);
	wr_ack_id_copy <= value;
endmethod

method Action _ackid_clear(Bit#(6) value);
	wr_ack_id_clear <= value;
endmethod

method Action _rd_ptr_in(Bit#(4) value);
       wr_rd_ptr_in <= value;
endmethod

method Action _prio_in(Bit#(2) value);
	wr_prio <= value;
endmethod
	
method Action _retrans(Bool value);
       wr_retrans <= value;
endmethod

method Action _stop_txmsn(Bool value);
	wr_stop_txmsn <= value;
endmethod

method Action _store(Bool value);
       wr_store <= value;
endmethod

method Action _copy(Bool value);
	wr_copy <= value;
endmethod

method Action _clear(Bool value);
	wr_clear <= value;
endmethod

method Action _lut_access_out(Bool value);
	wr_access <= value;
endmethod

method Action _ackid_access(Bit#(6) value);
	wr_ackid_access <= value;
endmethod


//Output Methods
method Bit#(4) out_00_();
	return rg_00;
endmethod

method Bit#(4) out_01_();
	return rg_01;
endmethod

method Bit#(4) out_10_();
	return rg_10;
endmethod

method Bit#(4) out_11_();
	return rg_11;
endmethod

method Bool out_00_enter_();
	return rg_00_enter;
endmethod

method Bool out_01_enter_();
	return rg_01_enter;
endmethod

method Bool out_10_enter_();
	return rg_10_enter;
endmethod

method Bool out_11_enter_();
	return rg_11_enter;
endmethod

method Bit#(2) prio_out_();
	return wr_prio_out;
endmethod

method Bit#(4) read_pointer_out_();
	return wr_read_pointer_out;
endmethod

method Bit#(6) last_deq_ackid_();
	return rg_ackid_last_deq;
endmethod


endmodule:mkAckId_Lut
endpackage:AckId_Lut

