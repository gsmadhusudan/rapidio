/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Phy Buffer Priority_11 Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2.A Single Buffer with 5 registers of each 2310 bits is created to hold the incoming packet with highest priority(11).
-- 3.This module is one amoung the 4 buffers in the Top Module
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIOPhy_Buffer11;


import RapidIO_DTypes ::*;
import DefaultValue ::*;
import RapidIOPhy_Buffer2 ::*;


interface Ifc_RapidIOPhy_Buffer11;
	method Action _tx_sof_n(Bool value);
	method Action _tx_eof_n(Bool value);
	method Action _tx_vld_n(Bool value);
	method Action _tx_data(DataPkt value);
	method Action _tx_rem(Bit#(4) value);
	method Action _read_ptr_valid(Bit#(4) value);//to give read pointer value from outside
	method Action _tx_crf(Bool value);
	method Action _tx_read_en(Bool value);//for enabling read operation
	method Action _tx_deq(Bool value);//packet accepted
	method Action _tx_ack(Bit#(6) value);//ackid to be assigned to the packet
	method Action _tx_retransmsn(Bool value);//enabling retxmsn at the time of restart from retry
	method Action _tx_stop_txmsn(Bool value); //to stop txmtng when either PNA or PR control symbol comes        
	method Action _pointer_deq(Bit#(4) value);//dequeing the acknowledged ackid(PA)                            
   
	method Bool lnk_tvld_n_();
	method Bool lnk_tsof_n_();
	method Bool lnk_teof_n_();
	method DataPkt lnk_td_();
	method Bit#(2) lnk_prio_();//for storing in lut
	method Bit#(4) lnk_trem_();
	method Bool lnk_tcrf_();
	method Bit#(1) valid_check_out_();//to check whether valid data is there at the current location
	method Bit#(1) valid_next_check_out_();//to check whether valid data is there at the next location
	method Bit#(4) out_wrt_ptr_(); //to give out write pointer value to check whether threshold has crossed                     
	method Bit#(4) out_rd_ptr_(); //to give out read pointer value
endinterface:Ifc_RapidIOPhy_Buffer11



(* synthesize *)
//(* always_enabled *)
//(* always_ready *)



module mkRapidIOPhy_Buffer11(Ifc_RapidIOPhy_Buffer11);

//set of 5 registers that will make up the priority "11" area in the buffer.
//Instantiating single register to make up the set of 5 registers
Ifc_RapidIOPhy_Buffer2 reg1 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg2 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg3 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg4 <- mkRapidIOPhy_Buffer2;
Ifc_RapidIOPhy_Buffer2 reg5 <- mkRapidIOPhy_Buffer2;


Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bool) wr_tx_crf <- mkDWire(True);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4))  wr_lnk_trem <- mkDWire(0);
Wire#(Bool)  wr_lnk_tcrf <- mkDWire(True);

Wire#(Bool) wr_read_en <- mkDWire(False);
Wire#(Bit#(6))  wr_tx_ack <- mkDWire(0);
Wire#(Bool) wr_deq <- mkDWire(False);          
Wire#(Bool)  wr_retrans <- mkDWire(False);
Wire#(Bool)  wr_stop_txmsn <- mkDWire(False);
Wire#(Bit#(4)) wr_write_ptr <- mkDWire(0);//write pointer
Wire#(Bit#(4)) wr_read_ptr <- mkDWire(0);//read pointer
Wire#(Bit#(4)) wr_read_ptr_lut <- mkDWire(0);
Wire#(Bit#(4)) wr_write_ptr_deq <- mkDWire(0);

Reg#(Bit#(4)) rg_write_ptr <- mkReg(4'b0001);//pointer to indicate the register position for write operation  
Reg#(Bit#(4)) rg_read_ptr <- mkReg(4'b0000);//pointer to indicate the register position for read operation        
Reg#(Bool) rg_read_en <- mkReg(False);//For enabling read operation - given from Top Module



// DURING WRITE,INCREMENTING WRITE POINTER
rule write_ptr_incr;                                         
	wr_write_ptr <= rg_write_ptr;
	if(wr_tx_eof_n == False)//Incrementing to next register when eof for previous data is arrived
		begin
		if(rg_write_ptr == 4'b0101)//set of 5 registers 
			rg_write_ptr <= 4'b0001;
		else
			rg_write_ptr <= rg_write_ptr + 1;
		end
endrule


// DURING READ, INCREMENTING READ POINTER
rule read_ptr_incr(rg_read_en == True);
	if(wr_lnk_teof_n == False)
	begin
		if(rg_read_ptr == 4'b0101) 
			rg_read_ptr <= 4'b0001;
		else
			rg_read_ptr <= rg_read_ptr + 1;
	end
endrule


rule read_en_deq_retrans_readptr_copy;
	wr_read_en <= rg_read_en;
	wr_read_ptr <= rg_read_ptr;
	$display("read_enable_11 = %b",rg_read_en);
endrule


//During initial transmission and retransmission the position is obtained from lut(top module)
rule read_ptr_initial_or_retrans(rg_read_ptr == 4'b0000 || wr_retrans == True);
	rg_read_ptr <= wr_read_ptr_lut;
endrule


//reading disabled for all registers
rule r1_read_disbl(wr_stop_txmsn == True);
	reg1._tx_read(!(wr_stop_txmsn));
	reg2._tx_read(!(wr_stop_txmsn));
	reg3._tx_read(!(wr_stop_txmsn));
	reg4._tx_read(!(wr_stop_txmsn));
	reg5._tx_read(!(wr_stop_txmsn));
endrule


//in case of retransmission,read and write counts of basicreg is reloaded
rule retrans_reload_cnt_values(wr_retrans == True);
	reg1._tx_retransmsn(wr_retrans);
	reg2._tx_retransmsn(wr_retrans);
	reg3._tx_retransmsn(wr_retrans);
	reg4._tx_retransmsn(wr_retrans);
	reg5._tx_retransmsn(wr_retrans);
endrule


rule r1_vld;
	if(wr_write_ptr == 4'b0001)
		reg1._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_vld_n(wr_tx_vld_n);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_vld_n(wr_tx_vld_n);
endrule


rule r1_sof;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_sof_n(wr_tx_sof_n);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_sof_n(wr_tx_sof_n);
endrule


rule r1_eof;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_eof_n(wr_tx_eof_n);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_eof_n(wr_tx_eof_n);
endrule



rule r1_data;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_data(wr_tx_data);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_data(wr_tx_data);
endrule


rule r1_rem;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_rem(wr_tx_rem);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_rem(wr_tx_rem);
endrule


rule r1_crf;

	if(wr_write_ptr == 4'b0001)
		reg1._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0010)
		reg2._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0011)
		reg3._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0100)
		reg4._tx_crf_n(wr_tx_crf);
	else if(wr_write_ptr == 4'b0101)
		reg5._tx_crf_n(wr_tx_crf);
endrule


// Value of AckID is given from Top module
rule r1_ack_assign(rg_read_en == True);
	if(rg_read_ptr == 4'b0001)
		reg1._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0010)
		reg2._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0011)
		reg3._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0100)
		reg4._tx_ack(wr_tx_ack);
	else if(rg_read_ptr == 4'b0101)
		reg5._tx_ack(wr_tx_ack);
endrule


// Value of read_en is given from Top module
rule r1_read_en(wr_stop_txmsn != True);
	if(wr_read_ptr == 4'b0001)
		reg1._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0010)
		reg2._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0011)
		reg3._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0100)
		reg4._tx_read(rg_read_en);
	else if(wr_read_ptr == 4'b0101)
		reg5._tx_read(rg_read_en);
endrule


//Value of the pointer for deq in the LUT given from Top Module.
rule r1_deq;
	if(wr_write_ptr_deq == 4'b0001)     
	begin
		reg1._tx_deq(wr_deq);
		$display(" Deq from buffer_11");
	end
	else if(wr_write_ptr_deq == 4'b0010)
		reg2._tx_deq(wr_deq);
	else if(wr_write_ptr_deq == 4'b0011)
		reg3._tx_deq(wr_deq);
	else if(wr_write_ptr_deq == 4'b0100)
		reg4._tx_deq(wr_deq);
	else if(wr_write_ptr_deq == 4'b0101)
		reg5._tx_deq(wr_deq);
endrule


//Outputs from register
rule r1_lnk_vld(wr_read_en == True);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_tvld_n <= reg1.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_tvld_n <= reg2.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_tvld_n <= reg3.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_tvld_n <= reg4.lnk_tvld_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_tvld_n <= reg5.lnk_tvld_n_();
endrule


rule r1_lnk_sof(wr_read_en == True);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_tsof_n <= reg1.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_tsof_n <= reg2.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_tsof_n <= reg3.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_tsof_n <= reg4.lnk_tsof_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_tsof_n <= reg5.lnk_tsof_n_();
endrule


rule r1_lnk_eof(wr_read_en == True);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_teof_n <= reg1.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_teof_n <= reg2.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_teof_n <= reg3.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_teof_n <= reg4.lnk_teof_n_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_teof_n <= reg5.lnk_teof_n_();
endrule


rule r1_lnk_data(wr_read_en == True);
	if(wr_read_ptr == 4'b0001)
		wr_lnk_td <= reg1.lnk_td_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_td <= reg2.lnk_td_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_td <= reg3.lnk_td_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_td <= reg4.lnk_td_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_td <= reg5.lnk_td_();
endrule


rule r1_lnk_rem(wr_read_en == True);

	if(wr_read_ptr == 4'b0001)
		wr_lnk_trem <= reg1.lnk_trem_();
	else if(wr_read_ptr == 4'b0010)
		wr_lnk_trem <= reg2.lnk_trem_();
	else if(wr_read_ptr == 4'b0011)
		wr_lnk_trem <= reg3.lnk_trem_();
	else if(wr_read_ptr == 4'b0100)
		wr_lnk_trem <= reg4.lnk_trem_();
	else if(wr_read_ptr == 4'b0101)
		wr_lnk_trem <= reg5.lnk_trem_();
endrule


rule r1_lnk_crf(wr_read_en == True);

	if(wr_read_ptr == 4'b0001)
			wr_lnk_tcrf <= reg1.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0010)
			wr_lnk_tcrf <= reg2.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0011)
			wr_lnk_tcrf <= reg3.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0100)
			wr_lnk_tcrf <= reg4.lnk_tcrf_();
	else if(wr_read_ptr == 4'b0101)
			wr_lnk_tcrf <= reg5.lnk_tcrf_();
endrule


//Input Methods
method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _read_ptr_valid(Bit#(4) value);
     wr_read_ptr_lut <= value;
endmethod

method Action _tx_crf(Bool value);
     wr_tx_crf <= value;
endmethod

method Action _tx_retransmsn(Bool value);
     wr_retrans <= value;
endmethod

method Action _tx_stop_txmsn(Bool value);
     wr_stop_txmsn <= value;
endmethod

method Action _tx_read_en(Bool value);
	rg_read_en <= value;
endmethod

method Action _tx_deq(Bool value);
     wr_deq <= value;
endmethod

method Action _tx_ack(Bit#(6) value);
		wr_tx_ack <= value;
endmethod

method Action _pointer_deq(Bit#(4) value);
	wr_write_ptr_deq <= value;
endmethod

//Output Methods
method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(2) lnk_prio_();
      return 2'b11;
endmethod

method Bit#(4) lnk_trem_();
      return  wr_lnk_trem;
endmethod

method Bool lnk_tcrf_();
      return  wr_lnk_tcrf;
endmethod

method Bit#(4) out_wrt_ptr_();
	return rg_write_ptr;
endmethod

method Bit#(4) out_rd_ptr_();
	return rg_read_ptr;
endmethod

method Bit#(1) valid_check_out_();
	case(wr_read_ptr)
	4'b0001:return reg1.valid_entry_();
	4'b0010:return reg2.valid_entry_();
	4'b0011:return reg3.valid_entry_();
	4'b0100:return reg4.valid_entry_();
	4'b0101:return reg5.valid_entry_();
	endcase
endmethod 

method Bit#(1) valid_next_check_out_();
	case(wr_read_ptr)
	4'b0001:return reg2.valid_entry_();
	4'b0010:return reg3.valid_entry_();
	4'b0011:return reg4.valid_entry_();
	4'b0100:return reg5.valid_entry_();
	4'b0101:return reg1.valid_entry_();
	endcase
endmethod 

endmodule:mkRapidIOPhy_Buffer11
endpackage:RapidIOPhy_Buffer11
